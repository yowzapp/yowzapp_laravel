<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Grounds extends Model
{
    use CrudTrait;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $table = 'grounds';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'max_member',
        'venue_id',
        'is_disabled',
        'slot_price',
        'start_time',
        'end_time',
        'sports_id',
        'sports_name',
        'is_deleted'];
    // protected $hidden = [];
    // protected $dates = [];
   protected $casts = ['is_disabled'=> 'boolean'];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Slot()
    {
        return $this->hasMany('App\Models\Slot', 'ground_id');
    }

    public function Venue()
    {
        return $this->belongsTo('App\Models\Venues', 'venue_id','id');
    }

    public function Sport() 
    {
        return $this->belongsTo('App\Models\Sports', 'sports_id','id');
    }


    public function Games()
    {
        return $this->hasMany('App\Models\Games','ground_id')->where('status',1);
    }

    public function Ranges()
    {
        return $this->hasMany('App\Models\Range','ground_id');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($ground) { // before delete() method call this
            $ground->Slot()->delete();
            // do the rest of the cleanup...
        });
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
