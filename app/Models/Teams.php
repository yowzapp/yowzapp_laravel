<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Teams extends Model
{
	use CrudTrait;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'teams';
	protected $primaryKey = 'id';
	// public $timestamps = false;
	// protected $guarded = ['id'];
	// protected $fillable = [];
	// protected $hidden = [];
    protected $casts = ['activity_meter'=> 'boolean','is_private'=> 'boolean'];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
    public function Sports()
    {
        return $this->belongsToMany('App\Models\Sports','sports_has_teams', 'team_id', 'sport_id')->withTimestamps();
    }
    public function Timeline()
    {
        return $this->hasMany('App\Models\TeamTimeline', 'team_id');
    }

    public function Players()
    {
        return $this->belongsToMany('App\Models\Users','team_user', 'team_id', 'user_id')->withPivot('image','is_super_admin','is_admin','points_earned','invitation_status')->withTimestamps();
    }



    public function Followers()
    {
        return $this->belongsToMany('App\Models\Users','followings', 'following_to', 'followed_by')->withPivot('type','id_value','is_requested')->withTimestamps();
    }

    public function Following()
    {
        return $this->belongsToMany('App\Models\Users','followings', 'followed_by', 'following_to')->withPivot('type','id_value','is_requested')->withTimestamps();
    }

    public function Galleries()
    {
        return $this->hasMany('App\Models\TeamGallery', 'team_id');
    }

    public function Games()
    {
        return $this->belongsToMany('App\Models\Games','games_has_teams', 'team_id', 'game_id')->withTimestamps();
    }
    public function Notifications()
    {
        return $this->hasMany('App\Models\Notification','type_id');
    }

    public function Activities()
    {
        return $this->hasMany('App\Models\ActivityLog','type_id');
    }

    public function RecentSearch()
    {
        return $this->hasMany('App\Models\SearchRecent','id');
    }

    public function Search()
    {
        return $this->hasOne('App\Models\Search','id');
    }

    public function Points()
    {
        return $this->hasMany('App\Models\Points', 'type_id');
    }


    public function Messages()
    {
        return $this->hasMany('App\Models\Messages', 'type_id');
    }
   
    protected static function boot() {
        parent::boot();

        static::deleting(function($team) { // before delete() method call this
            $team->Notifications()->where('type', 'LIKE', '%TME%')->delete();
            $team->Activities()->where('type', 1)->delete();
            $team->RecentSearch()->where('type', 2)->delete();
            $team->Timeline()->delete();
            $team->Galleries()->delete();
            // do the rest of the cleanup...
        });
    }





    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
