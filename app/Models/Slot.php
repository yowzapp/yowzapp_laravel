<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

    class Slot extends Model
    {
        use CrudTrait;

        /*
       |--------------------------------------------------------------------------
       | GLOBAL VARIABLES
       |--------------------------------------------------------------------------
       *///

        /*
       |--------------------------------------------------------------------------
       | GLOBAL VARIABLES
       |--------------------------------------------------------------------------
       */

        protected $table = 'slots';
        protected $primaryKey = 'id';
        // protected $guarded = ['id'];
        protected $fillable = ['slot_price','start_time','end_time','slot_duration','ground_id'];
        // protected $hidden = [];
        // protected $dates = [];

        /*
        |--------------------------------------------------------------------------
        | FUNCTIONS
        |--------------------------------------------------------------------------
        */

        /*
        |--------------------------------------------------------------------------
        | RELATIONS
        |--------------------------------------------------------------------------
        */

        public function Bookings()
        {
            return $this->hasMany('App\Models\Bookings', 'slot_id');
        }


        public function Ground()
        {
            return $this->belongsTo('App\Models\Grounds', 'ground_id')->where('is_disabled',0)->where('is_deleted',0);
        }

        public function Range()
        {
            return $this->belongsTo('App\Models\Range', 'range_id');
        }

        protected static function boot() {
            parent::boot();

            static::deleting(function($slot) { // before delete() method call this
                $slot->Bookings()->delete();
                // do the rest of the cleanup...
            });
        }
        /*
        |--------------------------------------------------------------------------
        | SCOPES
        |--------------------------------------------------------------------------
        */

        /*
        |--------------------------------------------------------------------------
        | ACCESORS
        |--------------------------------------------------------------------------
        */

        /*
        |--------------------------------------------------------------------------
        | MUTATORS
        |--------------------------------------------------------------------------
        */
    }
