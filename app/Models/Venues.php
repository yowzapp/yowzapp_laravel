<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Venues extends Model
{
	use CrudTrait;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'venues';
	protected $primaryKey = 'id';
	// public $timestamps = false;
	// protected $guarded = ['id'];
	protected $fillable = [
        'vendor_id',
        'name',
        'cover_pic',
        'description',
        'is_disabled',
        'is_verified',
        'sports_types',
        'location_name',
        'lat',
        'lng',
    ];
	// protected $hidden = [];
    // protected $dates = [];
     protected $casts = ['is_verified'=> 'boolean','is_disabled'=> 'boolean'];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

    public function Vendor()
    {
        return $this->belongsTo('App\Models\Users', 'vendor_id');
    }



//
//
//    public function Location()
//    {
//        return $this->belongsTo('App\Models\Locations', 'locations_id');
//    }

    public function Grounds()
    {
        return $this->hasMany('App\Models\Grounds', 'venue_id')->where('is_deleted',0);
    }

    public function Gallery()
    {
        return $this->hasMany('App\Models\VenueGallery', 'venue_id');
    }

    public function RatedUsers()
    {
        return $this->belongsToMany('App\Models\Users', 'venue_rating', 'venue_id','user_id')->withPivot('rating','game_id');
    }


    protected static function boot() {
        parent::boot();

        static::deleting(function($venue) { // before delete() method call this
            $venue->Grounds()->delete();
            // do the rest of the cleanup...
        });
    }



    public function Amenities()
    {
        $amenities =  $this->hasMany('App\Models\VenueAmenities','venue_id')->get();
        $amenity_list=array();
        foreach($amenities as $value){
            $value =  $value->hasOne('App\Models\Amenities','id','amenity_id')->first();
            $amenity_list[]=array(
                'id' => $value['id'],
                'name' => $value['name'],
            );
        }
        return $amenity_list;
    }

    public function Galleries()
    {
        return $this->hasMany('App\Models\VenueGallery', 'venue_id');
    }

    public function venueAmenities()
    {
        return $this->belongsToMany('App\Models\Amenities', 'venues_amenities','venue_id','amenity_id');
    }


	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
