<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Users extends Model
{
	use CrudTrait;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'users';
	protected $primaryKey = 'id';
	// public $timestamps = false;
	// protected $guarded = ['id'];
    protected $fillable = ['name','email','social_token','mobile','accessToken','role_id','password','mobile_verified','is_verified','is_private','is_social','otp','cover_pic','bio','profile_pic','lng','lng','device_type','device','fcm_token','device_id','pending_notification','primary_team','available_at','prefer_loaction','notificaiton'];
	// protected $hidden = ['password','role_id','remember_token','otp','created_at','updated_at','social_token'];
    protected $casts = ['mobile_verified'=> 'boolean','is_social'=> 'boolean','is_private'=> 'boolean'];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

    public function Role()
    {
        return $this->belongsTo('App\Models\Roles', 'role_id');
    }

    public function Device()
    {
        return $this->hasMany('App\Models\DeviceInfo', 'user_id');
    }

    public function Notifications()
    {
        return $this->hasMany('App\Models\Notification', 'user_id');
    }

    public function Venues() 
    {
        return $this->hasMany('App\Models\Venues', 'vendor_id');
    }

    public function RecentSearchs()
    {
        return $this->hasMany('App\Models\SearchRecent', 'user_id');
    }

    public function Activities()
    {
        return $this->hasMany('App\Models\ActivityLog','user_id');
    }

    public function Likes()
    {
        return $this->belongsToMany('App\Models\ActivityLog','likes','activity_id','user_id')->withTimestamps();
    }

    public function Comments() 
    {
        return $this->hasMany('App\Models\Comment','user_id');
    }

    public function Interests()
    {
        return $this->belongsToMany('App\Models\Sports','sports_has_users', 'users_id', 'sports_id')->withTimestamps();
    }

    public function Followers()
    {
        return $this->belongsToMany('App\Models\Users','followings', 'following_to', 'followed_by')->withPivot('type','id_value','is_requested')->withTimestamps();
    }

    public function Following()
    {
        return $this->belongsToMany('App\Models\Users','followings', 'followed_by', 'following_to')->withPivot('type','id_value','is_requested')->withTimestamps();
    }

    public function TeamsFollowers()
    {
        return $this->belongsToMany('App\Models\Teams','followings', 'following_to', 'followed_by')->withPivot('type','id_value','is_requested')->withTimestamps();
    }

    public function TeamsFollowing()
    {
        return $this->belongsToMany('App\Models\Teams','followings', 'followed_by', 'following_to')->withPivot('type','id_value','is_requested')->withTimestamps();
    }

    public function Teams()
    {
        return $this->belongsToMany('App\Models\Teams','team_user', 'user_id', 'team_id')->withPivot('invitation_status','image','is_super_admin','is_admin','points_earned')->withTimestamps();
    }

    public function Games()
    {
        return $this->belongsToMany('App\Models\Games','users_games', 'user_id', 'game_id')->withPivot('points','invitation_status','is_admin','is_super_admin','invited_to_pay')->withTimestamps()->where('status',1);
    }

    public function Transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'user_id');
    }

    public function profilePic()
    {
        return $this->profile_pic;
    }

    public function Points()
    {
        return $this->hasMany('App\Models\Points', 'type_id');
    }

    public function Level()
    {
        return $this->hasOne('App\Models\Levels', 'id','level_id');
    }

    public function Setting()
    {
        return $this->hasOne('App\Models\Settings', 'user_id');
    }


    public function VendorBookings()
    {
        return $this->hasMany('App\Models\Bookings', 'vendor_id');
    }


    public function RateVenue()
    {
        return $this->belongsToMany('App\Models\Venues', 'venue_rating', 'user_id', 'venue_id')->withPivot('rating','game_id');
    }


    public function Reports()
    {
        return $this->hasMany('App\Models\Report', 'vendor_id');
    }



    public function Messages()
    {
        return $this->hasMany('App\Models\Messages', 'type_id');
    }



    public function AccessTokens()
    {
        return $this->hasMany('App\Models\AccessToken','user_id');
    }


    public function Socket()
    {
        return $this->hasOne('App\Models\Socket', 'type_id');
    }



    protected static function boot() {
        parent::boot();

        static::deleting(function($user) { // before delete() method call this
            $user->Setting()->delete();
            $user->Messages()->delete();
            $user->Socket()->delete();
            $user->Device()->delete();
            $user->Notifications()->delete();
            // do the rest of the cleanup...
        });
    }





    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
