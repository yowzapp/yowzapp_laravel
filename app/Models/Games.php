<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Games extends Model
{
	use CrudTrait;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'games';
	protected $primaryKey = 'id';
	// public $timestamps = false;
	// protected $guarded = ['id'];
	 protected $fillable = ['is_private'];
	// protected $hidden = [];
     protected $dates = ['created_at','updated_at'];
    protected $casts = ['is_private'=> 'boolean','is_completed'=> 'boolean','attendance_status'=> 'boolean','shout'=> 'boolean'];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

    public function Sport()
    {
        return $this->belongsTo('App\Models\Sports', 'sport_id');
    }

    public function Ground()
    {
        return $this->belongsTo('App\Models\Grounds', 'ground_id')->where('is_deleted',0);
    }


    public function Players()
    {
        return $this->belongsToMany('App\Models\Users','users_games', 'game_id', 'user_id')->withPivot('points','invitation_status','is_admin','is_super_admin','invited_to_pay')->withTimestamps();
    }

    public function Teams()
    {
        return $this->belongsToMany('App\Models\Teams','games_has_teams', 'game_id', 'team_id')->withTimestamps();
    }


    public function Notifications()
    {
        return $this->hasMany('App\Models\Notification','type_id');
    }

    public function Activities()
    {
        return $this->hasMany('App\Models\ActivityLog','type_id');
    }

   
    public function Search()
    {
        return $this->hasOne('App\Models\Search','id');
    }


    public function Conversation()
    {
        return $this->hasOne('App\Models\Conversations','from_id');
    }



    public function Bookings()
    {
        return $this->hasMany('App\Models\Bookings','game_id');
    }

    public function Messages()
    {
        return $this->hasMany('App\Models\Messages', 'type_id');
    }

    // admin portal

    public function GetDate()
    {
        return date('jS F Y', strtotime($this['date']));
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
