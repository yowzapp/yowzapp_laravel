<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use CrudTrait;

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   *///

    /*
   |--------------------------------------------------------------------------
   | GLOBAL VARIABLES
   |--------------------------------------------------------------------------
   */

    protected $table = 'bookings_payment_transactions';
    protected $primaryKey = 'id';
    protected $fillable = ['amount', 'booking_id', 'user_id', 'transaction_number','details','transaction_identifier'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function Game()
    {
        return $this->hasOne('App\Models\Games','id','game_id')->where('status',1);
    }

    public function Bookings()
    {
        return $this->hasMany('App\Models\Bookings','id' ,'booking_id');
    }

    public function getBookingsPaymentStatus()
    {
        $bookings=Bookings::where('id',$this->booking_id)->first();
        if ($bookings['status'] == 0){
            return 'Rejected';
        }
        if($bookings['payment_status']==1 && $this['details'] == 'Payment mode cash'  ){
            return 'Cash';

        }else if($bookings['payment_status']==1){
            return 'Successfull';

        }else{
            return 'Failed';

        }
    }


    public function getBookingsAmount()
    {
        return $this['amount'].' AED';
    }

    public function getBookingsDate()
    {
        $bookings=Bookings::where('id',$this->booking_id)->first();
        return date('jS F Y', strtotime($bookings['date']));
    }



    public function User()
    {
        return $this->belongsTo('App\Models\Users','user_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}