<?php

namespace App\Console\Commands;


use App\Models\Bookings;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\Types\Null_;
use PushNotification;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
class SendpushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:pushnotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push notification sent successfully!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
        $today_date = $date->format('Y-m-d');
        $time=strtotime($date->format('Y-m-d H:i:s'));
        $bookings = Bookings::where('date',$today_date)->where('time_release','!=',Null)->get();
        foreach ($bookings as $booked){
            $from_time = strtotime($booked['time_release']);
            if ( round(abs($time - $from_time) / 60,2)>10) {
                $booked['payment_status'] = 0;
                $booked['time_release'] = Null;
                $booked->update();
            }
        }
        Log::info($today_date);
        Log::info($time);

    }
}
