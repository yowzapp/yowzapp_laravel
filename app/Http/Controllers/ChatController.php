<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\Conversations;
use App\Models\Games;
use App\Models\Messages;
use App\Models\Teams;
use App\Models\Users;
use Illuminate\Http\Request;

class ChatController extends Controller
{



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Chat History
     */

    public function  chatHistory(Request $request)
    {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user) {
            if ($request->has('type') && $request->has('id') ) {
                if ($request['type'] == 1) {
                    $messages_final_array = array();

                    $game = Games::whereId($request['id'])->first();
                    $conversation = \App\Models\Conversations::where('type',1)
                        ->where('from_id',$request['id'])
                        ->where('to_id',$request['id'])
                       ->first();

                    $messages = Messages::where('type',1)
                        ->where('conversation_id',$conversation['id'])
                        ->orderBy('date','desc')
                        ->paginate(30);

                    foreach ($messages as $message) {
                        if ($message['from_id'] == $user->id) {
                            $data['user_id'] = $user->id;
                            $data['sender_profile_pic'] = $user['profile_pic'];
                            $data['sender_name'] = $user['name'];
                        } else {
                            $otherUser = Users::where('id', $message['from_id'])->first();
                            $data['user_id'] = $otherUser->id;
                            $data['sender_profile_pic'] = $otherUser['profile_pic'];
                            $data['sender_name'] = $otherUser['name'];
                        }
                        $data['type_id'] = $game->id;

                        $data['date'] =  date('Y-m-d H:i:s', strtotime($message['date']));
                        $data['reciever_name'] = $game['name'];
                        $data['reciever_profile_pic'] = $game['cover_pic'];
                        $data['message'] = $message['content'];
                        $messages_final_array[] = $data;
                    }

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Chat History List',
                            'success' => $messages_final_array,
                            'currentPage' => $messages->currentPage(),
                            'hasMorePages' => $messages->hasMorePages()
                        ]
                    );


                }elseif ($request['type'] == 2) {
                    $messages_final_array = array();
                    $team = Teams::whereId($request['id'])->first();
                    $conversation = \App\Models\Conversations::where('type',2)
                        ->where('from_id',$request['id'])
                        ->where('to_id',$request['id'])
                        ->first();
                    $messages = Messages::where('type',2)
                        ->where('conversation_id',$conversation['id'])
                        ->orderBy('date','desc')
                        ->paginate(30);
                    foreach ($messages as $message) {
                        if ($message['from_id'] == $user->id) {
                            $data['user_id'] = $user->id;
                            $data['sender_profile_pic'] = $user['profile_pic'];
                            $data['sender_name'] = $user['name'];
                        } else {
                            $otherUser = Users::where('id', $message['from_id'])->first();
                            $data['user_id'] = $otherUser->id;
                            $data['sender_profile_pic'] = $otherUser['profile_pic'];
                            $data['sender_name'] = $otherUser['name'];
                        }
                        $data['type_id'] = $team->id;
                        $data['date'] = date('Y-m-d H:i:s', strtotime($message['date']));

                        $data['reciever_name'] = $team['name'];
                        $data['reciever_profile_pic'] = $team['cover_pic'];
                        $data['message'] = $message['content'];
                        $messages_final_array[] = $data;
                    }

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Chat History List',
                            'success' => $messages_final_array,
                            'currentPage' => $messages->currentPage(),
                            'hasMorePages' => $messages->hasMorePages()
                        ]
                    );


                }elseif ($request['type'] == 3) {
                    $messages_final_array = array();

                    $conversation = \App\Models\Conversations::where('from_id',$user['id'])
                        ->where('to_id',$request['id'])
                        ->where('type',3)
                        ->first();
                    if (!$conversation)
                        $conversation = \App\Models\Conversations::where('from_id',$request['id'])
                            ->where('to_id',$user['id'])
                            ->where('type',3)->first();

                    $otherUser = Users::whereId($request['id'])->first();

                    $messages = Messages::where('type',3)
                            ->where('conversation_id',$conversation['id'])
                            ->orderBy('date','desc')
                            ->paginate(30);


                    foreach ($messages as $message) {
                        if ($message['from_id'] == $user->id) {
                            $data['user_id'] = $user->id;
                            $data['type_id'] = $otherUser->id;
                            $data['sender_profile_pic'] = $user['profile_pic'];
                            $data['sender_name'] = $user['name'];
                            $data['reciever_name'] = $otherUser['name'];
                            $data['reciever_profile_pic'] = $otherUser['profile_pic'];
                        } else {
                            $data['user_id'] = $otherUser->id;
                            $data['type_id'] = $user->id;
                            $data['sender_profile_pic'] = $otherUser['profile_pic'];
                            $data['sender_name'] = $otherUser['name'];
                            $data['reciever_name'] = $user['name'];
                            $data['reciever_profile_pic'] = $user['profile_pic'];
                        }
                        $data['date'] = date('Y-m-d H:i:s', strtotime($message['date']));
                        $data['message'] = $message['content'];
                        $messages_final_array[] = $data;
                    }

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Chat History List',
                            'success' => $messages_final_array,
                            'currentPage' => $messages->currentPage(),
                            'hasMorePages' => $messages->hasMorePages()
                        ]
                    );
                }

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Json schema failed'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Chat Lising
     */

    public function  chatListing(Request $request){
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user) {
            if ($request) {
//->has('game') || $request->has('team') || $request->has('me')
                $all_messages_final = array();
                $conversations = array();
                // if ($request->has('me')) {
                //
                //     $sort = array();
                //     foreach ($all_messages_final as $key => $row)
                //     {
                //         $sort[$key] = $row['raw_date'];
                //     }
                //     array_multisort($sort, SORT_DESC, $all_messages_final);
                //
                // }
                // elseif ($request->has('team')) {
                //
                //     $sort = array();
                //     foreach ($all_messages_final as $key => $row)
                //     {
                //         $sort[$key] = $row['raw_date'];
                //     }
                //     array_multisort($sort, SORT_DESC, $all_messages_final);
                //
                // }
                // elseif ($request->has('game')) {
                //     }

                $conversations = Conversations::where('type', 3)
                    ->where('to_id', $user->id)
                    ->orWhere('from_id', $user->id)
                    ->whereHas('Messages', function($query) {
                        $query->where('type',3);
                    })
                    ->paginate(20);
                foreach ($conversations as $conversation) {
                    if ($conversation['from_id'] == $user->id) {
                        $otherUser = Users::where('id', $conversation['to_id'])->first();
                    } else {
                        $otherUser  = Users::where('id', $conversation['from_id'])->first();
                    }
                    $item['id'] = $otherUser['id'];
                    $item['type'] = 3;
                    $item['user_id'] = $otherUser['id'];
                    $item['user_name'] = $otherUser['name'];
                    $item['user_pic'] = $otherUser['profile_pic'];
                    $last_message = Messages::where('conversation_id', $conversation->id)
                        ->where('type',3)
                        ->orderBy('date', 'desc')
                        ->first();

                    if ($last_message) {
                        if ($last_message['from_id'] == $user->id)
                            $item['last_message'] = 'Me: ' . $last_message['content'];
                        else
                            $item['last_message'] = $otherUser['name'] . ': ' . $last_message['content'];
                        $item['date'] = getDateString($last_message['date']);
                        $item['raw_date'] = strtotime($last_message['date']);
                    } else {
                        $item['last_message'] = '';
                        $item['date'] = '';
                        $item['raw_date'] = '';
                    }

                    $item['unread'] = 0;
                    $all_messages_final[] = $item;
                }


                $conversations = $user->Teams()
                    ->wherePivot('invitation_status', 1)
                    ->orderBy('updated_at','asc')
                    ->paginate(20);

                foreach ($conversations as $team) {

                    $item['id'] = $team['id'];
                    $item['type'] = 2;
                    $item['user_id'] = $team['id'];
                    $item['user_name'] = $team['name'];
                    $item['user_pic'] = $team['cover_pic'];
                    $item['date'] = '';
                    $last_message = Messages::where('type',2)
                        ->where('to_id', $team->id)
                        ->orderBy('date', 'desc')->first();
                    if ($last_message) {
                        if ($last_message['from_id'] == $user->id){
                            $item['last_message'] = 'Me: ' . $last_message['content'];
                        }else{
                            $otherUser = Users::where('id', $last_message['from_id'])->first();
                            $item['last_message'] = $otherUser['name'] . ': ' . $last_message['content'];
                        }
                        $item['date'] = getDateString($last_message['date']);
                        $item['raw_date'] = strtotime($last_message['date']);

                    } else {
                        $item['last_message'] = '';
                        $item['raw_date'] = '';
                        $item['date'] = '';
                    }

                    $item['unread'] = 0;
                    $all_messages_final[] = $item;
                }
                $conversations = $user->Games()
                        ->where('status', 1)->wherePivot('invitation_status', 1)
                        ->orderBy('updated_at','asc')
                        ->paginate(20);

                  foreach ($conversations as $game) {

                        $item['id'] = $game['id'];
                        $item['user_id'] = $game['id'];
                        $item['type'] = 1;
                        $item['user_name'] = $game['name'];
                        $item['user_pic'] = $game['cover_pic'];
                        $last_message = Messages::where('type',1)
                            ->where('to_id', $game->id)
                            ->orderBy('date', 'desc')
                            ->first();
                        if ($last_message) {
                            if ($last_message['from_id'] == $user->id){
                                $item['last_message'] = 'Me: '.$last_message['content'];
                            }else{
                                $otherUser = Users::where('id', $last_message['from_id'])->first();
                                $item['last_message'] = $otherUser['name'] . ': ' . $last_message['content'];
                            }
                            $item['raw_date'] = strtotime($last_message['date']);
                            $item['date'] = getDateString($last_message['date']);
                        } else {
                            $item['last_message'] = '';
                            $item['date'] = '';
                            $item['raw_date'] = '';
                        }
                         $item['unread'] = 0;
                        $all_messages_final[] = $item;
                }

                $sort = array();
                foreach ($all_messages_final as $key => $row)
                {
                    $sort[$key] = $row['raw_date'];
                }
                array_multisort($sort, SORT_DESC, $all_messages_final);

                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Chat Player List',
                        'success' => $all_messages_final,
                        'currentPage' => $conversations->currentPage(),
                        'hasMorePages' => $conversations->hasMorePages()
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Json schema failed'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
    }



}
