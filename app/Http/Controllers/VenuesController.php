<?php

namespace App\Http\Controllers;


use App\Models\AccessToken;
use App\Models\Amenities;
use App\Models\Grounds;
use App\Models\Locations;
use App\Models\Search;
use App\Models\Slot;
use App\Models\Bookings;
use App\Models\Sports;
use App\Models\Users;
use App\Models\VenueAmenities;
use App\Models\VenueGallery;
use App\Models\Venues;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class VenuesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Create venue API
     */

    public function  createVenue(Request $request){
        if (isset($request['cover_pic'], $request['name'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                // check duplicating of venues
                $venuecheck = Venues::where('name', $request['name'])->first();
                if ($venuecheck)
                    if ($venuecheck['vendor_id'] == $user['id'])
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Venue already created'
                            ],500
                        );
                    $venue = new Venues();
                    $venue['name'] = $request['name'];
                    $venue['cover_pic'] = $request['cover_pic'];
                    $venue['description'] = '';
                if ($request->has('description'))
                        $venue['description'] = $request['description'];
                    $venue['vendor_id'] = $user['id'];
                    $venue['is_verified'] = filter_var($request['0'], FILTER_VALIDATE_BOOLEAN);
                    $venue['is_disabled'] = filter_var($request['0'], FILTER_VALIDATE_BOOLEAN);
                    $venue['is_available'] = filter_var($request['0'], FILTER_VALIDATE_BOOLEAN);
                    $venue['sports_types'] = '';
                if ($request->has('amenities')){
                    $amenitiesList = json_decode(json_encode($request['amenities']), true);
                    foreach($amenitiesList as $value) {
                        if ($value['id']) {
                            $aminitiesVenue = VenueAmenities::where('amenity_id', $value['id'])->where('venue_id', $request['venue_id'])->first();
                            if (!$aminitiesVenue) {
                                $aminitiesVenue = new VenueAmenities();
                                $aminitiesVenue['venue_id'] = $request['venue_id'];
                                $aminitiesVenue['amenity_id'] = $value['id'];
                                $aminitiesVenue->save();
                            }
                        }
                    }
                }
                $venue->save();

                $amenity = $venue->Amenities();
                $venue['amenities'] = $amenity;
                    unset($venue['vendor_id']);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Venue created successfully',
                        'success'  => $venue
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * update venue API
     */

    public function  editVenue(Request $request){
        if (isset($request['venue_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $venue = Venues::where('id', $request['venue_id'])->first();
                if ($venue) {
                    if ($request->has('name')){
                        $venuecheck = Venues::where('name', $request['name'])->first();
                        if ($venuecheck)
                            if ($venuecheck['vendor_id'] == $user['id'])
                                return response()->json(
                                    [
                                        'status' => 'error',
                                        'message' => 'Venue already created'
                                    ],500
                                );
                        $venue['name'] = $request['name'];
                    }

                    if ($request->has('cover_pic'))
                    $venue['cover_pic'] = $request['cover_pic'];
                      $venue['description'] = '';
                    if ($request->has('description'))
                        $venue['description'] = $request['description'];
                    if ($request->has('amenities')){
                        $aminitiesVenue =  VenueAmenities::where('venue_id', $request['venue_id'])->get();
                        if (count($aminitiesVenue)>0)
                             $aminitiesVenue->find($request['venue_id'])->delete();
                        $amenitiesList = json_decode(json_encode($request['amenities']), true);
                        foreach($amenitiesList as $value) {
                            if ($value['id']) {
                                    $aminitiesVenue = new VenueAmenities();
                                    $aminitiesVenue['venue_id'] = $request['venue_id'];
                                    $aminitiesVenue['amenity_id'] = $value['id'];
                                    $aminitiesVenue->save();
                            }
                        }
                    }
                    if ($request->has('lat')&&$request->has('lng')&&$request->has('location_name')){
                        $venue['lat'] = $request['lat'];
                        $venue['lng'] = $request['lng'];
                        $venue['location_name'] = $request['location_name'];
                    }
                    if ($request->has('is_disabled')){
                            $venue['is_disabled'] = filter_var($request['is_disabled'], FILTER_VALIDATE_BOOLEAN);
                    }

                    $grounds = $venue->Grounds()->orderBy('name','asc')->where('is_disabled',0)->get();
                    if (sizeof($grounds)>0 && $venue['lat'] != 0 && $venue['lng'] != 0  && $venue['location_name'] != ''){
                        $venue['is_available'] =  1;
                        $item['name'] = $venue->name;
                        $item['id'] = $venue->id;
                        $item['name'] = $venue->name;
                        $item['type'] = 4;
                        $search = Search::where('id',$venue->id)->where('type',4)->first();
                        if ($search)
                            $search->delete();

                        $search = new Search();
                        $search->save($item);
                    }else{
                        $venue['is_available'] =  0;
                        $search = Search::where('id',$venue->id)->where('type',4)->first();
                        if ($search)
                            $search->delete();
                    }


                    $venue->update();
                    $amenity = $venue->Amenities();
                    $data = $venue;
                    $data['amenities'] = $amenity;

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Venue updated successfully',
                            'success' => $data
                        ]
                    );

                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No venue found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }


        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }



    /**
     * add image to venue
     */


    public function  addVenueImage(Request $request){

        ///  ** sending vineu id in header
        if ($request->header('venueId')){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $venue = Venues::where('id', $request->header('venueId'))->first();
                if ($venue) {
                    $gallery = new VenueGallery();
                    if ($request->file('image')){
                        $file = $request->file('image');
                        $origin_name = $file->getClientOriginalName();
                        $date = date_create();
                        $timeStamp = date_timestamp_get($date);
                        $key =  'vendors/venues/'.str_replace(' ','_',$venue->name).$timeStamp.'/'.$origin_name;
                        Storage::disk('s3')->put($key, fopen($file, 'r+'),'public');
                        $url= Storage::disk('s3')->url($key);
                        $gallery['image']=  $url;
                        $gallery['venue_id'] = $venue->id;
                        $gallery->save();
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'message' => 'Gallery image added successfully',
                                'success' => $gallery
                            ]
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'message' => 'Venue image not found'
                        ],500
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No venue found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }


        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }





    /**
     * delete image from venue
     */


    public function  deleteVenueImage(Request $request){
        if (isset($request['venue_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $venue = Venues::where('id', $request['venue_id'])->first();
                if ($venue) {
                    if (isset($request['gallery_id'])) {
                        $gallery = VenueGallery::where('id', $request['gallery_id'])->first();
                        $gallery->delete();

                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'message' => 'Gallery deleted successfully',
                                'success' => $gallery
                            ]
                        );
                    }return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'No gallery images found'
                        ],500
                    );

                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No venue found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }


        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }





    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Venues listing HOT Spots API
     */

    public function  venuesListHot(Request $request)
    {
        if ($request->header('accessToken')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken)
                $user = $accesstoken->User()->first();
            else
                $user = [];
        }else{
            $user = [];
        }

        $venues_list_all = Venues::where('is_available',1)
            ->where('lng','!=',0)
            ->where('lat','!=',0)
            ->where('is_disabled',0)
            ->where('is_verified',1)
            ->orderBy('rating','desc')
            ->paginate(10);

        $venues_list=array();
        foreach($venues_list_all as $k=>$venues){
            $grounds = $venues->Grounds()->orderBy('name','asc')->where('is_disabled',0)->get();
            $amenity = $venues->Amenities();
            $gallery = $venues->Gallery()->limit(10)->pluck('image')->toArray();
            $gallery = array_merge($gallery,array($venues['cover_pic']));
            $amenity = array_column($amenity, 'name');

            if (count($amenity)>0)
                $amenity = implode( ",", $amenity );
            else
                $amenity = '';
            $venues["distance"] = 0.0;
            if ($user && $user['lat']&& $user['lng']){
                $degrees = rad2deg(acos((sin(deg2rad($user['lat']))*sin(deg2rad($venues['lat']))) + (cos(deg2rad($user['lat']))*cos(deg2rad($venues['lat']))*cos(deg2rad($user['lng']-$venues['lng'])))));
                $venues["distance"] = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
            }


            $venues_list[]=
                array(
                    'id' => $venues["id"],
                    'name' => $venues["name"],
                    'cover_pic' => $venues["cover_pic"],
                    'description' => $venues["description"],
                    'sports_types' => $venues["sports_types"],
                    'is_verified' => filter_var($venues["is_verified"], FILTER_VALIDATE_BOOLEAN),
                    'is_disabled' => filter_var($venues["is_disabled"], FILTER_VALIDATE_BOOLEAN),
                    'lat' => $venues["lat"],
                    'lng' => $venues["lng"],
                    'location_name' => $venues["location_name"],
                    'distance' =>  number_format((float)$venues["distance"], 1, '.', '').' km',
                    'rating' => round($venues['rating']),
                    'grounds' => $grounds,
                    'images' => $gallery,
                    'amenities' => $amenity
                );
        }
        return response()->json(
            [
                'status' => 'success',
                'statusCode' => 200,
                'message' => 'Venues List',
                'success' => $venues_list,
                'currentPage' => $venues_list_all->currentPage(),
                'hasMorePages' => $venues_list_all->hasMorePages()
            ]
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Venues listing API
     */

    public function  venuesList(Request $request)
    {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user){
            $venues_list_all = Venues::orderBy('updated_at','desc')->where('vendor_id', $user->id)->get();
            $venues_list=array();
            foreach($venues_list_all as $k=>$venues){
                $gallery = $venues->Gallery()->limit(10)->pluck('image')->toArray();
                $gallery = array_push($gallery,$venues['cover_pic']);
                $venues_list[]=
                    array(
                        'id' => $venues["id"],
                        'name' => $venues["name"],
                        'cover_pic' => $venues["cover_pic"],
                        'description' => $venues["description"],
                        'sports_types' => $venues["sports_types"],
                        'is_verified' => filter_var($venues["is_verified"], FILTER_VALIDATE_BOOLEAN),
                        'is_disabled' => filter_var($venues["is_disabled"], FILTER_VALIDATE_BOOLEAN),
                        'lat' => $venues["lat"],
                        'lng' => $venues["lng"],
                        'location_name' => $venues["location_name"],
                        'images' => $gallery,
                        'rating' => round($venues['rating']),
                    );
            }
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Venues List',
                    'success' => $venues_list,
                ]
            );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not found'
            ], 500
        );

    }




    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Vendor New Booking details API
     */

    public function  newBookingDetails(Request $request)
    {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            
            if ($user){
                $venues_list_all = Venues::orderBy('updated_at','desc')
                ->where('vendor_id', $user->id)
                ->where('is_available',1)
                ->orderBy('name','asc')->get();
                $ground_details = '';
                $venue_details = '';
                if (sizeof($venues_list_all) == 1){
                    $venues = $venues_list_all[0];
                    $venue_details = $venues;
                    $grounds_list = $venues->Grounds()->get();
                    if (sizeof($grounds_list)==1){
                        $ground_details = $grounds_list[0];
                    }
                }
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Venues List',
                        'venueList' => $venues_list_all,
                        'venueDetails' => $venue_details,
                        'groundDetails' => $ground_details,
                    ]
                );
            }

            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );

    }









    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Venue ground listing API
     */

    public function  VenuesGroundList(Request $request)
    {
        if ($request->has('venue_id')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user){
                $ground_list = array();
                $venue = $user->Venues()->whereId($request['venue_id'])->first();
                if ($venue) {
                    $ground_list = $venue->Grounds()->get();

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Venue Ground List',
                            'success' => $ground_list,
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Venue not found'
                    ], 500
                );
                } return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );

        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not found'
            ], 500
        );

    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Venue listing players app
     */
    public function  venuesListPlayer(Request $request)
    {
        if ($request->header('accessToken')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken)
                $user = $accesstoken->User()->first();
            else
                $user = [];
        }else{
            $user = [];
        }
            if ($request->has('lat')&&$request->has('lng')){
                $lat = $request['lat'];
                $lng = $request['lng'];
                $upper_latitude = $lat + (.50); //Change .50 to small values
                $lower_latitude = $lat - (.50); //Change .50 to small values
                $upper_longitude = $lng + (.50); //Change .50 to small values
                $lower_longitude = $lng - (.50); //Change .50 to small values
                $venues_list_all = Venues::whereBetween('lat', [$lower_latitude, $upper_latitude])
                    ->whereBetween('lng', [$lower_longitude, $upper_longitude])
                    ->where('is_available',1)
                    ->where('is_disabled',0)
                    ->where('is_verified',1)
                    ->where('lng','!=',0)
                    ->where('lat','!=',0)
                    ->paginate();
            }else if ($user && $user['lat']&& $user['lng']){
                    $lat = $user['lat'];
                    $lng = $user['lng'];
                $venues_list_all = Venues::
                    selectRaw("*,( 1.609344 * 3959 * acos( cos( radians(" . $lat . ") ) * cos( radians( lat) ) * cos( radians( lng ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians( lat ) ) ) ) as distance")
                        ->where('is_available',1)
                        ->where('lat','!=',0)
                        ->where('lng','!=',0)
                        ->where('is_disabled',0)
                        ->where('is_verified',1)
                        ->orderBy('distance', 'asc')->paginate(10);

            }else
                $venues_list_all = Venues::orderBy('name', 'asc')
                    ->where('is_available',1)
                    ->where('lng','!=',0)
                    ->where('lat','!=',0)
                    ->where('is_disabled',0)
                    ->where('is_verified',1)
                    ->paginate(10);
            $venues_list=array();
        foreach($venues_list_all as $k=>$venues){
            $grounds = $venues->Grounds()->orderBy('name','asc')->where('is_disabled',0)->get();
            $amenity = $venues->Amenities();
            $gallery = $venues->Gallery()->limit(10)->pluck('image')->toArray();
            $gallery = array_merge($gallery,array($venues['cover_pic']));
            $amenity = array_column($amenity, 'name');

            if (count($amenity)>0)
                 $amenity = implode( ",", $amenity );
            else
                $amenity = '';
            $venues["distance"] = 0.0;
            if ($user && $user['lat']&& $user['lng']){
                $degrees = rad2deg(acos((sin(deg2rad($user['lat']))*sin(deg2rad($venues['lat']))) + (cos(deg2rad($user['lat']))*cos(deg2rad($venues['lat']))*cos(deg2rad($user['lng']-$venues['lng'])))));
                $venues["distance"] = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
            }


            $venues_list[]=
                array(
                    'id' => $venues["id"],
                    'name' => $venues["name"],
                    'cover_pic' => $venues["cover_pic"],
                    'description' => $venues["description"],
                    'sports_types' => $venues["sports_types"],
                    'is_verified' => filter_var($venues["is_verified"], FILTER_VALIDATE_BOOLEAN),
                    'is_disabled' => filter_var($venues["is_disabled"], FILTER_VALIDATE_BOOLEAN),
                    'lat' => $venues["lat"],
                    'lng' => $venues["lng"],
                    'location_name' => $venues["location_name"],
                    'distance' =>  number_format((float)$venues["distance"], 1, '.', '').' km',
                    'rating' => round($venues['rating']),
                    'grounds' => $grounds,
                    'images' => $gallery,
                    'amenities' => $amenity
                );
        }
        return response()->json(
            [
                'status' => 'success',
                'statusCode' => 200,
                'message' => 'Venues List',
                'success' => $venues_list,
                'currentPage' => $venues_list_all->currentPage(),
                'hasMorePages' => $venues_list_all->hasMorePages()
            ]
        );
    }




    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Venue details API
     */

    public function  venueDetails(Request $request)
    {

        if ($request->has('venue_id')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user){
                $venue = $user->Venues()->whereId($request['venue_id'])->first();

                if ($venue) {
                    $sport_list = Sports::orderBy('name','asc')->get();
                    $aminities_list = Amenities::orderBy('name','asc')->get();
                    $venue['is_verified'] = filter_var($venue['is_verified'], FILTER_VALIDATE_BOOLEAN);
                    $venue['is_disabled'] = filter_var($venue['is_disabled'], FILTER_VALIDATE_BOOLEAN);
                    $venue['locationdetails'] = '';
                    $venue['grounddetails']  = '';
                    if ($venue['description'] == NULL)
                        $venue['description'] = 'N/A';
                    /// ** check for location status
                    if (empty($venue['lat']) || empty($venue['lng'])|| empty($venue['location_name']))
                        $venue['locations_status'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    else{
                        $venue['locations_status'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                        $locationDetails['lat'] = $venue['lat'];
                        $locationDetails['lng'] = $venue['lng'];
                        $locationDetails['location_name'] = $venue['location_name'];
                        $venue['locationdetails'] = $locationDetails;

                    }


                    ///  $venue['name'] = strtolower($venue['name']);

                    // for view bookings section
                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $today =  $date->format('Y-m-d');
                    $first_ground = $venue->Grounds()->orderBy('name','asc')->first();
                    if ($first_ground){
                        $slot = $first_ground->Slot()->first();
                        if(count($slot)){
                          $booking = Bookings::where('slot_id',$slot->id)->get();
                          if(count($booking)){
                            $bookings = $slot->Bookings()->where('payment_status',1)->where('date',date("Y-m-d", strtotime($today)))->orderBy('date','asc')->get();
                            if (sizeof($bookings) == 0){
                                $bookings_new = $slot->Bookings()->where('payment_status',1)->where('date','>',date("Y-m-d", strtotime($today)))->orderBy('date','asc')->first();
                                if ($bookings_new){
                                    $today = $bookings_new['date'];
                                }
                            }
                          }
                        }

                    }else{
                        $first_ground['name'] = '';
                        $first_ground['id'] = 0;
                    }
                    $venue['booking_ground_name'] =  $first_ground['name'];
                    $venue['booking_ground_id'] =  $first_ground['id'];
                    $venue['booking_date'] =  $today;


                    /// ** check for ground status
                    $grounds = $venue->Grounds()->orderBy('name','asc')->get();
                    $grounds_final=array();

                    foreach($grounds as $value){
                        $slot = $value->Ranges()->first();
                        $grounds_final[]=
                            array(
                                'id' => $value['id'],
                                'name' => $value['name'],
                                'start_time' => $slot['start_time'],
                                'end_time' => $slot['end_time'],
                                'slot_duration' => $slot['duration'],
                                'max_member' => $slot['total_players'],
                                'slot_price' => $slot['price'],
                                'is_disabled' =>filter_var($value['is_disabled'], FILTER_VALIDATE_BOOLEAN),
                                'venue_id' =>$value['venue_id'],
                                'sports_id' => $value['sports_id'],
                                'sports_name' => $value['sports_name'],
                                'sports_image' => $value['sport']['thumbnail'],
                            );
                    }
                    if (sizeof($grounds_final)>0) {
                        $venue['ground_status'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                        $venue['grounddetails']  = $grounds_final;

                    }else{
                        $venue['ground_status'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    }

                    //// *** check for aminities

                    $aminities = $venue->Amenities();
                    if (count($aminities)>0)
                        $venue['amenities_status'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                    else{
                        $venue['amenities_status'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                        $aminities = '';
                    }

                    $venue['allsports'] = $sport_list;
                    $venue['amenities'] = $aminities;
                    $venue['allamenities'] = $aminities_list;

                    /// ** check for gallery status
                    $venue['gallerydetails'] = '';
                    $venuepictures = $venue->Galleries()->first();
                    if (empty($venuepictures))
                        $venue['gallery_status'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    else{
                        $venue['gallery_status'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                        $galleryDetails =$venue->Galleries()->where('venue_id', $venue->id)->orderBy('updated_at','desc')->get();
                        $venue['gallerydetails'] = $galleryDetails;
                    }

                    if ($venue['sports_types']){
                        $sportsArray = explode(",", $venue['sports_types']);
                        $sportsArray = array_unique($sportsArray);
                        $venue['sports_types'] = implode(",", $sportsArray);
                    }



                    unset($venue['vendor_id']);
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Venue details',
                            'success' => $venue,
                        ]
                    );



                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Venue not found'
                    ], 500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }


    /**
     * Delete Venue
     */


    public function  deleteVenue(Request $request)
    {
        if (isset($request['venue_id'],$request['email'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $venue = Venues::where('id', $request['venue_id'])->first();
                if ($venue) {
                    if ($user['email']==$request['email']){
                        $venue->delete();
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'message' => 'Venue deleted successfully'
                            ]
                        );

                    }
                }return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'email doesn\'t match'
                    ], 500
                );

            }return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );
        }return response()->json(
        [
            'status' => 'error',
            'message' => 'Json schema failed'
        ], 500
        );
    }


        public function  GroundListVenue(Request $request)
    {
        // dd($request['sport_id']);
        Log::info($request->all());
        Log::info('--------------------------');
        Log::info($request['sport_id']);
        if (isset($request['sport_id'])) {
            Log::info($request->all());
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();

            if ($user){
                $grounds = Grounds::where('sports_id', $request['sport_id'])
                    ->where('is_disabled', 0)
                    ->where('is_deleted',0)
                    ->whereHas('Venue', function($query) {
                    $query->where('lng','!=',0);
                    $query->where('lat','!=',0);
                    $query->where('is_disabled',0);
                    $query->where('is_verified',1);
                })
                    ->with('Venue')
                    ->orderBy('venue_id','desc')
                    ->paginate(15);

                $grounds_list=array();
                foreach($grounds as $k=>$ground){
                    $ground_details[] = array(
                        'id' => $ground['id'],
                        'name' => $ground['name']
                    );
                        $venue = $ground['venue'];
                        $gallery = $venue->Gallery()->limit(10)->pluck('image')->toArray();
                        $gallery = array_merge($gallery,array($venue['cover_pic']));
                        $venue['images'] = $gallery;

                        if (  array_key_exists($venue['id'],$grounds_list)) {
                             $venueMod = $grounds_list[$venue->id];
                             $groundList = $venueMod['grounds'];
                             $groundList[] = $ground_details[0];
                             $venueMod['grounds'] =  $groundList;
                             $grounds_list[$venue->id] = $venueMod;
                         } else {
                             $venue['grounds'] = $ground_details;
                             $grounds_list[$venue->id] = $venue;
                         }
                         $ground_details = [];
                }
                    return response()->json(
                        [
                            'status' => 'success',
                            'message' => 'Ground  found',
                            'success' =>    array_values($grounds_list) ,
                            'currentPage' => $grounds->currentPage(),
                            'hasMorePages' => $grounds->hasMorePages()
                        ]
                    );
                }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );

            }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function  submitRating(Request $request)
    {
        if (isset($request['venue_id'],$request['rating'],$request['game_id'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $venue = Venues::where('id', $request['venue_id'])->first();
                $rated_already = $user->RateVenue()->where('venue_id',$request['venue_id'])->where('game_id',$request['game_id'])->first();
                if ($venue ) {                    
                    
                    if (!$rated_already) {
                        $rating = $request['rating'];
                        if ($rating > 5.0)
                            $rating = 5.0;
                        $user->RateVenue()->attach($user->id,['venue_id'=>$venue['id'],'rating' => $rating,'game_id' => $request['game_id']]);
                        $no_user_rated = $venue->RatedUsers()->wherePivot('venue_id',$venue['id'])->count();
                        $total_venue_point = $venue->RatedUsers()->wherePivot('venue_id',$venue['id'])->sum('rating');                       
                        $current_rating = $venue['rating'];
                        // $new_rating = ($current_rating + $rating)/($no_user_rated +1 );

                        $new_rating = ($total_venue_point + $rating) / ($no_user_rated + 1);
                        
                        $venue['rating'] = $new_rating;
                        $venue->update(); 
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Venue rated successfully'
                        ]
                    );
                }return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Venue not found'
                    ], 500
                );

            }return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );
        }return response()->json(
        [
            'status' => 'error',
            'message' => 'Json schema failed'
        ], 500
    );
    }



    /**
     * Image upload API
     */

    public function uploadImage(Request $request){

        if($request->file('image')){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $file = $request->file('image');
                $origin_name = $file->getClientOriginalName();
                $date = date_create();
                $timeStamp = date_timestamp_get($date);
                $key = 'vendors/venues/' . str_replace(' ', '_', $user->name) . $timeStamp . '/' . $origin_name;
                Storage::disk('s3')->put($key, fopen($file, 'r+'), 'public');
                $url = Storage::disk('s3')->url($key);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Image uploaded',
                        'success' => $url
                    ]
                );
            }return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ],500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Image missing'
            ],500
        );

    }
}
