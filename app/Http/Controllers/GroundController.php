<?php

namespace App\Http\Controllers;



use App\Models\AccessToken;
use App\Models\Amenities;
use App\Models\Grounds;
use App\Models\Locations;
use App\Models\Slot;
use App\Models\Bookings;
use App\Models\Sports;
use App\Models\Users;
use App\Models\VenueAmenities;
use App\Models\VenueGallery;
use App\Models\Venues;
use App\Models\Range;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class GroundController extends Controller
{


    /*
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * add ground to venue API
     */

    public function  addGround(Request $request){
        // if (isset($request['venue_id'],$request['start_time'],$request['end_time'],$request['slot_duration'],$request['max_member'],$request['slot_price'],$request['sports_id'],$request['sports_name'],$request['name'])){
        if (isset($request['venue_id'],$request['sports_id'],$request['sports_name'],$request['name'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $venue = Venues::where('id', $request['venue_id'])->first();
                if ($venue) {
                    $ground = $venue->Grounds()->where('name', $request['name'])->first()   ;
                    if ($ground){
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Ground with same name already exits'
                            ],500
                        );
                    }
                    $ground = new Grounds();
                    // $ground['max_member'] = $request['max_member'];
                    $ground['sports_id'] = $request['sports_id'];
                    $ground['sports_name'] = $request['sports_name'];
                    $ground['name'] = $request['name'];
                    $ground['venue_id'] = $request['venue_id'];
                    $ground['is_disabled'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    $ground ->save();

                    // $slot = new Slot();
                    // $slot['start_time'] = $request['start_time'];
                    // $slot['end_time'] = $request['end_time'];
                    // $slot['slot_duration'] = $request['slot_duration'];
                    // $slot['slot_price'] = $request['slot_price'];
                    // $slot['ground_id'] = $ground['id'];
                    // $slot->save();

                    if ($venue['sports_types']){
                        $venue['sports_types'] = $venue['sports_types'] . ',' . $request['sports_name'];
                        $sportsArray = explode(",", $venue['sports_types']);
                        $sportsArray = array_unique($sportsArray);
                        $venue['sports_types'] = implode(",", $sportsArray);

                    }
                    else{
                        $venue['sports_types'] = $request['sports_name'];
                    }
                    $venue['is_available'] =  1;
                    $venue->update();

                    // $ground['start_time'] = $request['start_time'];
                    // $ground['end_time'] = $request['end_time'];
                    // $ground['slot_duration'] = $request['slot_duration'];
                    // $ground['slot_price'] = $request['slot_price'];
                    $sports = Sports::whereId($request['sports_id'])->first();
                    $ground['sports_image'] = $sports['thumbnail'];
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Ground created successfully',
                            'success' => $ground
                        ]
                    );

                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No venue found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * edit ground to venue API
     */

    public function  editGround(Request $request){
        if (isset($request['ground_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $ground = Grounds::where('id', $request['ground_id'])->where('is_deleted',0)->first();
                if ($ground) {
                    $slot = $ground->Slot()->first();

                    // if ($request->has('start_time')){
                    //
                    //     $slot['start_time'] = $request['start_time'];
                    // }
                    //
                    // if ($request->has('end_time'))
                    //     $slot['end_time'] = $request['end_time'];
                    // if ($request->has('slot_duration'))
                    //     $slot['slot_duration'] = $request['slot_duration'];
                    // if ($request->has('max_member'))
                    //     $ground['max_member'] = $request['max_member'];
                    // if ($request->has('slot_price'))
                    //     $slot['slot_price'] = $request['slot_price'];
                    if ($request->has('sports_id')&&$request->has('sports_name')){
                        $ground['sports_id'] = $request['sports_id'];
                        $ground['sports_name'] = $request['sports_name'];
                    }
                    if ($request->has('name'))
                        $ground['name'] = $request['name'];
                    if ($request->has('is_disabled'))
                        $ground['is_disabled'] = filter_var($request['is_disabled'], FILTER_VALIDATE_BOOLEAN);
                    
                    // dd($ground);    
                    $ground ->update();



                    // $slot['updated_at'] =new \DateTime("now");
                    // $slot ->update();

                    $venue = $ground->Venue()->first();
                    
                    // if ($request->has('sports_id')&&$request->has('sports_name')) {
                        if ($venue['sports_types']){
                            $groundSportList = Grounds::where('venue_id',$venue->id)->where('is_disabled',0)->where('is_deleted',0)->pluck('sports_name')->toArray();
                            $sportsArray = array_unique($groundSportList);
                            $venue['sports_types'] = implode(",", $sportsArray);
                            
                            // $venue['sports_types'] = $venue['sports_types'] . ',' . $request['sports_name'];
                            // $sportsArray = explode(",", $venue['sports_types']);
                            // $sportsArray = array_unique($sportsArray);
                            // $venue['sports_types'] = implode(",", $sportsArray);
                        }
                        else{
                            
                            if(isset($request['sports_name'])){
                                $venue['sports_types'] = $request['sports_name'];
                            }                            
                        }
                    // }
                    $grounds = $venue->Grounds()->orderBy('name','asc')->get();
                    if (count($grounds)>0){
                        $venue['is_available'] =  1;
                    }
                    $venue->update();
                    // $ground['start_time'] = $slot['start_time'];
                    // $ground['end_time'] = $slot['end_time'];
                    // $ground['slot_price'] = $slot['slot_price'];
                    // $ground['slot_duration'] = $slot['slot_duration'];

                    if($request->has('sports_id')){

                      $sports = Sports::whereId($request['sports_id'])->first();
                      $ground['sports_image'] = $sports['thumbnail'];

                    }else{

                        $ground['sports_image'] = '';

                    }

                    //$ground['is_disabled'] = filter_var($ground['is_disabled'], FILTER_VALIDATE_BOOLEAN);

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Ground details updated successfully',
                            'success' => $ground
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No ground found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }

    /**
     * ground details API
     */

    public function deleteGround(Request $request){
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $ground = Grounds::whereId($request->ground_id)->first();

                $ground['is_deleted'] = 1;
                $ground['is_disabled'] = 1;

                $ground->save();

                $slotIds = Slot::where('ground_id',$ground->id)->pluck('id');

                $booking = Bookings::whereIn('slot_id',$slotIds)->update(['status'=>0]);

                $venue = $ground->Venue()->first();
                   
                if ($venue['sports_types']){
                    $groundSportList = Grounds::where('venue_id',$venue->id)->where('is_disabled',0)->where('is_deleted',0)->pluck('id')->toArray();
                    
                    $sportsArray = array_unique($groundSportList);
                    $venue['sports_types'] = implode(",", $sportsArray);                             
                    
                }
                else{
                    
                    if(isset($request['sports_name'])){
                        $venue['sports_types'] = $request['sports_name'];
                    }                            
                }

                $venue->save();

                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Ground deleted successfully!!!',
                        
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Json schema failed'
                ],500
            );
    } 

    public function  groundDetails(Request $request){

        if (isset($request['ground_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $ground = Grounds::where('id', $request['ground_id'])->where('is_deleted',0)->first();
                if ($ground) {
                    // $venue = $ground->Venue()->first();
                    $slot = $ground->Slot()->first();

                    $duration = $slot['slot_duration'];
                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $currenttime =  strtotime($date->format('H:i:s'));
                    $currentdate =  $date->format('d-m-Y');
                    $date = $date->format('d-m-Y');
                    if ($request->has('date')){
                        $old_date_timestamp = strtotime($request['date']);
                        $old_date = date('d-m-Y', $old_date_timestamp);
                        $date = $old_date;
                    }
                    $bookings_all = $slot->Bookings()->where('date',date("Y-m-d", strtotime($date)))->where('slot_id',$slot['id'])->where('payment_status',1)->where('status',1)->get();
                    if ($bookings_all->count() && $bookings_all[0]){
                        $booking = $bookings_all[0];
                        $duration = $booking['slot_duration'];
                    }
                    $array_of_slots = array ();
                    $start_time    = strtotime ($slot['start_time']); //change to strtotime
                    $end_time      = strtotime ($slot['end_time']);//change to strtotime
                    $diff = abs($start_time - $end_time) / 3600;
                    $diff = round(($diff*60)/$duration);
                    $duration  = $duration * 60;

                    for($i=1;$i<=$diff;$i++){
                        $next_time = $start_time + $duration;
                        $status = false;
                        if (($start_time > $currenttime && $date == $currentdate) || strtotime($date) > strtotime($currentdate)){
                            if ($bookings_all->count()){
                                foreach($bookings_all as $booking){
                                    $booking_startTime = strtotime($booking['start_time']);
                                    $booking_endTime = strtotime($booking['end_time']);
                                    if(date("g:i a", $start_time) >= date("g:i a", $booking_startTime) && date("g:i a", $next_time) <= date("g:i a", $booking_endTime)){
                                        $status= true;
                                        break;
                                    }else
                                        $status= false;
                                }
                            }

                            $slotPrice = Slot::where('start_time',date("H:i:s", $start_time))->where('end_time',date("H:i:s", $next_time))->where('ground_id',$request['ground_id'])->first();

                            $array_of_slots[]=
                                array(
                                    'date' =>  $date,
                                    'start_time' =>  $time = date("g:i a", $start_time),
                                    'end_time' =>  $time = date("g:i a", $next_time),
                                    'price' =>  $slotPrice ? $slotPrice['slot_price'] : 0,
                                    'total_players' =>  ($slotPrice && $slotPrice['Range']) ? $slotPrice['Range']['total_players'] : 0,
                                    'is_booked' => $status
                                );
                        }

                        $start_time = $next_time; // to check endtime
                    }

                    $ground['date'] = $date;
                    $ground['slot_id'] = $slot['id'];
                    $ground['slot_price'] = $slot['slot_price'];
                    $ground['slot_duration'] = ($duration/60).' mins';
                    $ground['slots'] = $array_of_slots;
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Ground details',
                            'success' => $ground
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No ground found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }

    public function  groundNewDetails(Request $request){ 

        if (isset($request['ground_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $ground = Grounds::where('id', $request['ground_id'])->where('is_deleted',0)->first();
                if ($ground) {

                  $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );

                  $currenttime =  strtotime($date->format('H:i:s'));

                  $currentdate =  $date->format('d-m-Y');

                  $date = $date->format('d-m-Y');

                  if ($request->has('date')){
                      $old_date_timestamp = strtotime($request['date']);
                      $old_date = date('d-m-Y', $old_date_timestamp);
                      $date = $old_date;
                  }

                  $day = date("l",strtotime($date));

                  $dayId = config('app.dayIds')[$day];

                    $rangeIds = $ground->Ranges()->where('day_id',$dayId)->pluck('id');
                    
                    $array_of_slots = array ();

                    if(count($rangeIds)){

                      $slots = $ground->Slot()->whereIn('range_id',$rangeIds)->get();

                      if(count($slots)){

                            foreach ($slots as $ks => $slot) {

                                $duration = $slot['slot_duration'];

                                $bookings_all = $slot->Bookings()->where('date',date("Y-m-d", strtotime($date)))->where('slot_id',$slot['id'])->where('payment_status',1)->where('status',1)->get();

                                if ($bookings_all->count() && $bookings_all[0]){
                                    $booking = $bookings_all[0];
                                    $duration = $booking['slot_duration'];
                                }

                                $start_time = strtotime ($slot['start_time']); //change to strtotime
                                $end_time = strtotime ($slot['end_time']);//change to strtotime

                                $status = false;

                                if (($start_time > $currenttime && $date == $currentdate) || strtotime($date) > strtotime($currentdate)){

                                    if ($bookings_all->count()){
                                        foreach($bookings_all as $booking){
                                            $booking_startTime = strtotime($booking['start_time']);
                                            $booking_endTime = strtotime($booking['end_time']);
                                            if(date("g:i a", $start_time) >= date("g:i a", $booking_startTime) && date("g:i a", $end_time) <= date("g:i a", $booking_endTime)){
                                                $status= true;
                                                break;
                                            }else
                                                $status= false;
                                        }
                                    }

                                    $array_of_slots[]=
                                        array(
                                            'id' =>  $slot['id'],
                                            'date' =>  $date,
                                            'duration' =>  $slot['slot_duration'],
                                            'start_time' =>  $time = date("g:i a", $start_time),
                                            'end_time' =>  $time = date("g:i a", $end_time),
                                            'price' => $slot['slot_price'],
                                            'total_players' =>  $slot['Range']['total_players'],
                                            'is_booked' => $status
                                        );
                                }

                        }

                      }

                    }

                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $date = $date->format('d-m-Y');

                    if ($request->has('date')){
                        $old_date_timestamp = strtotime($request['date']);
                        $old_date = date('d-m-Y', $old_date_timestamp);
                        $date = $old_date;
                    }

                    $ground['date'] = $date;
                    $ground['slots'] = $array_of_slots;

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Ground details',
                            'success' => $ground
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No ground found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }

    public function dayRange(Request $request,$groundId){

        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();

        if ($user) {

          $ground = Grounds::whereId($groundId)->where('is_deleted',0)->first();

          if ($ground) {

            $dayList = [];

            foreach (config('app.days') as $key => $value){
                $range = Range::where('day_id',$key)->where('ground_id',$groundId)->first();

                if($range){
                  $dayList[] = [
                    'id' => $key,
                    'name' => $value,
                    'data_available' => 1
                  ];
                }else{
                  $dayList[] = [
                    'id' => $key,
                    'name' => $value,
                    'data_available' => 0
                  ];
                }

            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Ground Day Range List',
                    'success' => $dayList
                ]
            );

          }

          return response()->json(
              [
                  'status' => 'error',
                  'message' => 'No ground found'
              ],500
          );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );

    }

    public function dayRangeDetail(Request $request,$groundId,$dayId){

        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();

        if ($user) {

          $ground = Grounds::whereId($groundId)->where('is_deleted',0)->first();

          if ($ground) {

            $range['details'] = Range::where('day_id',$dayId)->where('ground_id', $groundId)->get();

            foreach ($range['details'] as $key => $value) {
              $range['details'][$key]['start_time'] = date('g:i A',strtotime($value['start_time']));
              $range['details'][$key]['end_time'] = date('g:i A',strtotime($value['end_time']));

            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Ground Day Range Details',
                    'success' => $range
                ]
            );

          }

          return response()->json(
              [
                  'status' => 'error',
                  'message' => 'No ground found'
              ],500
          );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );

    }

    public function dayRangeSave(Request $request){

        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();        

        if ($user) {

          $ground = Grounds::whereId($request['ground_id'])->where('is_deleted',0)->first();

          if ($ground) {

            $details = $request['details'];

            if(isset($request['delete']) && $request['delete']){

                $deletedIds = explode(',',$request['delete']);

                if(!empty($deletedIds)){
                  foreach ($deletedIds as $kr => $deletedId) {

                    if($deletedId){
                      $slotIds = Slot::where('range_id',$deletedId)->pluck('id');

                      if(count($slotIds)){
                          $deleteBookings = Bookings::whereIn('slot_id',$slotIds)->delete();
                      }
                      $deleteSlots = Slot::where('range_id',$deletedIds)->delete();
                    }
                  }

                  $deleteRange = Range::whereIn('id',$deletedIds)->delete();

                }

            }

            foreach ($details as $key => $data) {

              if($data['apply_for_rest_days']){

                foreach (config('app.days') as $key => $value){

                    $range = new Range();

                    $rangeData = [];
                    $rangeData['day_id'] = $key;
                    $rangeData['ground_id'] = $request['ground_id'];

                    if(isset($data['apply_for_rest_days'])){
                        $rangeData['apply_for_rest_days'] = $data['apply_for_rest_days'];
                    }

                    if(isset($data['start_time'])){
                        $rangeData['start_time'] = date('H:i:s',strtotime($data['start_time']));
                    }

                    if(isset($data['end_time'])){
                        $rangeData['end_time'] = date('H:i:s',strtotime($data['end_time']));
                    }

                    if(isset($data['duration'])){
                        $rangeData['duration'] = $data['duration'];
                    }

                    if(isset($data['price'])){
                        $rangeData['price'] = $data['price'];
                    }

                    if(isset($data['total_players'])){
                        $rangeData['total_players'] = $data['total_players'];
                    }

                    if(isset($data['id']) && $data['id']){

                      $rangeId = $data['id'];
                      $rangeOld = Range::where('id',$rangeId)->first();

                      if($rangeOld){

                        if((isset($data['start_time']) && ($rangeData['start_time'] != $rangeOld['start_time'])) || (isset($data['end_time']) && ($rangeData['end_time'] != $rangeOld['end_time']))
                         || (isset($data['duration']) && ($rangeData['duration'] != $rangeOld['duration']))){

                               foreach (config('app.days') as $kd => $valueDay){

                                 $rangeOldData = Range::where('ground_id',$request['ground_id'])
                                             ->where('start_time',$rangeOld['start_time'])
                                             ->where('end_time',$rangeOld['end_time'])
                                             ->where('day_id',$kd)
                                             ->first();

                                 $rangeNewData = Range::where('ground_id',$request['ground_id'])
                                             ->where('start_time',$rangeData['start_time'])
                                             ->where('end_time',$rangeData['end_time'])
                                             ->where('day_id',$kd)
                                             ->first();

                                  if($rangeOldData){
                                    $rangeOldData = $rangeOldData->update(['apply_for_rest_days'=>0]);
                                  }

                                  if($rangeNewData){
                                    $rangeNewData = $rangeNewData->update(['apply_for_rest_days'=>0]);
                                  }

                               }

                              $slotIds = Slot::where('range_id',$rangeOld['id'])->pluck('id');

                              if(count($slotIds)){
                                  $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                  $date = $date->format('Y-m-d');
                                  $deleteBookings = Bookings::whereIn('slot_id',$slotIds)->where('date', '>',$date)->delete();
                              }

                              $deleteSlots = Slot::where('range_id',$rangeOld['id'])->delete();

                              $rangeOld->delete();

                              $range = $range->create($rangeData);
                              $rangeId = $range->id;

                        }else{

                          $range = $rangeOld->update($rangeData);
                          $rangeId = $rangeOld->id;

                        }

                      }else{
                        $range = $range->create($rangeData);
                        $rangeId = $range->id;
                      }

                      unset($data['id']);

                    }else{

                      $rangeOld = Range::where('ground_id',$request['ground_id'])
                                  ->where('start_time',$rangeData['start_time'])
                                  ->where('end_time',$rangeData['end_time'])
                                  ->where('day_id',$rangeData['day_id'])
                                  ->first();

                      if($rangeOld){

                        if((isset($data['start_time']) && ($rangeData['start_time'] != $rangeOld['start_time'])) || (isset($data['end_time']) && ($rangeData['end_time'] != $rangeOld['end_time']))
                         || (isset($data['duration']) && ($rangeData['duration'] != $rangeOld['duration']))){

                              $slotIds = Slot::where('range_id',$rangeOld['id'])->pluck('id');

                              if(count($slotIds)){
                                  $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                  $date = $date->format('Y-m-d');
                                  $deleteBookings = Bookings::whereIn('slot_id',$slotIds)->where('date', '>',$date)->delete();
                              }

                              $deleteSlots = Slot::where('range_id',$rangeOld['id'])->delete();
                        }

                        $range = $rangeOld->update($rangeData);
                        $rangeId = $rangeOld->id;
                      }else{
                        $range = $range->create($rangeData);
                        $rangeId = $range->id;
                      }
                    }

                    if($range){

                        $loopTime = intval((strtotime($data['end_time']) - strtotime($data['start_time'])) / (60 * $data['duration']));

                        for($i = 0; $i < $loopTime; $i++){

                          $slot = new Slot();

                          $rangeStartTime = strtotime($data['start_time']);
                          $startTime = date('H:i:s',strtotime(($data['duration'] * $i).' minutes',$rangeStartTime));
                          $rangeEndTime = strtotime($startTime);
                          $endTime = date('H:i:s',strtotime($data['duration'].' minutes',$rangeEndTime));

                          $slot['slot_price'] = $data['price'];
                          $slot['start_time'] = $startTime;
                          $slot['end_time'] = $endTime;
                          $slot['ground_id'] = $request['ground_id'];
                          $slot['slot_duration'] = $data['duration'];
                          $slot['range_id'] = $rangeId;
                          $slot->save();

                        }
                    }

                }

              }else{
                  

                    $range = new Range();

                    $rangeData = [];
                    $rangeData['day_id'] = $data['day_id'];
                    $rangeData['ground_id'] = $request['ground_id'];

                    $rangeData['apply_for_rest_days'] = $data['apply_for_rest_days'];

                    if(isset($data['start_time'])){
                        $rangeData['start_time'] = date('H:i:s',strtotime($data['start_time']));
                    }

                    if(isset($data['end_time'])){
                        $rangeData['end_time'] = date('H:i:s',strtotime($data['end_time']));
                    }

                    if(isset($data['duration'])){
                        $rangeData['duration'] = $data['duration'];
                    }

                    if(isset($data['price'])){
                        $rangeData['price'] = $data['price'];
                    }

                    if(isset($data['total_players'])){
                        $rangeData['total_players'] = $data['total_players'];
                    }
                    
                    if(isset($data['id']) && $data['id']){
                      $rangeId = $data['id'];

                      $rangeOld = Range::where('id',$rangeId)->first();
                    
                          if($rangeOld){
                            
                            if((isset($data['start_time']) && ($rangeData['start_time'] != $rangeOld['start_time'])) || (isset($data['end_time']) && ($rangeData['end_time'] != $rangeOld['end_time']))
                             || (isset($data['duration']) && ($rangeData['duration'] != $rangeOld['duration'])) || (isset($data['total_players']) && ($rangeData['total_players'] != $rangeOld['total_players']))
                             || (isset($data['price']) && ($rangeData['price'] != $rangeOld['price']))){

                               foreach (config('app.days') as $kd => $valueDay){

                                 $rangeOldData = Range::where('ground_id',$request['ground_id'])
                                             ->where('start_time',$rangeOld['start_time'])
                                             ->where('end_time',$rangeOld['end_time'])
                                             ->where('day_id',$kd)
                                             ->first();

                                 $rangeNewData = Range::where('ground_id',$request['ground_id'])
                                             ->where('start_time',$rangeData['start_time'])
                                             ->where('end_time',$rangeData['end_time'])
                                             ->where('day_id',$kd)
                                             ->first();

                                  if($rangeOldData){
                                    $rangeOldData = $rangeOldData->update(['apply_for_rest_days'=>0]);
                                  }

                                  if($rangeNewData){
                                    $rangeNewData = $rangeNewData->update(['apply_for_rest_days'=>0]);
                                  }

                               }
                               
                                $slotIds = Slot::where('range_id',$rangeOld['id'])->pluck('id');

                                if(count($slotIds)){
                                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                    $date = $date->format('Y-m-d');
                                    $deleteBookings = Bookings::whereIn('slot_id',$slotIds)->where('date', '>',$date)->delete();
                                }

                                $deleteSlots = Slot::where('range_id',$rangeOld['id'])->delete();

                                $rangeOld->delete();

                                $range = $range->create($rangeData);
                                $rangeId = $range->id;

                                if($range){
                        
                        
                                    $loopTime = intval((strtotime($data['end_time']) - strtotime($data['start_time'])) / (60 * $data['duration']));
            
                                    for($i = 0; $i < $loopTime; $i++){
            
                                      $slot = new Slot();
            
                                      $rangeStartTime = strtotime($data['start_time']);
                                      $startTime = date('H:i:s',strtotime(($data['duration'] * $i).' minutes',$rangeStartTime));
                                      $rangeEndTime = strtotime($startTime);
                                      $endTime = date('H:i:s',strtotime($data['duration'].' minutes',$rangeEndTime));
                                        
                                      $slot['slot_price'] = $data['price'];
                                      $slot['start_time'] = $startTime;
                                      $slot['end_time'] = $endTime;
                                      $slot['ground_id'] = $request['ground_id'];
                                      $slot['slot_duration'] = $data['duration'];
                                      $slot['range_id'] = $rangeId;
                                    //   dd($slot);
                                      if($rangeId != null){
                                        $slot->save();
                                      }                         
            
                                    }
                                }

                            }
                        //   }else{

                        //     $range = $rangeOld->update($rangeData);
                            
                        //     // $rangeId = $range->id;

                        //   }

                      }else{
                        $range = $range->create($rangeData);
                        $rangeId = $range->id;
                      }

                      unset($data['id']);

                    }else{
                      $range = $range->create($rangeData);
                      $rangeId = $range->id;
                      if($range){
                        
                        
                        $loopTime = intval((strtotime($data['end_time']) - strtotime($data['start_time'])) / (60 * $data['duration']));

                        for($i = 0; $i < $loopTime; $i++){

                          $slot = new Slot();

                          $rangeStartTime = strtotime($data['start_time']);
                          $startTime = date('H:i:s',strtotime(($data['duration'] * $i).' minutes',$rangeStartTime));
                          $rangeEndTime = strtotime($startTime);
                          $endTime = date('H:i:s',strtotime($data['duration'].' minutes',$rangeEndTime));
                            
                          $slot['slot_price'] = $data['price'];
                          $slot['start_time'] = $startTime;
                          $slot['end_time'] = $endTime;
                          $slot['ground_id'] = $request['ground_id'];
                          $slot['slot_duration'] = $data['duration'];
                          $slot['range_id'] = $rangeId;
                        //   dd($slot);
                          if($rangeId != null){
                            $slot->save();
                          }                         

                        }
                    }
                    }
                    
                    // if($range){
                        
                        
                    //     $loopTime = intval((strtotime($data['end_time']) - strtotime($data['start_time'])) / (60 * $data['duration']));

                    //     for($i = 0; $i < $loopTime; $i++){

                    //       $slot = new Slot();

                    //       $rangeStartTime = strtotime($data['start_time']);
                    //       $startTime = date('H:i:s',strtotime(($data['duration'] * $i).' minutes',$rangeStartTime));
                    //       $rangeEndTime = strtotime($startTime);
                    //       $endTime = date('H:i:s',strtotime($data['duration'].' minutes',$rangeEndTime));
                            
                    //       $slot['slot_price'] = $data['price'];
                    //       $slot['start_time'] = $startTime;
                    //       $slot['end_time'] = $endTime;
                    //       $slot['ground_id'] = $request['ground_id'];
                    //       $slot['slot_duration'] = $data['duration'];
                    //       $slot['range_id'] = $rangeId;
                    //     //   dd($slot);
                    //       if($rangeId != null){
                    //         $slot->save();
                    //       }                         

                    //     }
                    // }
                }
            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Ground Day Range saved',
                    'success' => 'Ground Day Range saved'
                ]
            );

          }

          return response()->json(
              [
                  'status' => 'error',
                  'message' => 'No ground found'
              ],500
          );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );

    }

}
