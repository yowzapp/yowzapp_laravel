<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\ActivityLog;
use App\Models\Comment;
use App\Models\Games;
use App\Models\Levels;
use App\Models\Teams;
use App\Models\Users;
use App\User;
use Illuminate\Http\Request;

class ActivityController extends Controller
{


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Activities listing API
     */

    public function  activitiesList(Request $request)
    {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user){
            $user_followers = $user->Followers()->wherePivot('type', 1)->wherePivot('is_requested',0)->pluck('id')->toArray();
            $user_following = $user->Following()->wherePivot('type', 1)->wherePivot('is_requested',0)->pluck('id')->toArray();
            $id_listing = array_merge ($user_followers,$user_following,[$user->id]);
            $id_listing = array_unique($id_listing);
            $activities_listing = ActivityLog::whereIn('user_id',array_values($id_listing))->orderBy('updated_at','DESC')->paginate(10);
            $activities_list_final = array();
            foreach($activities_listing as $item){

                $new_item = array();
                $is_team = false;
                $is_game = false;
                $is_points = false;
                $is_social = false;
                $new_item['content'] = $item['content'];
                $like_count = $item->Likes()->count();
                $new_item['like_count'] = $like_count;
                $comment_count = $item->Comments()->count();
                $new_item['comment_count'] = $comment_count;
                $new_item['date'] = getDateString($item['date']);

                $user_liked = $item->Likes()->wherePivot('user_id',$user->id)->first();
                if ($user_liked)
                    $is_liked = true;
                else
                    $is_liked = false;

                $new_item['is_liked'] = $is_liked;
                $new_item['id'] = $item->id;
                $new_item['title'] = 'Activity Log';
                switch ($item['type'])
                {
                    case 1:{
                        $new_item['type'] = 'team';
                        $new_item['title'] = 'New Team Created';
                        $is_team = true;
                        $team = Teams::whereId($item->type_id)->first();
                        if(!$team)
                            break;
                        $new_item['type_id'] = $team->id;
                        $new_item['name'] = $team->name;
                        $new_item['cover_pic'] = $team['cover_pic'];
                        $new_item['no_players'] = $team->Players()->wherePivot('team_id',$team->id)->wherePivot('invitation_status',1)->count();
                        $new_item['is_team'] = $is_team;
                        $new_item['is_game'] = $is_game;
                        $new_item['is_points'] = $is_points;
                        $new_item['is_social'] = $is_social;
                        $comments = $item->Comments()->with(['User' => function($query) {
                            $query->select('id','profile_pic');
                        }])->paginate(10);
                        $new_item['hasMoreComments'] = $comments->hasMorePages();
                        $new_item['comments'] =$comments;
                        $activities_list_final[] = $new_item;
                    }
                        break;
                    case 2:{
                        $new_item['type'] = 'game';
                        $new_item['title'] = 'New Game Created';
                        $is_game = true;
                        $game = Games::whereId($item->type_id)->first();
                        if(!$game)
                            break;
                        $new_item['type_id'] = $game->id;
                        $new_item['name'] = $game->name;
                        $new_item['cover_pic'] = $game['cover_pic'];
                        $new_item['no_players'] = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('invitation_status', 1)->count();
                        $new_item['is_team'] = $is_team;
                        $new_item['is_game'] = $is_game;
                        $new_item['is_points'] = $is_points;
                        $new_item['is_social'] = $is_social;
                        $comments = $item->Comments()->with(['User' => function($query) {
                            $query->select('id','profile_pic');
                        }])->paginate(10);
                        $new_item['hasMoreComments'] = $comments->hasMorePages();
//                $comments['data'] = $comments['']
                        $new_item['comments'] =$comments;
                        $activities_list_final[] = $new_item;
                    }
                        break;
                    case 3:{
                        $new_item['type'] = 'level_up';
                        $new_item['title'] = 'Level Up';
                        $is_points = true;
                        $level = Levels::whereId($item->type_id)->first();
                        $level_user = Users::whereId($item->user_id)->first();
                        if(!$level_user || !$level)
                            break;
                        $new_item['type_id'] = $level_user->id;
                        $new_item['name'] = $level->level_name;
                        $new_item['cover_pic'] = $level_user['profile_pic'];
                        $new_item['is_team'] = $is_team;
                        $new_item['is_game'] = $is_game;
                        $new_item['is_points'] = $is_points;
                        $new_item['is_social'] = $is_social;
                        $comments = $item->Comments()->with(['User' => function($query) {
                            $query->select('id','profile_pic');
                        }])->paginate(10);
                        $new_item['hasMoreComments'] = $comments->hasMorePages();
//                $comments['data'] = $comments['']
                        $new_item['comments'] =$comments;
                        $activities_list_final[] = $new_item;

                    }
                        break;

                    case 4:{
                        $new_item['type'] = 'social_connect';
                        $new_item['title'] = 'Social Connect';
                        $is_social = true;
                        $social_user = Users::whereId($item->user_id)->first();
                        if(!$social_user)
                            break;
                        $new_item['type_id'] = $social_user->id;
                        $new_item['name'] = $social_user->level_name;
                        $new_item['cover_pic'] = $social_user['profile_pic'];
                        $new_item['is_team'] = $is_team;
                        $new_item['is_game'] = $is_game;
                        $new_item['is_points'] = $is_points;
                        $new_item['is_social'] = $is_social;
                        $comments = $item->Comments()->with(['User' => function($query) {
                            $query->select('id','profile_pic');
                        }])->paginate(10);
                        $new_item['hasMoreComments'] = $comments->hasMorePages();
//                $comments['data'] = $comments['']
                        $new_item['comments'] =$comments;
                        $activities_list_final[] = $new_item;

                    }
                        break;
                }

            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Activity List',
                    'success' => $activities_list_final,
                    'currentPage' => $activities_listing->currentPage(),
                    'hasMorePages' => $activities_listing->hasMorePages()
                ]
            );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not found'
            ], 500
        );

    }



    /**
     * @param Request $request
     * @param $action
     * @return \Illuminate\Http\JsonResponse
     * Like or Unlike or Commnet Activity
     */

    public function  likeUnlikeComment(Request $request,$action){

        if (isset($request['id'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();

            if ($user) {

                $activity_log = ActivityLog::whereId($request['id'])->first();
                if (!$activity_log)
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Activity not found'
                        ],500
                    );
                if ($action == 'like' ){
                    $user_liked = $activity_log->Likes()->wherePivot('user_id',$user->id)->first();
                    if (!$user_liked)
                        $activity_log->Likes()->attach($user->id,['activity_id'=>$activity_log->id]);

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Activity liked successfully',
                            'success' => $activity_log,
                        ]
                    );
                }else if ($action == 'unlike' ){
                    $user_liked = $activity_log->Likes()->wherePivot('user_id',$user->id)->first();
                    if ($user_liked)
                        $activity_log->Likes()->detach(['activity_id'=>$activity_log->id,'user_id' => $user->id]);

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Activity disliked successfully',
                            'success' => $activity_log,
                        ]
                    );
                }else if ($action == 'comment' ) {
                    if (isset($request['comment'])) {
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                        $currenttime =  $date->format('Y-m-d H:i:s');
                        $comment = new Comment();
                        $comment['content'] = $request['comment'];
                        $comment['user_id'] = $user->id;
                        $comment['activity_id'] = $activity_log->id;
                        $comment['dateTime'] = $currenttime;
                        $comment->save();
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'message' => 'Activity commented successfully',
                                'success' => $activity_log,
                            ]
                        );
                    }
                }else if ($action == 'getComments'){

                    $comments = $activity_log->Comments()->with(['User' => function($query) {
                        $query->select('id','profile_pic');
                    }])->orderBy('updated_at','desc')->paginate(10);
                    $comments_final_array = array();

                    foreach($comments as $comment) {

                        $comments_final_array[] =  array(
                            'id' => $comment["id"],
                            'user_id' => $comment["user_id"],
                            'activity_id' =>$comment["activity_id"],
                            'content' =>$comment["content"],
                            'dateTime' =>getDateString($comment['dateTime']),
                            'user' => $comment["user"],
                        );
                    }
                    $comments_final_array =['data'=>$comments_final_array] ;

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Comment List',
                            'success' =>  $comments_final_array,
                            'currentPage' => $comments->currentPage(),
                            'hasMorePages' => $comments->hasMorePages()
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'JSON schema failed'
                    ],500
                );

            }return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500


        );


    }




}
