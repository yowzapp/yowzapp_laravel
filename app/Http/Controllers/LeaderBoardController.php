<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\Levels;
use App\Models\Points;
use App\Models\Teams;
use App\Models\Users;
use Illuminate\Http\Request;

class LeaderBoardController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     *  Leaderboard
     */
    public function  leaderBoardListing(Request $request)
    {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();

        // return $user;


        if ($user) {
            // return $user;

            if ($request->has('all') && $request->has('sport_id')) {
                if ($request['sport_id'] != 'all') {

                    $id_list  = Users::where('role_id', 3)->whereHas('Interests', function ($query) use ($request) {
                        $query->where('id', $request['sport_id']);
                    })->orderBy('points', 'desc')
                        ->orderBy('created_at', 'asc')
                        ->pluck('id')->toArray();

                    $players = Users::where('role_id', 3)->whereHas('Interests', function ($query) use ($request) {
                        $query->where('id', $request['sport_id']);
                    })->orderBy('points', 'desc')
                        ->orderBy('created_at', 'asc')
                        ->paginate(10);
                } else {

                    $id_list  = Users::where('role_id', 3)->orderBy('points', 'desc')->orderBy('created_at', 'asc')->pluck('id')->toArray();
                    $players = Users::where('role_id', 3)->orderBy('points', 'desc')->orderBy('created_at', 'asc')->paginate(10);
                }
            }else if ($request->has('followers') && $request->has('sport_id')) {

                $followers  = $user->Followers()->wherePivot('is_requested',0)->wherePivot('type', 1)->where('role_id',3)->pluck('id')->toArray();
                if ($request['sport_id'] != 'all')  {


                    $id_list  = Users::whereIn('id',$followers)->where('role_id', 3)->whereHas('Interests', function ($query) use ($request) {
                        $query->where('id', $request['sport_id']);
                    })->orderBy('points', 'desc')
                        ->orderBy('created_at', 'asc')
                        ->pluck('id')->toArray();

                    $players = Users::whereIn('id',$followers)->where('role_id', 3)->whereHas('Interests', function ($query) use ($request) {
                        $query->where('id', $request['sport_id']);
                    })->orderBy('points', 'desc')
                        ->orderBy('created_at', 'asc')
                        ->paginate(10);


                } else {

                    $id_list  = Users::whereIn('id',$followers)->where('role_id', 3)->orderBy('points', 'desc')->orderBy('created_at', 'asc')->pluck('id')->toArray();
                    $players = Users::whereIn('id',$followers)->where('role_id', 3)->orderBy('points', 'desc')->orderBy('created_at', 'asc')->paginate(10);

                }
            }else if ($request->has('followings') && $request->has('sport_id')) {
                $followings  = $user->Following()->wherePivot('is_requested',0)->wherePivot('type', 1)->where('role_id',3)->pluck('id')->toArray();
                // array_push($user->id, $followings);
                $followings[] = $user->id;
                
                if ($request['sport_id'] != 'all')  { 

                    $id_list = Users::whereIn('id',$followings)->where('role_id', 3)->whereHas('Interests', function ($query) use ($request) {
                        $query->where('id', $request['sport_id']);
                    })->orderBy('points', 'desc')
                        ->orderBy('created_at', 'asc')
                        ->pluck('id')->toArray();

                    $players = Users::whereIn('id',$followings)->where('role_id', 3)->whereHas('Interests', function ($query) use ($request) {
                        $query->where('id', $request['sport_id']);
                    })->orderBy('points', 'desc')
                        ->orderBy('created_at', 'asc')
                        ->paginate(10);

                } else {

                    $id_list  = Users::whereIn('id',$followings)->where('role_id', 3)->orderBy('points', 'desc')->orderBy('created_at', 'asc')->pluck('id')->toArray();
                    $players = Users::whereIn('id',$followings)->where('role_id', 3)->orderBy('points', 'desc')->orderBy('created_at', 'asc')->paginate(10);

                }
            }else if ($request->has('teams') && $request->has('sport_id')) {
                if ($request['sport_id'] != 'all')  {
                    $teams = Teams::whereHas('Sports', function ($query) use ($request) {
                        $query->where('id', $request['sport_id']);
                    })->orderBy('points', 'desc')->paginate(10);
                } else {
                    $teams = Teams::orderBy('points', 'desc')->paginate(10);
                }
                $teams_final_array = array();
                $curentPage = $teams->currentPage();
                $rank = ($curentPage - 1)*10+1;


                foreach ($teams as $value) {
                    $level = Levels::where('points_from','<=',$value["points"])->where('points_to','>=',$value["points"])->first();
                    if ($level){
                        $value['level_id'] = $level->id;
                        $value['level_name'] = $level['level_name'];
                        $value['badge_icon'] = $level['badge_icon'];
                    }else{
                        $value['level_name'] = 'Novice';
                        $value['level_id'] = 1;
                        $value["badge_icon"] = 'https://s3-ap-southeast-1.amazonaws.com/playthoraapp/badges/Playthora_UI_Novice.png';
                    }

                    $teams_final_array[] = array(
                        'level_id' => $value["level_id"],
                        'name' => $value["name"],
                        'points' => $value["points"],
                        'badge' => $value['badge_icon'],
                        'rank' => $rank,
                        'team_id' => $value["id"]
                    );
                    $rank = $rank + 1;
                }
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'success' => $teams_final_array,
                        'currentPage' => $teams->currentPage(),
                        'hasMorePages' => $teams->hasMorePages()
                    ]
                );
            }else{

                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Json schema failed'
                    ],500
                );
            }

            $mine = array();
            if (!in_array($user->id,$players->pluck('id')->toArray()) && in_array($user->id,$id_list) ){
                $user_index = array_search($user->id, $id_list);
                $my_rank = $user_index + 1;
                if ($user->level_id < 1)
                    $value['level_id'] = 1;

                $level = $user->Level()->first();
                if ($level)
                    $badge = $level["badge_icon"];
                else
                    $badge = 'https://s3-ap-southeast-1.amazonaws.com/playthoraapp/badges/Playthora_UI_Novice.png';

                $mine[] = array(
                    'level_id' => $user["level_id"],
                    'name' => $user["name"],
                    'badge' => $badge,
                    'points' => $user["points"],
                    'rank' => $my_rank,
                    'user_id' => $user["id"]
                );
            }
         
            $player_final_array = array();
            $curentPage = $players->currentPage();
            $rank = ($curentPage - 1)*10+1;

            foreach ($players as $value) {
                if ($value->level_id < 1)
                    $value['level_id'] = 1;
                $level = $value->Level()->first();
                if ($level)
                    $badge = $level["badge_icon"];
                else
                    $badge = 'https://s3-ap-southeast-1.amazonaws.com/playthoraapp/badges/Playthora_UI_Novice.png';

                $player_final_array[] = array(
                    'level_id' => $value["level_id"],
                    'name' => $value["name"],
                    'badge' => $badge,
                    'points' => $value["points"],
                    'rank' => $rank,
                    'user_id' => $value["id"]
                );
                $rank = $rank + 1;
            }
            
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $player_final_array,
                    'mine' => $mine,
                    'currentPage' => $players->currentPage(),
                    'hasMorePages' => $players->hasMorePages()
                ]
            );

        }


        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
    }




    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     *  Badges
     */
    public function  myBadges(Request $request)
    {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        if ($accesstoken)
            $user = $accesstoken->User()->first();
        else
            $user = array();
        if ($request->has('user_id')){
            $user = Users::whereId($request['user_id'])->where('role_id',3)->first();
        }
        if ($user){
            $badges = Levels::where('id','<=',$user['level_id'])->orderBy('id','asc')->get();
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $badges,
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not found'
            ], 500
        );

    }
    
    



}
