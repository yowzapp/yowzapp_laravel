<?php

namespace App\Http\Controllers;

use App\Models\Locations;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    public function  locationList(){

        $location_list = Locations::orderBy('name','asc')->get();

        if(!empty($location_list)){
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Locations List',
                    'success' => $location_list,
                ]
            );
        }
    }
}


