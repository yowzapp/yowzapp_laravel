<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\ActivityLog;
use App\Models\DeviceInfo;
use App\Models\Games;
use App\Models\Levels;
use App\Models\Notification;
use App\Models\Points;
use App\Models\Search;
use App\Models\SearchRecent;
use App\Models\Settings;
use App\Models\Sports;
use App\Models\SportsHasUsers;
use App\Models\Teams;
use App\Models\Users;
use App\User;
use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Log;

class PlayerController extends Controller
{

    /**
     * player email register api
     */

    public function emailRegister(Request $request)
    {
        if (isset($request['email'], $request['name'], $request['password'],$request['mobile'],$request['device_id'],$request['device_type'])) {

            $user = Users::whereEmail($request['email'])->first();
            $phone = $request['mobile'];
            if (strlen($phone) >= 9){
                $phone = "971".substr($phone, -9);
              }
  
              // dd($phone);
  
              $existMobileUser = Users::where('mobile',$phone)->first();
  
              if($existMobileUser)
                  return response()->json(
                      [
                          'status' => 'error',
                          'message' => 'Mobile No. already exist',
                      ],500
                  );

            if($user) {
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'User already exits, Please try logging in'
                    ],500
                );
            }

            
            $random_number = mt_rand(100000, 999999);
            $message = 'Your OTP ' . $random_number . ' to verify registration';

            $user = new Users();
            $level = Levels::where('id',1)->first();
            $user['email'] = $request['email'];
            $user['password'] = bcrypt($request['password']);
            $user['name'] = $request['name'];
            // role set to player
            $user['role_id'] = 3;
            $user['otp'] = $random_number;
            $user['mobile']  =  $phone;
            $user['mobile_verified'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            $user['is_private'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            $user['level_id'] = $level->id;
            $user['cover_pic'] = $level['badge_cover'];
            $user->save();

            $search = new Search();
            $search['id'] = $user->id;
            $search['name'] = $user->name;
            $search['type'] = 1;
            $search->save();


            $settings = new Settings();
            $settings['user_id'] = $user->id;
            $settings->save();

            //delete device related to old users
            $old_device = DeviceInfo::where('device_id',$request['device_id'])->get();
            foreach ($old_device as $item) {
                if ($item)
                    $item->delete();
            }
            //create device info
            $deviceinfos = new DeviceInfo();
            $deviceinfos->device_type=  $request['device_type'];
            $deviceinfos->device_id= $request['device_id'];
            if(isset($request['fcm_token'])){
                $deviceinfos->fcm_token = $request['fcm_token'];
            }
            $deviceinfos->user_id = $user->id;
            $deviceinfos->save();

            //create access token
            $accesstoken= new AccessToken();
            $accesstoken->access_token=  bin2hex(openssl_random_pseudo_bytes(16));
            $accesstoken->user_id=$user->id;
            $accesstoken->ttl = '259200';
            $accesstoken->save();

            $user->mobile_verified = filter_var($user['mobile_verified'], FILTER_VALIDATE_BOOLEAN);
            if ($user['profile_pic'])
                $user['profile_pic_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
            else
                $user['profile_pic_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            $interest = SportsHasUsers::where('users_id',$user->id)->get();
            if (sizeof($interest)>0)
                $user['interest_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
            else
                $user['interest_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            if ($user['lat']&&$user['location_name']&&$user['lng'])
                $user['location_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
            else
                $user['location_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            unset($user['password']);
            unset($user['remember_token']);
            unset($user['role_id']);
            unset($user['social_token']);
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' =>  'Hey, '.$user->name.' Welcome to Yowzapp!!',
                    'success' => $user,
                    'accessToken' => $accesstoken['access_token']
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );

    }


    /**
     * player scial register/login api
     */

    public function socialAuth(Request $request)
    {
        if (isset($request['email'], $request['name'], $request['device_id'], $request['device_type'], $request['fcm_token'], $request['token'])) {
            $user = Users::whereEmail($request['email'])->first();
            if ($user) {
                if ($user['name'] == ''){
                    $user['name'] = $request['name'];
                }

                $user['social_token'] = $request['token'];
                $user['role_id'] = 3;
                if ($request->has('profile_pic') && $user['profile_pic'] == ''){
                    if ($request['profile_pic'] != '' || $request['profile_pic'] != 'null')
                        $user['profile_pic'] = $request['profile_pic'];
                }



                if ($request->has('bio')){
                    if($user['bio'] == '')
                        $user['bio'] = $request['bio'];
                }

                $user->update();
                $search = Search::where('id', $user->id)->where('type', 1)->first();
                if ($search){
                    $search->delete();
                }
                    $search = new Search();
                    $search['id'] = $user->id;
                    $search['name'] = $user->name;
                    $search['type'] = 1;
                    $search->save();



                //Points info
                $points = Points::where('type_id',$user->id)->where('type',2)->first();
                if (!$points)
                    $points= new Points();
                $points['type'] = 2;
                $points['type_id'] = $user->id;
                $points['point'] = 4;
                $points['content'] = 'Connected social account';
                $points->save();

                // acticity log info
                $activity_log = ActivityLog::where('type_id',$user->id)->where('user_id',$user->id)->where('type',4)->first();
                if (!$activity_log)
                    $activity_log = new ActivityLog();
                $activity_log['user_id'] = $user->id;
                $activity_log['type_id'] = $user->id;
                $activity_log['type'] = 4;
                $activity_log['content'] = $user->name.' is connected';
                $act_date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                $act_currenttime_notification_time =  $act_date->format('Y-m-d H:i:s');
                $activity_log['date'] = $act_currenttime_notification_time;
                $activity_log->save();


                //delete device related to old users
                $old_device = DeviceInfo::where('device_id',$request['device_id'])->get();
                foreach ($old_device as $item) {
                    if ($item)
                        $item->delete();
                }

                //create device info
                $deviceinfos = new DeviceInfo();
                $deviceinfos->device_type = $request['device_type'];
                $deviceinfos->device_id = $request['device_id'];
                $deviceinfos->fcm_token = $request['fcm_token'];
                $deviceinfos->user_id = $user->id;
                $deviceinfos->save();

                //create access token
                $accesstoken = new AccessToken();
                $accesstoken->access_token = bin2hex(openssl_random_pseudo_bytes(16));
                $accesstoken->user_id = $user->id;
                $accesstoken->ttl = '7200';
                $accesstoken->save();
                $user->mobile_verified = filter_var($user['mobile_verified'], FILTER_VALIDATE_BOOLEAN);
                if ($user->role_id == 3) {
                    if ($user['profile_pic'])
                        $user['profile_pic_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                    else
                        $user['profile_pic_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    $interest = SportsHasUsers::where('users_id', $user->id)->get();
                    if (sizeof($interest) > 0)
                        $user['interest_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                    else
                        $user['interest_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    if ($user['lat'] && $user['location_name'] && $user['lng'])
                        $user['location_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                    else
                        $user['location_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                }
                unset($user['password']);
                unset($user['remember_token']);
                unset($user['role_id']);
                unset($user['social_token']);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Hey, '.$user->name.' Welcome back!!',
                        'success' => $user,
                        'accessToken' => $accesstoken['access_token']
                    ]
                );
            }
            $user = new User();
            $user['name'] = $request['name'];
            $user['email'] = $request['email'];
            $user['mobile_verified'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            // role set to player
            $user['is_private'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);;
            $user['is_social'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);;
            $user['social_token'] = $request['token'];
            $user['role_id'] = 3;
            if ($request['profile_pic'] != '' || $request['profile_pic'] != 'null')
                $user['profile_pic'] = $request['profile_pic'];
            else
                $user['profile_pic'] = 'http://www.hbc333.com/data/out/190/47199326-profile-pictures.png';

            if ($request->has('bio'))
                $user['bio'] = $request['bio'];

            $level = Levels::where('id',1)->first();
            $user['level_id'] = $level->id;
            $user['cover_pic'] = $level['badge_cover'];
            $user['mobile'] = '';
            $user['points'] = 4;
            $user->save();

            $points= new Points();
            $points['type'] = 2;
            $points['type_id'] = $user->id;
            $points['point'] = 4;
            $points['content'] = 'Connected social account';
            $points->save();


            $activity_log = new ActivityLog();
            $activity_log['user_id'] = $user->id;
            $activity_log['type_id'] = $user->id;
            $activity_log['type'] = 4;
            $activity_log['content'] = $user->name.' is connected';
            $act_date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
            $act_currenttime_notification_time =  $act_date->format('Y-m-d H:i:s');
            $activity_log['date'] = $act_currenttime_notification_time;
            $activity_log->save();

            $search = new Search();
            $search['id'] = $user->id;
            $search['name'] = $user->name;
            $search['type'] = 1;
            $search->save();

            $settings = new Settings();
            $settings['user_id'] = $user->id;
            $settings->save();

            //delete device related to old users
            $old_device = DeviceInfo::where('device_id',$request['device_id'])->get();
            foreach ($old_device as $item) {
                if ($item)
                    $item->delete();
            }
            //create device info
            $deviceinfos = new DeviceInfo();
            $deviceinfos->device_type = $request['device_type'];
            $deviceinfos->device_id = $request['device_id'];
            $deviceinfos->fcm_token = $request['fcm_token'];
            $deviceinfos->user_id = $user->id;
            $deviceinfos->save();

            //create access token
            $accesstoken = new AccessToken();
            $accesstoken->access_token = bin2hex(openssl_random_pseudo_bytes(16));
            $accesstoken->user_id = $user->id;
            $accesstoken->ttl = '259200';
            $accesstoken->save();

            $user->mobile_verified = filter_var($user['mobile_verified'], FILTER_VALIDATE_BOOLEAN);
            if ($user['profile_pic'])
                $user['profile_pic_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
            else
                $user['profile_pic_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            $interest = SportsHasUsers::where('users_id', $user->id)->get();
            if (sizeof($interest) > 0)
                $user['interest_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
            else
                $user['interest_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            if ($user['lat'] && $user['location_name'] && $user['lng'])
                $user['location_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
            else
                $user['location_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
            unset($user['password']);
            unset($user['remember_token']);
            unset($user['role_id']);
            unset($user['social_token']);
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Hey, '.$user->name.' Welcome to Designstring!!',
                    'success' => $user,
                    'accessToken' => $accesstoken['access_token']
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ], 500
        );

    }


    /**
     * get user profile
     */

    public function playerDetails(Request $request)
    {
        if ($request->has('user_id') || $request->hasHeader('accessToken')) {
            $player = array();

            $user = array();
            if ($request->hasHeader("accessToken") && $request->header('accessToken') != '') {
                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                if ($accesstoken)
                    $user = Users::whereId($accesstoken->user_id)->where('role_id', 3)->first();
                else
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'User not exits',
                        ], 500
                    );
            if ($user){
                $player = $user;
                $points = $player->Points()->where('type',1)->pluck('point')->toArray();
                $points = array_sum($points);
                $level = Levels::where('points_from','<=',$points)->where('points_to','>=',$points)->first();
                if ($level){
                    $player['level_id'] = $level->id;
                    $player['cover_pic'] = $level['badge_cover'];
                    $player->update();
                    $player['level_name'] = $level['level_name'];
                }else{
                    $player['level_name'] = '';
                    $player['level_id'] = 1;
                }
                $player['is_mine'] = true;
            }
            }

            if ($request->has('user_id')){
                // $player = Users::whereId($request['user_id'])->where('role_id', 3)->first();
                $player = Users::whereId($request['user_id'])->first();
                // dd($player);
                $points = $player->Points()->where('type',1)->pluck('point')->toArray();
                $points = array_sum($points);
                $level = Levels::where('points_from','<=',$points)->where('points_to','>=',$points)->first();

                if ($level){
                    $player['level_id'] = $level->id;
                    $player['cover_pic'] = $level['badge_cover'];
                    $player->update();
                    $player['level_name'] = $level['level_name'];
                }else{
                    $player['level_name'] = '';
                    $player['level_id'] = 1;
                }
                if ($user && $player && ($user->id == $player->id)){
                    $player['is_mine'] = true;
                }else
                    $player['is_mine'] = false;
            }
            if ($player) {
                $player['is_following'] = false;
                $player['is_requested'] = false;
                $player['is_confirm'] = false;
                $player['is_follower'] = false;

                if ($user){
                    $is_following = $user->Following()->wherePivot('type', 1)->where('id', $player->id)->first();
                    if ($is_following){
                        $is_requested = $user->Following()->wherePivot('type', 1)->where('id', $player->id)->where('is_requested', 1)->first();
                        if ($is_requested){
                            $player['is_requested'] = true;
                        }else{
                            $player['is_following'] = true;
                        }
                    }

                    $is_followed = $user->Followers()->wherePivot('followed_by', $player['id'])->where('role_id', 3)->wherePivot('type', 1)->first();

                    if ($is_followed){
                        //$player['is_follower'] = true;
                        $is_requested = $user->Followers()->wherePivot('followed_by', $player['id'])->where('role_id', 3)->wherePivot('type', 1)->wherePivot('is_requested', 1)->first();
                        if ($is_requested){
                            $player['is_following'] = false;
                            $player['is_requested'] = false;
                            $player['is_confirm'] = true;
                        }
                    }

                    if ($request->has('q')){
                        $recent_search = SearchRecent::where('id',$player->id)->where('user_id',$user->id)->where('type',1)->first();
                        if (!$recent_search)
                            $recent_search = new SearchRecent();
                        $recent_search['type'] = 1;
                        $recent_search['id'] = $player->id;
                        $recent_search['user_id'] = $user->id;
                        $recent_search->save();
                    }

                }
                $player->mobile_verified = filter_var($player['mobile_verified'], FILTER_VALIDATE_BOOLEAN);
                if ($player['profile_pic'])
                    $player['profile_pic_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                else
                    $player['profile_pic_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                $interest = $player->Interests()->get();
                if (sizeof($interest) > 0)
                    $player['interest_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                else
                    $player['interest_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                if ($player['lat'] && $player['location_name'] && $player['lng'])
                    $player['location_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                else
                    $player['location_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                $notification_count = $player->Notifications()->where('user_id', $player->id)->where('is_read', 0)->where('viewed',0)->count();
                if ($player['pending_notification'] == 0) {
                    $notification_count = 0;
                }
                $player['notification_count'] = $notification_count;
                $player['followers_count'] = $player->Followers()->where('role_id',3)->wherePivot('is_requested',0)->wherePivot('type', 1)->count();
                $player['following_count'] = $player->Following()->wherePivot('is_requested',0)->wherePivot('type', 1)->where('role_id',3)->count() +  $player->TeamsFollowing()->wherePivot('type', 2)->count();

                $today = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                $current_date =  $today->format('Y-m-d H:i');
                $games_count  = $player->Games()
                    ->where('date','<',$current_date)
                    ->wherePivot('invitation_status',1)
                    ->where('status',1)
                    ->whereHas('Bookings', function($query)  {
                        $query->where('payment_status',1);
                    })->orderBy('date','desc')->get();


                $player['games_count'] = sizeof($games_count);

                $points = $player->Points()->where('type',1)->pluck('point')->toArray();
                $points = array_sum($points);
                $player['points'] = $points;


                $entityBody = stripslashes($player['available_at']);
                $availability = json_decode($entityBody, true);
                if ($availability === NULL){
                    $availability["days"] = array();
                    $availability["available_from"] = "";
                    $availability["available_to"] = "";
                }

                $player['available_at'] = $availability;
                $player['fav_sports'] = $interest;
                $teams = $player->Teams()->wherePivot('invitation_status',1)->orderBy('name','asc')->get();
                if ($player['primary_team']){
                    $primary_team = $player->Teams()->where('id', $player['primary_team'])->first();
                }else{
                    $primary_team = $player->Teams()->wherePivot('is_super_admin', 1)->first();
                }

                $player['top_sports'] = $player->Interests()->limit(3)->get();;
                if (!$primary_team) {
                    $primary_team = $player->Teams()->wherePivot('invitation_status',1)->orderBy('updated_at', 'asc')->first();
                    if (!$primary_team)
                        $player['primary_team'] = array();
                }
                if ($primary_team){
                    $primary_team['members_count'] = $primary_team->Players()->wherePivot('invitation_status', 1)->count();
                    $primary_team['followers_count'] = $primary_team->Followers()->wherePivot('type', 2)->count();
                }
                $player['primary_team'] = $primary_team;
                $player['teams'] = $teams;
                unset($player['password']);
                unset($player['remember_token']);
                unset($player['pending_notification']);
                unset($player['social_token']);
                unset($player['role_id']);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Player details',
                        'success' => $player,
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not exits',
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid JSON schema'
            ],500
        );
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     * my Points
     */
    public function  myPoints(Request $request)
    {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user) {
            $points = $user->Points()->orderBy('updated_at','desc')->get();
            $all_points_final = array();
            foreach ($points as $value) {



                switch ($value['type'])
                {
                    case 1:{
                        $value['type'] = 'game';
                    }
                        break;
                    case 2:{
                        $value['type'] = 'social';
                    }
                        break;
                }
                $value['date'] = date('j M y', strtotime($value['created_at']));

                $all_points_final[] = $value;
            }
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $all_points_final
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
    }


    /**
     * get interest list
     */


    public function  insterestList(){

        $sport_list = Sports::orderBy('name','asc')->get();

        return response()->json(
            [
                'status' => 'success',
                'statusCode' => 200,
                'message' => 'Interest List',
                'success' => $sport_list,
            ]
        );
    }

    /**
     * get players listing
     */

    public function  playerList(Request $request){

        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        if ($accesstoken)
            $user = $accesstoken->User()->first();
        else
            $user = array();
        if ($request->has('user_id')){
            $user = Users::whereId($request['user_id'])->where('role_id',3)->first();

        }
            if ($user) {
                if ($request->has('me') && ($request['me'] == 'followers')){
                    $players = $user->Followers()->wherePivot('type', 1)->wherePivot('is_requested',0)->where('role_id',3)->orderBy('name','asc')->paginate(10);
                }elseif ($request->has('me') && ($request['me'] == 'following')){
                    $players = $user->Following()->wherePivot('type', 1)->wherePivot('is_requested',0)->where('role_id',3)->orderBy('name','asc')->paginate(10);
                }elseif ($request->has('me') && ($request['me'] == 'followingTeams')){
                    $players = $user->TeamsFollowing()->wherePivot('type', 2)->orderBy('name','asc')->paginate(10);
                }elseif ($request->has('me') && ($request['me'] == 'others')){
                    $playersFollowers = $user->Followers()->wherePivot('type', 1)->wherePivot('is_requested',0)->where('role_id',3)->pluck('id')->toArray();
                    $playersFollowing = $user->Following()->wherePivot('type', 1)->wherePivot('is_requested',0)->where('role_id',3)->pluck('id')->toArray();
                    $result = array_unique(array_merge(array($user->id),$playersFollowers,$playersFollowing));
                    $players = Users::whereNotIn('id',$result)->where('role_id',3)->orderBy('name','asc')->paginate(10);
                }else{
                    $players = Users::whereNotIn('id',[$user->id])->where('role_id',3)->orderBy('name','asc')->paginate(10);
                }

                return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Players list',
                            'success' => $players,
                            'currentPage' => $players->currentPage(),
                            'hasMorePages' => $players->hasMorePages()
                        ]
                    );

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );

    }

    /**
     * follow/unfollow API
     */

    public function  followUnfollow(Request $request,$type,$action){

        if (isset($request['id'])) {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
            if ($user) {

            if ($action == 'follow' && $type == 'player'){
                $player = $user->Following()->wherePivot('followed_by', $user['id'])->wherePivot('following_to', $request['id'])->wherePivot('type', 1)->first();
                if (!$player){
                    $following_to = Users::whereId($request['id'])->first();
                    if ($following_to['is_private'] == true || $following_to['is_private'] == 1 || $following_to['is_private'] == 'true') {
                        $user->Following()->attach($user->id,['is_requested' => 1,'following_to'=>$request['id'],'type' => 1]);
                        $device = $following_to->Device()->where('user_id',$following_to->id)->orderBy('id','desc')->first();
                        $notification = Notification::where('user_id',$following_to->id)->where('type','PLY-FR')->where('type_id',$user->id)->first();
                        if ($notification)
                            $notification->delete();

                        $device_token_array = '';

                        if ($device && $following_to['notificaiton'] == 1){
                            $settings = $following_to->Setting()->first();
                            if ($settings['new_follower'] == 1){
                                $device_token_array = $device['fcm_token'];
                            }
                        }

                        $notification = new Notification();
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                        $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                        $notification['date'] = $currenttime_notification_time;
                        $notification['type'] = 'PLY-FR';
                        $notification['user_id'] = $following_to->id;
                        $notification['content'] = $user->name.' has requested to follow you';
                        $notification['type_id'] = $user->id;
                        $notification['status'] = 0;
                        $notification->save();
                        $following_to['pending_notification'] = 1;
                        $following_to->update();

                        if ($device_token_array ) {
                            $data = "Follow requested";
                            $notification_type = 8;
                            $data = array(
                              'title' => 'Follow request',
                              'notification_type' => $notification_type,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,$user->name.' has requested to follow you',$data, $notification_type, $user->id);

                        }

                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Requested to follow'
                            ]
                        );

                    } else {
                        $user->Following()->attach($user->id,['following_to'=>$request['id'],'type' => 1,'is_requested' => 0]);
                        $device = $following_to->Device()->where('user_id',$following_to->id)->orderBy('id','desc')->first();
                        $notification = Notification::where('user_id',$following_to->id)->where('type','PLY-FT')->where('type_id',$user->id)->first();
                        $device_token_array = '';
                        if (!$notification){
                            if ($device  && $following_to['notificaiton'] == 1){
                                $settings = $following_to->Setting()->first();
                                if ($settings['new_follower'] == 1){
                                    $device_token_array = $device['fcm_token'];
                                }
                            }
                            $notification = new Notification();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                            $notification['date'] = $currenttime_notification_time;
                            $notification['type'] = 'PLY-FT';
                            $notification['user_id'] = $following_to->id;
                            $notification['content'] = $user->name.' started following you';
                            $notification['type_id'] = $user->id;
                            $notification['status'] = 0;
                            $notification->save();
                            $following_to['pending_notification'] = 1;
                            $following_to->update();
                        }
                        if ($device_token_array) {
                            $data = "Follow Request";
                            $notification_type = 8;
                            $data = array(
                              'title' => 'Follow Request',
                              'notification_type' => $notification_type,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,$user->name.' started following you',$data, $notification_type, $user->id);
                        }

                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Player followed successfully'
                            ]
                        );
                    }
                }
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'success' => 'Player already followed'
                    ]
                );


            }elseif ($action == 'unfollow' && $type == 'player'){
                $player = $user->Following()->wherePivot('following_to', $request['id'])->wherePivot('type', 1)->first();
                if ($player){
                    $user->Following()->detach(['following_to'=>$request['id'],'type' => 1]);
                    $notification = Notification::where('user_id', $user->id)->where('type', 'PLY-FR')->where('type_id', $player->id)->first();
                    if ($notification) {
                        $notification->delete();
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Player unfollowed successfully'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'success' => 'Player already unfollowed'
                    ]
                );

            }elseif ($action == 'follow' && $type == 'team'){
                $team = Teams::whereId($request['id'])->first();
                if ($team) {
                    $device_token_array = '';
                    $player = $user->TeamsFollowing()->wherePivot('following_to', $request['id'])->wherePivot('type', 2)->first();
                    $team_admin  = $team->Players()->wherePivot('is_super_admin', 1)->first();
                    if (!$player ){
                        $user->TeamsFollowing()->attach($user->id,['following_to'=>$request['id'],'type' => 2]);
                        if($team_admin){
                        $device = $team_admin->Device()->where('user_id',$team_admin->id)->orderBy('id','desc')->first();
                        $notification = Notification::where('user_id',$team_admin->id)->where('type','TME-PF')->where('type_id',$team->id)->first();
                        if (!$notification){
                            if ($device && $team_admin['notificaiton'] == 1)
                                $device_token_array = $device['fcm_token'];
                            $notification = new Notification();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                            $notification['date'] = $currenttime_notification_time;
                            $notification['type'] = 'TME-PF';
                            $notification['user_id'] = $team_admin->id;
                            $notification['content'] = $user->name.' started following your team '.$team->name;
                            $notification['type_id'] = $team->id;
                            $notification['status'] = 0;
                            $notification->save();
                            $team_admin['pending_notification'] = 1;
                            $team_admin->update();
                        }
                      }
                        if ($device_token_array) {
                            $data = "Team follow Request";
                            $notification_type = 8;
                            $data = array(
                              'title' => 'Team follow Request',
                              'notification_type' => $notification_type,
                              'team_id' => $team->id,
                              'team_name' => $team->name,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,$user->name . ' started following your team ' . $team->name,$data, $notification_type, $user->id);
                        }
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Team followed successfully'
                            ]
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Team already followed'
                        ]
                    );
                }return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Team not found'
                    ],500
                );

            }elseif ($action == 'unfollow' && $type == 'team'){
                $team = Teams::whereId($request['id'])->first();
                if ($team) {
                    $player = $user->TeamsFollowing()->wherePivot('following_to', $request['id'])->wherePivot('type', 2)->first();
                    if ($player) {
                        $user->TeamsFollowing()->detach(['following_to' => $request['id'], 'type' => 2]);
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Team unfollowed successfully'
                            ]
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Team already unfollowed'
                        ]
                    );
                }

            } return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Team not exits'
                ],500
            );


        }return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
    );


    }







    /**
     * follow Accept or Reject API
     */

    public function  acceptReject(Request $request,$action){

        if (isset($request['id'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {

                if ($action == 'accept') {
                    $player = $user->Followers()->wherePivot('following_to', $user['id'])->wherePivot('followed_by', $request['id'])->wherePivot('type', 1)->first();
                    if ($player) {
                        $user->Followers()->detach(['followed_by' => $request['id'], 'type' => 1]);
                        $user->Followers()->attach($user->id, ['followed_by' => $request['id'], 'type' => 1, 'is_requested' => 0]);
                        $followed_by = Users::whereId($request['id'])->first();
                        $oldnotification = Notification::where('user_id', $user->id)->where('type', 'PLY-FR')->where('type_id', $followed_by->id)->first();
                        if ($oldnotification) {
                            $oldnotification['content'] = 'You accepted the follow request from ' . $followed_by['name'];
                            $oldnotification['status'] = 1;
                            $oldnotification['is_read'] = 1;
                            $oldnotification->update();
                        }
                        $device = $followed_by->Device()->where('user_id', $followed_by->id)->orderBy('id', 'desc')->first();
                        $device_token_array = '';


                        if ($device && $followed_by['notificaiton'] == 1)
                            $device_token_array = $device['fcm_token'];
                        $notification = new Notification();
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                        $currenttime_notification_time = $date->format('Y-m-d H:i:s');
                        $notification['date'] = $currenttime_notification_time;
                        $notification['type'] = 'PLY-FR';
                        $notification['user_id'] = $followed_by->id;
                        $notification['content'] = $user->name . ' has accepted your follow request';
                        $notification['type_id'] = $user->id;
                        $notification['status'] = 1;
                        $notification->save();
                        $followed_by['pending_notification'] = 1;
                        $followed_by->update();

                        if ($device_token_array) {
                            $data = "Follow request accepted";
                            $notification_type = 8;
                            $data = array(
                              'title' => 'Follow request accepted',
                              'notification_type' => $notification_type,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,$user->name . ' has accepted your follow request',$data, $notification_type, $user->id);
                        }

                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Follow request accepted'
                            ]
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Player not requested'
                        ], 500
                    );
                } elseif ($action == 'reject') {

                    $followed_by = Users::whereId($request['id'])->first();
                    $player = $user->Followers()->wherePivot('followed_by', $request['id'])->where('role_id', 3)->wherePivot('type', 1);
                    if (!$player) {
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Follow request rejected already'
                            ]
                        );
                    }
                    $user->Followers()->detach(['followed_by' => $request['id'], 'type' => 1]);
                    $notification = Notification::where('user_id', $user->id)->where('type', 'PLY-FR')->where('type_id', $followed_by->id)->first();
                    if ($notification) {
                        $notification->delete();
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Follow request rejected sucessfully'
                        ]
                    );
                }elseif ($action == 'rejectRequest') {

                    $following_to = Users::whereId($request['id'])->first();
                    $player = $user->Following()->wherePivot('following_to', $request['id'])->wherePivot('type', 1);
                    if (!$player) {
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Follow request rejected already'
                            ]
                        );
                    }
                    $user->Following()->detach(['following_to' => $request['id'], 'type' => 1]);
                    $notification = Notification::where('user_id', $request['id'])->where('type', 'PLY-FR')->where('type_id', $user->id)->first();
                    if ($notification) {
                        $notification->delete();
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Follow request cancelled sucessfully'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'JSON schema failed'
                    ],500
                );
            }

            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );

    }

    /**
     * @param Request $request
     * @param $type
     * @param $action
     * @return \Illuminate\Http\JsonResponse
     * remove/make admin API
     */

    public function  removeMakeAdmin(Request $request,$type,$action)
    {

        if (isset($request['user_id']) && ($request->has('game_id') || $request->has('team_id'))) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                if ($action == 'remove' && $type == 'game') {
                    $game = Games::whereId($request['game_id'])->first();
                    if (!$game)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Game not found'
                            ], 500
                        );
                    $player = $game->Players()->where('id', $request['user_id'])->first();

                    if ($player){
                        $player_admin=  $game->Players()->where('id',$request['user_id'])->wherePivot('is_super_admin', 1)->first();

                        if (!$player_admin){

                            if ($player->id != $user->id){
                                $game->Players()->detach(['user_id',$request['user_id']]);
                                $notification = Notification::where('type_id',$game->id)->where('user_id',$request['user_id'])->get();
                                foreach ($notification as $value){
                                    if ($value)
                                        $value->delete();
                                }
                            }else
                                return response()->json(
                                    [
                                        'status' => 'error',
                                        'message' => 'You cannot remove yourself'
                                    ],500);

                        }else
                            return response()->json(
                                [
                                    'status' => 'error',
                                    'message' => 'You cannot remove a super admin'
                                ], 500 );
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Player removed from the game'
                        ]
                    );
                }elseif ($action == 'makeAdmin' && $type == 'game'){
                    $game = Games::where('id', $request['game_id'])->first();
                    if (!$game)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Game not found'
                            ], 500
                        );
                    $player = $game->Players()->where('id',$request['user_id'])->first();
                    if ($player){
                        if ($player['pivot']['is_admin'] == 0 && $player['pivot']['is_super_admin'] == 0){
                            $game->Players()->updateExistingPivot($player->id, ['game_id' => $game->id, 'is_admin' => 1]);
                            $device = $player->Device()->where('user_id',$player->id)->orderBy('id','desc')->first();
                            $device_token_array = '';
                            if ($device && $player['notificaiton'] == 1)
                                $device_token_array = $device['fcm_token'];
                            $notification = new Notification();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                            $notification['date'] = $currenttime_notification_time;
                            $notification['type'] = 'GME-MA';
                            $notification['user_id'] = $player->id;
                            $notification['content'] = 'You have been made an admin of '.$game->name;
                            $notification['type_id'] = $game->id;
                            $notification['status'] = 1;
                            $notification->save();
                            $player['pending_notification'] = 1;
                            $player->update();
                            if ($device_token_array) {
                                $data = "You are Admin now";
                                $notification_type = 9;
                                $data = array(
                                  'title' => 'You are Admin now',
                                  'notification_type' => $notification_type,
                                  'game_id' => $game->id,
                                  'game_name' => $game->name,
                                  'user_id' => $user->id,
                                  'user_name' => $user->name,
                                  'profile_pic' => $user->profile_pic,
                                  'email' => $user->email
                                );
                                sendNotificationwithBody($device_token_array,'You have been made an admin of '.$game->name,$data, $notification_type, $game->id);
                            }
                        }
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Player changed to admin'
                            ]
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'error',
                            'success' => 'Player must be a member of game'
                        ],500
                    );
                }elseif ($action == 'remove' && $type == 'team') {
                    $team = Teams::where('id',$request['team_id'])->first();
                    if (!$team)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Team not found'
                            ], 500
                        );
                    $player = $team->Players()->where('id',$request['user_id'])->first();
                    if ($player){
                        $player=  $team->Players()->where('id',$request['user_id'])->wherePivot('is_super_admin', 1)->first();
                        if (!$player){
                            $team->Players()->detach(['user_id',$request['user_id']]);
                            $notification = Notification::where('type_id',$team->id)->where('more',$request['user_id'])->first();
                            if ($notification)
                                $notification->delete();
                        }else
                            return response()->json(
                                [
                                    'status' => 'error',
                                    'message' => 'You cannot remove a super admin'
                                ], 500 );
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Player removed from the team'
                        ]
                    );
                }elseif ($action == 'makeAdmin' && $type == 'team'){
                    $team = Teams::where('id',$request['team_id'])->first();
                    if (!$team)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Team not found'
                            ], 500
                        );
                    $player = $team->Players()->where('id',$request['user_id'])->first();
                    if ($player){
                        if ($player['pivot']['is_admin'] == 0 && $player['pivot']['is_super_admin'] == 0){
                            $team->Players()->updateExistingPivot($player->id, ['team_id' => $team->id, 'is_admin' => 1]);
                            $device = $player->Device()->where('user_id',$player->id)->orderBy('id','desc')->first();
                            $device_token_array = '';
                                if ($device && $player['notificaiton'] == 1)
                                    $device_token_array = $device['fcm_token'];
                                $notification = new Notification();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                                $notification['date'] = $currenttime_notification_time;
                                $notification['type'] = 'TME-MA';
                                $notification['user_id'] = $player->id;
                                $notification['content'] = 'You have been made an admin of '.$team->name;
                                $notification['type_id'] = $team->id    ;
                                $notification['status'] = 1;
                                $notification->save();
                                $player['pending_notification'] = 1;
                                $player->update();
                            if ($device_token_array) {
                                $data = "You are Admin of a team";
                                $notification_type = 7;
                                $data = array(
                                  'title' => 'Follow request accepted',
                                  'notification_type' => $notification_type,
                                  'team_id' => $team->id,
                                  'team_name' => $team->name,
                                  'user_id' => $user->id,
                                  'user_name' => $user->name,
                                  'profile_pic' => $user->profile_pic,
                                  'email' => $user->email
                                );
                                sendNotificationwithBody($device_token_array,'You have been made an admin of '.$team->name,$data, $notification_type, $team->id);
                            }
                        }
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'success' => 'Player changed to admin'
                            ]
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'error',
                            'success' => 'Player must be a member of team'
                        ],500
                    );
                }

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ], 500


        );

    }



    function sendtestmessage()
    {
        $server_key = 'AAAA4-HIsSI:APA91bFS__Y9IHCmMLxvdXM1iCfijvWTbaA5j8hpTCHkvUvPhmwvJRrmrcR4vAJ00BATNLkUc3_SgEjtPtfw337MGNo87E3aNZ74ZRF8Ouh939hKrHs8o1KuUa_fzCrDHciouU2wxeVW';
        $device_token_array = 'dOeoWIXSLgw:APA91bF6FE8E-CpdzWgmyxPj0H2IAbDGqB1BDfGxwT4lj_4BpLZ1_GmLlMokcsT3PzUE1JzInCdHVgpJRUFaORpvR_h9vFLsVsUGGD5kCZQrMmbzHAv-JJsAZQwC85sWkzseptitgINq';
        if ($device_token_array) {

            $fields = array();
            $url = 'https://fcm.googleapis.com/fcm/send';
            $notification = array();
            $notification['body'] = 'online 2';
            $notification['title'] = 'Yowzapp';
            $notification['icon'] = 'ic_logo_play';
            $notification['sound'] = 'default';
           // $notification['tag'] = 'chat';
            $notification['collapse_key'] = 'chat';
//            $notification['key_name'] = 'test';
//            $notification['operation'] = 'create';


//            $fields['operation'] = 'create';
//            $fields['notification_key_name'] = 'test';
//            $fields['dry_run'] = true;

            $fields['notification'] = $notification;
            $fields['condition'] = '&&';

           // $fields['topics'] = 'test';
            $fields['to'] = $device_token_array;


            //header with content_type api key
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=' . $server_key
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
    }


}
