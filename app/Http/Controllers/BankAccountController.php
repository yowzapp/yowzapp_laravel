<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Models\AccessToken;
use App\Models\BankAccount;
use App\Models\Users;
use App\Models\Vendors;
use App\User;
use Illuminate\Http\Request;
use Log;

class BankAccountController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Add bank account
     */

    public function addBankAccount(Request $request)
    {
        Log::info('addBankAccount ---------------------------');
        Log::info($request->all());
        if (isset($request['account_name'], $request['bank_name'], $request['branch_name'],$request['ifsc_code'],$request['account_number'],$request['is_primary'],$request['id'])) {
            $user = Vendors::where("user_id", $request->id)->first();
            if($user) {
                Log::info($user);
                $is_bankAccount = BankAccount::where("account_number", $request->account_number)->first();
                if ($is_bankAccount)
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Bank Account already exits',
                        ],500
                    );
                Log::info('Bank Account not exits');
                $bankAccount = new BankAccount();
                $bankAccount->account_name = $request['account_name'];
                $bankAccount->vendor_id = $request['id'];
                $bankAccount->bank_name = $request['bank_name'];
                $bankAccount->branch_name = $request['branch_name'];
                $bankAccount->ifsc_code = $request['ifsc_code'];
                $bankAccount->account_number = $request['account_number'];
                $bankAccount->is_primary = filter_var($request['is_primary'], FILTER_VALIDATE_BOOLEAN);
                $bankAccount->save();
                Log::info($bankAccount);
                unset($bankAccount['vendor_id']);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Bank account created successfully',
                        'success' => $bankAccount,
                    ]
                );
            }
            Log::info('Vendor not found');
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Vendor not found',
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * edit bank account
     */

    public function editBankAccount(Request $request)
    {
        if (isset($request['id'],$request['account_id'])) {
            $user = Vendors::where("user_id", $request->id)->first();
            if($user) {
                $bankAccount = BankAccount::where("id", $request->account_id)->first();
                if ($bankAccount){

                    if ($request->has('account_name'))
                        $bankAccount->account_name = $request['account_name'];
                    if ($request->has('bank_name'))
                        $bankAccount->bank_name = $request['bank_name'];
                    if ($request->has('branch_name'))
                        $bankAccount->branch_name = $request['branch_name'];
                    if ($request->has('ifsc_code'))
                        $bankAccount->ifsc_code = $request['ifsc_code'];
                    if ($request->has('account_number'))
                        $bankAccount->account_number = $request['account_number'];
                    if ($request->has('is_primary'))
                        $bankAccount->is_primary = filter_var($request['is_primary'], FILTER_VALIDATE_BOOLEAN);
                        $bankAccount->update();
                unset($bankAccount['vendor_id']);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Bank account updated successfully',
                        'success' => $bankAccount,
                    ]
                );
                }return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Bank account not exits',
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Vendor not found',
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * delete bank account
     */
    public function  deleteBankAccount(Request $request){
        if (isset($request['id'],$request['account_id'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = Users::whereId($accesstoken->user_id)->first();
            if ($user) {
                $bankAccount = BankAccount::where("id", $request->account_id)->first();
                if ($bankAccount) {
                    $bankAccount->delete();
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Bank account deleted successfully'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Bank account not exits',
                    ], 500
                );


            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );
        }return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Bank account list
     */

    public function  bankAccountList(Request $request){
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = Users::whereId($accesstoken->user_id)->first();
        if ($user){
        $accountlist = BankAccount::orderBy('updated_at','desc')->where('vendor_id', $user->id)->get();
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Bank Account List',
                    'success' => $accountlist,
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not found'
            ],500
        );

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Bank account details
     */

    public function  bankAccountDetails(Request $request){
        if ($request->has('account_id')){
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = Users::whereId($accesstoken->user_id)->first();
        if ($user){
            $bankAccountDetails = BankAccount::where('id', $request->account_id)->get();
            unset($bankAccountDetails['vendor_id']);
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Bank Account Details',
                    'success' => $bankAccountDetails,
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not found'
            ],500
        );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not found'
            ],500
        );
    }



}
