<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\Games;
use App\Models\Grounds;
use App\Models\Notification;
use App\Models\Settings;
use App\Models\Teams;
use App\Models\TeamTimeline;
use App\Models\Users;
use App\User;
use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Log;
use FCM;

class NotificationController extends Controller
{
    public function  notificationList(Request $request){ 
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user) {
            $notification_list = Notification::where('user_id',$user->id)->orderBy('date','desc')->paginate(20);

            $notifications = Notification::where('user_id',$user->id)->where('viewed', 0)->update(['viewed' => 1]);


            $all_notifications_final = array();
            foreach($notification_list as $k=>$notification) {

                $pic = '';
                $type = '';
                if ($notification["type"] == 'TME-PA' || $notification["type"] == 'TME-PR' || $notification["type"] == 'TME-PF' ||$notification["type"] == 'TME-MA'){
                    $type = 'team';
                    $team = Teams::whereId($notification['type_id'])->first();
                    if ($team)
                        $pic = $team['cover_pic'];
                }elseif ($notification["type"] == 'GME-PA' ||$notification["type"] == 'GME-PY'|| $notification["type"] == 'GME-MA'|| $notification["type"] == 'GME-PR'|| $notification["type"] == 'GME-PR' || $notification["type"] == 'GMERR' || $notification["type"] == 'GME-RA'){
                    $type = 'game';
                    $game = Games::whereId($notification['type_id'])->first();
                    if ($game)
                        $pic = $game['cover_pic'];
                }elseif ($notification["type"] == 'PLY-FT' ||$notification["type"] == 'PLY-FB'||$notification["type"] == 'PLY-FR' ){
                    $type = 'user';
                    $player = Users::whereId($notification['type_id'])->first();
                    if ($player)
                        $pic = $player['profile_pic'];
                }elseif($notification["type"] == 'GMER'){
                    
                    
                    if ($notification['status'] == 1){
                        $type = 'game';
                        $game = Games::whereId($notification['type_id'])->first();
                        
                        if ($game)
                            $pic = $game['cover_pic'];
                    }else{
                        $type = 'user';
                        $player = Users::whereId($notification['more'])->first();
                        
                        if ($player)
                            $pic = $player['profile_pic'];

                            // return $player;
                    }
                }elseif ($notification["type"] == 'VDR-BOKNG' || $notification["type"] == 'VDR-CNCL'){
                    $type = 'vendor cancelation';
                    $ground = Grounds::whereId($notification['type_id'])->where('is_deleted',0)->first();
                    if ($ground){
                        $sport = $ground->Sport()->first();
                        $pic = $sport['thumbnail'];
                    }
                }
                $all_notifications_final[] =  array(
                    'id' => $notification["id"],
                     'cover_pic' => $pic,
                    'type' => $notification["type"],
                    'content' => $notification["content"],
                    'type_id' => $notification["type_id"],
                    'status' =>$notification["status"],
                    'is_read' =>$notification["is_read"],
                    'time' =>getDateString($notification['date']),
                    'more' =>$notification["more"],
                    'belongsTo' => $type
                );
                if (sizeof($all_notifications_final)>0){
                    $user['pending_notification'] = 0;
                    $user->update();
                }
            }
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Notification List',
                    'success' => $all_notifications_final,
                    'currentPage' => $notification_list->currentPage(),
                    'hasMorePages' => $notification_list->hasMorePages()
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *  Read Notification
     */

    public function  readNotification(Request $request){
        if ($request->has('notification_id')){
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user) {
            $notification = $user->Notifications()->where('id',$request['notification_id'])->first();
            if ($notification){
                $notification['is_read'] = 1;
                $notification->update();
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Notification read successfully',
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Notification not found'
                ],500
            );


            }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Request - Accept -- reject API
     */

    public function  action(Request $request,$type,$action) 
    {
        if (isset($request['type_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {

                if ($type == 'team'){
                    $team = Teams::where('id',$request['type_id'])->first();
                    if ($team){
                        $requested_player = $team->Players()->wherePivot('user_id',$request['user_id'])->wherePivot('team_id',$team['id'])->first();
                        $team_admin = $team->Players()->wherePivot('user_id',$user['id'])->wherePivot('team_id',$team['id'])->first();
                        if ($requested_player && $team_admin ) {
                            if ($action == 'accept') {
                                $notification = $user->Notifications()->where('user_id',$team_admin->id)->where('type_id',$team->id)->where('more',$request['user_id'])->first();
                                if($notification){
                                    $notification['content'] = 'You\'ve added '.$requested_player->name.' to '.$team->name;
                                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                    $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                                    $notification['date'] = $currenttime_notification_time;
                                    $notification['status'] = 1;
                                    $notification['is_read'] = 1;
                                    $notification->update();
                                }
                                    $team->Players()->updateExistingPivot($requested_player->id, ['team_id' => $team->id, 'invitation_status' => 1,'is_requested' => 0]);;
                                    $notification = new Notification();
                                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                    $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                                    $notification['date'] = $currenttime_notification_time;
                                    $notification['type'] = 'TME-RA';
                                    $notification['user_id'] = $requested_player->id;
                                    $notification['content'] = $team_admin->name.' accepted your request to join '.$team->name;
                                    $notification['type_id'] = $team->id;
                                    $notification['status'] = 1;
                                    $notification['is_read'] = 0;
                                    $notification->save();
                                    $requested_player['pending_notification'] = 1;
                                    $requested_player->update();
                                    $device = $requested_player->Device()->where('user_id',$requested_player->id)->orderBy('id','desc')->first();
                                    $device_token_array = '';
                                    if ($device && $requested_player['notificaiton'] == 1)
                                        $device_token_array = $device['fcm_token'];
                                    if ($device_token_array) {
                                        $data = "Team request accepted";
                                        $type = 7;
                                        $data = array(
                                          'title' => 'Team request accepted',
                                          'notification_type' => $type,
                                          'team_id' => $team->id,
                                          'team_name' => $team->name,
                                          'user_id' => $user->id,
                                          'user_name' => $user->name,
                                          'profile_pic' => $user->profile_pic,
                                          'email' => $user->email
                                        );
                                        sendNotificationwithBody($device_token_array,$team_admin->name.' accepted your request to join '.$team->name,$data, $type);
                                    }

                                $team_timeline = new TeamTimeline();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                $currentdate =  $date->format('Y-m-d');
                                $team_timeline['team_id'] = $team->id;
                                $team_timeline['date'] = $currentdate;
                                $team_timeline ['content'] = $requested_player->name.' joined the team'. $team->name;
                                $team_timeline->save();
                                return response()->json(
                                    [
                                        'status' => 'success',
                                        'statusCode' => 200,
                                        'message' => 'Player joined team',
                                        'data' => $team
                                    ]
                                );
                            } elseif ($action == 'reject') {
                                $notification = $user->Notifications()->where('user_id',$user->id)->where('type_id',$team->id)->where('more',$request['user_id'])->first();
                                if($notification){
                                   $notification->delete();
                                };
                                $team->Players()->detach($requested_player->id, ['team_id' => $team->id]);
                                return response()->json(
                                    [
                                        'status' => 'success',
                                        'statusCode' => 200,
                                        'message' => 'Rejected successfully',

                                    ]
                                );

                            }
                        }
                    }
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Team not found'
                        ],500
                    );

                }elseif ($type == 'game'){
                        $game = Games::where('id',$request['type_id'])->first();
                        if ($game){
                            $player = $game->Players()->where('user_id',$user->id)->first();
                            if ($player) {
                                if ($action == 'accept') {
                                    if ($request->has('user_id')) {
                                        $requested_player = $game->Players()->wherePivot('user_id', $request['user_id'])->wherePivot('game_id', $game['id'])->first();
                                        $game_admin = $game->Players()->wherePivot('user_id', $user['id'])->wherePivot('game_id', $game['id'])->first();
                                        if ($requested_player && $game_admin) {
                                            $notification = $user->Notifications()->where('user_id', $game_admin->id)->where('type_id', $game->id)->where('more', $request['user_id'])->first();
                                            if ($notification) {
                                                $notification['content'] = 'You\'ve added ' . $requested_player->name;
                                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                                $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                                                $notification['date'] = $currenttime_notification_time;
                                                $notification['status'] = 1;
                                                $notification['is_read'] = 1;
                                                $notification->update();
                                            }
                                            $game->Players()->updateExistingPivot($requested_player->id, ['game_id' => $game->id, 'invitation_status' => 1, 'is_requested' => 0]);;
                                            $notification = new Notification();
                                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                                            $currenttime_notification_time = $date->format('Y-m-d H:i:s');
                                            $notification['date'] = $currenttime_notification_time;
                                            $notification['type'] = 'GME-RA';
                                            $notification['user_id'] = $requested_player->id;
                                            $notification['content'] = $game_admin->name . ' accepted your request to join ' . $game->name;
                                            $notification['type_id'] = $game->id;
                                            $notification['status'] = 1;
                                            $notification['is_read'] = 0;
                                            $notification->save();
                                            $requested_player['pending_notification'] = 1;
                                            $requested_player->update();
                                            $device = $requested_player->Device()->where('user_id', $requested_player->id)->orderBy('id', 'desc')->first();
                                            $device_token_array = '';
                                            if ($device)
                                                $device_token_array = $device['fcm_token'];
                                            if ($device_token_array && $requested_player['notificaiton'] == 1) {
                                                $data = "Game request accepted";
                                                $type = 6;
                                                $data = array(
                                                  'title' => 'Team request accepted',
                                                  'notification_type' => $type,
                                                  'game_id' => $game->id,
                                                  'game_name' => $game->name,
                                                  'user_id' => $user->id,
                                                  'user_name' => $user->name,
                                                  'profile_pic' => $user->profile_pic,
                                                  'email' => $user->email
                                                );
                                                sendNotificationwithBody($device_token_array,$game_admin->name . ' accepted your request to join ' . $game->name,$data, $type);
                                            }
                                        }
                                    } else {

                                        $playerCount = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('invitation_status', 1)->count();
                                        $ground = $game->Ground()->first();
                                        $maxCount = $game["max_members"];
                                        if ($playerCount < $maxCount) {
                                            $game->Players()->updateExistingPivot($player->id, ['game_id' => $game->id, 'invitation_status' => 1]);
                                            $notification = $player->Notifications()->where('user_id', $player->id)->where('type_id', $game->id)->first();
                                            if ($notification) {
                                                $notification['content'] = 'You\'ve joined game ' . $game->name;
                                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                                $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                                                $notification['date'] = $currenttime_notification_time;
                                                $notification['status'] = 1;
                                                $notification['is_read'] = 1;
                                                $notification->update();
                                            }
                                            return response()->json(
                                                [
                                                    'status' => 'success',
                                                    'statusCode' => 200,
                                                    'message' => 'Accepted game request',
                                                    'data' => $game
                                                ]
                                            );
                                        }
                                        $notification = $player->Notifications()->where('user_id', $player->id)->where('type_id', $game->id)->first();
                                        if ($notification)
                                            $notification->delete();


                                        return response()->json(
                                            [
                                                'status' => 'error',
                                                'message' => 'Oops!! Sorry you\'re late, HOUSEFULL'
                                            ], 500);
                                        }
                                } elseif ($action == 'reject') {
                                    if ($request->has('user_id')) {
                                        $requested_player = $game->Players()->wherePivot('user_id', $request['user_id'])->wherePivot('game_id', $game['id'])->first();
                                        $game_admin = $game->Players()->wherePivot('user_id', $user['id'])->wherePivot('game_id', $game['id'])->first();
                                        if ($requested_player && $game_admin) {
                                            $notification = $user->Notifications()->where('user_id', $user->id)->where('type_id', $game->id)->where('more', $request['user_id'])->first();
                                            if ($notification) {
                                                $notification->delete();
                                            };
                                            $game->Players()->detach($requested_player->id, ['game_id' => $game->id]);
                                        }
                                    } else {
                                        $player = $game->Players()->where('id', $user->id)->first();
                                        if ($player) {
                                            $game->Players()->detach($player->id, ['game_id' => $game->id]);
                                            $notification = $player->Notifications()->where('user_id', $player->id)->where('type_id', $game->id)->first();
                                            if ($notification) {
                                                $notification->delete();
                                            }
                                        }
                                    }
                                    return response()->json(
                                        [
                                            'status' => 'success',
                                            'statusCode' => 200,
                                            'message' => 'Rejected successfully'

                                        ]
                                    );
                                }
                            }
                            return response()->json(
                                [
                                    'status' => 'error',
                                    'message' => 'Player not found'
                                ],500);
                        }
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Game not found'
                        ],500
                    );

                }

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );

    }


    /**
     * Get Notification setting
     */

    public function getNotificaitonSetting(Request $request)
    {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = Users::whereId($accesstoken->user_id)->first();
            if ($user) {
                $setting = $user->Setting()->first();
                if (!$setting){
                    $setting = new Settings();
                    $setting['user_id'] = $user->id;
                    $setting->save();
                }

                if($user['notificaiton'] == 1){
                    $setting['push_notification'] = true;
                }else{
                    $setting['push_notification'] = false;
                }
                return response()->json(
                    [
                        'status' => 'success',
                        'statuscode' => 200,
                        'message' => 'Notification list',
                        'success' => $setting,
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not exits',
                ],500
            );
    }

    /**
     * Notification setting
     */

    public function putNotificaitonSetting(Request $request)
    {
        if(isset($request['push_notification'])||isset($request['new_follower'])||isset($request['new_team'])||isset($request['open_game'])||isset($request['game_invite'])||isset($request['challenge_game'])) {

            Log::info($request->all());
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = Users::whereId($accesstoken->user_id)->first();
            if ($user) {

                if ($request->has('push_notification')){
                    if ($request['push_notification'] == 'true' || $request['push_notification'] == 1  ) {
                        $user['notificaiton'] = 1;
                    } else {
                        $user['notificaiton'] = 0;
                    }
                    $user->update();
                }
                $setting = $user->Setting()->first();
                if (!$setting){
                    $setting = new Settings();
                    $setting['user_id'] = $user->id;
                    $setting->save();
                }


                if ($request->has('new_follower')){
                    if ( $request['new_follower'] == 'true'  || $request['new_follower'] == 1) {
                        $setting['new_follower'] = 1;
                    } else {
                        $setting['new_follower'] = 0;
                    }
                }

                if ($request->has('new_team')){
                    if ( $request['new_team'] == 'true'  || $request['new_team'] == 1) {
                        $setting['new_team'] = 1;
                    } else {
                        $setting['new_team'] = 0;
                    }
                }
                if ($request->has('open_game')){

                    if ($request['open_game'] == 'true'  || $request['open_game'] == 1) {

                        $setting['open_game'] = 1;
                    }else {
                        $setting['open_game'] = 0;
                    }
                }
                if ($request->has('game_invite')){
                    if ( $request['game_invite'] == 'true'  || $request['game_invite'] == 1) {
                        $setting['game_invite'] = 1;
                    } else {
                        $setting['game_invite'] = 0;
                    }
                }

                if ($request->has('game_pay_invite')){
                    if ( $request['game_pay_invite'] == 'true' || $request['game_pay_invite'] == 1) {
                        $setting['game_pay_invite'] = 1;
                    } else {
                        $setting['game_pay_invite'] = 0;
                    }
                }

                if ($request->has('challenge_game')){
                    if ( $request['challenge_game'] == 'true' || $request['challenge_game'] == 1) {
                        $setting['challenge_game'] = 1;
                    } else {
                        $setting['challenge_game'] = 0;
                    }
                }

                $setting->update();
                return response()->json(
                    [
                        'status' => 'success',
                        'statuscode' => 200,
                        'message' => 'Notification settings updated',
                        'success' => $setting,
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not exits',
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );
    }


    /**
     *   send sms
     */

    public function sendSms(Request $request){
      if($request->hasHeader("accessToken") && $request->header('accessToken') != ''){
          $accessToken = AccessToken::where('access_token',$request->header('accessToken'))->first();
          $user = User::whereId($accessToken->user_id)->first();
          $mobile = $user->mobile;
          if (strlen($mobile) >= 9){
            $mobile = "971".substr($mobile, -9);
          }
          $url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
          $message = urlencode($request->message);
          $ch = curl_init();
          if (!$ch){die("Couldn't initialize a cURL handle");}
          $ret = curl_setopt($ch, CURLOPT_URL,$url);
          curl_setopt ($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
          curl_setopt ($ch, CURLOPT_POSTFIELDS,
          "User=yowzapp&passwd=53267794&mobilenumber=$mobile&message=$message&sid=yowzap&mtype=N&DR=Y");
          $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $curlresponse = curl_exec($ch);
          if(curl_errno($ch))
          echo 'curl error : '. curl_error($ch);
          if (empty($ret)) {
          curl_close($ch);
          return response()->json(
              [
                  'status' => 'error',
                  'message' => curl_error($ch)
              ],500
          );
          }
          $info = curl_getinfo($ch);
          curl_close($ch);
          return response()->json(
              [
                  'status' => 'success',
                  'statuscode' => 200,
                  'message' => 'Notification settings updated',
                  'success' => $curlresponse,
              ]
          );
      }
      return response()->json(
          [
              'status' => 'error',
              'message' => 'Invalid JSON schema'
          ],500
      );

    }

    /**
     *  setting table update
     */

    public function updateSettings(Request $request)
    {
        $users = Users::get();

        foreach ($users as $user){
            $setting = new Settings();
            $setting['user_id'] = $user->id;
            $setting->save();
        }
        return 'done';

    }

}
