<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\BannersRequest as StoreRequest;
use App\Http\Requests\BannersRequest as UpdateRequest;
use App\Models\DeviceInfo;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
// VALIDATION: change the requests to match your own file names if you need form validation


class BannerCrudController extends CrudController
{

    public function setUp()
    {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Banner");
        $this->crud->setRoute("admin/banner");
        $this->crud->setEntityNameStrings('banner', 'banners');



        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

//        $this->crud->setFromDb();


        $this->crud->addField([ // image
            'name' => "banner_image",
            'label' => "Banner",
            'type' => 'file',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        ]);


        $this->crud->addField([
            'name' => 'url',
            'label' => "Promotional URL",
        ]);

        $this->crud->addColumn([
            'name' => 'id',
            'label' => "Id",
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'label' => "Banner",
            'type' => 'image',
            'aspect_ration' => 1
        ]);


        $this->crud->addColumn([
            'name' => 'url',
            'label' => "Promotional URL",
        ]);

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');





        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
         $this->crud->allowAccess(['list', 'create', 'delete', 'reorder','update']);
         //$this->crud->denyAccess(['update']);


        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
         $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->with(); // eager load relationships
         $this->crud->orderBy('id','asc');
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	public function store(StoreRequest $request)
	{

        if($request->file('banner_image')){

            $file = $request->file('banner_image');
            $origin_name = $file->getClientOriginalName();
            $key =  'sports/'.str_replace(' ','_',time()).'/'.$origin_name;
            Storage::disk('s3')->put($key, fopen($file, 'r+'),'public');
            $url= Storage::disk('s3')->url($key);
            $request->request->add(['image'=>$url]);

        }

        return parent::storeCrud($request);
	}

    public function update(UpdateRequest $request)
    {
        if ($request->file('banner_image')) {
            $file = $request->file('banner_image');
            $origin_name = $file->getClientOriginalName();
            $key = 'sports/' . str_replace(' ', '_', time()) . '/' . $origin_name;
            Storage::disk('s3')->put($key, fopen($file, 'r+'), 'public');
            $url = Storage::disk('s3')->url($key);
            $request->request->add(['thumbnail' => $url]);
        }
        return parent::updateCrud($request);
    }



    public function pushNotification(Request $request){

        if ($request->get('message')) {

            $message = $request->get('message');
            $title = $request->get('title');

            $userIds = User::pluck('id')->toArray();

            $device_token_array = DeviceInfo::where('fcm_token', '!=', null)->whereIn('user_id',$userIds)->pluck('fcm_token')->toArray();

            if ($device_token_array) {
                $fields = array();
                $server_key = 'AAAA4-HIsSI:APA91bFS__Y9IHCmMLxvdXM1iCfijvWTbaA5j8hpTCHkvUvPhmwvJRrmrcR4vAJ00BATNLkUc3_SgEjtPtfw337MGNo87E3aNZ74ZRF8Ouh939hKrHs8o1KuUa_fzCrDHciouU2wxeVW';
                $fields['collapse_key'] = 'Promotions';
                $url = 'https://fcm.googleapis.com/fcm/send';
                $notification = array();
                $notification['body'] = $message;
                $notification['title'] = $title;
                $notification['icon'] = 'ic_logo_play';
                $notification['sound'] = 'default';
                $notification['tag'] = 'promotion';
                $fields['notification'] = $notification;
                if (is_array($device_token_array)) {
                    $fields['registration_ids'] = $device_token_array;
                } else {
                    $fields['to'] = $device_token_array;
                }

                //header with content_type api key
                $headers = array(
                    'Content-Type:application/json',
                    'Authorization:key=' . $server_key
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close($ch);
                if ($result) {
                    \Alert::success('Notification sent successfully.')->flash();
                }
            }
        }
        return view('admin.promotions.pushNotification');
    }

}
