<?php

namespace App\Http\Controllers\Admin;

use App\Models\Amenities;
use App\Models\Sports;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\VenuesRequest as StoreRequest;
use App\Http\Requests\VenuesRequest as UpdateRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


class VenuesCrudController extends CrudController {

	public function setUp() {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Venues");
        $this->crud->setRoute("admin/venues");
        $this->crud->setEntityNameStrings('venues', 'venues');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'Id',
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Name',
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Name',
        ]);

        $this->crud->addColumn([
            'name' => 'cover_pic',
            'label' => 'Cover Pic',
            'type'=> 'image'
        ]);


        $this->crud->addField([ // image
            'name' => 'cover_pic',
            'label' => "Cover Pic",
            'type' => 'file',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        ]);

//        $this->crud->addColumn([
//            'name' => 'description',
//            'label' => 'Description',
//
//        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => 'Description',
        ]);



        $this->crud->addField([
            'name' => 'is_disabled',
            'label' => 'Availability',
            'type' => 'radio',
            'inline' => true,
            'options'=> [0=>'Enabled',1=>'Disabled']
        ]);

        $this->crud->addField([
            'name' => 'is_verified',
            'label' => 'Verification',
            'type' => 'radio',
            'inline' => true,
            'options'=> [1=>'Verified',0=>'Not Verified']
        ]);


        $this->crud->addColumn([
            'label' => "Vendor",
            'type' => 'select',
            'name' => 'vendor_id',
            'entity' => 'Vendor',
            'attribute' => 'name',
            'model' => "App\Models\Users"
        ]);

        $this->crud->addColumn([
            'name' => 'location_name',
            'label' => 'Location',
        ]);

        $this->crud->addColumn([
            'name' => 'is_verified',
            'label' => "Verification",
            'type' => 'icon',

        ]);


        $this->crud->addColumn([
            'name' => 'is_disabled',
            'label' => "Available",
            'type' => 'icon_reverse',

        ]);

        $this->crud->addColumn([
            'name' => 'rating',
            'label' => "Rating",

        ]);

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['list','show','update']);
        $this->crud->denyAccess(['create', 'reorder', 'delete']);

        $this->crud->enableAjaxTable();

        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }


    /**
     * @param $id
     * @param $vid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * store and update
     */

    public function store(StoreRequest $request)
    {
        if($request->file('cover_pic_upload')){
            $file = $request->file('cover_pic_upload');
            $origin_name = $file->getClientOriginalName();
            $key =  'venues/'.str_replace(' ','_',$request['name']).'_'.date('Y_m_d_h_i_s').'/'.$origin_name;
            Storage::disk('s3')->put($key, fopen($file, 'r+'),'public');
            $url= Storage::disk('s3')->url($key);
            $request->request->add(['cover_pic'=>$url]);
        }
        if($request->has('name')){
            $request->request->add(['name'=>$request['name']]);
        }
        if($request->has('description')){
            $request->request->add(['name'=>$request['description']]);
        }

        if($request->has('is_disabled')){
            $request->request->add(['is_disabled'=>$request['is_disabled']]);
        }

        if($request->has('is_verified')){
            $request->request->add(['is_verified'=>$request['is_verified']]);
        }
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        if($request->file('cover_pic_upload')){
            $file = $request->file('cover_pic_upload');
            $origin_name = $file->getClientOriginalName();
            $key =  'venues/'.str_replace(' ','_',$request['name']).'_'.date('Y_m_d_h_i_s').'/'.$origin_name;
            Storage::disk('s3')->put($key, fopen($file, 'r+'),'public');
            $url= Storage::disk('s3')->url($key);
            $request->request->add(['cover_pic'=>$url]);
        }
        return parent::updateCrud($request);
    }


    /**
     * @param $id
     * @param $vid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * venue amenities
     */

    public function venueAmenitiesDelete($id, $vid)
    {
        //pivot_id

        DB::table('venues_amenities')->where('venue_id',$vid)->where('amenity_id',$id)->delete();

        \Alert::success('Aminity deleted successfully')->flash();
        return redirect('/admin/venues/'.$vid.'?tab=amenity');
    }
    public function venueGroundsDelete($id, $vid)
    {
        //pivot_id
        DB::table('grounds')->where('venue_id',$vid)->where('id',$id)->delete();
        \Alert::success('Ground deleted successfully')->flash();
        return redirect('/admin/venues/'.$vid.'?tab=grounds');
    }

    public function venueAmenity(StoreRequest $request)
    {
        $exists=DB::table('venues_amenities')->where(

            [
                'amenity_id'=>$request['amenity_id'],
                'venue_id' => $request['venue_id']
            ]
        )->first();

        if(!$exists){
            DB::table('venues_amenities')->insert(
                [
                    'amenity_id'=>$request['amenity_id'],
                    'venue_id' => $request['venue_id']
                ]
            );
        }

        \Alert::success('Aminity Added successfully')->flash();
        return redirect('/admin/venues/'.$request['venue_id'].'?tab=amenity');
    }





    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        // $this->crud->getListView() returns 'list' by default, or 'list_ajax' if ajax was enabled

        return view('admin.venues.list', $this->data);
    }


    /**
     *
     *
     * show detail view of venue
     */

    public function show($id)
    {
        $this->crud->hasAccessOrFail('show');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['sports'] = Sports::pluck('name','id');
        $this->data['amenities'] = Amenities::pluck('name','id');
        $this->data['title'] = trans('backpack::crud.preview').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('admin.venues.show', $this->data);
    }


}
