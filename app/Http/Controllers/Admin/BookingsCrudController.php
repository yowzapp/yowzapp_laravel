<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bookings;
use App\Models\Transaction;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BookingsRequest as StoreRequest;
use App\Http\Requests\BookingsRequest as UpdateRequest;

class BookingsCrudController extends CrudController {

	public function setUp() {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Transaction");
        $this->crud->setRoute("admin/bookings");
        $this->crud->setEntityNameStrings('bookings', 'bookings');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        //// **** this controller is using Transactions model *** NOT BOOKING MODEL **** please refer above





        $this->crud->addColumn([
            'name' => 'transaction_identifier',
            'label' => "Identifier",
        ]);

        $this->crud->addColumn([
            'name' => 'amount',
            'label' => "Amount",
            'type' => 'model_function',
            'function_name' => 'getBookingsAmount',
        ]);



        $this->crud->addColumn([
            'label' => "Users",
            'type' => 'select',
            'name' => 'user_id',
            'entity' => 'User',
            'attribute' => 'name',
            'model' => "App\Models\User"
        ]);


        $this->crud->addColumn([
            'label' => "Payment Status",
            'name' => 'booking_id',
            'type' => 'model_function',
            'id' => 'payment-id',
            'function_name' => 'getBookingsPaymentStatus',
        ]);


        $this->crud->addColumn([
            'label' => "Date",
            'name' => 'booking_id',
            'type' => 'model_function',
            'function_name' => 'getBookingsDate',
        ]);


		//$this->crud->setFromDb();

		// ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
         $this->crud->allowAccess(['list', 'show', 'reorder']);
         $this->crud->denyAccess(['create', 'update', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
//         $this->crud->enableAjaxTable();


        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
         $this->crud->orderBy('updated_at','desc');
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}

    /**
     *
     *
     * show detail view of venue
     */

    public function show($id)
    {
        $this->crud->hasAccessOrFail('show');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = trans('backpack::crud.preview').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('admin.bookings.show', $this->data);
    }



    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index() 
    {
        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        // $this->crud->getListView() returns 'list' by default, or 'list_ajax' if ajax was enabled
        return view('admin.bookings.list', $this->data);
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Cancel Booking Admin
     */

    public function  cancelBookingAdmin($id){

        $booking = Bookings::where('transaction_identifier',$id)->first();
        if (!$booking)
        {
            return redirect('admin/bookings');
        }
        $all_boookings = Bookings::where('transaction_identifier',$booking['transaction_identifier'])->get();
        foreach ($all_boookings as $booked) {
            if ($booked) {
                $booked['status'] = 0;
                $booked->update();
            }
        }
        $transaction = Transaction::where('transaction_identifier',$id)->first();
        $game = $transaction->Game()->first();
        if ($game){
            $game_admin = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('is_super_admin', 1)->first();
            $notification = new Notification();
            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
            $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
            $notification['date'] = $currenttime_notification_time;
            $notification['type'] = 'GME-PA';
            $notification['user_id'] = $game_admin->id;
            $notification['content'] = 'Your booking was canceled for ID '.$id.', Please contact support for further queries';
            $notification['type_id'] = $game->id;
            $notification['status'] = 1;
            $notification['is_read'] = 0;
            $notification->save();
            $game_admin['pending_notification'] = 1;
            $game_admin->update();
            $device = $game_admin->Device()->where('user_id',$game_admin->id)->orderBy('id','desc')->first();
            $device_token_array = '';
            if ($device && $game_admin['notificaiton'] == 1)
                $device_token_array = $device['fcm_token'];
            if ($device_token_array) {
							$data = 'cancel Booking';
						  // sendNotificationwithBody($device_token_array,'Your booking was canceled for ID '.$id.', Please contact support for further queries', $data, 3);
            }

            $ground = $game->Ground()->first();
            $venue = $ground->Venue()->fist();

            /// notifiy the vendor about the booking
            $more = array(
                $venue['name'],
                $venue['id'],
                $ground['name'],
                $ground['id'],
                date('Y-m-d', strtotime($game['date']))
            );
            $more = implode(",",$more);
            $vendor = $booking->Vendor()->first();
            $notification = new Notification();
            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
            $currenttime =  $date->format('Y-m-d H:i:s');
            $notification['date'] = $currenttime;
            $notification['type'] = 'VDR-BOKNG';
            $notification['content'] = 'Your Booking for '. $ground->name .' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time']).' as cancelled. Please contact Admin');
            $notification['type_id'] = $ground->id;
            $notification['more'] = $more;
            $notification['status'] = 0;
            $notification['user_id'] = $vendor->id;
            $notification->save();
            $vendor['pending_notification'] = 1;
            $vendor->update();
            $device_token_array = '';
            $device = $vendor->Device()->where('user_id',$vendor->id)->orderBy('id','desc')->first();
            if ($device)
                $device_token_array = $device['fcm_token'];
            if ($device_token_array) {
								$data = 'Booking cancelled from admin';
								$type = 3;
                // sendNotificationwithBody($device_token_array,'Your Booking for '. $ground->name .' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time']).' as cancelled. Please contact Admin'),$data, $type);
            }
        }
        return redirect('admin/bookings');
    }
}
