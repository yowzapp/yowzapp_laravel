<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\Grounds;
use App\Models\Sports;
use Illuminate\Http\Request;

class SportsController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * sports list api
     */
    public function  sportsList(Request $request){
        $user = [];
        $user_interest = [];
        $user_interest_id = [];
        if ($request->header('accessToken')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken)
                $user = $accesstoken->User()->first();
        }
        if ($request->has('user_id')) {
            $user = Users::whereId($request['user_id'])->first();
        }

        $sportids = Grounds::where('is_disabled', 0)
                    ->where('is_deleted',0)
                    ->whereHas('Venue', function($query) {
                          $query->where('lng','!=',0);
                          $query->where('lat','!=',0);
                          $query->where('is_disabled',0);
                          $query->where('is_verified',1);
                      })->pluck('sports_id');

        if(count($sportids)){
          $sportids = array_unique($sportids->toArray());
        }else{
          $sportids = [];
        }

        if ($user){
            $user_interest_id = $user->Interests()->pluck('id')->toArray();
            $user_interest = $user->Interests()->whereIn('id',$sportids)->orderBy('updated_at','asc')->get();
            $user_interest = iterator_to_array($user_interest, true);
            $sport_list = Sports::whereIn('id',$sportids)->whereNotIn('id',$user_interest_id)->orderBy('name','asc')->get();
            $sport_list = iterator_to_array($sport_list, true);
            $sport_list = array_merge($user_interest,$sport_list);
        } else
            $sport_list = Sports::whereIn('id',$sportids)->orderBy('name','asc')->get();
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Sports List',
                    'success' => $sport_list,
                ]
            );
    }





    /**
     * @return \Illuminate\Http\JsonResponse
     * sports list api
     */
    public function  sportsListGround(Request $request){
        $user = [];
        $user_interest = [];
        $user_interest_id = [];
        if ($request->header('accessToken')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken)
                $user = $accesstoken->User()->first();
        }
        if ($request->has('user_id')) {
            $user = Users::whereId($request['user_id'])->first();
        }

        $sportids = Grounds::where('is_disabled', 0)
                    ->where('is_deleted',0)
                    ->whereHas('Venue', function($query) {
                          $query->where('lng','!=',0);
                          $query->where('lat','!=',0);
                          $query->where('is_disabled',0);
                          $query->where('is_verified',1);
                      })->pluck('sports_id');

        if(count($sportids)){
          $sportids = array_unique($sportids->toArray());
        }else{
          $sportids = [];
        }

        if ($user){
            $user_interest_id = $user->Interests()->pluck('id')->toArray();
            $user_interest = $user->Interests()->whereIn('id',$sportids)->orderBy('updated_at','asc')->get();
            $user_interest = iterator_to_array($user_interest, true);

            $sport_list = Sports::whereNotIn('id',$user_interest_id)->whereIn('id',$sportids)
                ->whereHas('Grounds', function ($query) use ($request) {})
                ->orderBy('name','asc')->get();
            $sport_list = iterator_to_array($sport_list, true);
            $sport_list = array_merge($user_interest,$sport_list);

        } else{
            $sport_list = Sports::whereIn('id',$sportids)->whereHas('Grounds', function ($query) {})
                ->orderBy('name','asc')->get();
        }

        return response()->json(
            [
                'status' => 'success',
                'statusCode' => 200,
                'message' => 'Sports List',
                'success' => $sport_list,
            ]
        );
    }
}
