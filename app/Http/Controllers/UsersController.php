<?php

namespace App\Http\Controllers;
use App\Models\ActivityLog;
use App\Models\Banner;
use App\Models\Levels;
use App\Models\Locations;
use App\Models\Notification;
use App\Models\Search;
use App\Models\SportsHasUsers;
use App\User;
use Illuminate\Support\Facades\Storage;
use Mail;
use Auth;
use App\Models\AccessToken;
use App\Models\DeviceInfo;
use App\Models\Users;
use App\Models\Vendors;
use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use FCM;
use Log;







class UsersController extends Controller
{




    ////////////// ***********************   vendor api's *****************************////////////////

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * User Login API
     */

    public function login(\Illuminate\Http\Request $request){
        if(isset($request['email'], $request['password'], $request['device_id'], $request['device_type'])){

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                if ($request['type'] == 'player')
                    $user = Users::where("email", $request->email)->where('role_id', 3)->first();
                else if ($request['type'] == 'vendor')
                    $user = Users::where("email", $request->email)->where('role_id', 2)->first();
                else
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Please send type for login',
                        ], 500
                    );
                if ($user && $user->role_id != 1) {
                    //create access token
                    $accesstoken = new AccessToken();
                    $accesstoken->access_token = bin2hex(openssl_random_pseudo_bytes(16));
                    $accesstoken->user_id = $user->id;
                    $accesstoken->ttl = '259200';
                    $accesstoken->save();


                    //delete device related to old users
                    $old_device = DeviceInfo::where('device_id', $request['device_id'])->get();
                    foreach ($old_device as $item) {
                        if ($item)
                            $item->delete();
                    }

                    //create device info
                    $deviceinfos = new DeviceInfo();
                    $deviceinfos->device_type = $request['device_type'];
                    $deviceinfos->device_id = $request['id'];
                    $deviceinfos->user_id = $user->id;
                    if(isset($request['fcm_token'])){
                        $deviceinfos->fcm_token = $request['fcm_token'];
                    }
                    $deviceinfos->save();


                    $user->mobile_verified = filter_var($user['mobile_verified'], FILTER_VALIDATE_BOOLEAN);
                    $message = 'Logged in';
                    if ($user->role_id == 3) {
                        if ($user['profile_pic'])
                            $user['profile_pic_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                        else
                            $user['profile_pic_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                        $interest = SportsHasUsers::where('users_id', $user->id)->get();
                        if (sizeof($interest) > 0)
                            $user['interest_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                        else
                            $user['interest_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                        if ($user['lat'] && $user['location_name'] && $user['lng'])
                            $user['location_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                        else
                            $user['location_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);

                        $message = 'Hey, ' . $user->name . ' Welcome back!!';
                    }
                    unset($user['password']);
                    unset($user['remember_token']);
                    unset($user['social_token']);
                    unset($user['role_id']);
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => $message,
                            'success' => $user,
                            'accessToken' => $accesstoken->access_token
                        ]
                    );
                }

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user',
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ], 500
        );
    }



    /*
     * logout user
     */

    public function logout(Request $request){
        Log::info('logout api');
        Log::info($request->all());
        Log::info($request->header('accessToken'));
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = Users::whereId($accesstoken->user_id)->first();
        if ($accesstoken && $user && isset($request['device_id'])){
            $accesstoken->delete();
            $deviceinfo =  DeviceInfo::where('id',$user->id)->where('device_id',$request['device_id'])->orderBy('updated_at','desc')->first();
            if ($deviceinfo)
                $deviceinfo->delete();
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'logged out successfullu',
                ]
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user or device Id missing',
            ],500
        );

    }

    /*
     * forgot password
     */

    public function forgotPassword(Request $request){

        if ($request->has('email')) {
            $user = Users::where('email', $request['email'])->first();
            if ($user) {                
                $mobile = $user['mobile'];
                if ($mobile == Null || $mobile == '' || $mobile == 'null')
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Mobile no. not verified, Please contact support.',
                        ], 500
                    );
                $random_number = mt_rand(100000, 999999);
                $message = 'Your OTP to reset password is ' . $random_number;
                $message = urlencode($message);
                if (strlen($mobile) >= 9){
                  $mobile = "971".substr($mobile, -9);
                }
                $url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
                $message = urlencode($message);
                $ch = curl_init();
                if (!$ch){die("Couldn't initialize a cURL handle");}
                $ret = curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt ($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt ($ch, CURLOPT_POSTFIELDS,
                "User=yowzapp&passwd=53267794&mobilenumber=$mobile&message=$message&sid=yowzap&mtype=N&DR=Y");
                $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $curlresponse = curl_exec($ch);
                $info = curl_getinfo($ch);
                curl_close($ch);
                $data = [
                    'otp' => $random_number,
                ];
                $user->update($data);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'OTP sent to your registered mobile number',
                        'success' => $request['email'],
                        'otp'=>$random_number
                    ]
                );

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user',
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ], 500
        );
    }




    /*
     * forgot password Verify
     */

    public function forgotPasswordVerify(Request $request){

        if ($request->has('email') && $request->has('otp') && $request->has('newPassword')) {
            $user = Users::where('email', $request['email'])->first();
            if ($user) {
                if ($user['otp'] != $request['otp']) {
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'OTP mis-match',
                        ], 500
                    );
                }
                $user['password'] = bcrypt($request['newPassword']);
                $user->update();
                $access_tokens = $user->AccessTokens()->get();
                foreach ($access_tokens as $access_token) {
                    if ($access_token)
                        $access_token->delete();
                }
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Password reset successful',
                    ]
                );

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user',
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * reset password API
     */
    public function resetPassword(Request $request){
        if ($request->has('user_id'))
            $user = Users::where('id',$request['user_id'])->first();
        else
            $user = Users::where('id',$request['id'])->first();
        if ($request->hasHeader("accessToken") && $request->header('accessToken') != '') {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken)
                $user = Users::whereId($accesstoken->user_id)->first();
        }
        if (!$user)
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found',
                ],500
            );
        if ($user && $request['newPassword'] && $request['oldPassword'] ){
            if (password_verify($request['oldPassword'], $user->password)) {
                $user['password'] = bcrypt($request['newPassword']);
                $user->update();
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'password reset successfully',
                    ]
                );

            }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Old password doesn\'t match',
                    ],500
                );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid Json schema',
            ],500
        );
    }




    /*
     * vendor signup API
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function vendorRegister(Request $request)
    {
        if (isset($request['email'], $request['name'], $request['password'])) {

            $user = Users::whereEmail($request['email'])->first();
                if($user) {
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'User already exits',
                        ],500
                    );
                }
                    $data['email'] = $request['email'];
                    $data['password'] = bcrypt($request['password']);
                    $data['name'] = $request['name'];
                    // role set to vendor
                    $data['role_id'] = 2;
                    $data['mobile_verified'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);;
                    $createdUser = Users::create($data);

                    //delete device related to old users
                    $old_device = DeviceInfo::where('device_id',$request['device_id'])->get();
                    foreach ($old_device as $item) {
                        if ($item)
                            $item->delete();
                    }
                    //create device info
                    $deviceinfos = new DeviceInfo();
                    $deviceinfos->device_type=  $request['device_type'];
                    $deviceinfos->device_id= $request['device_id'];
                    $deviceinfos->user_id = $createdUser->id;
                    $deviceinfos->save();


                    unset($createdUser['password']);
                    unset($createdUser['remember_token']);
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Vendor created successfully.',
                            'success' => $createdUser,
                        ]
                    );

        }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'JSON schema failed',
                ],500
            );

    }

    /*
     * vendor mobile verify
     *
     */


    public function mobileVerification(Request $request)
    {
        // Log::info($request);
        if (isset($request['id'], $request['mobile']) || isset($request['user_id'], $request['mobile'])) {
            $phone = $request['mobile'];
            if ($request->has('user_id'))
                $user = Users::where('id',$request['user_id'])->first();
            else
                $user = Users::where('id',$request['id'])->first();

            if ($user){
            $random_number = mt_rand(100000, 999999);
            $message = 'Your OTP ' . $random_number . ' to verify registration';

            if (strlen($phone) >= 9){
              $phone = "971".substr($phone, -9);
            }

            // dd($phone);

            $existMobileUser = Users::where('mobile',$phone)->first();

            if($existMobileUser)
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Mobile No. already exist',
                    ],500
                );

            $url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
            $message = urlencode($message);
            $ch = curl_init();
            if (!$ch){die("Couldn't initialize a cURL handle");}
            $ret = curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt ($ch, CURLOPT_POSTFIELDS,
            "User=yowzapp&passwd=53267794&mobilenumber=$phone&message=$message&sid=yowzap&mtype=N&DR=Y");
            $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $curlresponse = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);

            $data['otp'] = $random_number;

            if($user->mobile_verified != 1){
                $data['mobile'] = $phone;
            }

            $user->update($data);
                return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'OTP sent to '.$phone,
                    'OTP' => $random_number
                ]);
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not exist',
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );
    }



    /*
     * vendor otp verify
     *
     */
    public function otpVerification(Request $request)
    {
        // Log::info('-------- otp verification');
        // Log::info($request->all());
        if (isset($request['otp'],$request['id']) || isset($request['otp'],$request['user_id'])) {
            $otp = $request['otp'];
            $phone = $request['mobile'];

            if ($request->has('user_id'))
                $user = Users::where('id',$request['user_id'])->first();
            else
                $user = Users::where('id',$request['id'])->first();
            if (!$user){
                return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not exits',
                ],500
            );}

            if ($user['otp'] == $otp){

                if (strlen($phone) >= 9){
                  $phone = "971".substr($phone, -9);
                }

                // $user['mobile'] = $phone;
                $user['mobile_verified'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                $user->save();

                // Log::info($user);
                return response()->json(
                    [
                        'status' => 'success',
                        'statuscode' => 200,
                        'message' => 'OTP verification successfull',
                        'OTP' => $user['otp']
                    ]
                );

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Oops!! OTP mismatch',
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );
    }


    /*
     * vendor resend OTP API
     *
     */

    public function resendOtp(Request $request)
    {
        if(isset($request['id'],$request['mobile'])||isset($request['user_id'],$request['mobile'])) {
            $phone = $request['mobile'];
            if ($request->has('user_id'))
                $user = Users::where('id',$request['user_id'])->first();
            else
                $user = Users::where('id',$request['id'])->first();
            $random_number = mt_rand(100000, 999999);
            $message = 'Your OTP ' . $random_number . ' to verify registration';
            if (strlen($phone) >= 9){
              $phone = "971".substr($phone, -9);
            }
            $url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
            $message = urlencode($message);
            $ch = curl_init();
            if (!$ch){die("Couldn't initialize a cURL handle");}
            $ret = curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt ($ch, CURLOPT_POSTFIELDS,
            "User=yowzapp&passwd=53267794&mobilenumber=$phone&message=$message&sid=yowzap&mtype=N&DR=Y");
            $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $curlresponse = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            $data = [
                'otp' =>$random_number,
                'mobile' => $phone
            ];
            $user->update($data);
            return response()->json(
                [
                    'status' => 'success',
                    'statuscode' => 200,
                    'message' => 'OTP resent succesful',
                     'OTP' => $user['otp']
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );
    }



    /*
     * email exits or not API
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function emailExits(Request $request){
        if($request->get('email')){
            $user = Users::where("email", $request->email)->first();
            if (!$user)
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'email not exits'
                    ]
                );
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'email already exits'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }


    /**
     * get user profile
     */

    public function getUser(Request $request)
    {
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = Users::whereId($accesstoken->user_id)->first();
        if($user) {

            $user->mobile_verified = filter_var($user['mobile_verified'], FILTER_VALIDATE_BOOLEAN);
            $notification_count = 0;
            if ($user->role_id == 3){
                $points = $user->Points()->where('type',1)->pluck('point')->toArray();
                $points = array_sum($points);
                $old_level = $user['level_id'];
                $level = Levels::where('points_from','<=',$points)->where('points_to','>=',$points)->first();
                if ($level){
                    $user['level_id'] = $level->id;
                    $user['cover_pic'] = $level['badge_cover'];
                    $user->update();
                }
                $user['points'] = $points;
                if ($old_level<$user['level_id']){
                    $old_level =  Levels::whereId($old_level)->first();
                    $user['level_up'] = true;
                    $level_details['level_from'] = $old_level['level_name'];
                    $level_details['level_to'] = $level['level_name'];
                    $level_details['badge_icon'] = $level['badge_icon'];
                    $level_details['badge_cover'] = $level['badge_cover'];
                    $level_details['content'] = 'Congratulations!! '.$user['name'].' moved from level '.$old_level['level_name'].' to '.$level['level_name'];
                    $user['level_details'] = $level_details;

                    $activity_log = new ActivityLog();
                    $activity_log['user_id'] = $user['id'];
                    $activity_log['type_id'] = $level->id;
                    $activity_log['type'] = 3;
                    $activity_log['content'] = $user->name.' moved from level '.$old_level['level_name'].' to '.$level['level_name'];
                    $act_date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $act_currenttime_notification_time =  $act_date->format('Y-m-d H:i:s');
                    $activity_log['date'] = $act_currenttime_notification_time;
                    $activity_log->save();

                }else{
                    $user['level_up'] = false;
                    $level_details = [];
                    $user['level_details'] = json_encode($level_details);
                }

                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                $current_time_date =  $date->format('Y-m-d H:i');


                $expiredGames =  $user->Games()
                    ->where('date','<',$current_time_date)
                    ->where('status',1)->wherePivot('is_super_admin',1)
                    ->whereHas('Bookings', function($query)  {
                        $query->where('payment_status',0);
                    })->orderBy('date','desc')->get();


                foreach ($expiredGames as $game){
                    if ($game) {
                        $game->Players()->detach();
                        $game->Teams()->detach();
                        $notifications = $game->Notifications()->where('type', 'LIKE', '%GME%')->get();
                        foreach ($notifications as $notification) {
                            if ($notification)
                                $notification->delete();
                        }
                        $activities = $game->Activities()->where('type', 2)->get();
                        foreach ($activities as $activity) {
                            if ($activity)
                                $activity->delete();
                        }
                        $game->Search()->where('type', 3)->delete();
                        $game['status'] = 0;
                        $game->update();
                    }
                }

                $rated_venues = $user->RateVenue()->pluck('game_id')->toArray();
                $venue_to_rate = $user->Games()
                    ->whereNotIn('id',$rated_venues)
                    ->where('date','<',$current_time_date)
                    ->where('status',1)->wherePivot('is_super_admin',1)
                    ->whereHas('Bookings', function($query)  {
                        $query->where('payment_status',1);
                    })->with('Ground.Venue')->orderBy('date','desc')->get();

                $user['rating'] = false;
                $rating_details = [];
                $user['rating_details'] = json_encode($rating_details);

                if ($venue_to_rate &&  $user['level_up'] == false){
                    if (sizeof($venue_to_rate)>0) {
                        $game = $venue_to_rate[0];
                        $venue_to_rate = $venue_to_rate->pluck('ground')->pluck('venue');
                        if (sizeof($venue_to_rate) > 0) {
                            $venue = $venue_to_rate[0];
                            $user['rating'] = true;
                            $rating_details['venue_id'] = $venue->id;
                            $rating_details['game_id'] = $game['id'];
                            $rating_details['content'] = 'You just completed playing at ' . $venue->location_name . ' for game ' . $game->name;
                            $rating_details['venue_name'] = $venue->name;
                            $user['rating_details'] = $rating_details;
                        }
                    }
                }

                $notification_count = $user->Notifications()->where('user_id',$user->id)->where('is_read',0)->where('viewed',0)->count();

                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                $challenge_notifications = Notification::where('user_id',$user->id)
                    ->where('user_id',$user->id)
                    ->where('created_at','<',$currenttime_notification_time)
                    ->where('type','GMER')
                    ->where('status',0)
                    ->get();

                foreach ($challenge_notifications as $value){
                    $value['status'] = 2;
                    $value->update();

                    $challenged_player = Users::whereId($value['more'])->first();

                    if ($challenged_player){
                        $notification = new Notification();
                        $notification['date'] = $currenttime_notification_time;
                        $notification['type'] = 'GMERR'    ;
                        $notification['user_id'] = $challenged_player['id'];
                        $notification['content'] = 'Your challenge to '.$user->name.' got expired';
                        $notification['more'] = $user->id;
                        $notification['status'] = 0;
                        $notification['is_read'] = 0;
                        $challenged_player['pending_notification'] = 1;
                        $challenged_player->update();
                        $device = $challenged_player->Device()->where('user_id',$challenged_player->id)->orderBy('id','desc')->first();
                        $device_token_array = '';
                        if ($device && $challenged_player['notificaiton'] == 1)
                            $device_token_array = $device['fcm_token'];
                        if ($device_token_array) {
                            $data = "Challenge Expired";
                            $notification_type = 11;
                            $data = array(
                              'title' => 'Challenge Expired',
                              'notification_type' => $notification_type,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,'Your challenge to '.$user->name.' got expired',$data, $type);
                        }
                    }
                }

                if ($user['pending_notification'] == 0){
                    $notification_count = 0;
                }
                if ($user['profile_pic']) {
                    $user['profile_pic_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                }else
                    $user['profile_pic_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                $interest = SportsHasUsers::where('users_id',$user->id)->get();
                if (sizeof($interest)>0)
                    $user['interest_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                else
                    $user['interest_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                if ($user['lat']&&$user['location_name']&&$user['lng'])
                    $user['location_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                else
                    $user['location_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);

            }elseif ($user->role_id == 2){
                $notification_count = $user->Notifications()->where('user_id',$user->id)->where('is_read',0)->where('viewed',0)->count();
                if ($user['pending_notification'] == 0){
                    $notification_count = 0;
                }
                unset($user['bio']);
                unset($user['level_id']);
                unset($user['points']);
                unset($user['primary_team']);
                unset($user['prefer_location']);
                unset($user['cover_pic']);
                unset($user['lat']);
                unset($user['lng']);
                unset($user['is_social']);
                unset($user['is_private']);
                unset($user['location_name']);
                unset($user['available_at']);
            }
            $user['notification_count'] = $notification_count;
            unset($user['password']);
            unset($user['remember_token']);
            unset($user['social_token']);
            unset($user['role_id']);
            unset($user['otp']);
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'User details',
                    'success' => $user,
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'User not exits',
            ],500
        );
    }



    /**
     * edit user profile
     */

    public function editUser(Request $request)
    {
      // dd($request);
      // return response()->json(
      //     [
      //         'status' => 'success',
      //         'message' => $request,
      //     ], 200
      // );
        if(isset($request['profile_pic'])||isset($request['name'])||isset($request['interests'])||isset($request['lat'])||isset($request['lng'])||isset($request['bio'])||isset($request['mobile'])||isset($request['location_name'])||isset($request['is_private'])) {

            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = Users::whereId($accesstoken->user_id)->first();
            if ($user) {

                if ($request->file('profile_pic')) {
                    $file = $request->file('profile_pic');
                    $origin_name = $file->getClientOriginalName();
                    $date = date_create();
                    $timeStamp = date_timestamp_get($date);
                    $key = 'vendors/' . str_replace(' ', '_', $user->name) . $timeStamp . '/' . $origin_name;
                     if ($request->has('player'))
                        $key = 'players/' . str_replace(' ', '_', $user->name) . $timeStamp . '/' . $origin_name;
                    if ($file) {
                        Storage::disk('s3')->put($key, fopen($file, 'r+'), 'public');
                        $url = Storage::disk('s3')->url($key);
                        $user['profile_pic'] = $url;
                    } else {
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'file name is empty',
                            ], 500
                        );
                    }
                }else if ($request->has('profile_pic')){
                    $user['profile_pic'] = $request['profile_pic'];
                }



                if ($request->has('location_name'))
                    $user['location_name']=  $request['location_name'];
                if ($request->has('lat'))
                    $user['lat']=  $request['lat'];
                if ($request->has('lng'))
                    $user['lng']=  $request['lng'];
                if ($request->has('bio'))
                    $user['bio']=  $request['bio'];
                if ($request->has('mobile')){
                    $user['mobile']=  $request['mobile'];
                    $user['mobile_verified'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                }
                if ($request->has('is_private')){
                    if ($request->is_private == 'true') {
                        $user['is_private'] = filter_var(1, FILTER_VALIDATE_BOOLEAN);
                    } else {
                        $user['is_private'] = filter_var(0, FILTER_VALIDATE_BOOLEAN);
                    }

                }
                if ($request->has('interests')){
//                    $interestList = json_decode(json_encode($request['interests']), true);
                    $interestList = array($request['interests']);
                    $user->Interests()->detach();

                    foreach($interestList as $value) {
                      foreach($value as $inValue){
                        // dd($value);
                          $sports_has_many = $user->Interests()->wherePivot('sports_id',$inValue['id'])->first();
                          if (!$sports_has_many)
                              $user->Interests()->attach($user->id,['sports_id'=>$inValue['id']]);
                      }
                    }
                }
                if ($request->has('name')) {
                    $user['name'] = $request['name'];

                if ($user['role_id'] == 3){


                        if ($request->has('player')) {
                            $search = Search::where('id', $user->id)->where('type', 1)->first();
                            if (!$search){
                                $search = new Search();
                                $search['id'] = $user->id;
                                $search['type'] = 3;
                            }
                            $search['name'] = $user->name;
                            $search->save();
                        }
                    }

                    if ($request->has('team_id')) {
                        $user['primary_team'] = $request['team_id'];
                    }
                    if ($request->has('availability')) {
                        $availability = json_decode(json_encode($request['availability']), true);
                        $user['available_at'] =  json_encode($availability, true);
                    }

                    if ($request->has('prefer_location')) {
                        $prefer_location = json_decode(json_encode($request['prefer_location']), true);
                        $prefer_location = implode(",",$prefer_location);
                        $user['prefer_location'] = $prefer_location;
                    }
                }
                $user->update();
                $user->mobile_verified = filter_var($user['mobile_verified'], FILTER_VALIDATE_BOOLEAN);
                if ($user->role_id == 3){
                    if ($user['profile_pic'])
                        $user['profile_pic_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                    else
                        $user['profile_pic_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    $interest = SportsHasUsers::where('users_id',$user->id)->get();
                    if (sizeof($interest)>0)
                        $user['interest_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                    else
                        $user['interest_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                    if ($user['lat']&&$user['location_name']&&$user['lng'])
                        $user['location_updated'] = filter_var('1', FILTER_VALIDATE_BOOLEAN);
                    else
                        $user['location_updated'] = filter_var('0', FILTER_VALIDATE_BOOLEAN);
                }
                unset($user['password']);
                unset($user['remember_token']);
                unset($user['social_token']);
                unset($user['role_id']);

            return response()->json(
                [
                    'status' => 'success',
                    'statuscode' => 200,
                    'message' => 'Profile update successfully',
                    'success' => $user
                ]
            );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not exits',
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed',
            ],500
        );
    }


    public function  bannerList(){

        $banner_list = Banner::orderBy('id','asc')->get();
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'banner List',
                    'success' => $banner_list
                ]
            );
    }


}
