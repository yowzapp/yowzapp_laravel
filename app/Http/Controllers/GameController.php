<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\ActivityLog;
use App\Models\Bookings;
use App\Models\Games;
use App\Models\Grounds;
use App\Models\Notification;
use App\Models\Points;
use App\Models\Report;
use App\Models\Search;
use App\Models\Sports;
use App\Models\Teams;
use App\Models\TeamTimeline;
use App\Models\Transaction;
use App\Models\Users;     //
use App\Models\Settings;
use App\Models\SportsHasUsers;
use App\Models\DeviceInfo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Log;

class GameController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Create Game
     */
    public function createGame(Request $request)
    {
        if (isset($request['name'], $request['sport_id'], $request['ground_details'], $request['is_private'], $request['members'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) { 
                $game = [];
                if ($request->has('game_id')){
                    $game = Games::whereId($request['game_id'])->where('status',1)->first();
                }
                if (!$game)
                $game = new Games();
                $game['name'] = $request['name'];
                $game['sport_id'] = $request['sport_id'];
                $sport = Sports::where('id',$request['sport_id'])->first();
                $game['cover_pic'] = $sport['thumbnail'];
                $game['discription']  = '';
                if ($request->has('discription'))
                    $game['discription'] = $request['discription'];
                if ($request->is_private == 'true') {
                    $game->is_private = 1;
                } else {
                    $game->is_private = 0;
                }

                $ground_details = json_decode(json_encode($request['ground_details']), true);
                if (!isset($ground_details['total_price'])){
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Total price missing'
                        ],500
                    );
                }
                $ground = Grounds::where('id', $ground_details['ground_id'])->where('is_deleted',0)->first();
                if ($ground) {
                    $venue = $ground->Venue()->first();
                    $vendor = $venue->Vendor()->first();
                    $phone_no = $user['mobile'];
                    $vendor_id = '';
                    if ($vendor)
                        $vendor_id = $vendor->id;
                    $game['ground_id'] = $ground_details['ground_id'];
                    $slot = $ground->Slot()->where('ground_id', $ground_details['ground_id'])->first();
                    $booked_date = '';
                    $no_players = $ground_details['no_players'];
                    $game['max_members'] = $no_players;
                    $game['status'] = 1;
                    $game['is_completed'] = 0;
                    $price = $slot['slot_price'];
                    $duration = $slot['slot_duration'];
                    $bookslots = json_decode(json_encode($ground_details['slots']), true);
                    $transaction_identifier = 'YZPBKG_'.bin2hex(openssl_random_pseudo_bytes(10));
                    $booking_ids = array();
                    if (sizeof($bookslots)>0){
                        $start_time =  strtotime ( $bookslots[0]['start_time']);
                        $start_time = date('H:i', $start_time);
                        $game['start_time'] = $start_time;
                    }
                    $game->save();

                    // to be added here if has team id it would be added here to the team.

                    if($request->has('team_id') && $request->team_id != ''){
                        $game->Teams()->attach($request->team_id);
                    }
                    foreach($bookslots as $value) {
                        $date = date("Y-m-d", strtotime($value['date']));
                        $start_time =  strtotime ($value['start_time']);
                        $end_time =  strtotime ($value['end_time']);
                        $start_time = date('H:i', $start_time);
                        $end_time = date('H:i', $end_time);
                        $game['date'] = date('Y-m-d H:i', strtotime("$date $end_time"));
                        $game['end_time'] = $end_time;
                        $booking = Bookings::where('slot_id', $value['id'])->where('date',$date)->where('start_time',$start_time)->where('end_time',$end_time)->where('payment_status',1)->get();
                        if (count($booking)==0){
                            $booking = new Bookings();
                            $booking['date'] = $date;
                            $booked_date = $date;
                            $booking['start_time'] =$start_time;
                            $booking['end_time'] = $end_time;
                            $booking['slot_price'] = $price;
                            $booking['slot_duration'] = $value['duration'];
                            $booking['payment_status'] = 0;
                            $booking['slot_id'] = $value['id'];
                            $booking['user_id'] = $user['id'];
                            $booking['game_id'] = $game['id'];
                            $booking['vendor_id'] = $vendor_id;
                            $booking['mobile'] = $phone_no;
                            $booking['player_name'] = $user->name;
                            $booking['no_players'] = $no_players;
                            $booking['transaction_identifier'] = strtoupper($transaction_identifier);
                            $booking->save();
                            $booking_ids[] = $booking['id'];
                        }else{
                            $game->delete(); 
                            return response()->json(
                                [
                                    'status' => 'error',
                                    'message' => 'Slots already booked'
                                ],500
                            );
                        }
                    }
                    $game->update();
                    $transaction = new Transaction();
                    $transaction['user_id'] = $user['id'];
                    $transaction['amount'] = $ground_details['total_price'];
                    $transaction['transaction_identifier'] =  strtoupper($transaction_identifier);;
                    $transaction['booking_id'] = implode(', ', $booking_ids);
                    $transaction['details'] = 'Payment Pending';
                     $transaction->save();
                    $members = json_decode(json_encode($request['members']), true);
                    $players_members = json_decode(json_encode($members['players']), true);
                    $teams_members = json_decode(json_encode($members['teams']), true);
                    if ($teams_members){
                        foreach ($teams_members as $value){
                            $team = Teams::where('id',$value)->first();
                            if ($team){
                                $team_timeline = new TeamTimeline();
                                $current_date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                $current_date =  $current_date->format('Y-m-d');
                                $team_timeline['team_id'] = $team->id;
                                $team_timeline['date'] = $current_date;
                                $team_timeline ['content'] = 'joined game '.$game->name;
                                $team_timeline->save();
                                $game->Teams()->attach($game->id,['team_id'=>$value]);
                                $team_members_id = $team->Players()->pluck('id')->toarray();
                                $players_members = array_merge($players_members,$team_members_id);
                            }
                        }
                    }

                    $players_members = array_unique($players_members);

                    // Accept challenge notification
                    $challenge_notification  = $user->Notifications()->where('type','GMER')->where('status',0)->where('type_id',$request['sport_id'])->first();
                    if ($challenge_notification) {
                        if (in_array($challenge_notification['more'], $players_members)) {
                            $challenged_player = Users::whereId($challenge_notification['more'])->first();
                            $challenge_date  = date("Y-m-d", strtotime($challenge_notification['created_at']));
                            if ((strtotime($booked_date) == strtotime($challenge_date))&& $challenged_player) {
                                $challenge_notification['is_read'] = 1;
                                $challenge_notification['status'] = 1;
                                $challenge_notification['content'] = 'You accepted challenge';
                                $challenge_notification['type_id'] = $game->id;
                                $challenge_notification->update();

                                $notification = new Notification();
                                $current_date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                $current_date =  $current_date->format('Y-m-d H:i:s');
                                $notification['date'] = $current_date;
                                $notification['type'] = 'GMER';
                                $notification['user_id'] = $challenged_player->id;
                                $notification['content'] = $user->name.' accepted your challenge, Get ready for battle';
                                $notification['type_id'] = $game->id;
                                $notification['status'] = 1;
                                $notification->save();
                                $challenged_player['pending_notification'] = 1;
                                $challenged_player->update();
                                $device = $challenged_player->Device()->where('user_id', $challenged_player->id)->orderBy('id', 'desc')->first();
                                if ($device && $challenged_player['notificaiton'] == 1){
                                    $device_token_array[] = $device['fcm_token'];
                                    $data = "Game Challenge";
                                    $notification_type = 6;
                                    $data = array(
                                      'title' => 'Game Challenge',
                                      'notification_type' => $notification_type,
                                      'game_id' => $game->id,
                                      'game_name' => $game->name,
                                      'user_id' => $user->id,
                                      'user_name' => $user->name,
                                      'profile_pic' => $user->profile_pic,
                                      'email' => $user->email
                                    );
                                    sendNotificationwithBody($device_token_array,$user->name.' accepted your challenge, Get ready for battle and it\'s Game On',$data,$notification_type,$user->id);
                                }
                                $game->Players()->attach($game->id,['user_id'=>$challenged_player->id,'points' => 0,'is_admin' => 0,'is_super_admin' => 0,'invitation_status' => 1]);
                                $players_members = array_diff($players_members,[$challenged_player->id]);
                            }
                        }
                    }

                    $game->Players()->attach($game->id,['user_id'=>$user->id,'points' => 0,'is_admin' => 1,'is_super_admin' => 1,'invitation_status' => 1]);
                    $device_token_array = array();
                    foreach ($players_members as $value){
                            if ($value != $user['id']){
                                $player = Users::where('id',$value)->first();
                                if ($player) {
                                    $device = $player->Device()->where('user_id', $value)->orderBy('id', 'desc')->first();
                                    if ($device && $player['notificaiton'] == 1 ){
                                        $settings = $player->Setting()->first();
                                        if ($settings['game_invite'] == 1){
                                            $device_token_array[] = $device['fcm_token'];
                                        }
                                    }
                                }
                                $notification = new Notification();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                                $notification['date'] = $currenttime_notification_time;
                                $notification['type'] = 'GME-PA';
                                $notification['user_id'] = $player->id;
                                $notification['content'] = ('You have been invited to game ' . $game->name . ' by ' .$user->name);
                                $notification['type_id'] = $game->id;
                                $notification['status'] = 0;
                                $notification->save();
                                $player['pending_notification'] = 1;
                                $player->update();
                                $game->Players()->attach($game->id,['user_id'=>$value,'points' => 0,'is_admin' => 0,'is_super_admin' => 0,'invitation_status' => 0]);
                            }
                    }
                    $device_token_array = array_filter($device_token_array);
                    if ($device_token_array) {
                        $data = "Game invite";
                        $notification_type = 6;
                        $data = array(
                          'title' => 'Game invite',
                          'notification_type' => $notification_type,
                          'game_id' => $game->id,
                          'game_name' => $game->name,
                          'user_id' => $user->id,
                          'user_name' => $user->name,
                          'profile_pic' => $user->profile_pic,
                          'email' => $user->email
                        );
                        sendNotificationwithBody($device_token_array,'You have been invited to game ' . $game->name . ' by ' . $user->name,$data,$notification_type,$user->id);
                    }
                    $game['members'] = $game->Players()->wherePivot('user_id','!=',$user->id)->get();
                    $game['invited_members'] = $game->Players()->wherePivot('user_id','!=',$user->id)->orderby('name', 'asc')->get();
                    $search = new Search();
                    $search['id'] = $game->id;
                    $search['name'] = $game->name;
                    $search['type'] = 3;
                    $search->save();

                    $activity_log = new ActivityLog();
                    $activity_log['user_id'] = $user->id;
                    $activity_log['type_id'] = $game->id;
                    $activity_log['type'] = 2;
                    $activity_log['content'] = $user->name.' has created a new game '.$game->name;
                    $act_date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $act_currenttime_notification_time =  $act_date->format('Y-m-d H:i:s');
                    $activity_log['date'] = $act_currenttime_notification_time;
                    $activity_log->save();

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Game created successfully',
                            'success' => $game,
                            'date' =>  $booked_date
                        ]
                    );
                }else{
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'No Ground found'
                        ], 500
                    );
                }


            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Edit Game
     */


    public function editGame(Request $request)
    {

        if (isset($request['game_id'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $game = Games::where('id',$request['game_id'])->where('status',1)->first();
                if (!$game) {
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Game not found'
                        ], 500
                    );
                }
                if ($request->has('is_private')){
                    if ($request->is_private == 'true') {
                        $game->is_private = 1;
                    } else {
                        $game->is_private = 0;
                    }
                    $game->update();
                }
                if ($request->has('members')){
                    $members = json_decode(json_encode($request['members']), true);
                    $players_members = json_decode(json_encode($members['players']), true);
                    $teams_members = json_decode(json_encode($members['teams']), true);
                    $teams_members = array_unique($teams_members);
                    if ($teams_members){
                        foreach ($teams_members as $value){
                            $team = Teams::where('id',$value)->first();
                            if ($team){
                                $is_team_der = $game->Teams()->wherePivot('team_id',$team->id)->wherePivot('game_id',$game->id)->first();
                                if (!$is_team_der){
                                    $game->Teams()->attach($game->id,['team_id'=>$value]);
                                }

                                $team_members_id = $team->Players()->pluck('id')->toarray();
                                $players_members = array_merge($players_members,$team_members_id);
                            }
                        }
                    }

                    $players_members = array_unique($players_members);
                        $device_token_array = array();
                        foreach ($players_members as $value) {
                                $is_player_der = $game->Players()->wherePivot('user_id',$value)->wherePivot('game_id',$game->id)->first();
                            if (!$is_player_der) {
                                $player = Users::where('id', $value)->first();
                                if ($player) {
                                    $device = $player->Device()->where('user_id', $value)->orderBy('updated_at', 'desc')->first();
                                    if ($device && $player['notificaiton'] == 1){
                                        $settings = $player->Setting()->first();
                                        if ($settings['game_invite'] == 1){
                                            $device_token_array[] = $device['fcm_token'];
                                        }
                                    }
                                $notification = new Notification();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                                $currenttime_notification_time = $date->format('Y-m-d H:i:s');
                                $notification['date'] = $currenttime_notification_time;
                                $notification['type'] = 'GME-PA';
                                $notification['user_id'] = $player->id;
                                $notification['content'] = ('You have been invited to game ' . $game->name . ' by ' . $user->name);
                                $notification['type_id'] = $game->id;
                                $notification['status'] = 0;
                                $notification->save();
                                $player['pending_notification'] = 1;
                                $player->update();
                                $game->Players()->attach($game->id,['user_id' => $value,'points' => 0, 'is_admin' => 0, 'is_super_admin' => 0, 'invitation_status' => 0]);
                                }
                            }
                        }
                    $device_token_array = array_filter($device_token_array);
                    if ($device_token_array) {
                        $data = "Game edit";
                        $notification_type = 6;
                        $data = array(
                          'title' => 'Game edit',
                          'notification_type' => $notification_type,
                          'game_id' => $game->id,
                          'game_name' => $game->name,
                          'user_id' => $user->id,
                          'user_name' => $user->name,
                          'profile_pic' => $user->profile_pic,
                          'email' => $user->email
                        );
                        sendNotificationwithBody($device_token_array,'You have been invited to game ' . $game->name . ' by ' . $user->name,$data,$notification_type, $user->id);
                    }
                    $game->update();
                    $game['members'] = $game->Players()->wherePivot('user_id','!=',$user->id)->get();
                    $game['invited_members'] = $game->Players()->wherePivot('user_id','!=',$user->id)->orderby('name', 'asc')->get();
                }
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Game updated successfully',
                        'success' => $game,
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Game details
     */

    public function  gameDetails(Request $request)
    {
        if ($request->has('game_id')) {
            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
            $currenttime =  strtotime($date->format('H:i:s'));
            $currentdate =  $date->format('Y-m-d');
            $user = [];
            if ($request->header('accessToken')) {
                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                if ($accesstoken)
                    $user = $accesstoken->User()->first();
            }
                $game = Games::whereId($request['game_id'])->where('status',1)->first();
                if ($game) {
                    $ground = $game->Ground()->first();
                    if($ground){
                    $venue = $ground->Venue()->first();
                    $players_count = $game->Players()->wherePivot('invitation_status', 1)->count();
                    $sport = $game->Sport()->first();
                    $game['sport_name'] = $sport["name"];
                    $game['no_members'] = $players_count;
                    $game['location'] = $venue['location_name'];
                    $payment_required = false;
                    $is_requested = false;
                    $game['showRSVP'] = false;
                    $game['is_mine'] = false;
                    $game['is_member'] = false;
                    $game['can_join'] = false;
                    $game['is_super_admin'] = false;
                    $game['mark_attendance'] = false;
                    $game['is_refundable'] = false;
                    if ($user) {
                        $is_admin = $game->Players()->wherePivot('is_admin', 1)->wherePivot('user_id', $user->id)->wherePivot('game_id', $game->id)->first();
                        $is_super_admin = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('is_super_admin', 1)->wherePivot('user_id', $user->id)->first();

                        if ($game['attendance_status'] == 0 && $is_super_admin) {
                            $game['mark_attendance'] = true;
                            if ($game["max_members"] < 3) {
                                $game["max_members"] = $ground['max_member'];
                            }
                        } else {
                            $game['mark_attendance'] = false;
                        }
                        if ($is_admin) {
                            $game['is_mine'] = true;
                            if ($is_super_admin)
                                $game['is_super_admin'] = true;
                            $payee = $game->Players()->wherePivot('invited_to_pay', 1)->first();
                            if (!$payee)
                                $payment_required = true;
                        } else {
                            $game['is_mine'] = false;
                        }

                        $game['payment_required'] = $payment_required;
                        $is_member = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('is_admin', 0)->wherePivot('user_id', $user->id)->first();

                        if ($is_member) {
                            if ($is_member['pivot']['invitation_status'] == 1) {
                                $game['is_member'] = true;
                            } else {
                                $game['is_member'] = false;
                                $game['showRSVP'] = true;
                            }
                        } else
                            $game['is_member'] = false;


                        $is_invited_pay = $game->Players()->wherePivot('invited_to_pay', 1)->wherePivot('user_id', $user->id)->first();
                        if ($is_invited_pay) {
                            $game['is_invited_pay'] = true;
                            $game['can_join'] = false;
                        } else
                            $game['is_invited_pay'] = false;
                        $is_requested = $game->Players()->wherePivot('is_requested', 1)->wherePivot('game_id', $game->id)->wherePivot('user_id', $user->id)->first();
                        if ($is_requested)
                            $is_requested = true;
                        else
                            $is_requested = false;

                    }
                    $slot = $ground->Slot()->first();
                    $slotIds = $ground->Slot()->pluck('id');
                    $bookings = Bookings::whereIn('slot_id',$slotIds)->where('game_id', $game['id'])->groupBy('date')->get();
                    $is_completed = $game['is_completed'];
                    $booked_date = date("Y-m-d", strtotime($game['date']));
                    $start_time = date("H:i:s", strtotime($game['start_time']));
                    $end_time = date("H:i:s", strtotime($game['end_time']));
                    $match_status = '';
                    if (strtotime($booked_date) < strtotime($currentdate)) {
                        $game['is_complete'] = 1;
                        $is_completed = true;
                        $match_status = 'completed';
                    } elseif ($booked_date == $currentdate) {
                        $start_time = strtotime($start_time); //change to strtotime
                        $end_time = strtotime($end_time); //change to strtotime
                        if ($start_time > $currenttime) {
                            $diff = abs($start_time - $currenttime) / 3600;
                            $hours = floor($diff);
                            if ($hours > 0)
                                $match_status = 'starts in ' . floor($diff) . ' hours';
                            else {
                                $match_status = 'starts soon';
                            }
                        } else if ($end_time > $currenttime) {
                            $game['mark_attendance'] = false;
                            $match_status = 'in progress';
                        } else {
                            $match_status = 'completed';
                            $is_completed = true;
                            $game['is_completed'] = 1;
                        }
                    } elseif (strtotime($booked_date) > strtotime($currentdate)) {
                        $datediff = abs(strtotime($booked_date) - strtotime($currentdate));
                        $no_days = floor($datediff / (60 * 60 * 24));
                        if ($no_days > 1)
                            $match_status = 'starts in ' . $no_days . ' days';
                        else
                            $match_status = 'starts in ' . $no_days . ' day';
                    }
                    if (sizeof($bookings) > 0) {
                        $booked = $bookings[0];
                        if ($booked['payment_status'] == 0) {
                            if ($is_completed) {
                                $match_status = 'expired';

                            } elseif ($booked['status'] == 0) {
                                $match_status = 'expired';
                                $game['payment_required'] = false;
                                $game['is_invited_pay'] = false;
                                $game['can_join'] = false;
                            } else
                                $match_status = 'pending';
                        }
                        if ($booked['payment_status'] == 1 || $is_completed) {
                            $game['payment_required'] = false;
                            $game['is_invited_pay'] = false;
                        }
                        $transaction = $booked->Transaction()->where('booking_id', $booked['id'])->where('transaction_identifier', $booked['transaction_identifier'])->first();
                        $game['price'] = $transaction['amount'];
                    }

                    $game['match_status'] = strtoupper($match_status);
                    $game['date'] = date('l jS F ', strtotime($game['date']));
                    $game['raw_date'] = $booked_date;
                    $game['is_completed'] = $is_completed;
                    $game['is_requested'] = $is_requested;
                    $game['start_time'] = date('h:i A', strtotime($game['start_time']));
                    $game['end_time'] = date('h:i A', strtotime($game['end_time']));

                    if ($game['mark_attendance'] && !$game['is_completed'])
                        $game['mark_attendance'] = false;

                    if (!$game['showRSVP'] && !$game['is_private'] && !$game['is_requested'] && !$game['is_completed'] && !$game['is_member'] && !$game['is_mine'] && ($game['no_members'] < $game['max_members'])) {
                        $game['can_join'] = true;
                        $game['showRSVP'] = false;
                    }

                    if ($game['is_requested']) {
                        $game['can_join'] = false;
                        $game['showRSVP'] = false;
                        $game['is_requested'] = true;
                    }
                    if ($game['is_invited_pay'] && !$game['is_completed']) {
                        $game['can_join'] = false;
                        $game['showRSVP'] = true;
                    }

                    $gameDateTime = strtotime(date('Y-m-d H:i', strtotime("$booked_date $start_time")));
                    $currentDateTime = strtotime(date('Y-m-d H:i', strtotime("$currentdate $currenttime")));
                    if ($gameDateTime > $currentDateTime) {
                        $diff = abs($gameDateTime - $currentDateTime) / 3600;
                        $hours = floor($diff);
                        if ($hours > 24)
                            $game['is_refundable'] = true;
                    }

                    /// can shout
                    $game['can_shout'] = false;
                    if ($game['is_super_admin'] || $game['is_mine']) {
                        if (!$game['is_completed']) {
                            if ($game['shout'] == false || $game['shout'] == 0) {
                                if ($game['no_members'] < $game['max_members'])
                                    $game['can_shout'] = true;
                            }

                        }
                    }
                    $game['members'] = $game->Players()->wherePivot('invitation_status', 1)->orderby('name', 'asc')->get();
                    if ($user)
                        $game['invited_members'] = $game->Players()->wherePivot('user_id', '!=', $user->id)->orderby('name', 'asc')->get();
                    else
                        $game['invited_members'] = $game->Players()->orderby('name', 'asc')->get();

                    unset($game['shout']);
                    unset($game['attendance_status']);

                    if (sizeof($bookings) > 0) {
                        $booked = $bookings[0];
                        if ($booked['status'] == 0) {
                            $game['can_shout'] = false;
                            $game['payment_required'] = false;
                            $game['is_invited_pay'] = false;
                            $game['can_join'] = false;
                            $game['showRSVP'] = false;
                            $game['mark_attendance'] = false;
                            $game['is_invited_pay'] = false;
                        }
                    }

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Game details',
                            'success' => $game,
                            'ground_name' => $ground->name,
                            'venue' => $venue->name
                            
                        ]
                    );
                }
            }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Game not found'
                    ], 500
                );


        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Game players
     */

    public function  gamePlayers(Request $request)
    {
        if ($request->has('game_id')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $game = Games::whereId($request['game_id'])->where('status',1)->first();
                if ($game) {

                    $players = $game->Players()->where('id','!=',$user->id)->get();
                    $all_players_final = array();
                    foreach($players as $k=>$player) {

                        $all_players_final[] =  array(
                            'id' => $player["id"],
                            'profile_pic' => $player['profile_pic'],
                            'name' => $player["name"]
                        );

                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Players list',
                            'success' => $all_players_final
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Game not found'
                    ], 500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Mark attendance
     */

    public function  markAttendance(Request $request)
    {
        if (isset($request['game_id'],$request['members'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $game = Games::whereId($request['game_id'])->where('status',1)->first();
                $is_super_admin = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('is_super_admin', 1)->wherePivot('user_id', $user->id)->first();

               // check for completion should come here

                if ($game && $is_super_admin) {
                    $total_teams = $game->Teams()->get();
                    if ($total_teams){
                        foreach ($total_teams as $team){
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currentdate =  $date->format('Y-m-d');
                            $team_timeline = new TeamTimeline();
                            $team_timeline['team_id'] = $team->id;
                            $team_timeline['date'] = $currentdate;
                            $team_timeline ['content'] = 'Completed playing '.$game->name;
                            $team_timeline->save();

                            $points= new Points();
                            $points['type'] = 2;
                            $points['type_id'] = $team->id;
                            $points['more'] = $game->id;
                            $team['points'] = $team['points'] + 4;
                            $team->update();
                            $points['point'] = 4;
                            $points['content'] = 'Played '.$game->name;
                            $points->save();
                        }
                    }



                    $total_members = json_decode(json_encode($request['members']), true);
                    $total_members = array_diff($total_members,[$user->id]);
                    $total_members = array_unique($total_members);
                    if ($total_members){
                        $old_members = $game->Players()->where('id','!=',$user->id)->pluck('id')->toArray();;
                        $new_players = array_diff($total_members, $old_members);
                        $game->Players()->detach();
                        $game->Players()->attach($game->id,['user_id'=>$user->id,'points' => 0,'is_admin' => 1,'is_super_admin' => 1,'invitation_status' => 1]);

                        $points= new Points();
                        $points['type'] = 1;
                        $points['type_id'] = $user->id;
                        $points['more'] = $game->id;
                        $user['points'] = $user['points'] + 4;
                        $user->update();
                        $points['point'] = 4;
                        $points['content'] = 'Played game '.$game->name;
                        $points->save();
                        foreach ($total_members as $value) {
                            if ($value != $user['id']) {
                                $player = Users::where('id', $value)->first();
                                if ($player) {
                                    $game->Players()->attach($game->id, ['user_id' => $value, 'points' => 0, 'is_admin' => 0, 'is_super_admin' => 0, 'invitation_status' => 1]);
                                    $points= new Points();
                                    $points['type'] = 1;
                                    $points['type_id'] = $player->id;
                                    $points['more'] = $game->id;
                                    $player['points'] = $player['points'] + 4;
                                    $player->update();
                                    $points['point'] = 4;
                                    $points['content'] = 'Played '.$game->name;
                                    $points->save();
                                }
                            }
                        }
                        $device_token_array = array();
                        foreach ($new_players as $value) {
                            $player = Users::where('id', $value)->first();
                            if ($player) {
                                $device = $player->Device()->where('user_id', $value)->orderBy('id', 'desc')->first();
                                if ($device && $player['notificaiton'] == 1)
                                    $device_token_array[] = $device['fcm_token'];
                                $notification = new Notification();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                                $currenttime_notification_time = $date->format('Y-m-d H:i:s');
                                $notification['date'] = $currenttime_notification_time;
                                $notification['type'] = 'GME-PY';
                                $notification['user_id'] = $player->id;
                                $notification['content'] = 'You have completed '.$game->name;
                                $notification['type_id'] = $game->id;
                                $notification['status'] = 1;
                                $notification->save();
                                $player['pending_notification'] = 1;
                                $player->update();
                            }
                        }
                        if ($device_token_array) {
                            $data = "Mark attendance";
                            $notification_type = 6;
                            $data = array(
                              'title' => 'Mark attendance',
                              'notification_type' => $notification_type,
                              'game_id' => $game->id,
                              'game_name' => $game->name,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,$user->name.', said that you\'ve played '.$game->name,$data, $notification_type, $user->id);
                        }
                        $game['attendance_status'] = 1;
                        $game['is_completed'] = 1;
                        $game->update();

                        $notifications = $game->Notifications()->where('type','GME-PA')->where('status',0)->get();
                        foreach ($notifications as $notification) {
                            if ($notification)
                                $notification->delete();
                        }



                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'message' => 'Mark attendence complete',
                            ]
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Members not found'
                        ], 500
                    );

                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Game or Super admin not exits'
                    ], 500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Game Listing
     */

    public function  gameListing(Request $request)
    {
        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
        $currenttime =  strtotime($date->format('H:i:s'));
        $currentdate =  $date->format('Y-m-d');
        $user = [];
        if ($request->header('accessToken')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken)
                $user = $accesstoken->User()->first();
        }


        if ($request->has('user_id')) {
            $user = Users::whereId($request['user_id'])->first();
        }
        $my_games_final = array();
        $all_games_final = array();
        $id_list = array();
        $current_time = date('H:i:s',$currenttime);
        $current_date_time = date('Y-m-d H:i:s', strtotime("$currentdate $current_time"));
        $today = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
        $current_date =  $today->format('Y-m-d H:i');

        if ($user){
            $my_games[] = array();
            if ($request->has('is_completed')){

                $my_games = $user->Games()
                    ->where('date','<',$current_date)
                    // ->where('is_completed',0)
                    ->wherePivot('invitation_status',1)
                    ->where('status',1)
                    ->whereHas('Bookings', function($query)  {
                        $query->where('payment_status',1);
                    })->orderBy('date','desc')->paginate(10);

            }else{
                $my_games = $user->Games()
                    ->orderBy('date','desc')->get();
            }
          if (sizeof($my_games)>0)
                $id_list =  $my_games->pluck('id')->toArray();
            $my_mark_attandace_final = array();
            $my_attandance_markers = $user->Games()
                ->whereNotIn('id',$id_list)
                ->where('date','<',$current_date_time)
                ->wherePivot('is_admin',1)
                ->wherePivot('is_super_admin',1)
                ->where('attendance_status',0)
                ->whereHas('Bookings', function($query)  {
                    $query->where('payment_status',1);
                    $query->where('status',1);
                })
                ->orderBy('date','desc')->get();

            if ($my_attandance_markers && !$request->has('is_completed')){
                foreach($my_attandance_markers as $k=>$game) {
                    $ground = $game->Ground()->first();
                    $venue = $ground->Venue()->first();
                    $players_count = $game->Players()
                        ->wherePivot('invitation_status', 1)
                        ->count();
                    $sport = $game->Sport()->first();
                    if ($game["max_members"]  < 3){
                        $game["max_members"] = $ground['max_member'];
                    }
                    $is_private = $game["is_private"];
                    $match_status = 'Hey, Howzatt!!';
                    $is_completed = true;
                    $my_mark_attandace_final[] = array(
                        'id' => $game["id"],
                        'name' => $game["name"],
                        'discription' => $game["discription"],
                        'sport_id' => $sport["id"],
                        'sport_name' => $sport["name"],
                        'cover_pic' => $game["cover_pic"],
                        'is_private' => filter_var($is_private, FILTER_VALIDATE_BOOLEAN),
                        'max_members' => $game["max_members"],
                        'no_members' => $players_count,
                        'location' => $venue['location_name'],
                        'venue' => $venue->name,
                        'match_status' => strtoupper($match_status),
                        'date' => date('jS F', strtotime($game['date'])),
                        //'date' => $game['date'],
                        'ground_name' => $ground->name,
                        'is_completed' => $is_completed,
                        'start_time' => date('h:i A', strtotime($game['start_time'])),
                        'end_time' => date('h:i A', strtotime($game['end_time'])),
                    );
                }
            }

            foreach($my_games as $k=>$game){
                $ground = $game->Ground()->first();                
                $slot = $ground->Slot()->first(); 
                $slotIds = $ground->Slot()->pluck('id');
                $venue = $ground->Venue()->first();
                $players_count = $game->Players()->wherePivot('invitation_status',1)->count();
                $sport = $game->Sport()->first();
                $bookings = Bookings::whereIn('slot_id',$slotIds)->where('game_id', $game['id'])->groupBy('date')->get();
                $is_private = $game["is_private"];
                $match_status = '';
                $is_completed = $game['is_completed'];
                $booked_date = date("Y-m-d", strtotime($game['date']));
                if (strtotime($booked_date)<strtotime($currentdate)){
                    $game['is_completed'] = 1;
                    $notifications = $game->Notifications()->where('type','GME-PA')->where('status',0)->get();
                    foreach ($notifications as $notify){
                        if ($notify)
                            $notify->delete();
                    }
                    $game->update();
                    $is_completed = true;
                    $match_status = 'completed';
                    $is_private = 1;
                }elseif($booked_date==$currentdate){
                    $start_time = date("H:i", strtotime($game['start_time']));
                    $end_time = date("H:i", strtotime($game['end_time']));
                    $start_time    = strtotime ($start_time); //change to strtotime
                    $end_time    = strtotime ($end_time); //change to strtotime
                    if ($start_time>$currenttime){
                        $diff = abs($start_time - $currenttime) / 3600;
                        $hours = floor($diff);
                        if ($hours>0)
                            $match_status = 'starts in '.floor($diff).' hours';
                        else{
                            $match_status = 'starts soon';
                            if ($players_count<$game["max_members"])
                                $is_private = 0;
                        }
                    }else if ($end_time>$currenttime) {
                        $match_status = 'in progress';
                    }else{
                            $match_status = 'completed';
                            $is_private = 1;
                            $is_completed = true;
                            $game['is_completed'] = 1;
                        $notifications = $game->Notifications()->where('type','GME-PA')->where('status',0)->get();
                        foreach ($notifications as $notify){
                            if ($notify)
                                $notify->delete();
                        }
                            $game->update();
                        }
                }elseif(strtotime($booked_date)>strtotime($currentdate)){
                    $datediff = abs(strtotime($booked_date) - strtotime($currentdate));
                    $no_days = floor($datediff / (60 * 60 * 24));
                    if ($no_days>1)
                        $match_status = 'starts in '.$no_days.' days';
                    else
                        $match_status = 'starts in '.$no_days.' day';
                }
                if (sizeof($bookings)>0){
                    $booked = $bookings[0];
                    if ($booked['payment_status'] == 0) {
                        if ($is_completed){
                            $match_status = 'expired';
                            $is_private = 1;
                        }elseif ($booked['status'] == 0){
                            $match_status = 'expired';
                            $is_private = 1;
                        }else
                            $match_status = 'pending';
                    }else{

                        if ($is_completed && $game['attendance_status']==0 && $game['pivot']['is_super_admin'] == 1)
                            $match_status = 'Hey, Howzzat!!!';
                    }
                }
                    $my_games_final[] =  array(
                        'id' => $game["id"],
                        'name' => $game["name"],
                        'discription' => $game["discription"],
                        'sport_id' => $sport["id"],
                        'sport_name' => $sport["name"],
                        'cover_pic' => $game["cover_pic"],
                        'is_private' =>filter_var( $is_private , FILTER_VALIDATE_BOOLEAN),
                        'max_members' =>$game["max_members"],
                        'no_members' =>$players_count,
                        'location' => $venue['location_name'],
                        'venue' => $venue->name,
                        'match_status' => strtoupper($match_status),
                        'date' =>date('jS F', strtotime($game['date'])),
                        //'date' => $game['date'],
                        'ground_name' => $ground->name,
                        'is_completed' => $is_completed,
                        'start_time' => date('h:i A', strtotime($game['start_time'])),
                        'end_time' => date('h:i A', strtotime($game['end_time'])),
                    );

            }
            if ($request->has('is_completed')){
                $final['my_games'] = $my_games_final;
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Game listing',
                        'success' => $final,
                        'currentPage' => $my_games->currentPage(),
                        'hasMorePages' => $my_games->hasMorePages()

                    ]
                );
            }
            $my_games_final = array_merge($my_mark_attandace_final,$my_games_final);
        }

        //whereNotIn('id',$id_list)->

        if ($id_list)
            $all_games = Games::whereNotIn('id',$id_list)->where('date','>=',$current_date_time)->where('status',1)->orderBy('date','desc')->paginate(10);
        else
            $all_games = Games::where('date','>=',$current_date_time)->orderBy('date','desc')->where('status',1)->paginate(10);

        foreach($all_games as $k=>$game) {
            $is_private = $game["is_private"];
            $ground = $game->Ground()->first();
            $slot = $ground->Slot()->first();
            $slotIds = $ground->Slot()->pluck('id');
            $sport = $game->Sport()->first();
            $venue = $ground->Venue()->first();
            $players_count = $game->Players()->wherePivot('invitation_status',1)->count();
            $bookings = Bookings::whereIn('slot_id',$slotIds)->where('game_id', $game['id'])->groupBy('date')->get();
            $match_status = '';
            $is_completed = $game['is_completed'];
            $booked_date = date("Y-m-d", strtotime($game['date']));
            if (strtotime($booked_date)<strtotime($currentdate)){
                $game['is_completed'] = 1;
                $notifications = $game->Notifications()->where('type','GME-PA')->where('status',0)->get();
                foreach ($notifications as $notify){
                    if ($notify)
                        $notify->delete();
                }
                $game->update();
                $is_completed = true;
                $match_status = 'completed';
                $is_private = 1;
            }elseif($booked_date==$currentdate){
                $start_time = date("H:i", strtotime($game['start_time']));
                $end_time = date("H:i", strtotime($game['end_time']));
                $start_time    = strtotime ($start_time); //change to strtotime
                $end_time    = strtotime ($end_time); //change to strtotime
                if ($start_time>$currenttime){
                    $diff = abs($start_time - $currenttime) / 3600;
                    $hours = floor($diff);
                    if ($hours>0)
                        $match_status = 'starts in '.floor($diff).' hours';
                    else{
                        $match_status = 'starts soon';
                        if ($players_count<$game["max_members"])
                            $is_private = 0;
                    }
                }else if ($end_time>$currenttime) {
                    $match_status = 'in progress';
                }else{
                    $match_status = 'completed';
                    $is_private = 1;
                    $is_completed = true;
                    $game['is_completed'] = 1;
                    $notifications = $game->Notifications()->where('type','GME-PA')->where('status',0)->get();
                    foreach ($notifications as $notify){
                        if ($notify)
                            $notify->delete();
                    }
                    $game->update();
                }
            }elseif(strtotime($booked_date)>strtotime($currentdate)){
                $datediff = abs(strtotime($booked_date) - strtotime($currentdate));
                $no_days = floor($datediff / (60 * 60 * 24));
                if ($no_days>1)
                    $match_status = 'starts in '.$no_days.' days';
                else
                    $match_status = 'starts in '.$no_days.' day';
            }
            if (sizeof($bookings)>0){
                $booked = $bookings[0];
                if ($booked['payment_status'] == 0) {
                    if ($is_completed){
                        $match_status = 'expired';
                        $is_private = 1;
                    }elseif ($booked['status'] == 0){
                        $match_status = 'expired';
                        $is_private = 1;
                    }else
                        $match_status = 'pending';
                }
            }
            $all_games_final[] =  array(
                'id' => $game["id"],
                'name' => $game["name"],
                'discription' => $game["discription"],
                'sport_id' => $sport["id"],
                'sport_name' => $sport["name"],
                'cover_pic' => $game["cover_pic"],
                'is_private' =>filter_var( $is_private , FILTER_VALIDATE_BOOLEAN),
                'max_members' =>$game["max_members"],
                'no_members' =>$players_count,
                'location' => $venue['location_name'],
                'venue' => $venue->name,
                'match_status' => strtoupper($match_status),
                'date' =>date('jS F', strtotime($game['date'])),
//                'date' => $game['date'],
                'ground_name' => $ground->name,
                'is_completed' => $is_completed,
                'start_time' => date('h:i A', strtotime($game['start_time'])),
                'end_time' => date('h:i A', strtotime($game['end_time'])),
            );


        }
        $final['my_games'] = $my_games_final;
        $final['all_games'] = $all_games_final;
        return response()->json(
            [
                'status' => 'success',
                'statusCode' => 200,
                'message' => 'Game listing',
                'success' => $final,
            ]
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     * Request to joinGame
     */

    public function  joinGame(Request $request)
    {

        if (isset($request['game_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $game =Games::where('id',$request['game_id'])->where('status',1)->first();
                if ($game){
                    $player_exists = $game->Players()->wherePivot('user_id', $user->id)->wherePivot('game_id', $game->id)->first();
                    if ($player_exists)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Player already exists in team'
                            ],500
                        );
                    $game->Players()->attach($game->id,['user_id'=>$user->id,'points' => 0,'is_admin' => 0,'is_super_admin' => 0,'invitation_status' => 0,'is_requested' => 1]);
                    $admin = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('is_super_admin', 1)->first();
                    $device = $admin->Device()->where('user_id',$admin->id)->orderBy('updated_at','desc')->first();
                    $notification = Notification::where('user_id',$admin->id)->where('type','GME-PR')->where('type_id',$game->id)->where('more',$user->id)->first();
                    if ($notification){
                        $notification->delete();
                    }
                    if ($device && $admin['notificaiton'] == 1)
                        $device_token_array[] = $device['fcm_token'];
                    $notification = new Notification();
                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                    $notification['date'] = $currenttime_notification_time;
                    $notification['type'] = 'GME-PR'    ;
                    $notification['user_id'] = $admin->id;
                    $notification['content'] = $user->name.' has requested to join your game '.$game->name;
                    $notification['type_id'] = $game->id;
                    $notification['more'] = $user->id;
                    $notification['status'] = 0;
                    $notification['is_read'] = 0;
                    $notification->save();
                    $admin['pending_notification'] = 1;
                    $admin->update();
                    $device = $admin->Device()->where('user_id',$admin->id)->orderBy('id','desc')->first();
                    $device_token_array = '';
                    if ($device  && $admin['notificaiton'] == 1)
                        $device_token_array = $device['fcm_token'];
                    if ($device_token_array) {
                        $data = "Join game";
                        $notification_type = 6;
                        $data = array(
                          'title' => 'Join game',
                          'notification_type' => $notification_type,
                          'game_id' => $game->id,
                          'game_name' => $game->name,
                          'user_id' => $user->id,
                          'user_name' => $user->name,
                          'profile_pic' => $user->profile_pic,
                          'email' => $user->email
                        );
                        sendNotificationwithBody($device_token_array,$user->name.' requested to join your game '.$game->name,$data, $notification_type, $user->id);
                    }

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Requested to join game successfully'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Game not found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );

    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     * Shout for game
     */

    public function  shoutForGame(Request $request)
    {

        if (isset($request['game_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $game = $user->Games()->where('id',$request['game_id'])->first();                
                        
                if ($game) {
                    if ($game['shout'] == 0 || $game['shout'] == false){
                        $game['shout'] = 1;                        
                        $game->update();
                        $player_id = $game->Players()->pluck('id')->toArray();
                        $ground = $game->Ground()->first();
                        $games = $ground->Games()->with(['Players' => function($query) use($player_id,$game) {
                            $query->wherePivot('invitation_status',1);
                            $query->whereNotIn('id',$player_id);
                            $query->whereHas('Interests', function($query) use($game) {
                                $query->where('id',$game['sports_id']);
                            });
                        }])->get();                        
                        
                                                    
                        // return $games;
                        $device_token_array = array();
                        // foreach ($games as $game){

                            $usersShout = SportsHasUsers::where('sports_id',$game->sport_id)->pluck('users_id');

                            $players = User::whereIn('id',$usersShout)->get();
                            // $players = $game['Players']; 
                            foreach ($players as $player){
                                $device = DeviceInfo::where('user_id',$player->id)->orderBy('id', 'desc')->first();
                                if ($device && $player['notificaiton'] == 1){
                                    $settings = Settings::where('user_id',$player->id)->first();
                                    if ($settings['open_game'] == 1){
                                        $device_token_array[] = $device['fcm_token'];
                                        $data = "Shout for game";
                                        $notification_type = 6;
                                        $data = array(
                                            'title' => 'Shout for game',
                                            'notification_type' => $notification_type,
                                            'game_id' => $game->id,
                                            'game_name' => $game->name,
                                            'user_id' => $player->id,
                                            'user_name' => $player->name,
                                            'profile_pic' => $player->profile_pic,
                                            'email' => $player->email
                                        );
                                        
                                        sendNotificationwithBody($device_token_array,'Hey, Check this out',$data, $notification_type, $player->id);
                                    
                                    }
                                }
                                $notification = new Notification();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                                $currenttime_notification_time = $date->format('Y-m-d H:i:s');
                                $notification['date'] = $currenttime_notification_time;
                                $notification['type'] = 'GME-PY';
                                $notification['user_id'] = $player->id;
                                $notification['content'] = 'Hey '.$player['name'].', check this game';
                                $notification['type_id'] = $game->id;
                                $notification['status'] = 1;
                                $notification->save();
                                $player['pending_notification'] = 1;
                                $player->update();
                            }
                        // }
                        
                    }

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Shouted for clan'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Game not found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );

    }




    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     * Request to game
     */

    public function  requestGame(Request $request)
    {

        if (isset($request['sport_id'],$request['user_id'],$request['date'],$request['from'],$request['to'],$request['location_name'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            Log::info($request->all());
            if ($user) {
                $sport = Sports::where('id',$request['sport_id'])->first();
                $player = Users::whereId($request['user_id'])->where('role_id',3)->first();
                if ($sport && $player){
                        $date_on = date("M jS", strtotime($request['date']));
                        $time_on = date("H:i:s", strtotime($request['from']));
                        $created_at = date('Y-m-d H:i:s', strtotime("$date_on $time_on"));
                        $location_name = explode(',', $request['location_name']);
                        if (sizeof($location_name)>1)
                            $location_name = implode(',',array_slice($location_name, 0, 2));
                        else
                            $location_name = implode(',',$location_name);
                        $notification = new Notification();
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                        $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                        $notification['created_at'] = $created_at;
                        $notification['date'] = $currenttime_notification_time;
                        $notification['type'] = 'GMER'    ;
                        $notification['user_id'] = $player->id;
                        $notification['content'] = $user->name.' challenged you for a game '.$sport->name.' on '.$date_on.' between '.$request['from'].' to '.$request['to'].' near locatioin: '.$location_name;
                        $notification['type_id'] = $sport->id;
                        $notification['more'] = $user->id;
                        $notification['status'] = 0;
                        $notification['is_read'] = 0;
                        $notification->save();
                        $player['pending_notification'] = 1;
                        $player->update();
                        $device = $player->Device()->where('user_id',$player->id)->orderBy('id','desc')->first();
                        $device_token_array = '';
                        if ($device && $player['notificaiton'] == 1){
                            $settings = $player->Setting()->first();
                            if ($settings['challenge_game'] == 1){
                                $device_token_array = $device['fcm_token'];
                            }
                        }

                        if ($device_token_array) {
                            $data = "Request game";
                            $notification_type = 10;
                            $data = array(
                              'title' => 'Request game',
                              'notification_type' => $notification_type,
                              'game_id' => "",
                              'game_name' => "",
                              'sport_id' => $sport->id,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,$user->name.' challenged you for a game this weekend for '.$sport->name.'near '.$request['location_name'].' between '.$request['form'].' to '.$request['to'],$data,  $notification_type, $user->id);

                        }


                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Requested for a game successfully'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Sports or player not found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );

    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * delete Game
     */

    public function  deleteGame(Request $request)
    {

        if (isset($request['game_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $game = $user->Games()->where('id',$request['game_id'])->wherePivot('is_super_admin', 1)->first();
                if ($game){
                    $ground = $game->Ground()->first();
                    $venue = $ground->Venue()->first();
                    $game->Players()->detach();
                    $game->Teams()->detach();
                    $notifications = $game->Notifications()->where('type', 'LIKE', '%GME%')->get();
                    foreach ($notifications as $notification){
                        if ($notification)
                             $notification->delete();
                    }
                    $activities = $game->Activities()->where('type',2)->get();
                    foreach ($activities as $activity){
                        if ($activity)
                            $activity->delete();
                    }
                    $game->Search()->where('type',3)->delete();
                    $game['status'] = 0;
                    $game->update();


                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $currenttime =  strtotime($date->format('H:i:s'));
                    $currentdate =  $date->format('Y-m-d');
                    $current_time = date('H:i:s',$currenttime);
                    $current_date_time = date('Y-m-d H:i:s', strtotime("$currentdate $current_time"));
                    if ($game['date'] >= $current_date_time){
                     // notify vendor about the cancelation

                        $more = array(
                            $venue['name'],
                            $venue['id'],
                            $ground['name'],
                            $ground['id'],
                            date('Y-m-d', strtotime($game['date']))
                        );
                        $more = implode(",",$more);

                        $book_updated = $game->Bookings()->first();
                        if ($book_updated) {
                            $vendor = $book_updated->Vendor()->first();
                            $ground = $game->Ground()->first();

                            $notification = new Notification();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                            $currenttime = $date->format('Y-m-d H:i:s');
                            $notification['date'] = $currenttime;
                            $notification['type'] = 'VDR-CNCL';
                            $notification['content'] = $user->name . ' has canceled his booking for your ground ' . $ground->name . ' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time']));
                            $notification['type_id'] = $ground->id;
                            $notification['more'] = $more;
                            $notification['status'] = 0;
                            $notification['user_id'] = $vendor->id;
                            $notification->save();
                            $vendor['pending_notification'] = 1;
                            $vendor->update();
                            $device_token_array = '';
                            $device = $vendor->Device()->where('user_id', $vendor->id)->orderBy('id', 'desc')->first();
                            if ($device)
                                $device_token_array = $device['fcm_token'];
                            if ($device_token_array) {
                                $data = "Delete Game";
                                $notification_type = 6;
                                $data = array(
                                  'title' => 'Delete game',
                                  'notification_type' => $notification_type,
                                  'game_id' => $game->id,
                                  'game_name' => $game->name,
                                  'ground_name' => $ground->name,
                                  'user_id' => $user->id,
                                  'user_name' => $user->name,
                                  'profile_pic' => $user->profile_pic,
                                  'email' => $user->email
                                );
                                sendNotificationwithBody($device_token_array, $user->name . ' has canceled his booking from your ground ' . $ground->name . ' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time'])), $data, $notification_type, $user->id);
                            }

                            // update vendor report

                            $report = new Report();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currentdate =  $date->format('Y-m-d');
                            $content = $user->name.' canceled his booking from your ground '. $ground->name .' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time']));
                            $report ['content'] = $content;
                            $report['date'] = $currentdate;
                            $report['vendor_id'] = $vendor->id;
                            $report->save();

                        }

                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Game deleted successfully'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Game not found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * upload image Game
     */

    public function uploadImage(Request $request){

        if($request->file('image')){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $file = $request->file('image');
                $origin_name = $file->getClientOriginalName();
                $date = date_create();
                $timeStamp = date_timestamp_get($date);
                $key = 'players/games/' . str_replace(' ', '_', $user->name) . $timeStamp . '/' . $origin_name;
                Storage::disk('s3')->put($key, fopen($file, 'r+'), 'public');
                $url = Storage::disk('s3')->url($key);
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Image uploaded',
                        'success' => $url
                    ]
                );
            }return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ],500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Image missing'
            ],500
        );

    }
}
