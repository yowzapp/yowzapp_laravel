<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\Bookings;
use App\Models\Games;
use App\Models\Grounds;
use App\Models\Notification;
use App\Models\Report;
use App\Models\Transaction;
use App\Models\Users;
use App\Models\Venues; 
use App\Models\Slot;
use App\Models\Sports;
use App\Models\Range;
use Carbon\Carbon;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Log;
use FCM;


class BookingController extends Controller
{


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Book ground
     */

    public function  bookGround(Request $request){
        if (isset($request['ground_id'],$request['slots'],$request['slot_id'],$request['no_players'],$request['total_price'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $ground = Grounds::where('id', $request['ground_id'])->where('is_deleted',0)->first();
                if ($ground) {
                    $venue = $ground->Venue()->first();
                    $vendor = $venue->Vendor()->fist();
                    $vendor_id = '';
                    $phone_no = $user['mobile'];
                    if ($vendor)
                        $vendor_id = $vendor->id;
                    $slot = $ground->Slot()->where('ground_id', $request['ground_id'])->first();
                    $booked_date = '';
                    $no_players = $request['no_players'];
                    $price = $slot['slot_price'];
                    $duration = $slot['slot_duration'];
                    $bookslots = json_decode(json_encode($request['slots']), true);
                    $transaction_identifier = 'YZPBKG_'.bin2hex(openssl_random_pseudo_bytes(10));
                    $booking_ids = array();
                    foreach($bookslots as $value) {
                        $slotPrice = Slot::whereId($value['slot_id'])->first();
                        $date = date("Y-m-d", strtotime($value['date']));
                        $start_time =  strtotime ($slotPrice['start_time']);
                        $end_time =  strtotime ($slotPrice['end_time']);
                        $start_time = date('H:i', $start_time);
                        $end_time = date('H:i', $end_time);
                        $booking = Bookings::where('slot_id', $value['slot_id'])->where('date',$date)->where('start_time',$start_time)->where('end_time',$end_time)->where('payment_status',1)->where('status',1)->get();
                        if (count($booking)==0){
                            $booking = new Bookings();
                            $booking['date'] = $date;
                            $booked_date = $date;
                            $booking['start_time'] =$start_time;
                            $booking['end_time'] = $end_time;
                            $booking['slot_price'] = $slot['slot_price'];
                            $booking['slot_duration'] = $value['duration'];
                            $booking['payment_status'] = 0;
                            $booking['slot_id'] = $value['slot_id'];
                            $booking['user_id'] = $user['id'];
                            $booking['no_players'] = $no_players;
                            $booking['vendor_id'] = $vendor_id;
                            $booking['mobile'] = $phone_no;
                            $booking['player_name'] = $user->name;
                            $booking['transaction_identifier'] = strtoupper($transaction_identifier);
                            $booking->save();
                            $booking_ids[] = $booking['id'];
                        }else{
                            return response()->json(
                                [
                                    'status' => 'error',
                                    'message' => 'Slots already booked'
                                ],500
                            );
                        }
                    }
                    $transaction = new Transaction();
                    $transaction['user_id'] = $user['id'];
                    $transaction['amount'] = $request['total_price'];
                    $transaction['transaction_identifier'] =  strtoupper($transaction_identifier);;
                    $transaction['booking_id'] = implode(', ', $booking_ids);
                    $transaction['details'] = 'Payment Pending';
                    $transaction->save();

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Slots booked',
                            'date' => $booked_date
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No ground found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Vendor create booking
     */

    public function  vendorBookGround(Request $request){
        
        if (isset($request['ground_id'],$request['slots'],$request['slot_id'],$request['player_name'],$request['date'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $ground = Grounds::where('id', $request['ground_id'])->where('is_deleted',0)->first(); 
                if ($ground) {
                    $venue = $ground->Venue()->first();
                    $vendor = $venue->Vendor()->first();
                    $vendor_id = '';
                    $phone_no = '';
                    if ($request->has('mobile'))
                        $phone_no = $request['mobile'];


                    // create a array of dummmy 7 objects
                    $dates_array = [0,0,0,0,0,0,0];
                    $date = date("Y-m-d", strtotime($request['date']));
                    $day_index_array = [0,0,0,0,0,0,0];
                    $dw = date( "w",strtotime($date));


                    for ($k = 0 ; $k < 7; $k++){
                        $dw_new = date( "w",strtotime($date));
                        $dates_array[$dw_new] = $date;
                        $date = date('Y-m-d', strtotime($date . ' + 1 day'));
                    }
                    $content = 'You booked ground '.$ground->name.' for '.$request['player_name'].' on '.date('jS F', strtotime($request['date']));
                    if ($request->has('repeat_weekly')){
                        if ($request->repeat_weekly == 'true' || $request->repeat_weekly == true ) {
                            $day_index_array[$dw] = 1;
                            $content = 'You booked ground '.$ground->name.' for '.$request['player_name'].' every week from '.date('jS F', strtotime($request['date']));
                        }
                    }else if ($request->has('repeat_daily') ){
                        if ($request->repeat_daily == 'true'|| $request->repeat_daily == true) {
                            $day_index_array = [1,1,1,1,1,1,1];
                            $content = 'You booked ground '.$ground->name.' for '.$request['player_name'].' every day from '.date('jS F', strtotime($request['date']));
                        }
                    }else if ($request->has('repeat_custom')){
                        $days = json_decode(json_encode($request['repeat_custom']), true);
                        $day_index_array = $days;
                        $content = 'You booked ground '.$ground->name.' for '.$request['player_name'].' on his preferred days from '.date('jS F', strtotime($request['date']));
                        if (sizeof($days)<7)
                            $day_index_array = [0,0,0,0,0,0,0];
                    }

                    if ($request->has('repeat_weekly') || $request->has('repeat_daily') || $request->has('repeat_custom')) {

                        for ($k = 0; $k < 7; $k++) {
                            if ($day_index_array[$k] == 1) {
                                for ($j = 0; $j < 10; $j++) {
                                    if ($vendor)
                                        $vendor_id = $vendor->id;
                                    $payment_status = 0;
                                    if ($vendor_id == $user->id)
                                        $payment_status = 1;
                                    $slot = $ground->Slot()->where('ground_id', $request['ground_id'])->first();
                                    $no_players = $ground['max_member'];
                                    $price = $slot['slot_price'];
                                    $duration = $slot['slot_duration'];
                                    $bookslots = json_decode(json_encode($request['slots']), true);
                                    $transaction_identifier = 'YZPBKG_' . bin2hex(openssl_random_pseudo_bytes(10));
                                    $booking_ids = array();
                                    foreach ($bookslots as $value) { 
                                        $slotPrice = Slot::whereId($value['slot_id'])->first();                                       
                                        $date = date("Y-m-d", strtotime($dates_array[$k]));
                                        $start_time = strtotime($value['start_time']);
                                        $end_time = strtotime($value['end_time']);
                                        $start_time = date('H:i', $start_time);
                                        $end_time = date('H:i', $end_time);
                                        $booking = Bookings::where('slot_id', $slot->id)->where('date', $date)
                                        // ->where('start_time', $start_time)->where('end_time', $end_time)
                                        ->where('payment_status', 1)->where('status',1)->get();
                                        if (count($booking) == 0) {
                                            $slotDetails = Slot::whereId($slot->id)->first();
                                            $booking = new Bookings();
                                            $booking['date'] = $date;
                                            $booking['start_time'] = date('H:i', strtotime($slotPrice['start_time']));
                                            $booking['end_time'] = date('H:i', strtotime($slotPrice['end_time']));
                                            $booking['slot_price'] = $slotPrice['slot_price'];
                                            $booking['slot_duration'] = $value['duration'];
                                            $booking['payment_status'] = $payment_status;
                                            $booking['slot_id'] = $value['slot_id'];
                                            $booking['user_id'] =  $user['id'];
                                            $booking['no_players'] = $no_players;
                                            $booking['vendor_id'] = $vendor_id;
                                            $booking['mobile'] = $phone_no;
                                            $booking['player_name'] = $request['player_name'];
                                            $booking['is_repeated'] = 1;
                                            $booking['transaction_identifier'] = strtoupper($transaction_identifier);
                                            $booking->save();
                                            $booking_ids[] = $booking['id'];
                                        } else {
                                            return response()->json(
                                                [
                                                    'status' => 'error',
                                                    'message' => 'Slots already booked'
                                                ], 500
                                            );
                                        }
                                    }
                                    $dates_array[$k] = date('Y-m-d', strtotime($dates_array[$k] . '+ 1 week'));
                                    $transaction = new Transaction();
                                    $transaction['user_id'] = $user['id'];
                                    $transaction['amount'] = $slot['slot_price'] * sizeof($bookslots);
                                    $transaction['transaction_identifier'] = strtoupper($transaction_identifier);;
                                    $transaction['booking_id'] = implode(', ', $booking_ids);
                                    $transaction['details'] = 'Payment mode cash';
                                    $transaction->save();

                                }
                            }

                        }
                    }else{
                        if ($vendor)
                            $vendor_id = $vendor->id;
                        $payment_status = 0;
                        if ($vendor_id == $user->id)
                            $payment_status = 1;
                        $slot = $ground->Slot()->where('ground_id', $request['ground_id'])->first();
                        $no_players = $ground['max_member'];
                        $price = $slot['slot_price'];
                        $duration = $slot['slot_duration'];
                        $bookslots = json_decode(json_encode($request['slots']), true);
                        $transaction_identifier = 'YZPBKG_' . bin2hex(openssl_random_pseudo_bytes(10));
                        $booking_ids = array();
                        
                        foreach ($bookslots as $value) {
                            $slotPrice = Slot::whereId($value['slot_id'])->first();
                            // dd($slotPrice);
                            $date = date("Y-m-d", strtotime($value['date']));
                            $start_time = strtotime($value['start_time']);
                            $end_time = strtotime($value['end_time']);
                            $start_time = date('H:i', $start_time);
                            $end_time = date('H:i', $end_time);
                            $booking = Bookings::where('slot_id', $slot->id)
                                ->where('date', $date)->where('start_time', $start_time)
                                ->where('end_time', $end_time)
                                ->where('payment_status', 1)
                                ->where('status',1)
                                ->get();

                             
                            if (count($booking) == 0) {                                  
                            
                                $booking = new Bookings();
                                $booking['date'] = $date;
                                $booking['start_time'] = date('H:i', strtotime($slotPrice['start_time']));
                                $booking['end_time'] = date('H:i', strtotime($slotPrice['end_time']));
                                $booking['slot_price'] = $slotPrice['slot_price'];
                                $booking['slot_duration'] = $slotPrice['slot_duration'];
                                $booking['payment_status'] = $payment_status;
                                $booking['slot_id'] = $value['slot_id'];
                                $booking['user_id'] = $user['id'];
                                $booking['no_players'] = $no_players;
                                $booking['vendor_id'] = $vendor_id;
                                $booking['mobile'] = $phone_no;
                                $booking['player_name'] = $request['player_name'];
                                $booking['is_repeated'] = 0;
                                $booking['transaction_identifier'] = strtoupper($transaction_identifier);
                                $booking->save();
                                $booking_ids[] = $booking['id'];
                            } else {
                                return response()->json(
                                    [
                                        'status' => 'error',
                                        'message' => 'Slots already booked'
                                    ], 500
                                );
                            }
                        }
                        $transaction = new Transaction();
                        $transaction['user_id'] = $user['id'];
                        $transaction['amount'] = 0;
                        $transaction['transaction_identifier'] = strtoupper($transaction_identifier);;
                        $transaction['booking_id'] = implode(', ', $booking_ids);
                        $transaction['details'] = 'Payment mode cash';
                        $transaction->save();
                    }


                    // update vendor report

                    $report = new Report();
                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $currentdate =  $date->format('Y-m-d');
                    $report ['content'] = $content;
                    $report['date'] = $currentdate;
                    $report['vendor_id'] = $vendor->id;
                    $report->save();



                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Slots booked',

                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No ground found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Book Listing Vendor
     */

    public function  vendorBookingList(Request $request){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                $date =  $date->format('Y-m-d');
                $bookings = $user->VendorBookings()->where('payment_status',1)->where('status',1)->where('date',date("Y-m-d", strtotime($date)))->orderBy('date','asc')->get();

                $final_booking_array = array();

                if (sizeof($bookings) == 0){
                    $bookings_new = $user->VendorBookings()->where('payment_status',1)->where('status',1)->where('date','>',date("Y-m-d", strtotime($date)))->orderBy('date','asc')->first();
                    if ($bookings_new){
                        $date = $bookings_new['date'];
                    }
                }

                // $venues = $user->Venues()->orderBy('name','asc')->get();

                $venues = Venues::where('vendor_id',$user->id)->get();

               

                foreach ($venues as $venue) {

                    $grounds = $venue->Grounds()->orderBy('name','asc')->get();

                   

                    $ground_details_array = array();

                    foreach ($grounds as $ground){

                        $slot_details_array = array();

                        $day = date("l",strtotime($date));
                        $dayId = config('app.dayIds')[$day];

                        $rangeIds = $ground->Ranges()->where('day_id',$dayId)->pluck('id');
                        $sport = Sports::whereId($ground->sports_id)->first();
                        $bookings_count = 0;
                        
                        if(count($rangeIds)){

                          $slots = $ground->Slot()->whereIn('range_id',$rangeIds)->get();
                            
                          if(count($slots)){

                              $slotIds = $ground->Slot()->whereIn('range_id',$rangeIds)->pluck('id');

                              $bookings_count = Bookings::whereIn('slot_id',$slotIds)
                                  ->where('payment_status',1)
                                  ->where('status',1)
                                  ->where('date',date("Y-m-d", strtotime($date)))
                                  ->count(); 

                              foreach ($slots as $key => $slot) {

                          $bookingsData = $slot->Bookings()
                              ->where('payment_status',1)
                              ->where('status',1)
                              ->where('date',date("Y-m-d", strtotime($date)))
                              ->count();

                          $start_time = strtotime ($slot['start_time']); //change to strtotime
                          $end_time = strtotime ($slot['end_time']);//change to strtotime
                          $duration = $slot['slot_duration'];
                          $start_time = date('h:i A',$start_time);
                          $end_time = date('h:i A',$end_time );

                          $slot_details_array[] = [
                            'id'=>$slot['id'],
                            'is_booked'=>$bookingsData ? true : false,
                            'duration'=>$duration,
                            'duration'=>$slot['slot_price'],
                            'start_time'=>$start_time,
                            'end_time'=>$end_time,
                          ];

                        }

                            }
                            $ground_details_array[] = array(
                                'id'=>$ground['id'],
                                'name'=>$ground['name'],
                                'cover_pic'=>$sport['thumbnail'],
                                'booked_slots'=>$bookings_count,
                                'free_slots'=>$slotIds->count(),
                                'sport_type'=>$sport['name'],
                                'slot'=>$slot_details_array
                            );
                          }

                        
                    }

                    $venue_details['name'] = $venue['name'];
                    $venue_details['id'] = $venue['id'];
                    $venue_details['grounds'] = $ground_details_array;
                    $final_booking_array[] = $venue_details;

                }

                $raw_date = $date;
                if (strtotime($date) == strtotime("today"))
                    $date =  "Today";
                else if (strtotime($date) ==  strtotime("tomorrow"))
                    $date = "Tomorrow";
                else
                    $date =  date('jS F', strtotime($date));

                return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Booking List',
                            'date' => $date,
                            'raw_date' => $raw_date,
                            'success' => $final_booking_array
                        ]
                    );
                }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }





    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Book Slot Listing Vendor
     */

    public function  vendorSlotsList(Request $request){
        
        if (isset($request['ground_id'],$request['date'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $ground = Grounds::where('id', $request['ground_id'])->where('is_deleted',0)->first();
                if (!$ground ) {
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Ground not found'
                        ], 500
                    );
                }

                $day = date('l', strtotime($request['date']));
                $day_id = config('app.dayIds')[$day];
                
                $ranges = Range::where('day_id',$day_id)->where('ground_id',$request['ground_id'])->pluck('id');

                $slot = $ground->Slot()->first();
                $slots = Slot::whereIn('range_id',$ranges)->get();               

                $array_of_free_slots = array();
                $array_of_booked_slots = array();

                if($slots->count()){
                
                foreach ($slots as $key => $slot) {

                    $bookings = Bookings::where('date', date("Y-m-d", strtotime($request['date'])))
                        ->where('slot_id',$slot['id'])
                        ->where('payment_status',1)
                        ->where('status',1)
                        ->orderBy('start_time','asc')->get();

                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $currenttime =  strtotime($date->format('H:i:s'));
                    $currentdate =  $date->format('d-m-Y');
                    $date = date('d-m-Y',strtotime($request['date']));
                    $duration = $slot['slot_duration'];

                    $start_time    = strtotime ($slot['start_time']); //change to strtotime
                    $end_time      = strtotime ($slot['end_time']);//change to strtotime

                    $next_time = $end_time;

                    if ($bookings->count()> 0){
                       
                            $value = Bookings::where('slot_id',$slot->id)
                                ->where('date', date("Y-m-d", strtotime($request['date'])))
                                ->where('slot_id',$slot['id'])
                                ->where('payment_status',1)
                                ->where('status',1)
                                // ->where('start_time',date("H:i:s", $start_time))
                                ->first();

                            if ($value) {
                                    $array_of_booked_slots[] =
                                        array(
                                            'id' => $value['id'],
                                            'price' => $value['slot_price'],
                                            'slot_duration' => $value['slot_duration'],
                                            'player_name' => $value['player_name'],
                                            'mobile' => $value['mobile'],
                                            'date' => $date,
                                            'slot_id' => $slot['id'],
                                            'start_time' => date("g:i a", $start_time),
                                            'end_time' => date("g:i a", $next_time),
                                        );
                            }else{
                                if (!($start_time > $currenttime && $date == $currentdate) || strtotime($date) > strtotime($currentdate)) {
                                    $array_of_free_slots[] =
                                        array(
                                            'date' => $date,
                                            'slot_id' => $slot['id'],
                                            'start_time' => date("g:i a", $start_time),
                                            'end_time' => date("g:i a", $next_time),
                                            'slot_price' => $slot['slot_price'],
                                            'slot_duration' => $slot['slot_duration']
                                        );
                                }
                            }
                    }else{
                        if (($start_time > $currenttime && $date == $currentdate) || strtotime($date) > strtotime($currentdate)) {
                            $array_of_free_slots[] =
                                array(
                                    'date' => $date,
                                    'slot_id' => $slot['id'],
                                    'start_time' => date("g:i a", $start_time),
                                    'end_time' => date("g:i a", $next_time),
                                    'slot_price' => $slot['slot_price'],
                                    'slot_duration' => $slot['slot_duration'],
                                );
                        }
                    }

                }

                $ground['date'] = $date;
                $ground['slot_id'] = $slot['id'];
                $ground['slot_price'] = $slot['slot_price'];
                $ground['slot_duration'] = ($duration/60).' mins';
                $ground['free_slots'] = $array_of_free_slots;
                $ground['booked_slots'] = $array_of_booked_slots;
            }else{
                $ground['date'] = $request['date'];
                $ground['slot_id'] = 0;
                $ground['slot_price'] = 0;
                $ground['slot_duration'] = '0 mins';
                $ground['free_slots'] = [];
                $ground['booked_slots'] = [];
            }
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Ground details',
                        'success' => $ground
                    ]
                );

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }




    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Book Slot Details Vendor
     */

    public function  slotDetails(Request $request){

        if (isset($request['booking_id'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $booking = Bookings::where('id',$request['booking_id'])->first();
                if (!$booking)
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Booking not found'
                        ], 500
                    );

                $slot = $booking->Slot()->first();
                $transaction = $booking->Transaction()->first();
                if (!$transaction)
                    $transaction['details'] = 'NOT AVAILABLE';

                $booking['start_time'] =  date("g:i a", strtotime($booking['start_time']));
                $booking['end_time'] =  date("g:i a", strtotime($booking['end_time']));
                $booking['date'] =  date("jS F Y", strtotime($booking['end_time']));
                $booking['payment_status'] = filter_var($booking["payment_status"], FILTER_VALIDATE_BOOLEAN);
                $booking['payment_mode'] = $transaction['details'];
                if ($booking['game_id'] == Null)
                    $booking['can_cancel'] = true;
                else
                    $booking['can_cancel'] = false;

                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Booking details',
                        'success' => $booking
                    ]
                );

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Cancel Booking
     */

    public function  cancelBooking(Request $request){

        if (isset($request['booking_id'])) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $booking = Bookings::where('id',$request['booking_id'])->first();
                if (!$booking)
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Booking not found'
                        ], 500
                    );
                if ($booking['game_id'] != Null)
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Booking cannot be canceled'
                        ], 500
                    );
                $all_boookings = Bookings::where('transaction_identifier',$booking['transaction_identifier'])->get();
                foreach ($all_boookings as $booked) {
                    if ($booked) {
                        $booked['status'] = 0;
                        $booked->update();
                    }
                }

                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Booking cancelled successfully'
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * invite other user to pay for game.
     */

    public function inviteToPay(Request $request)
    {

        if (($request->has('user_id') && $request->has('game_id'))) {

            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            $invidted_user = Users::where('id',$request['user_id'])->first();

            if ($user) {
                if (!$invidted_user){
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Invited user not exits'
                        ], 500
                    );
                }
                if ($request->has('game_id')) {
                    $game = Games::where('id', $request['game_id'])->first();
                    if ($game){
                        $player = $game->Players()->wherePivot('game_id', $game->id)->wherePivot('user_id', $request['user_id'])->first();
                        if ($player){
                            $device_token_array = '';
                            $device = $player->Device()->where('user_id',$player->id)->orderBy('id','desc')->first();
                            if ($device && $player['notificaiton'] == 1){
                                $settings = $player->Setting()->first();
                                if ($settings['game_pay_invite'] == 1){
                                    $device_token_array = $device['fcm_token'];
                                }
                            }

                            $present_payee = $game->Players()->wherePivot('game_id',$game->id)->wherePivot('user_id','!=',$player->id)->wherePivot('invited_to_pay',1)->first();
                            if ($present_payee)
                                $game->Players()->updateExistingPivot($present_payee->id, ['game_id' => $game->id,'invited_to_pay'=>0]);
                            $game->Players()->updateExistingPivot($player->id, ['game_id' => $game->id,'invited_to_pay'=>1]);
                            $old_notification = Notification::where('user_id',$player->id)->where('type_id',$game->id)->first();
                            if($old_notification){
                                $old_notification->delete();
                            }
                            $notification = new Notification();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currenttime =  $date->format('Y-m-d H:i:s');
                            $notification['date'] = $currenttime;
                            $notification['type'] = 'GME-PY';
                            $notification['user_id'] = $player->id;
                            $notification['content'] = $user->name.' wants you to join and pay for a game of '.$game->name.'. Press Yes to continue or No to decline.';
                            $notification['type_id'] = $game->id;
                            $notification['status'] = 0;
                            $notification['is_read'] = 0;
                            $notification->save();
                            $player['pending_notification'] = 1;
                            $player->update();
                            if ($device_token_array) {
                                $data = "invitation to pay";
                                $notification_type = 4;
                                $data = array(
                                  'title' => 'invitation to pay',
                                  'notification_type' => $notification_type,
                                  'user_id' => $user->id,
                                  'game_id' => $game->id,
                                  'user_name' => $user->name,
                                  'profile_pic' => $user->profile_pic,
                                  'email' => $user->email
                                );
                                sendNotificationwithBody($device_token_array,$user->name.' wants you to join and pay for a game of '.$game->name,$data, $notification_type);
                            }
                            return response()->json(
                                [
                                    'status' => 'success',
                                    'statusCode' => 200,
                                    'success' => 'Player invited to pay successfully',
                                    'data' => $player,
                                    'notification' => $notification
                                ]
                            );
                        }
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Player not found'
                            ], 500
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'game not found'
                        ], 500
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'json schema failed'
                    ], 500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Reject payment.
     */

    public function rejectPayment(Request $request)
    {

        if ( $request->has('game_id')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {

                    $game = Games::where('id', $request['game_id'])->first();
                    if ($game) {
                        $player = $game->Players()->wherePivot('user_id', $user['id'])->wherePivot('game_id', $game['id'])->first();
                        $notification = Notification::where('user_id', $player->id)->where('type_id', $game->id)->first();
                        if ($notification)
                            $notification->delete();
                        if ($player) {
                            if ($player['pivot']['invitation_status'] == 1){
                                $game->Players()->updateExistingPivot($player->id, ['game_id' => $game->id, 'invited_to_pay' => 0]);

                            }else
                                $game->Players()->detach($player->id, ['game_id' => $game->id]);
                            $game_admin = $game->Players()->wherePivot('game_id', $game['id'])->wherePivot('is_super_admin', 1)->first();
                            $device_token_array = '';
                            $device = $game_admin->Device()->where('user_id', $game_admin->id)->orderBy('id', 'desc')->first();
                            if ($device && $game_admin['notificaiton'] == 1)
                                $device_token_array = $device['fcm_token'];
                            $notification = new Notification();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currenttime =  $date->format('Y-m-d H:i:s');
                            $notification['date'] = $currenttime;
                            $notification['type'] = 'GME-PY';
                            $notification['user_id'] = $game_admin->id;
                            $notification['content'] = $player->name . ' has denied the payment request for ' . $game->name;
                            $notification['type_id'] = $game->id;
                            $notification['status'] = 1;
                            $notification['is_read'] = 0;
                            $notification->save();
                            $game_admin['pending_notification'] = 1;
                            $game_admin->update();
                            if ($device_token_array) {
                                $data = 'Reject payment';
                                $notification_type = 4;
                                $data = array(
                                  'title' => 'Reject payment',
                                  'notification_type' => $notification_type,
                                  'user_id' => $player->id,
                                  'game_id' => $game->id,
                                  'user_name' => $player->name,
                                  'profile_pic' => $player->profile_pic,
                                  'email' => $player->email
                                );
                                sendNotificationwithBody($device_token_array,$player->name . ' has denied the payment request for ' . $game->name,$data, $notification_type);
                            }
                            return response()->json(
                                [
                                    'status' => 'success',
                                    'statusCode' => 200,
                                    'success' => 'Payment request rejected successfully'
                                ]
                            );
                        }
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Player not found'
                            ], 500
                        );
                    }
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Game not found'
                        ], 500
                    );

            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ], 500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ], 500
        );
    }




    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Payment completed
     */

    public function  transaction(Request $request,$status){
        if (isset($request['transaction_id'],$request['date'],$request['game_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                if ($request->has('game_id')){
                    $game = Games::where('id',$request['game_id'])->first();
                    if (!$game)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Game not found'
                            ],500
                        );

                    $ground = $game->Ground()->first();

                    $venue = $ground->Venue()->first();

                    $slot = $ground->Slot()->first();

                    // if(isset($request['slot'])){
                    //   $slot_ids = explode(',',$request['slot']);
                    // }else{
                    //       return response()->json(
                    //           [
                    //               'status' => 'error',
                    //               'message' => 'Slots not found -- bookings dont exits'
                    //           ],500
                    //       );
                    // }

                    // $bookings = $slot->Bookings()->where('date',$request['date'])->where('game_id',$request['game_id'])->get();

                    $bookings = Bookings::where('date',$request['date'])->where('game_id',$request['game_id'])->get();

                    if (count($bookings) == 0)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Bookings not found -- bookings dont exits'
                            ],500
                        );

                    $book['payment_status'] = 0;

                    foreach ($bookings as $book){
                        if ($status == 'success')
                            $book['payment_status'] = 1;
                        else
                            $book['payment_status'] = 0;
                        $book['time_release'] = Null;
                        $book->update();
                        // change admin for a game
                    }

                    if ($status == 'success')
                        $book_updated = Bookings::where('date',$request['date'])->where('game_id',$request['game_id'])->where('payment_status',1)->first();
                    else
                        $book_updated = Bookings::where('date',$request['date'])->where('game_id',$request['game_id'])->where('payment_status',0)->first();

                    if (!$book_updated)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Transactions not found -- bookings dont exits'
                            ],500
                        );

                    $transaction = $book_updated->Transaction()->where('transaction_identifier',$book_updated['transaction_identifier'])->first();
                    $transaction['transaction_number'] = $request['transaction_id'];
                    if ($status == 'success'){
                        $transaction['details'] = 'payment completed';
                        $mobile = $user['mobile'];
                        $message = 'Dear customer, Thanks for choosing Yowzapp. Your payment was successful. Please refer this ID '.$transaction['transaction_identifier'].' for further queries.';
                        // $message = urlencode($message);
                        if (strlen($mobile) >= 9){
                          $mobile = "971".substr($mobile, -9);
                        }
                        $url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
                        // $message = urlencode($message);
                        $ch = curl_init();
                        if (!$ch){die("Couldn't initialize a cURL handle");}
                        $ret = curl_setopt($ch, CURLOPT_URL,$url);
                        curl_setopt ($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                        curl_setopt ($ch, CURLOPT_POSTFIELDS,
                        "User=yowzapp&passwd=53267794&mobilenumber=$mobile&message=$message&sid=yowzap&mtype=N&DR=Y");
                        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $curlresponse = curl_exec($ch);
                        $info = curl_getinfo($ch);
                        curl_close($ch);
                    }else{
                        $transaction['details'] = 'payment failed';                        
                        $mobile = $user['mobile'];
                        $message = 'Dear customer, Your payment was Unsuccessful. Please try again.';
                        // $message = urlencode($message);
                        if (strlen($mobile) >= 9){
                          $mobile = "971".substr($mobile, -9);
                        }
                        $url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
                        // $message = urlencode($message);
                        $ch = curl_init();
                        if (!$ch){die("Couldn't initialize a cURL handle");}
                        $ret = curl_setopt($ch, CURLOPT_URL,$url);
                        curl_setopt ($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                        curl_setopt ($ch, CURLOPT_POSTFIELDS,
                        "User=yowzapp&passwd=53267794&mobilenumber=$mobile&message=$message&sid=yowzap&mtype=N&DR=Y");
                        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $curlresponse = curl_exec($ch);
                        $info = curl_getinfo($ch);
                        curl_close($ch);
                    }

                    $transaction['user_id'] = $user['id'];
                    $transaction['game_id'] = $game['id'];
                    $transaction->update();

                    if ($status == 'success'){
                        $player = $game->Players()->wherePivot('is_super_admin', 1)->wherePivot('game_id', $game->id)->first();
                        if ($player){
                            if ($player['id'] != $user['id']) {
                                $game->Players()->updateExistingPivot($player->id, ['game_id' => $game->id, 'invitation_status' => 1, 'is_super_admin' => 0]);
                                $game->Players()->updateExistingPivot($user['id'], ['game_id' => $game->id, 'invitation_status' => 1, 'is_super_admin' => 1, 'is_admin' => 1]);
                                $notification = $user->Notifications()->where('user_id', $player->id)->where('type_id', $game->id)->where('type', 'GME-PY')->first();
                                if ($notification)
                                    $notification->delete();
                                $notification = new Notification();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                                $currenttime =  $date->format('Y-m-d H:i:s');
                                $notification['date'] = $currenttime;
                                $notification['type'] = 'GME-PY';
                                $notification['content'] = $user->name.' has completed the payment request for “' . $game->name .'”';
                                $notification['type_id'] = $game->id;
                                $notification['status'] = 1;
                                $notification['user_id'] = $player->id;
                                $notification->save();
                                $player['pending_notification'] = 1;
                                $player->update();
                                $device_token_array = '';
                                $device = $player->Device()->where('user_id',$player->id)->orderBy('id','desc')->first();
                                if ($device && $player['notificaiton'] == 1)
                                    $device_token_array = $device['fcm_token'];
                                if ($device_token_array) {
                                    $data = "Transaction success";
                                    $notification_type = 5;
                                    $data = array(
                                      'title' => 'Transaction success',
                                      'notification_type' => $notification_type,
                                      'game_id' => $game->id,
                                      'game_name' => $game->name,
                                      'user_id' => $user->id,
                                      'user_name' => $user->name,
                                      'profile_pic' => $user->profile_pic,
                                      'email' => $user->email
                                    );
                                    sendNotificationwithBody($device_token_array,$user->name.' has completed the payment request for “' . $game->name .'”'. $game->name,$data, $notification_type);
                                }
                            }else {
                                $player_payee = $game->Players()->wherePivot('is_super_admin', 1)->wherePivot('game_id', $game->id)->wherePivot('invited_to_pay', 1)->first();
                                if ($player_payee) {
                                    $game->Players()->updateExistingPivot($player->id, ['game_id' => $game->id, 'invited_to_pay' => 0]);
                                }
                            }
                        }

                        /// notifiy the vendor about the booking
                        $more = array(
                            $venue['name'],
                            $venue['id'],
                            $ground['name'],
                            $ground['id'],
                            date('Y-m-d', strtotime($game['date']))
                        );
                        $more = implode(",",$more);
                        $vendor = $book_updated->Vendor()->first();
                        $notification = new Notification();
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                        $currenttime =  $date->format('Y-m-d H:i:s');
                        $notification['date'] = $currenttime;
                        $notification['type'] = 'VDR-BOKNG';
                        $notification['content'] = $user->name.' as booked your ground '. $ground->name .' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time']));
                        $notification['type_id'] = $ground->id;
                        $notification['more'] = $more;
                        $notification['status'] = 0;
                        $notification['user_id'] = $vendor->id;
                        $notification->save();
                        $vendor['pending_notification'] = 1;
                        $vendor->update();
                        $device_token_array = '';
                        $device = $vendor->Device()->where('user_id',$vendor->id)->orderBy('id','desc')->first();
                        if ($device)
                            $device_token_array = $device['fcm_token'];
                        if ($device_token_array) {
                            $data = "Ground Booked";
                            $notification_type = 5;
                            $data = array(
                              'title' => 'Ground Booked',
                              'notification_type' => $notification_type,
                              'ground_id' => $ground->id,
                              'ground_name' => $ground->name,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,$user->name.' as booked your ground '. $ground->name .' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time'])). $game->name,$data, $notification_type);
                        }

                        // update vendor report

                        $report = new Report();
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                        $currentdate =  $date->format('Y-m-d');
                        $content = $user->name.' booked your ground '. $ground->name .' on '.date('jS F', strtotime($game['date'])).' from '.date('h:i A',strtotime($game['start_time'])).' to '.date('h:i A',strtotime($game['end_time']));
                        $report ['content'] = $content;
                        $report['date'] = $currentdate;
                        $report['vendor_id'] = $vendor->id;
                        $report->save();

                    }else{
                        $player = $game->Players()->wherePivot('is_super_admin', 1)->wherePivot('game_id', $game->id)->first();
                        if ($player){
                            if ($player['id'] != $user['id']) {
                                $game->Players()->updateExistingPivot($player->id, ['game_id' => $game->id, 'invitation_status' => 1, 'is_super_admin' => 0]);
                                $game->Players()->updateExistingPivot($user['id'], ['game_id' => $game->id, 'invitation_status' => 1, 'is_super_admin' => 1, 'is_admin' => 1]);
                                $device_token_array = '';
                                $device = $player->Device()->where('user_id',$player->id)->orderBy('id','desc')->first();
                                if ($device && $player['notificaiton'] == 1)
                                    $device_token_array = $device['fcm_token'];
                                if ($device_token_array) {
                                    $data = "Transaction incompletee";
                                    $notification_type = 5;
                                    $data = array(
                                      'title' => 'Transaction incompletee',
                                      'notification_type' => $notification_type,
                                      'game_id' => $game->id,
                                      'game_name' => $game->name,
                                      'user_id' => $user->id,
                                      'user_name' => $user->name,
                                      'profile_pic' => $user->profile_pic,
                                      'email' => $user->email
                                    );
                                    sendNotificationwithBody($device_token_array,$user->name.' was unsuccessful to complete the payment request for “' . $game->name .'”'. $game->name,$data, $notification_type);
                                }
                            }
                        }
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => 'Transaction updated'
                        ]
                    );
                }
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * pay check API
     */


    public function  isBookingAvailable(Request $request){
        if (isset($request['game_id'],$request['date'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                if ($request->has('game_id')){
                    $game = Games::where('id',$request['game_id'])->first();
                    $ground = $game->Ground()->first();
                    $slot = $ground->Slot()->first();

                    $slotIds = $ground->Slot()->pluck('id');
                    $bookings = Bookings::where('slot_id',$slotIds)->where('date',$request['date'])->where('game_id',$request['game_id'])->where('payment_status',0)->get();
                    $status = false;
                    foreach ($bookings as $book){
                        $status = true;
                        $book['payment_status'] = 1;
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                        $timestamp = $date->format('Y-m-d H:i');
                        $book['time_release'] = $timestamp;
                        $book->update();
                    }
                    return response()->json(
                        [
                            'status' => $status,
                            'statusCode' => 200,
                            'success' => 'Booking status'
                        ]
                    );
                }
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     * my transactions
     */
    public function  myTransactions(Request $request){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
                if ($user) {
                    $transactions = $user->Transactions()->where('transaction_number','!=',NULL)->orderBy('updated_at','desc')->get();
                    $all_transactions_final = array();
                    foreach ($transactions as $value){
                        if ($value['game_id']){
                            $game = $value->Game()->first();
                            if (!$game){
                                break;
                            }
                            $ground = $game->Ground()->first();
                            $venue = $ground->Venue()->first();
                            if (($value['transaction_number'] == 'NOTAVAILABLE')  || ($value['transaction_number'] == '')){
                                $transaction_number = '';
                                $is_success = false;
                            }else{
                                $is_success = true;
                                $transaction_number = $value['transaction_number'];
                            }
                            $booking = $value->Bookings()->where('transaction_identifier',$value['transaction_identifier'])->first();
                            $date  =  strtotime ($booking['date']);
                            $date = date('j M y', $date);
                            $all_transactions_final[] = array(
                                'transaction_status' => $is_success,
                                'amount' => $value['amount'],
                                'transaction_number' => strtoupper($transaction_number),
                                'booking_id' =>  $value['booking_id'],
                                'user_id' =>  $value['user_id'],
                                'details' =>  $value['details'],
                                'game_id' =>  $value['game_id'],
                                'game_name' =>  $game['name'],
                                'game_location' =>  $venue['location_name'],
                                'venue_name' =>  $venue['name'],
                                'date' =>  $date,
                            );
                        }
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'success' => $all_transactions_final
                        ]
                    );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
    }




    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \LaravelFCM\Message\InvalidOptionException
     * vendor Reports
     */
    public function  getReports(Request $request){
        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user) {
            if ($request->has('venue_id'))
                $venue = Venues::where('id', $request['venue_id'])->first();
            else
                $venue = $user->Venues()->orderBy('id','asc')->first();
            $now = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
            $today =  $now->format('Y-m-d');
            $date_array = array();
            if ($request->has('type')){

                if ($request['type'] == 'daily'){
                    $date_array[] = $today ;
                    for ($i = 1; $i < 6; $i++) {
                        $date_array[] =  date("Y-m-d", strtotime($i."days ago"));
                    }
                }else if  ($request['type'] == 'monthly'){
                    $date_array[] = $now->format('Y-m-01'); ;
                    for ($i = 1; $i < 3; $i++) {
                        $date_array[] =  date("Y-m-01", strtotime($i."months ago"));
                    }
                }
            }else{
                $request['type'] = 'monthly';
                for ($i = 1; $i < 3; $i++) {
                    $date_array[] =  date("Y-m-01", strtotime($i."months ago"));
                }
            }
            $date_array = array_reverse($date_array);
            if ($venue) {
                $grounds = $venue->Grounds()->get();
                $final_array = array();
                $grounds_new = array();

                foreach ($grounds as $ground) {
                    $ground['hex'] = random_color(0,100);
                    $details['hex'] = $ground['hex'];
                    $details['name'] = $ground['name'];
                    $grounds_new[] = $ground;
                }
                $final_details['venue_id'] = $venue['id'];
                $final_details['venue_name'] = $venue['name'];

                $i = 0;
                foreach ($date_array as $item) {
                        $gronds_array = array();
                        foreach ($grounds_new as $ground) {
                            $slot = $ground->Slot()->first();
                            if ($request['type'] == 'daily'){
                                $bookings = $slot->Bookings()
                                    ->where('date', $item)                                    
                                    ->pluck('slot_price')
                                    // ->sortBy('date')
                                    ->toArray();
                            }else if  ($request['type'] == 'monthly'){

                                $bookings = $slot->Bookings()
                                    ->whereBetween('date', [$item,date("Y-m-t", strtotime($date_array[$i]))])
                                    ->pluck('slot_price') 
                                    // ->sortBy('date')                                   
                                    ->toArray();
                                    
                            }
                            
                            $details['amount'] = 0;
                            $details['hex'] = $ground['hex'];
                            $details['name'] = $ground['name'];
                            $details['amount'] = array_sum($bookings);
                            $gronds_array[] = $details;
                        }
                        if ($request['type'] == 'monthly'){
                            $ground_details['date']  = strtoupper(date('M',strtotime($item)));
                        }else{
                            $ground_details['date']  = date('jS D',strtotime($item));
                        }
                        $ground_details['grounds'] = $gronds_array;
                        $final_array[] = $ground_details;
                    $i++;
                    }
                $final_details['graph'] = $final_array;
                if ($request['type'] == 'monthly'){
                    $final_details['date']  = date('F',strtotime($today));
                }else{
                    $final_details['date']  = date('jS l',strtotime($today));
                }
                if (sizeof($final_array)>1){
                    $pie_values = $final_array[0]['grounds'];
                }else{
                    $gronds_pie_array = array();
                    foreach ($grounds_new as $ground) {
                        $slot = $ground->Slot()->first();
                        if ($request['type'] == 'daily'){
                            $bookings = $slot->Bookings()
                                ->where('date', $today)
                                ->pluck('slot_price')
                                ->toArray();
                        }else {

                            $bookings = $slot->Bookings()
                                ->where('date', '>=',$date_array[0])
                                ->pluck('slot_price')
                                ->toArray();
                        }
                        $details['amount'] = 0;
                        $details['hex'] = $ground['hex'];
                        $details['name'] = $ground['name'];
                        $details['amount'] = array_sum($bookings);
                        $gronds_pie_array[] = $details;
                    }
                    if ($request['type'] == 'monthly'){
                        $ground_details['date']  = strtoupper(date('M',strtotime($today)));
                    }else{
                        $ground_details['date']  = date('jS D',strtotime($today));
                    }
                    $ground_details['grounds'] = $gronds_pie_array;
                    $pie_values = $ground_details;
                }
                $final_details['pie'] = $pie_values;
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'success' => $final_details,
                    ]
                );
            }
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => [],
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
    }


    /** Vendor old get Reports  */


    public function  getReportsOld(Request $request){

        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = Users::whereId($accesstoken->user_id)->first();
        if ($user) {

            $reports = $user->Reports()->orderBy('id', 'desc')->get();
            $reports_final = array();
            foreach ($reports as $report) {
                $day = date('jS', strtotime($report['date']));
                $month = date('F', strtotime($report['date']));

                $reports_final[] = array(
                    'id' => $report["id"],
                    'content' => $report["content"],
                    'vendor_id' => $report["vendor_id"],
                    'day' => $day,
                    'month' => strtoupper($month),
                );
            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'reports List',
                    'success' => $reports_final
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Vendor not exits',
            ],500
        );
    }


}
