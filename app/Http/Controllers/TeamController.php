<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\ActivityLog;
use App\Models\Levels;
use App\Models\Notification;
use App\Models\Search;
use App\Models\SearchRecent;
use App\Models\Slot;
use App\Models\Sports;
use App\Models\TeamGallery;
use App\Models\Teams;
use App\Models\TeamTimeline;
use App\Models\Users;
use App\Models\Bookings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class TeamController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * create Team
     */
    public function  createTeam(Request $request){
        if (isset($request['cover_pic'],$request['name'],$request['members'],$request['sports'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $team = new Teams();
                $team['name'] = $request['name'];
                $team['cover_pic'] = $request['cover_pic'];
                $team['activity_meter'] = 0;
                $team['points'] = 0;
                if ($request->has('discription'))
                    $team['discription'] = $request['discription'];
                $team->save();

                $activity_log = new ActivityLog();
                $activity_log['user_id'] = $user->id;
                $activity_log['type_id'] = $team->id;
                $activity_log['type'] = 1;
                $activity_log['content'] = $user->name.' has created a new team '.$team->name;
                $activity_log->save();

                $search = new Search();
                $search['id'] = $team->id;
                $search['name'] = $team->name;
                $search['type'] = 2;
                $search->save();

                $sports_list = json_decode(json_encode($request['sports']), true);
                $sports_list = array_unique($sports_list);
                foreach ($sports_list as $value){
                    $team->Sports()->attach($team->id,['sport_id'=>$value]);
                }
                $members = json_decode(json_encode($request['members']), true);
                $members = array_unique($members);
                $team->Players()->attach($team->id,['user_id'=>$user->id,'is_admin' => 1,'is_super_admin' => 1,'points_earned' => 0,'invitation_status'=>1]);
               $device_token_array = array();
                foreach ($members as $value){
                    $player = Users::where('id',$value)->first();
                    if ($player) {
                        $device = $player->Device()->where('user_id', $value)->orderBy('id', 'desc')->first();
                        if ($device && $player['notificaiton'] == 1){
                            $settings = $player->Setting()->first();
                            if ($settings['new_team'] == 1){
                                $device_token_array[] = $device['fcm_token'];
                            }
                        }
                        $notification = new Notification();
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                        $currenttime = $date->format('Y-m-d H:i:s');
                        $notification['date'] = $currenttime;
                        $notification['type'] = 'TME-PA';
                        $notification['user_id'] = $player->id;
                        $notification['content'] = 'You have been added to ' . $team->name . ' by ' . $user->name;
                        $notification['type_id'] = $team->id;
                        $notification['status'] = 1;
                        $notification->save();
                        $player['pending_notification'] = 1;
                        $player->update();
                        $team->Players()->attach($team->id, ['user_id' => $value, 'is_admin' => 0, 'is_super_admin' => 0, 'points_earned' => 0, 'invitation_status' => 1]);
                    }
                }

                $device_token_array = array_filter($device_token_array);
                if ($device_token_array) {
                    $data = "Added to a team";
                    $notification_type = 7;
                    $data = array(
                      'title' => 'Added to a team',
                      'notification_type' => $notification_type,
                      'team_id' => $team->id,
                      'team_name' => $team->name,
                      'user_id' => $user->id,
                      'user_name' => $user->name,
                      'profile_pic' => $user->profile_pic,
                      'email' => $user->email
                    );
                    sendNotificationwithBody($device_token_array,'You have been added to '.$team->name.' by '.$user->name,$data,$notification_type, $user->id);
                }

                // Timeline update
                $team_timeline = new TeamTimeline();
                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                $currentdate =  $date->format('Y-m-d');
                $members_string = '';
                $members = $team->Players()->where('id','!=',$user->id)->pluck('name')->toArray();
                $count = $team->Players()->count();
                if (sizeof($members)>3){
                    $members = array_slice($members,0,3, true);
                }
                if ($members){
                    $members_string = implode(', ',$members);
                }
                $count = $count - count($members) ;
                if ($count<3)
                    $members_string = $members_string.' joined this team';
                else
                    $members_string = $members_string.' and '.$count.' others joined this team';
                $team_timeline ['content'] = $user->name.' created this team';
                $team_timeline['date'] = $currentdate;
                $team_timeline['team_id'] = $team->id;
                $team_timeline->save();
                $team_timeline = new TeamTimeline();
                $team_timeline['team_id'] = $team->id;
                $team_timeline['date'] = $currentdate;
                $team_timeline ['content'] = $members_string;
                $team_timeline->save();
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Team created successfully',
                            'success' => $team
                        ]
                    );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * edit Team
     */

    public function  editTeam(Request $request){
        if (isset($request['team_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $team = Teams::where('id', $request['team_id'])->first();
                if ($team) {
                    if ($request->has('name')) {
                        $team['name'] = $request['name'];
                        $search = Search::where('id', $team->id)->where('type', 2)->first();
                        if ($search){
                            $search->delete();
                        }
                        $search = new Search();
                        $search['id'] = $team->id;
                        $search['name'] = $team->name;
                        $search['type'] = 2;
                        $search->save();
                    }
                    if ($request->has('cover_pic'))
                        $team['cover_pic'] = $request['cover_pic'];
                    $team['activity_meter'] = 0;
                    $team['points'] = 0;
                    if ($request->has('discription'))
                        $team['discription'] = $request['discription'];

                    $team->update();


                    if ($request->has('sports')) {
                        $team->Sports()->detach();
                        $sports_list = json_decode(json_encode($request['sports']), true);
                        $sports_list = array_unique($sports_list);
                        foreach ($sports_list as $value) {
                            $team->Sports()->attach($team->id, ['sport_id' => $value]);
                        }
                    }
                    if ($request->has('members')) {
                        $device_token_array = array();
                        $members = json_decode(json_encode($request['members']), true);
                        $members[] = $user->id;
                        $members = array_unique($members);
                        $exists_palyers = $team->Players()->wherePivot('team_id', $team->id)->pluck('id')->toArray();
                        $new_players = array_diff($members, $exists_palyers);
                        $remove_players = array_diff($exists_palyers, $members);
                        foreach ($new_players as $value) {
                            $player = Users::where('id', $value)->first();
                            if ($player) {
                                $device = $player->Device()->where('user_id', $value)->orderBy('id', 'desc')->first();
                                if ($device && $player['notificaiton'] == 1)
                                    $device_token_array[] = $device['fcm_token'];
                                $notification = new Notification();
                                $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                                $currenttime_notification_time = $date->format('Y-m-d H:i:s');
                                $notification['date'] = $currenttime_notification_time;
                                $notification['type'] = 'TME-PA';
                                $notification['user_id'] = $player->id;
                                $notification['content'] = 'You have been added to ' . $team->name . ' by ' . $user->name;
                                $notification['type_id'] = $team->id;
                                $notification['status'] = 1;
                                $notification->save();
                                $player['pending_notification'] = 1;
                                $player->update();
                                $team->Players()->attach($team->id, ['user_id' => $value, 'is_admin' => 0, 'is_super_admin' => 0, 'points_earned' => 0, 'invitation_status' => 1]);
                            }
                        }
                        if ($new_players) {
                            $team_timeline = new TeamTimeline();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
                            $currentdate = $date->format('Y-m-d');
                            $members_string = '';
                            $members = $team->Players()->where('id', $new_players)->pluck('name')->toArray();
                            $count = $team->Players()->count();
                            if (sizeof($members) > 3) {
                                $members = array_slice($members, 0, 3, true);
                            }
                            if ($members) {
                                $members_string = implode(', ', $members);
                            }
                            $count = $count - count($members);
                            if ($count < 3)
                                $members_string = $members_string . ' joined this team';
                            else
                                $members_string = $members_string . ' and ' . $count . ' others joined this team';
                            $team_timeline = new TeamTimeline();
                            $team_timeline['team_id'] = $team->id;
                            $team_timeline['date'] = $currentdate;
                            $team_timeline ['content'] = $members_string;
                            $team_timeline->save();
                        }

                        foreach ($remove_players as $value) {
                            $remove_player = $team->Players()->wherePivot('team_id', $team->id)->wherePivot('user_id', $value)->first();
                            if ($remove_player && $remove_player['pivot']['is_super_admin'] == 0)
                                $team->Players()->detach($value, ['team_id' => $team->id]);

                        }

                        if ($device_token_array) {
                            $data = "Added to a Team";
                            $notification_type = 7;
                            $super_admin = $team->Players()->wherePivot('is_admin', 1)->wherePivot('is_super_admin', 1)->first();
                            $data = array(
                              'title' => 'Added to a team',
                              'notification_type' => $notification_type,
                              'team_id' => $team->id,
                              'team_name' => $team->name,
                              'user_id' => $user->id,
                              'user_name' => $user->name,
                              'profile_pic' => $user->profile_pic,
                              'email' => $user->email
                            );
                            sendNotificationwithBody($device_token_array,'You have been added to ' . $team->name . ' by ' . $super_admin->name,$data, $notification_type, $user->id);
                        }
                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Team updated successfully',
                            'success' => $team
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Team not found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Team details
     */

    public function  teamDetails(Request $request,$type)
    {
               
        if ($request->has('team_id')) {
            
            $user = [];
            if ($request->header('accessToken')) {
                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                
                if ($accesstoken)
                    $user = $accesstoken->User()->first();
            }
                $team = Teams::whereId($request['team_id'])->first();
                
                if ($team) {
                    
                    if ($type == 'team'){
                    $members_count = $team->Players()->wherePivot('invitation_status', 1)->count();
                    
                    $all_games = $team->Games()->orderBy('updated_at','desc')->get();
                    
                        if ($user) {
                            if ($request->has('q')){
                                $recent_search = SearchRecent::where('id',$team->id)->where('user_id',$user->id)->where('type',2)->first();
                                if (!$recent_search)
                                    $recent_search = new SearchRecent();
                                $recent_search['type'] = 2;
                                $recent_search['id'] = $team->id;
                                $recent_search['user_id'] = $user->id;
                                $recent_search->save();
                            }
                            $is_following = $team->Followers()->wherePivot('type', 2)->where('id', $user->id)->first();
                            if ($is_following)
                                $team['is_following'] = true;
                            else
                                $team['is_following'] = false;
                            $is_admin = $team->Players()->wherePivot('is_admin', 1)->wherePivot('invitation_status', 1)->where('id', $user->id)->first();
                            if ($is_admin)
                                $team['is_mine'] = true;
                            else
                                $team['is_mine'] = false;
                            $is_member = $team->Players()->where('id', $user->id)->wherePivot('invitation_status', 1)->first();
                            if ($is_member)
                                $team['is_member'] = true;
                            else
                                $team['is_member'] = false;
                            $is_requested = $team->Players()->wherePivot('is_requested', 1)->wherePivot('team_id', $team->id)->wherePivot('user_id', $user->id)->first();
                            if ($is_requested && !$team['is_member'] && !$team['is_mine'])
                                $team['is_requested'] = true;
                            else
                                $team['is_requested'] = false;
                             $team_members=  $team->Players()->wherePivot('invitation_status', 1)->orderBy('name','asc')->get();
                        }else{
                            $team['is_following'] = false;
                            $team['is_mine'] = false;
                            $team['is_member'] = false;
                            $team_members =  $team->Players()->wherePivot('invitation_status', 1)->orderBy('name','asc')->get();
                        }
                        $team_members_final_array = array();
                        foreach ($team_members as $member){
                            if ($member['level_id'] == 0)
                                $member['level_id'] = 1;
                            $member_level = Levels::where('id',$member['level_id'])->first();

                            $member['level_name'] = $member_level->level_name;
                            $team_members_final_array[] = $member;
                        }
                        $team['members'] = $team_members_final_array;

                        $points = $team->Points()->where('type',2)->pluck('point')->toArray();
                        $points = array_sum($points);
                        $team['points'] = $points;
                        $level = Levels::where('id','<=',$points)->where('points_to','>=',$points)->first();
                        if ($level){
                            $team['level_id'] = $level->id;
                            $team['level_name'] = $level['level_name'];
                        }else{
                            $team['level_name'] = 'Novice';
                            $team['level_id'] = 1;
                        }
                    $team['members_count'] = $members_count;
                    $team['sports'] =  $team->Sports()->orderBy('name','asc')->get();
                    }elseif ($type == 'teamGallery'){
                        $team['Galleries'] =  $team->Galleries()->orderBy('updated_at','desc')->get();
                    }elseif ($type == 'teamGames'){
                        $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                        $currenttime =  strtotime($date->format('H:i:s'));
                        $currentdate =  $date->format('Y-m-d');
                        $all_games =  $team->Games()->orderBy('updated_at','desc')->get();
                        
                        $all_games_final = array();
                        foreach($all_games as $k=>$game) {
                            $ground = $game->Ground()->first();
                            $slot = $ground->Slot()->first();
                            $sport = Sports::whereId($game['sport_id'])->first();
                            $venue = $ground->Venue()->first();
                            $players_count = $game->Players()->count();
                            $slotIds = $ground->Slot()->pluck('id');
                            $bookings = Bookings::whereIn('slot_id',$slotIds)->where('game_id', $game['id'])->where('payment_status',1)->groupBy('date')->get();
                                                        
                            foreach ($bookings as $k=>$booked){
                                
                                $match_status = '';
                                $is_completed = false;
                                $booked_date = date("Y-m-d", strtotime($booked['date']));
                                if ($booked_date<$currentdate){
                                    $match_status = 'match completed';
                                    $is_completed = true;
                                }elseif($booked_date==$currentdate){
                                    $start_time = date("H:i:s", strtotime($booked['start_time']));
                                    $start_time    = strtotime ($start_time); //change to strtotime
                                    if ($start_time>$currenttime){
                                        $diff = abs($start_time - $currenttime) / 3600;
                                        $hours = floor($diff);
                                        if ($hours>0)
                                            $match_status = 'match starts in '.floor($diff).' hours';
                                        else
                                            $match_status = 'match starts soon';
                                    }else{
                                        $match_status = 'match completed';
                                        $is_completed = true;
                                    }
                                }elseif($booked_date>$currentdate){
                                    $datediff = abs(strtotime($booked_date) - strtotime($currentdate));
                                    $no_days = floor($datediff / (60 * 60 * 24));
                                    if ($no_days>1)
                                        $match_status = 'match starts in '.$no_days.' days';
                                    else
                                        $match_status = 'match starts in '.$no_days.' day';
                                }
                                
                                $all_games_final[] =  array(
                                    'id' => $game["id"],
                                    'name' => $game["name"],
                                    'discription' => $game["discription"],
                                    'sport_id' => $sport["id"],
                                    'sport_name' => $sport["name"],
                                    'cover_pic' => $game["cover_pic"],
                                    'is_private' =>filter_var($game["is_private"], FILTER_VALIDATE_BOOLEAN),
                                    'max_members' =>$ground["max_member"],
                                    'no_members' =>$players_count,
                                    'location' => $venue['location_name'],
                                    'venue' => $venue->name,
                                    'match_status' => strtoupper($match_status),
                                    'date' =>date('d F', strtotime($booked['date'])),
                                    'is_completed' => $is_completed,
                                    'start_time' => date('h:i A', strtotime($booked['start_time'])),
                                    'end_time' => date('h:i A', strtotime($booked['end_time'])),
                                );
                            }
                        }
                        
                        $team['games'] = $all_games_final;
                    }else if ($type == 'timeline'){
                        $time_line = $team->Timeline()->orderBy('id','desc')->get();
                        $time_line_final = array();
                        foreach ($time_line as $timeLine) {
                            $day =  date('jS', strtotime($timeLine['date']));
                            $month =  date('F', strtotime($timeLine['date']));

                            $time_line_final[] =  array(
                                'id' => $timeLine["id"],
                                'content' => $timeLine["content"],
                                'team_id' => $timeLine["team_id"],
                                'day' => strtoupper($day),
                                'month' => strtoupper($month),
                            );
                        }


                        $team['time_line'] = $time_line_final;

                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Team details',
                            'success' => $team
                            // 'team_games' => $all_games
                        ]
                    );

                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Team not found'
                    ], 500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User not found'
                ], 500
            );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Request to join Team
     */

    public function  joinTeam(Request $request) 
    {

        if (isset($request['team_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $team =Teams::where('id',$request['team_id'])->first();
                if ($team){
                    $player_exists = $team->Players()->wherePivot('user_id', $user->id)->first();
                    if ($player_exists)
                        return response()->json(
                            [
                                'status' => 'error',
                                'message' => 'Player already exists in team'
                            ],500
                        );
                    $team->Players()->attach($team->id,['user_id'=>$user->id,'points_earned' => 0,'is_admin' => 0,'is_super_admin' => 0,'invitation_status' => 0,'is_requested' => 1]);
                    $admin = $team->Players()->wherePivot('team_id', $team->id)->wherePivot('is_super_admin', 1)->first();
                        $device = $admin->Device()->where('user_id',$admin->id)->orderBy('updated_at','desc')->first();
                        $notification = Notification::where('user_id',$admin->id)->where('type_id',$team->id)->where('type','TME-PR')->where('more',$user->id)->first();
                        if (!$notification){
                            if ($device && $admin['notificaiton'] == 1)
                                $device_token_array[] = $device['fcm_token'];
                            $notification = new Notification();
                            $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                            $currenttime_notification_time =  $date->format('Y-m-d H:i:s');
                            $notification['date'] = $currenttime_notification_time;
                            $notification['type'] = 'TME-PR';
                            $notification['user_id'] = $admin->id;
                            $notification['content'] = $user->name.' as requested to join your team '.$team->name;
                            $notification['type_id'] = $team->id;
                            $notification['more'] = $user->id;
                            $notification['status'] = 0;
                            $notification['is_read'] = 0;
                            $notification->save();
                            $admin['pending_notification'] = 1;
                            $admin->update();
                            $device = $admin->Device()->where('user_id',$admin->id)->orderBy('id','desc')->first();
                            $device_token_array = '';
                            if ($device && $admin['notificaiton'] == 1)
                                $device_token_array = $device['fcm_token'];
                            if ($device_token_array) {
                                $data = "team join request";
                                $notification_type = 12;
                                $data = array(
                                  'title' => 'team join request',
                                  'notification_type' => $notification_type,
                                  'team_id' => $team->id,
                                  'team_name' => $team->name,
                                  'user_id' => $user->id,
                                  'user_name' => $user->name,
                                  'profile_pic' => $user->profile_pic,
                                  'email' => $user->email
                                );
                                sendNotificationwithBody($device_token_array,$user->name.' requested to join your team '.$team->name,$data, $notification_type, $user->id);
                            }
                        }

                    return response()->json(
                        [
                            'status' => 'success',
                            'statusCode' => 200,
                            'message' => 'Requested to join team successfully'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Team not found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * my Team Listing
     */

    public function  teamListing(Request $request)
    {
        $user = [];
        if ($request->header('accessToken')) {
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken)
                $user = $accesstoken->User()->first();
        }
            $my_teams_final = array();
            $all_teams_final = array();
            $id_list = array();


        if ($request->has('mine')){
            if (!$user)
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Player not found'
                    ],500
                );
            $my_teams = $user->Teams()->wherePivot('invitation_status',1)->orderBy('updated_at','desc')->get();
            foreach($my_teams as $team) {
                $my_teams_final[] =  array(
                    'id' => $team["id"],
                    'name' => $team["name"],
                    'cover_pic' => $team["cover_pic"],
                    'members_count' => $team->Players()->wherePivot('invitation_status', 1)->count(),
                    'followers_count' =>$team->Followers()->wherePivot('type', 2)->count()
                );
            }
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Team listing',
                    'success' => $my_teams_final
                ]
            );
        }

             if ($user){
                $my_teams = $user->Teams()->wherePivot('invitation_status',1)->orderBy('updated_at','desc')->get();
                $id_list =  $my_teams->pluck('id')->toArray();
                foreach($my_teams as $team) {
                    $my_teams_final[] =  array(
                        'id' => $team["id"],
                        'name' => $team["name"],
                        'cover_pic' => $team["cover_pic"],
                        'members_count' => $team->Players()->wherePivot('invitation_status', 1)->count(),
                        'followers_count' =>$team->Followers()->wherePivot('type', 2)->count()
                    );
                }
             }
            if ($request->has('invite'))
                $all_teams = Teams::orderBy('updated_at','desc')->paginate(10);
            else
                $all_teams = Teams::whereNotIn('id',$id_list)->orderBy('updated_at','desc')->paginate(10);

            foreach($all_teams as $k=>$team) {
                $all_teams_final[] =  array(
                    'id' => $team["id"],
                    'name' => $team["name"],
                    'cover_pic' => $team["cover_pic"],
                    'members_count' => $team->Players()->wherePivot('invitation_status', 1)->count(),
                    'followers_count' =>$team->Followers()->wherePivot('type', 2)->count()
                );
            }

            $final['my_teams'] = $my_teams_final;
            $final['all_teams'] = $all_teams_final;
            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Team listing',
                    'success' => $final,
                    'currentPage' => $all_teams->currentPage(),
                    'hasMorePages' => $all_teams->hasMorePages()
                ]
            );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * delete Team
     */

    public function  deleteTeam(Request $request)
    {

        if (isset($request['team_id'])){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $team = $user->Teams()->where('id',$request['team_id'])->wherePivot('is_super_admin', 1)->first();
                if ($team){
                    $team->Games()->detach();
                    $team->Players()->detach();
                    $team->Sports()->detach();
                    $team->Search()->where('type',2)->delete();
                    $team->delete();
                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'message' => 'Team deleted successfully'
                    ]
                );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Team not found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );

    }


    /**
     * @param Request $request
     * @param $action
     * @return \Illuminate\Http\JsonResponse
     * add team image
     */
    public function  teamImage(Request $request,$action){

        ///  ** sending team id in header
        if ($request->header('teamId') && ($action=='delete' || $action == 'add')){
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($user) {
                $team = Teams::where('id', $request->header('teamId'))->first();
                if ($team) {
                    if ($action == 'add'){
                        $gallery = new TeamGallery();
                        if ($request->file('image')){
                            $file = $request->file('image');
                            $origin_name = $file->getClientOriginalName();
                            $date = date_create();
                            $timeStamp = date_timestamp_get($date);
                            $key = 'players/teams/'.str_replace(' ','_',$team->name).$timeStamp.'/'.$origin_name;
                            Storage::disk('s3')->put($key, fopen($file, 'r+'),'public');
                            $url= Storage::disk('s3')->url($key);
                            $gallery['image']=  $url;
                            $gallery['team_id'] = $team->id;
                            $gallery->save();
                            return response()->json(
                                [
                                    'status' => 'success',
                                    'statusCode' => 200,
                                    'message' => 'Gallery image added successfully',
                                    'success' => $gallery
                                ]
                            );
                        }
                        return response()->json(
                            [
                                'status' => 'success',
                                'message' => 'Team image not found'
                            ],500
                        );
                    }elseif($action == 'delete'&&$request->header('galleryId')){
                        $team->Galleries()->where('id',$request->header('galleryId'))->delete();
                        return response()->json(
                            [
                                'status' => 'success',
                                'statusCode' => 200,
                                'message' => 'Gallery image deleted successfully',
                            ]
                        );

                    }
                    return response()->json(
                        [
                            'status' => 'success',
                            'message' => 'Json schema failed'
                        ],500
                    );
                }
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'No Team found'
                    ],500
                );
            }
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Invalid user'
                ],500
            );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => 'Json schema failed'
            ],500
        );
    }
}
