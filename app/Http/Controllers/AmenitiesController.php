<?php

namespace App\Http\Controllers;

use App\Models\Amenities;
use Illuminate\Http\Request;

class AmenitiesController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * amenities list api
     */
    public function  amenitiesList(){

        $amenities_list = Amenities::orderBy('name','asc')->get();


            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Amenities List',
                    'success' => $amenities_list,
                ]
            );
    }
}
