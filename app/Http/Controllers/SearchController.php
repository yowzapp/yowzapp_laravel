<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use App\Models\Games;
use App\Models\Search;
use App\Models\Teams;
use App\Models\Users;
use App\Models\Venues;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * search ALL API
     */
    public function  searchALL(Request $request){
        if(isset($_GET['q'])) {
            $search = $_GET['q'];
            $user = [];
            if ($request->header('accessToken')) {
                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                if ($accesstoken)
                    $user = $accesstoken->User()->first();
            }

            $all_items_final = array();
//             $all_items = Search::where('name', 'LIKE', '%' .  $search . '%')
// //                ->orderBy('name','asc')
//                 ->paginate(10);  

            $all_items = Users::where('name', 'LIKE', '%' .  $search . '%')->orderBy('name','asc')->where('role_id',3)->paginate(10);

            foreach($all_items as $player) {
                $new_item = array();
                $is_team = false;
                $is_player = true;
                $is_venue = false;
                $is_game = false;
                unset($player['created_at']);
                unset($player['updated_at']);
                unset($player['password']);
                unset($player['otp']);
                unset($player['social_token']);
                $new_item['type'] = 'player';
                $new_item['id'] = $player->id;
                $new_item['name'] = $player->name;
                $new_item['cover_pic'] = $player['profile_pic'];
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;
                $new_item['points'] = 0;
                $new_item['player_details'] = $player;
                $all_items_final[] = $new_item;
            }

            $all_items_teams = Teams::where('name', 'LIKE', '%' .  $search . '%')->orderBy('name','asc')->paginate(10);

            foreach($all_items_teams as $team) {
                $new_item = array();
                $is_team = true;
                $is_player = false;
                $is_venue = false;
                $is_game = false;
                unset($team['created_at']);
                unset($team['updated_at']);
                $new_item['type'] = 'team';
                $new_item['id'] = $team->id;
                $new_item['name'] = $team->name;
                $new_item['cover_pic'] = $team['cover_pic'];
                $new_item['no_players'] = $team->Players()->wherePivot('team_id',$team->id)->wherePivot('invitation_status',1)->count();
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;

                $all_items_final[] = $new_item;
            }
            
            $all_items_venues = Venues::where('name', 'LIKE', '%' .  $search . '%')->where('lat','!=',0)->where('lng','!=',0)->where('is_available',1)->paginate(10);

            foreach($all_items_venues as $venue) {
                $new_item = array();
                $is_team = false;
                $is_player = false;
                $is_venue = true;
                $is_game = false;
                unset($venue['created_at']);
                unset($venue['updated_at']);
                $new_item['id'] = $venue->id;
                $new_item['name'] = $venue->name;
                $new_item['cover_pic'] = $venue['cover_pic'];

                $grounds = $venue->Grounds()->orderBy('name','asc')->where('is_disabled',0)->get();
                $amenity = $venue->Amenities();
                $amenity = array_column($amenity, 'name');
                if (count($amenity)>0)
                    $amenity = implode( ",", $amenity );
                else
                    $amenity = '';
                $venue["distance"] = 0.0;
                if ($user && $user['lat'] != 0&& $user['lng'] != 0){
                    $degrees = rad2deg(acos((sin(deg2rad($user['lat']))*sin(deg2rad($venue['lat']))) + (cos(deg2rad($user['lat']))*cos(deg2rad($venue['lat']))*cos(deg2rad($user['lng']-$venue['lng'])))));
                    $venues["distance"] = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                }
                $gallery = $venue->Gallery()->pluck('image')->toArray();
                $gallery = array_merge($gallery,array($venue['cover_pic']));
                $venue_details['description'] = $venue["description"];
                $venue_details['sports_types'] = $venue["sports_types"];
                $venue_details['is_verified'] = filter_var($venue["is_verified"], FILTER_VALIDATE_BOOLEAN);
                $venue_details['is_disabled'] = filter_var($venue["is_disabled"], FILTER_VALIDATE_BOOLEAN);
                $venue_details['lat'] = $venue["lat"];
                $venue_details['lng'] = $venue["lng"];
                $venue_details['location_name'] = $venue["location_name"];
                $venue_details['distance'] =  number_format((float)$venue["distance"], 1, '.', '').' km';
                $venue_details['rating'] = round($venue['rating']);
                $venue_details['grounds'] = $grounds;
                $venue_details['amenities'] = $amenity;
                $venue_details['images'] = $gallery;
                $new_item['venue_details'] = $venue_details;
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;
                $all_items_final[] = $new_item;
            }

            $all_items_games = Games::where('name', 'LIKE', '%' .  $search . '%')->orderBy('name','asc')->paginate(10);

            foreach($all_items_games as $game) {
                $new_item = array();
                $is_team = false;
                $is_player = false;
                $is_venue = false;
                $is_game = true;
                unset($game['created_at']);
                unset($game['updated_at']);
                $new_item['type'] = 'game';
                $new_item['id'] = $game->id;
                $new_item['name'] = $game->name;
                $new_item['cover_pic'] = $game['cover_pic'];
                $new_item['no_players'] = $game->Players()->wherePivot('game_id',$game->id)->wherePivot('invitation_status',1)->count();
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;
                $all_items_final[] = $new_item;
            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $all_items_final,
                    'currentPage' => $all_items->currentPage(),
                    'hasMorePages' => $all_items->hasMorePages()
                ]
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );
    }




    /**
     * @return \Illuminate\Http\JsonResponse
     * search Recent API
     */
    public function  recentSearch(Request $request){

        $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
        $user = $accesstoken->User()->first();
        if ($user) {

            $recent_searchs_teams = $user->RecentSearchs()->where('type',2)->orderBy('updated_at','desc')->limit(10)->get();
            $all_teams_final=array();

            foreach($recent_searchs_teams as $item) {
                $new_item = array();
                $is_player = false;
                $is_venue = false;
                $is_game = false;
                $new_item['type'] = 'team';
                $is_team = true;
                $team = Teams::whereId($item->id)->first();
                $new_item['id'] = $team->id;

                $new_item['name'] = $team->name;
                $new_item['cover_pic'] = $team['cover_pic'];
                $new_item['no_players'] = $team->Players()->wherePivot('team_id',$team->id)->wherePivot('invitation_status',1)->count();
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;
                $all_teams_final[] = $new_item;
            }

            $recent_searchs_players = $user->RecentSearchs()->where('type',1)->orderBy('updated_at','desc')->limit(10)->get();
            $all_players_final=array();
            foreach($recent_searchs_players as $item) {
                $new_item = array();
                $is_player = true;
                $is_venue = false;
                $is_game = false;
                $new_item['type'] = 'player';
                $is_team = false;
                $player = User::whereId($item->id)->first();
                if ($player) {
                  $new_item['id'] = $player->id;
                  $new_item['name'] = $player->name;
                  $new_item['cover_pic'] = $player['profile_pic'];
                  $new_item['is_player'] = $is_player;
                  $new_item['is_team'] = $is_team;
                  $new_item['is_game'] = $is_game;
                  $new_item['is_venue'] = $is_venue;
                  $all_players_final[] = $new_item;
                }
            }


            $final_search_list['teams'] = $all_teams_final;
            $final_search_list['players'] = $all_players_final;

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $final_search_list
                ]
            );
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Invalid user'
            ],500
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Search team API
     */
    public function  searchTeam(Request $request){
        if(isset($_GET['q']) && $_GET['q'] != '') {
            $search = $_GET['q'];
            $user = [];
            if ($request->header('accessToken')) {
                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                if ($accesstoken)
                    $user = $accesstoken->User()->first();

            }

            $all_items_final = array();
            $all_items = Teams::where('name', 'LIKE', '%' .  $search . '%')->orderBy('name','asc')->paginate(10);

            foreach($all_items as $team) {
                $new_item = array();
                $is_team = true;
                $is_player = false;
                $is_venue = false;
                $is_game = false;
                unset($team['created_at']);
                unset($team['updated_at']);
                $new_item['type'] = 'team';
                $new_item['id'] = $team->id;
                $new_item['name'] = $team->name;
                $new_item['cover_pic'] = $team['cover_pic'];
                $new_item['no_players'] = $team->Players()->wherePivot('team_id',$team->id)->wherePivot('invitation_status',1)->count();
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;

                $all_items_final[] = $new_item;
            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $all_items_final,
                    'currentPage' => $all_items->currentPage(),
                    'hasMorePages' => $all_items->hasMorePages()
                ]
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Search game API
     */

    public function  searchGame(Request $request){
        if(isset($_GET['q']) && $_GET['q'] != '') {
            $search = $_GET['q'];
            $user = [];
            if ($request->header('accessToken')) {
                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                if ($accesstoken)
                    $user = $accesstoken->User()->first();
            }

            $all_items_final = array();
            $all_items = Games::where('name', 'LIKE', '%' .  $search . '%')->orderBy('name','asc')->paginate(10);

            foreach($all_items as $game) {
                $new_item = array();
                $is_team = false;
                $is_player = false;
                $is_venue = false;
                $is_game = true;
                unset($game['created_at']);
                unset($game['updated_at']);
                $new_item['type'] = 'game';
                $new_item['id'] = $game->id;
                $new_item['name'] = $game->name;
                $new_item['cover_pic'] = $game['cover_pic'];
                $new_item['no_players'] = $game->Players()->wherePivot('game_id',$game->id)->wherePivot('invitation_status',1)->count();
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;
                $all_items_final[] = $new_item;
            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $all_items_final,
                    'currentPage' => $all_items->currentPage(),
                    'hasMorePages' => $all_items->hasMorePages()
                ]
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );
    }





    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Search player API
     */

    public function  searchPlayer(Request $request){
        if(isset($_GET['q']) && $_GET['q'] != '') {
            $search = $_GET['q'];
            $user = [];
            if ($request->header('accessToken')) {

                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                if ($accesstoken){
                    $user = $accesstoken->User()->first();
                }

            }

            $all_items_final = array();
            if ($user){
                $remove_id = array($user->id);
                $all_items = Users::whereNotIn('id',$remove_id)->where('name', 'LIKE', '%' .  $search . '%')->orderBy('name','asc')->where('role_id',3)->paginate(10);
            }else
                $all_items = Users::where('name', 'LIKE', '%' .  $search . '%')->orderBy('name','asc')->where('role_id',3)->paginate(10);

            foreach($all_items as $player) {
                $new_item = array();
                $is_team = false;
                $is_player = true;
                $is_venue = false;
                $is_game = false;
                unset($player['created_at']);
                unset($player['updated_at']);
                unset($player['password']);
                unset($player['otp']);
                unset($player['social_token']);
                $new_item['type'] = 'player';
                $new_item['id'] = $player->id;
                $new_item['name'] = $player->name;
                $new_item['cover_pic'] = $player['profile_pic'];
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;
                $new_item['points'] = 0;
                $new_item['player_details'] = $player;
                $all_items_final[] = $new_item;
            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $all_items_final,
                    'currentPage' => $all_items->currentPage(),
                    'hasMorePages' => $all_items->hasMorePages()
                ]
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Search venue API
     */

    public function  searchVenue(Request $request){
        if(isset($_GET['q']) && $_GET['q'] != '') {
            $search = $_GET['q'];
            $user = [];
            if ($request->header('accessToken')) {
                $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
                if ($accesstoken)
                    $user = $accesstoken->User()->first();
            }

            $all_items_final = array();
            $all_items = Venues::where('name', 'LIKE', '%' .  $search . '%')->where('lat','!=',0)->where('lng','!=',0)->where('is_available',1)->paginate(10);

            foreach($all_items as $venue) {
                $new_item = array();
                $is_team = false;
                $is_player = false;
                $is_venue = true;
                $is_game = false;
                unset($venue['created_at']);
                unset($venue['updated_at']);
                $new_item['id'] = $venue->id;
                $new_item['name'] = $venue->name;
                $new_item['cover_pic'] = $venue['cover_pic'];

                $grounds = $venue->Grounds()->orderBy('name','asc')->where('is_disabled',0)->get();
                $amenity = $venue->Amenities();
                $amenity = array_column($amenity, 'name');
                if (count($amenity)>0)
                    $amenity = implode( ",", $amenity );
                else
                    $amenity = '';
                $venue["distance"] = 0.0;
                if ($user && $user['lat'] != 0&& $user['lng'] != 0){
                    $degrees = rad2deg(acos((sin(deg2rad($user['lat']))*sin(deg2rad($venue['lat']))) + (cos(deg2rad($user['lat']))*cos(deg2rad($venue['lat']))*cos(deg2rad($user['lng']-$venue['lng'])))));
                    $venues["distance"] = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                }
                $gallery = $venue->Gallery()->pluck('image')->toArray();
                $gallery = array_merge($gallery,array($venue['cover_pic']));
                $venue_details['description'] = $venue["description"];
                $venue_details['sports_types'] = $venue["sports_types"];
                $venue_details['is_verified'] = filter_var($venue["is_verified"], FILTER_VALIDATE_BOOLEAN);
                $venue_details['is_disabled'] = filter_var($venue["is_disabled"], FILTER_VALIDATE_BOOLEAN);
                $venue_details['lat'] = $venue["lat"];
                $venue_details['lng'] = $venue["lng"];
                $venue_details['location_name'] = $venue["location_name"];
                $venue_details['distance'] =  number_format((float)$venue["distance"], 1, '.', '').' km';
                $venue_details['rating'] = round($venue['rating']);
                $venue_details['grounds'] = $grounds;
                $venue_details['amenities'] = $amenity;
                $venue_details['images'] = $gallery;
               $new_item['venue_details'] = $venue_details;
                $new_item['is_player'] = $is_player;
                $new_item['is_team'] = $is_team;
                $new_item['is_game'] = $is_game;
                $new_item['is_venue'] = $is_venue;
                $all_items_final[] = $new_item;
            }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'success' => $all_items_final,
                    'currentPage' => $all_items->currentPage(),
                    'hasMorePages' => $all_items->hasMorePages()
                ]
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Search following API
     */

    public function  searchFollowing(Request $request){
        if(isset($_GET['q']) && $_GET['q'] != '') {
            $search = $_GET['q'];
            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            $user = $accesstoken->User()->first();
            if ($request->has('user_id')){
                $new_user = Users::whereId($request['id'])->first();
                if ($new_user)
                    $user = $new_user;
            }
            if ($user){

                $new_item = array();
                $is_team = false;
                $is_player = false;
                $is_venue = false;
                $is_game = false;

                $all_items_final = array();

                $usersFollowing =  $user->Following()
                    ->wherePivot('is_requested',0)
                    ->where('name', 'LIKE', '%' .  $search . '%')
                    ->orderBy('name','asc')
                    ->limit(5)->get();

                $usersTeamsFollowing =  $user->TeamsFollowing()
                    ->where('name', 'LIKE', '%' .  $search . '%')
                    ->orderBy('name','asc')
                    ->limit(5)->get();


                foreach ($usersFollowing as $value){
                    $new_item['type'] = 'player';
                    $is_player = true;
                    $user = Users::where('id',$value['id'])->first();
                    if (!$user)
                        break;
                    $new_item['id'] = $user->id;
                    $new_item['name'] = $user->name;
                    $new_item['cover_pic'] = $user['profile_pic'];
                    $new_item['points'] = $user['points'];
                    unset($user['created_at']);
                    unset($user['updated_at']);
                    unset($user['password']);
                    unset($user['otp']);
                    unset($user['social_token']);
                    $new_item['player_details'] = $user;
                    $new_item['is_player'] = $is_player;
                    $new_item['is_team'] = $is_team;
                    $new_item['is_game'] = $is_game;
                    $new_item['is_venue'] = $is_venue;
                    $all_items_final[] = $new_item;
                }

                foreach ($usersTeamsFollowing as $value){
                    $new_item['type'] = 'team';
                    $is_team = true;
                    $team = Teams::whereId($value->id)->first();
                    if (!$team)
                        break;
                    $new_item['id'] = $team->id;
                    $new_item['name'] = $team->name;
                    $new_item['cover_pic'] = $team['cover_pic'];
                    $new_item['no_players'] = $team->Players()->wherePivot('team_id',$team->id)->wherePivot('invitation_status',1)->count();
                    $new_item['is_player'] = $is_player;
                    $new_item['is_team'] = $is_team;
                    $new_item['is_game'] = $is_game;
                    $new_item['is_venue'] = $is_venue;
                    $all_items_final[] = $new_item;
                }

                return response()->json(
                    [
                        'status' => 'success',
                        'statusCode' => 200,
                        'success' => $all_items_final
                    ]
                );
            }

            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'User Not found'
                ],500
            );

        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'JSON schema failed'
            ],500
        );
    }


    public function  updateTable(){

            $all_players = Users::orderBy('name','asc')->where('role_id',3)->get();
            $all_teams = Teams::orderBy('name','asc')->get();
            $all_games = Games::orderBy('name','asc')->get();
            $all_venues = Venues::orderBy('name','asc')->where('is_available',1)
                ->where('lng','!=',0)
                ->where('lat','!=',0)
                ->get();
        foreach ($all_players as $player){
            $search = new Search();
            $search['id'] = $player->id;
            $search['name'] = $player->name;
            $search['type'] = 1;
            $search->save();
        }

        foreach ($all_teams as $team){
            $search = new Search();
            $search['id'] = $team->id;
            $search['name'] = $team->name;
            $search['type'] = 2;
            $search->save();
        }

        foreach ($all_games as $game){
            $search = new Search();
            $search['id'] = $game->id;
            $search['name'] = $game->name;
            $search['type'] = 3;
            $search->save();
        }

        foreach ($all_venues as $venue){
            $search = new Search();
            $search['id'] = $venue->id;
            $search['name'] = $venue->name;
            $search['type'] = 4;
            $search->save();
        }

            return response()->json(
                [
                    'status' => 'success',
                    'statusCode' => 200,
                    'message' => 'Search listing',
                    'all_players' => $all_players,
                    'all_games' => $all_games,
                    'all_teams' => $all_teams,
                ]
            );


    }

}
