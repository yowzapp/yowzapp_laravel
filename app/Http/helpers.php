<?php
use Illuminate\Support\Facades\Log;


function getDateString($date_recieved )
{
    if (!$date_recieved)
        $date_recieved = '';
    $date = date("Y-m-d", strtotime($date_recieved));
    $time = date("h.i A", strtotime($date_recieved));
    $today = new \DateTime("now", new \DateTimeZone('Asia/Dubai'));
    $current_date = $today->format('Y-m-d');
    $current_time = $today->format('h.i A');
    $datediff = abs(strtotime($current_date) - strtotime($date));
    $timediff = abs(strtotime($current_time) - strtotime($time));
    $no_hours = floor($timediff / (60 * 60));
    $minutes = (($timediff / 60) % 60);
    $seconds = $timediff % 60;
    $no_days = floor($datediff / (60 * 60 * 24));
    if ($no_days > 6) {
        $current_date = $today->format('d/m/Y');
        $notification_time = $current_date;
    }else if ($no_days>1)
        $notification_time = $no_days.' days ago';
    else if ($no_days == 1)
        $notification_time = $no_days.' day ago';
    else {
        if ($no_hours>1)
            $notification_time = $no_hours.' hours ago';
        else if ($no_hours == 1)
            $notification_time = $no_hours.' hour ago';
        else{
            if ($minutes>=1)
                $notification_time = $minutes.' min ago';
            else{
                if ($seconds>30)
                    $notification_time = $seconds.' sec ago';
                else
                    $notification_time =  'just now';
            }
        }
    }
    return $notification_time;
}


function random_color ($minVal = 0, $maxVal = 255)
{

    // Make sure the parameters will result in valid colours
    $minVal = $minVal < 0 || $minVal > 255 ? 0 : $minVal;
    $maxVal = $maxVal < 0 || $maxVal > 255 ? 255 : $maxVal;

    // Generate 3 values
    $r = mt_rand($minVal, $maxVal);
    $g = mt_rand($minVal, $maxVal);
    $b = mt_rand($minVal, $maxVal);

    // Return a hex colour ID string
    return sprintf('#%02X%02X%02X', $r, $g, $b);

}


function chatNotification($request,$data,$tag)
{
     $server_key = 'AAAA4-HIsSI:APA91bFS__Y9IHCmMLxvdXM1iCfijvWTbaA5j8hpTCHkvUvPhmwvJRrmrcR4vAJ00BATNLkUc3_SgEjtPtfw337MGNo87E3aNZ74ZRF8Ouh939hKrHs8o1KuUa_fzCrDHciouU2wxeVW';

     $device_token_array = $request['token'];
     $name = $request['user'];
     $message = $request['message'];
     if ($device_token_array) {

         $fields = array();

         $fields['collapse_key'] = 'Chat';
         $fields['data'] = $data;
         $url = 'https://fcm.googleapis.com/fcm/send';
         $notification = array();
         $notification['body'] = $message;
         $notification['title'] = $name;
         $notification['icon'] = 'ic_logo_play';
         $notification['sound'] = 'default';
         $notification['tag'] = $tag;
         $fields['notification'] = $notification;
         if (is_array($device_token_array)) {
             $fields['registration_ids'] = $device_token_array;
         } else {
             $fields['to'] = $device_token_array;
         }

         //header with content_type api key
         $headers = array(
             'Content-Type:application/json',
             'Authorization:key=' . $server_key
         );

         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_POST, true);
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
         $result = curl_exec($ch);
         curl_close($ch);
         return $result;
     }
 }


function sendNotificationwithBody($device_token_array,$content,$data = NULL, $type, $user_game_team_id = NULL)
{
    $server_key = 'AAAA4-HIsSI:APA91bFS__Y9IHCmMLxvdXM1iCfijvWTbaA5j8hpTCHkvUvPhmwvJRrmrcR4vAJ00BATNLkUc3_SgEjtPtfw337MGNo87E3aNZ74ZRF8Ouh939hKrHs8o1KuUa_fzCrDHciouU2wxeVW';
    if (is_array($device_token_array)) {
        if (sizeof($device_token_array) == 0)
            return 0;
    } else {
        if ($device_token_array == Null || $device_token_array == '')
            return 0;
    }

    if ($device_token_array) {

        $fields = array();
        $fields['data'] = $data;
        $url = 'https://fcm.googleapis.com/fcm/send';
        $notification = array();
        $notification['body'] = ucfirst($content);
        $notification['title'] = $data['title'];
        $notification['icon'] = 'ic_logo_play';
        $notification['sound'] = 'default';
        // $notification['notification_type'] = $type;
        $fields['notification'] = $notification;
        $fields['collapse_key'] = 'notification';
        if (is_array($device_token_array)) {
            $fields['registration_ids'] = $device_token_array;
        } else {
            $fields['to'] = $device_token_array;
        }
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $server_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
