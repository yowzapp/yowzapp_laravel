<?php

namespace App\Http\Middleware;

use App\Models\AccessToken;
use Closure;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Log;

class apiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */



    public function handle($request, Closure $next)
    {
        Log::info($request);
        Log::info($request->header('accessToken').' ---- header');
        if($request->header('accessToken')){

            $accesstoken = AccessToken::where('access_token', $request->header('accessToken'))->first();
            if ($accesstoken) {
                $today = date("Y-m-d H:i:s");
                $to_time = strtotime($today);
                $from_time = strtotime($accesstoken['updated_at']);
                if ( round(abs($to_time - $from_time) / 60,2)>86400){
                    $accesstoken->delete();
                    return response()->json(
                        [
                            'status' => 'error',
                            'message' => 'Session timed out, Please login again',
			                'statusCode' => 400
                        ],400
                    );
                }
                return $next($request);
            }else{
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'Invalid access token',
			                'statusCode' => 401
                    ],401
                );
            }
        }
        return response()->json(
            [
                'status' => 'error',
                'message' => 'Access token required',
                'statusCode' => 400
            ],400
        );

    }
}
