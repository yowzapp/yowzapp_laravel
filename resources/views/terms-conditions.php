<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html>
<head>
    <title>Privacy policy</title>

    <style>
        body{
            text-align: center;
            font-family: 'Raleway', sans-serif;
        }
        form{
            width: 640px;
            display: block;
            margin: 0 auto;
            overflow: hidden;
            position: relative;
            padding-bottom: 30px;
        }
        form input[type=email]{
            border: 1px solid #ecedee;
            border-radius: 70px 0 0 70px;
            display: inline-block;
            height: 42px;
            width:320px;
            outline: none;
            font-family: 'Raleway', sans-serif;
            font-weight: 400;;
        }
        .flash-msg{font-family: 'Raleway', sans-serif;font-weight: 400;}
        form .parsley-errors-list{
            position: absolute;
            left: 25px;
            right: 0;
            top: 29px;
            z-index: 999;
        }
        form .parsley-errors-list li{
            color: #ff0000;
            font-family: 'Raleway', sans-serif;
            font-weight: 400;
        }
        input[type=submit]{
            border: none;
            border-radius: 0 70px 70px 0;
            color: #ffffff;
            display: inline-block;
            font-size: 16px;
            height: 46px;
            left: -6px;
            position: relative;
            text-align: center;
            width:220px;
            cursor: pointer;
            outline: none;
            font-family: 'Raleway', sans-serif;;
            background-image: url('img/Shape.jpg');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }

        .tagline{
            text-align: center;
            padding-top: 18px;
            color:#345560;
            font-size: 36px;
            font-family: 'Raleway', sans-serif;
            font-weight: 500;;
        }


        input[placeholder]{
            color:#345560 ;
            padding-left:22px;
            font-size:16px;
        }
        ul{
            list-style: none;
            padding: 0;
        }
        ul li{
            padding: 0 10px;
            float: left;
            display: inline-block;
        }
        ul li a{
            display: inline-block;
        }
        @media(max-width: 640px){
            body{overflow: auto;}
            .designstring-logo{width: 65%;}
            .tagline{font-size: 28px !important;}
            form{width: 100% !important;


            }
            form input[type=email]{width: 170px;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
            }
            .parsley-errors-list{
                top: 36px !important;
            }
            form input[type=submit]{width: 100px;}
        }
    </style>
</head>


<body>

<div class="container">
    <div class="terms-palythora-block">
        <h3>
            Designstring terms and condition
        </h3>
        <p>
            Terms and condition comming soon..
        </p>
    </div>

</div>
</body>
</html>
