@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ Auth::user()->name[0] }}"
                         class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">{{ trans('backpack::base.administration') }}</li>

                <li>
                    <a href="{{ url('admin/bookings') }}">
                        <i class="fa fa-bookmark"></i>
                        <span>Bookings</span>
                    </a>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gg-circle"></i>
                        <span>Venue Partners</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url('admin/venues') }}">
                                <i class="fa fa-sitemap"></i>
                                <span>Venues Listing</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/vendors') }}">
                                <i class="fa fa-user"></i>
                                <span>Vendors List</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Players</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url('admin/players') }}">
                                <i class="fa fa-user"></i>
                                <span>List of Players</span>
                            </a>

                        </li>
                        <li>
                            <a href="{{ url('admin/games') }}">
                                <i class="fa fa-futbol-o"></i>
                                <span>List of Games</span>
                            </a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="{{ url('admin/points') }}">--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<span>Players Points</span>--}}
                            {{--</a>--}}

                        {{--</li>--}}
                        <li>
                            <a href="{{ url('admin/teams') }}">
                                <i class="fa fa-users"></i>
                                <span>List of Teams</span>
                            </a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="{{ url('admin/teampoints') }}">--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<span>List of Teams Points</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}

                    </ul>
                </li>



                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-rocket"></i>
                        <span>Promotions</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url('admin/banner') }}">
                                <i class="fa fa-mobile"></i>
                                <span>App Banners</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="{{ url('admin/pushNotification') }}">
                                <i class="fa fa-bell"></i>
                                <span>Send Push Notification to All</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-sliders"></i>
                        <span>Configurations</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url('admin/sports') }}">
                                <i class="fa fa-trophy"></i>
                                <span>Sports</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/locations') }}">
                                <i class="fa fa-location-arrow"></i>
                                <span>Supported Locations</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/amenities') }}">
                                <i class="fa fa-flag"></i>
                                <span>Amenities</span>
                            </a>
                        </li>
                    </ul>
                </li>





                {{--<li class="treeview">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-user"></i>--}}
                        {{--<span>Users</span>--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}

                        {{--<li>--}}
                            {{--<a href="{{ url('admin/vendors') }}">--}}
                                {{--<span>Vendors</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                            {{--<a href="{{ url('admin/users') }}">--}}
                                {{--<span>Admin</span>--}}
                            {{--</a>--}}

                        {{--</li>--}}

                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a href="{{ url('admin/roles') }}">--}}
                        {{--<i class="fa fa-black-tie"></i>--}}
                        {{--<span>Roles</span>--}}
                    {{--</a>--}}
                {{--</li>--}}






                {{--<li>--}}
                    {{--<a href="{{ url('admin/orders') }}">--}}
                        {{--<i class="fa fa-shopping-cart"></i>--}}
                        {{--<span>Orders</span>--}}
                    {{--</a>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a href="{{ url('admin/products') }}">--}}
                        {{--<i class="fa fa-star "></i>--}}
                        {{--<span>Products</span>--}}
                    {{--</a>--}}
                {{--</li>--}}








                <!-- ================================================ -->
                <!-- ==== Recommended place for admin menu items ==== -->
                <!-- ================================================ -->
                <!-- ======================================= -->
                <li class="header">{{ trans('backpack::base.user') }}</li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i
                                class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
@endif
