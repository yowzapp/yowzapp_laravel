@extends('backpack::layout')

@section('content-header')
	<section class="content-header">
		<h1>
			{{ trans('backpack::crud.preview') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
			<li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
			<li class="active">{{ trans('backpack::crud.preview') }}</li>
		</ol>
	</section>
@endsection

@section('content')
	@if ($crud->hasAccess('list'))
		<a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
	@endif

	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title" style="margin-top: 0">
				{{$entry->name}}
			</h3>
		</div>
		<div class="box-body">
			<div class="row">

				<div class="col-md-4">

					<img  src="{{$entry->image}}" alt="" style=" max-width: 250px;" />

				</div>
				<div class="col-md-4">
					<p style="display: inline-flex">
						<strong style="width:100px;display: inline-block">Created On : </strong>
						{{ date('jS M, Y', strtotime($entry->created_at)) }}
					</p>
					<br>
					<p style="display: inline-flex">
						<strong style="width:100px;display: inline-block">Updated On : </strong>
						{{ date('jS M, Y', strtotime($entry->updated_at)) }}
					</p>

					<p>
						<strong style="width:100px;display: inline-block">Description: </strong>
						@if($entry->description)
							{{ $entry->description }}
						@else
							N/A
						@endif
					</p>
					<p>
						<strong style="width:100px;display: inline-block">About Domain: </strong>
						@if($entry->about_domain)
							{{ $entry->about_domain }}
						@else
							N/A
						@endif
					</p>

					<p>
						<strong style="width:100px;display: inline-block">Status: </strong>
						@if($entry->status==1):
						<i class="fa fa-check green"></i>
						@else
							<i class="fa fa-cross red"></i>
						@endif
					</p>
				</div>
				<div class="col-md-4">
					<p>
						<strong style="width:100px;display: inline-block">Industry Size: </strong>
						@if($entry->industry_size)
							{{ $entry->industry_size }}
						@else
							N/A
						@endif
					</p>
					<p>
						<strong style="width:100px;display: inline-block">Maturity of Domain: </strong>
						@if($entry->maturity_of_domain)
							{{ $entry->maturity_of_domain }}
						@else
							N/A
						@endif
					</p>
					<p>
						<strong style="width:100px;display: inline-block">Technology Used: </strong>
						@if($entry->technology_used)
							{{ $entry->technology_used }}
						@else
							N/A
						@endif
					</p>
					<p>
						<strong style="width:100px;display: inline-block">Jobs Available: </strong>
						@if($entry->jobs_available)
							{{ $entry->jobs_available }}
						@else
							N/A
						@endif
					</p>
					<p>
						<strong style="width:100px;display: inline-block">Salary Level: </strong>
						@if($entry->salary_level)
							{{ $entry->salary_level }}
						@else
							N/A
						@endif
					</p>
				</div>
			</div>

			<div style="margin-top: 25px;">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation"  class="active">
						<a href="#speaker" aria-controls="" role="tab" data-toggle="tab">
							Domain Speakers
						</a>
					</li>
					<li role="presentation"  class="">
						<a href="#document" aria-controls="" role="tab" data-toggle="tab">
							Domain Documents
						</a>
					</li>
					<li role="presentation"  class="">
						<a href="#company" aria-controls="" role="tab" data-toggle="tab">
							Domain Companies
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="speaker">
						<p style="margin: 20px 0">
							<a data-toggle="modal" data-target="#add-speaker" class="btn btn-primary">+ Spekers</a>
						</p>

						<table class="table table-striped" id="">
							<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Image</th>
								<th>Description</th>
								<th>Status</th>
								<th>Link</th>
								<th class="actions">Actions</th>
							</tr>
							</thead>
							@if(count($entry->domainSpeaker))
								@foreach($entry->domainSpeaker as $k=>$speaker)
									<tr>
										<td>{{$speaker->id}}</td>
										<td>{{$speaker->name}}</td>
										<td><img style="width: 112px; height: 101px;" src="{{$speaker->image}}" alt=""></td>
										<td>{{$speaker->description}}</td>
										<td>
											@if($speaker->status==1)
												<i class="fa fa-check green"></i>
											@else
												<i class="fa fa-cross red"></i>
											@endif
										</td>
										<td>{{$speaker->link}}</td>
										<td>
											<a href="/admin/domainspeaker/{{$speaker->id}}/edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i> </a>
											<a href="javascript: void(0)" data-id="/admin/domainspeaker/delete/{{$speaker->id.'/'.$entry->id}}" class="btn btn-xs btn-default remove-speaker" data-toggle="modal" data-target=".delete-modal"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="6" class="text-center"> No Data Found</td>
								</tr>

							@endif
						</table>
					</div>
					<div role="tabpanel" class="tab-pane" id="document">
						<p style="margin: 20px 0">
							<a data-toggle="modal" data-target="#add-document" class="btn btn-primary">+ Document</a>
						</p>

						<table class="table table-striped" id="">
							<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Image</th>
								<th>Description</th>
								<th>Status</th>
								<th>Link</th>
								<th class="actions">Actions</th>
							</tr>
							</thead>
							@if(count($entry->domainDocument))
								@foreach($entry->domainDocument as $k=>$document)
									<tr>
										<td>{{$document->id}}</td>
										<td>{{$document->name}}</td>
										<td><img style="width: 112px; height: 101px;" src="{{$document->image}}" alt=""></td>
										<td>{{$document->description}}</td>
										<td>
											@if($document->status==1)
												<i class="fa fa-check green"></i>
											@else
												<i class="fa fa-cross red"></i>
											@endif
										</td>
										<td>{{$document->link}}</td>
										<td>
											<a href="/admin/domaindocument/{{$document->id}}/edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i> </a>
											<a href="javascript: void(0)" data-id="/admin/domaindocument/delete/{{$document->id.'/'.$entry->id}}" class="btn btn-xs btn-default remove-speaker" data-toggle="modal" data-target=".delete-modal"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="6" class="text-center"> No Data Found</td>
								</tr>

							@endif
						</table>
					</div>
					<div role="tabpanel" class="tab-pane" id="company">
						<table class="table table-striped" id="">
							<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Image</th>
								<th>Description</th>
								<th>Status</th>
								<th>Link</th>
							</tr>
							</thead>
							@if(count($entry->domainCompany))
								@foreach($entry->domainCompany as $k=>$company)
									<tr>
										<td>{{$company->id}}</td>
										<td>{{$company->name}}</td>
										<td><img style="width: 112px; height: 101px;" src="{{$company->image}}" alt=""></td>
										<td>{{$company->description}}</td>
										<td>
											@if($company->status==1)
												<i class="fa fa-check green"></i>
											@else
												<i class="fa fa-cross red"></i>
											@endif
										</td>
										<td>{{$company->link}}</td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="6" class="text-center"> No Data Found</td>
								</tr>

							@endif
						</table>
					</div>
				</div>
			</div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
	<div class="modal fade" id="add-speaker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel" style="display: inline-block">Add Domain Speaker </h4>

				</div>
				<div class="modal-body">
					{!! Form::open(array('url' => 'admin/domainspeaker', 'method' => 'post' ,'enctype'=>'multipart/form-data','data-parsley-validate'  => '','novalidate')) !!}

					<ul class="list-unstyled">
						<li>
							<div class="form-group">
								<label>Name</label><span style="color:red">*</span> <br>
								<input class="form-control" name="name" required>
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Image </label><span style="color:red">*</span> <br>
								<div class="img-wrapper" style="margin-bottom:30px;width:70px; height: 70px;display: none;">
									<img class="selected-image" src="#" alt="" style="width: 100%;">
								</div>
								<input class="image-upload speaker-profile-pic" type="file" name="image_upload" required />
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Description</label><span style="color:red">*</span> <br>
								<textarea class="form-control" name="description" rows="3" required></textarea>
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Link</label><span style="color:red">*</span> <br>
								<input class="form-control" name="link" required>
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Status</label>
								<select class="form-control" name="status">
									@foreach(config('app.status') as $k=>$v)
										<option value="{{$k}}">{{$v}}</option>
									@endforeach
								</select>
							</div>
						</li>
						<li>
							<div class="form-group">
								<input type="hidden" name="domain_id" value="{{$entry->id}}" />
								<input type="submit" class="btn btn-primary" value="Save" />
							</div>
						</li>
					</ul>
					</form>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="modal fade" id="add-document" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel" style="display: inline-block">Add Domain Document </h4>

				</div>
				<div class="modal-body">
					{!! Form::open(array('url' => 'admin/domaindocument', 'method' => 'post' ,'enctype'=>'multipart/form-data','data-parsley-validate'  => '','novalidate')) !!}

					<ul class="list-unstyled">
						<li>
							<div class="form-group">
								<label>Name</label><span style="color:red">*</span> <br>
								<input class="form-control" name="name" required>
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Image </label><span style="color:red">*</span> <br>
								<div class="img-wrapper" style="margin-bottom:30px;width:70px; height: 70px;display: none;">
									<img class="selected-image" src="#" alt="" style="width: 100%;">
								</div>
								<input class="image-upload speaker-profile-pic" type="file" name="image_upload" required />
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Description</label><span style="color:red">*</span> <br>
								<textarea class="form-control" name="description" rows="3" required></textarea>
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Link</label><span style="color:red">*</span> <br>
								<input class="form-control" name="link" required>
							</div>
						</li>
						<li>
							<div class="form-group">
								<label>Status</label>
								<select class="form-control" name="status">
									@foreach(config('app.status') as $k=>$v)
										<option value="{{$k}}">{{$v}}</option>
									@endforeach
								</select>
							</div>
						</li>
						<li>
							<div class="form-group">
								<input type="hidden" name="domain_id" value="{{$entry->id}}" />
								<input type="submit" class="btn btn-primary" value="Save" />
							</div>
						</li>
					</ul>
					</form>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="modal delete-modal fade" id="product-options " tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Delete</h4>
				</div>
				<div class="modal-body">
					<div class="text-center">
						<h3 style=" margin: 0 0 35px;">Are you sure you want to delete?</h3>
						<ul class="list-inline">
							<li>
								<a href="/admin/cmeSpeaker/1/delete" id="delete" class="btn btn-primary confirm-delete-detail">Yes</a>
							</li>
							<li>
								<a class="cancel-gallery btn btn-danger">No</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
    $(document).ready(function($) {

        $('.cancel-gallery').click(function(){
            location.reload();
        });

        $('.remove-speaker').click(function(){
            var $this = $(this);
            var id = $this.data('id');
            $('.confirm-delete-detail').attr('href', id);
        });
    });


</script>
