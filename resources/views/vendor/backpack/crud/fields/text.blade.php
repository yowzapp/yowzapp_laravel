<!-- text input -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>

    @if(isset($field['prefix']) || isset($field['suffix'])) <div class="input-group"> @endif
        @if(isset($field['prefix'])) <div class="input-group-addon">{!! $field['prefix'] !!}</div> @endif
        <input
                @if(isset($field['id'])) id="{{isset($field['id'])?$field['id']:''}}" @endif
               @if(isset($field['disabled'])) disabled="{{isset($field['disabled'])?$field['disabled']:''}}" @endif
               @if(isset($field['required'])) required="{{isset($field['required'])?$field['required']:''}}" @endif
               @if(isset($field['readonly'])) {{isset($field['readonly'])?$field['readonly']:''}} @endif
               @if(isset($field['data-parsley-error-message'])) data-parsley-error-message="{{isset($field['data-parsley-error-message'])?$field['data-parsley-error-message']:''}}" @endif
               @if(isset($field['data-parsley-type'])) data-parsley-type="{{isset($field['data-parsley-type'])?$field['data-parsley-type']:''}}" @endif
               @if(isset($field['data-parsley-maxlength'])) data-parsley-maxlength="{{isset($field['data-parsley-maxlength'])?$field['data-parsley-maxlength']:''}}" @endif
               @if(isset($field['data-parsley-remote-message'])) data-parsley-remote-message="{{isset($field['data-parsley-remote-message'])?$field['data-parsley-remote-message']:''}}" @endif
            type="text"
            name="{{ $field['name'] }}"
            value="{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : '' )) }}"
            @include('crud::inc.field_attributes')
        >
        @if(isset($field['suffix'])) <div class="input-group-addon">{!! $field['suffix'] !!}</div> @endif
    @if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>


{{-- FIELD EXTRA CSS  --}}
{{-- push things in the after_styles section --}}

    {{-- @push('crud_fields_styles')
        <!-- no styles -->
    @endpush --}}


{{-- FIELD EXTRA JS --}}
{{-- push things in the after_scripts section --}}

    {{-- @push('crud_fields_scripts')
        <!-- no scripts -->
    @endpush --}}


{{-- Note: you can use @if ($crud->checkIfFieldIsFirstOfItsType($field, $fields)) to only load some CSS/JS once, even though there are multiple instances of it --}}