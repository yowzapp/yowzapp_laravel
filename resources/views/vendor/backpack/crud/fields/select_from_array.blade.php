<!-- select -->
<div class="form-group col-md-12" id="@if(isset($field['id'])){{$field['id']}}@endif">
	<label>{{ $field['label'] }}
		@if(isset($field['required']))
			<span style="color: red">*</span>
		@endif</label>
	<select
			class="form-control @if(isset($field['class'])) {{ $field['class'] }} @endif"

	@foreach ($field as $attribute => $value)
		@if (is_string($value))
			{{ $attribute }}="{{ $value }}"
		@endif
	@endforeach

	@if(isset($field['required']))
		required
	@endif
	>

	@if (isset($field['allows_null']) && $field['allows_null']==true)
		<option value="">-</option>
	@endif
	<option value="">Select</option>

	@if (count($field['options']))

		@foreach ($field['options'] as $key => $value)


			<option value="{{ $key }}"

					@if (isset($field['value']) && $key==$field['value'])
					@if ($key==$field['value'])

					selected
					@endif
					@endif

			>{{ $value }}</option>
			@endforeach
			@endif
			</select>
</div>
