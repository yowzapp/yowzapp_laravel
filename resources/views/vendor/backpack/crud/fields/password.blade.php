<!-- password -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    <input
    	type="password"
        @if(isset($field['required'])) required="{{isset($field['required'])?$field['required']:''}}" @endif
        @if(isset($field['id'])) id="{{isset($field['id'])?$field['id']:''}}" @endif
        @if(isset($field['data-parsley-max'])) data-parsley-max="{{isset($field['data-parsley-max'])?$field['data-parsley-max']:''}}" @endif
        @if(isset($field['data-parsley-min'])) data-parsley-min="{{isset($field['data-parsley-min'])?$field['data-parsley-min']:''}}" @endif
        @if(isset($field['data-parsley-pattern'])) data-parsley-pattern="{{isset($field['data-parsley-pattern'])?$field['data-parsley-pattern']:''}}" @endif
        @if(isset($field['data-parsley-error-message'])) data-parsley-error-message="{{isset($field['data-parsley-error-message'])?$field['data-parsley-error-message']:''}}" @endif
        @if(isset($field['data-parsley-equalto'])) data-parsley-equalto="{{isset($field['data-parsley-equalto'])?$field['data-parsley-equalto']:''}}" @endif
    	name="{{ $field['name'] }}"
        @include('crud::inc.field_attributes')
    	>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>