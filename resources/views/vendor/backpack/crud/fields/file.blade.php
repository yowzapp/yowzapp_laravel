<div @include('crud::inc.field_wrapper_attributes') >

    <label>{!! $field['label'] !!}
        @if(isset($field['required']))
            <span style="color: red">*</span>
        @endif
        @if(isset($field['tooltip']))
            <i class="fa fa-question-circle"  data-toggle="tooltip" data-placement="left" title="{{$field['tooltip']}}"></i>
        @endif</label>

    @if(isset($field['prefix']) || isset($field['suffix'])) <div class="input-group"> @endif
        @if(isset($field['prefix'])) <div class="input-group-addon">{!! $field['prefix'] !!}</div> @endif

        @if(isset($field['value']))
        <div class="img-wrapper" style="margin-bottom:30px;width:180px;">
            <img class="selected-image" src="{{$field['value']}}" alt="" style="width: 100%;">
        </div>
        @elseif(isset($entry->image) &&  $entry->image)
            <div class="img-wrapper" style="margin-bottom:30px;width:180px;">
                <img class="selected-image" src="{{$entry->image}}" alt="" style="width: 100%;">
                <input type="hidden" name="image_link" value="{{$entry->image}}" class="image-link" />
                <ul class="img-list"></ul>
            </div>
        @elseif(isset($entry->cover_pic) &&  $entry->cover_pic)
            <div class="img-wrapper" style="margin-bottom:30px;width:180px;">
                <img class="selected-image" src="{{$entry->cover_pic}}" alt="" style="width: 100%;">
                <input type="hidden" name="image_link" value="{{$entry->cover_pic}}" class="image-link" />
                <ul class="img-list"></ul>
            </div>
        @else
            <div class="img-wrapper" style="margin-bottom:30px;width:180px; display:none;">
                <img class="selected-image" src="" style="width:100%;">
                <input type="hidden" name="image_link" value="" class="image-link" />
                <ul class="img-list"></ul>
            </div>
        @endif


        <input  @if(isset($field['required']) && (!(isset($entry->image)|| isset($entry->cover_pic))))
                required
                @endif
                type="file"
                class="image-upload form-control"
                name="{{ $field['name'] }}"
                value="{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : '' )) }}"
                @include('crud::inc.field_attributes')
        >
        @if(isset($field['suffix'])) <div class="input-group-addon">{!! $field['suffix'] !!}</div> @endif
        @if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
<style>
    .img-list {
        list-style: none;
    }
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>

    $(document).ready(function() {
        $(".image-upload").change(function() {
            readURL($(this));
        });
    });

    function readURL(input) {

        if ( input[0] && input[0]['files'] ) {

            input.closest('div').find('.img-wrapper').show();
            $.each(input[0]['files'], function(index, file) {
//                    console.log(file);
                var reader = new FileReader();

                reader.onload = function (e) {
                    input.closest('div').find('.img-wrapper .selected-image').attr('src', e.target.result);

                }

                reader.readAsDataURL(file);
            });

        } else {
            console.log('error');
        }
    }
</script>