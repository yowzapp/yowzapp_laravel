<div class="col-md-12" style="font-size: 13px;font-weight: 600;">
	<p style="margin-bottom: 0;color: #8f8f94;">Use 8-20 characters </p>
	<p style="margin-bottom: 0;color: #8f8f94;">Have at least:</p>
	<p style="margin-bottom: 0;color: #8f8f94;">1 lowercase letter</p>
	<p style="margin-bottom: 0;color: #8f8f94;">1 upper case letter</p>
	<p style="margin-bottom: 0;color: #8f8f94;">1 numerical value</p>
	<p style="color: #8f8f94;">1 special character: !@#$&amp;</p>
</div>