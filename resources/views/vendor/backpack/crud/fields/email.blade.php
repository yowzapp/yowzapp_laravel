<!-- text input -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    <input
    	type="email"
    	name="{{ $field['name'] }}"
        @if(isset($field['required'])) required="{{isset($field['required'])?$field['required']:''}}" @endif
        @if(isset($field['data-parsley-error-message'])) data-parsley-error-message="{{isset($field['data-parsley-error-message'])?$field['data-parsley-error-message']:''}}" @endif
        value="{{ old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : '' )) }}"
        @include('crud::inc.field_attributes')
    	>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>