@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::crud.preview') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{$entry->name}}
            </h3>
        </div>
        <div class="box-body">

            <div class="row">

                <div class="col-md-4">

                    <img  src="{{$entry->cover_pic}}" alt="" style=" max-width: 250px;" />

                </div>
                <div class="col-md-4">
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Name : </strong>
                        {{$entry->name }}
                    </p>
                    <br>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Updated On : </strong>
                        {{ date('jS M, Y', strtotime($entry->updated_at)) }}
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Description: </strong>
                        @if($entry->description)
                            {{ $entry->description }}
                        @else
                            N/A
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Activity Meter: </strong>
                        {{$entry->activity_meter}}
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Points: </strong>
                        {{$entry->points}}
                    </p>

                </div>
                <div class="col-md-4">

                </div>
            </div>
            <div style="margin-top: 25px;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"  class="active">
                        <a href="#members" aria-controls="" role="tab" data-toggle="tab">
                            Members
                        </a>
                    </li>
                    <li role="presentation"  class="">
                        <a href="#games" aria-controls="" role="tab" data-toggle="tab">
                            Games
                        </a>
                    </li>
                    <li role="presentation"  class="">
                        <a href="#gallery" aria-controls="" role="tab" data-toggle="tab">
                            Team Gallery
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="members">
                        <div role="tabpanel" class="tab-pane active" id="members">
                            <table class="table table-striped" id="">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Points</th>
                                    <th>Invitation Status</th>
                                    <th>Is Admin?</th>
                                    <th>Is Super Admin?</th>
                                    <th>Is Invited to Pay?</th>
                                    <th>Is Requested?</th>
                                </tr>
                                </thead>
                                @if(count($entry->Players))

                                    @foreach($entry->Players as $k=>$member)
                                       @if($member->pivot->invitation_status == 1)
                                        <tr>
                                            <td>{{$member->name}} <br>
                                                {{$member->mobile}} <br>
                                                {{$member->email}}

                                            </td>
                                            <td><img style="width: 112px; height: 101px;" src="{{$member->profile_pic}}" alt=""></td>
                                            <td>{{$member->points}}

                                            </td>
                                            <td>{{$member->pivot_invitation_status}}
                                                @if($member->pivot->invitation_status==1)
                                                    <i class="fa fa-check green"></i>
                                                @else
                                                    <i class="fa fa-times red"></i>
                                                @endif
                                            </td>
                                            <td>
                                                @if($member->pivot->is_admin==1)
                                                    <i class="fa fa-check green"></i>
                                                @else
                                                    <i class="fa fa-times red"></i>
                                                @endif
                                            </td>
                                            <td>
                                                @if($member->pivot->is_super_admin==1)
                                                    <i class="fa fa-check green"></i>
                                                @else
                                                    <i class="fa fa-times red"></i>
                                                @endif
                                            </td>
                                            <td>
                                                @if($member->pivot->invited_to_pay==1)
                                                    <i class="fa fa-check green"></i>
                                                @else
                                                    <i class="fa fa-times red"></i>
                                                @endif
                                            </td>
                                            <td>
                                                @if($member->pivot->is_requested==1)
                                                    <i class="fa fa-check green"></i>
                                                @else
                                                    <i class="fa fa-times red"></i>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6" class="text-center"> No Data Found</td>
                                    </tr>

                                @endif
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="games">
                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            @if(count($entry->Games))
                                @foreach($entry->Games as $k=>$game)
                                    <tr>

                                        <td>{{$game->name}}</td>
                                        <td><img style="width: 112px; height: 101px;" src="{{$game->cover_pic}}" alt=""></td>
                                        <td>{{$game->description}}</td>
                                        <td>
                                            <a href="/admin/games/{{$game->id}}" class="btn btn-xs btn-default remove-speaker" ><i class="fa fa-eye"></i></a>

                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="gallery">

                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Image</th>
                            </tr>
                            </thead>
                            @if(count($entry->Galleries))
                                @foreach($entry->Galleries as $k=>$gallery)
                                    <tr>
                                        <td><img style="width: 112px; height: 101px;" src="{{$gallery->image}}" alt=""></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="1" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection
