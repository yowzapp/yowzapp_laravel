@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::crud.preview') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title" style="margin-top: 0">
                {{$entry->transaction_identifier}}
            </h3>
        </div>
        <div class="box-body">
            <div class="row">

                <div class="col-md-4">
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Player name : </strong>
                        {{$entry->User->name}}
                   </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Mobile: </strong>
                        @if($entry->Bookings[0])
                            {{ $entry->Bookings[0]->mobile }}
                        @else
                            "N/A"
                        @endif
                    </p>
                   <p style="display: inline-flex">
                       <strong style="width:100px;display: inline-block">Total Amount : </strong>
                       {{ $entry->amount.' AED' }}
                   </p>
                    <br>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Transaction Number : </strong>
                        @if($entry->transaction_number)
                            {{ $entry->transaction_number }}
                        @else
                            N/A
                        @endif
                    </p>


                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Details : </strong>
                        @if($entry->details)
                            {{ strtoupper($entry->details) }}
                        @else
                            "N/A"
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <strong style="width:100px;display: inline-block">Venue: </strong>
                        @if($entry->Bookings[0])
                            {{ $entry->Bookings[0]->Slot->Ground->Venue->name }}
                        @else
                            "N/A"
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Ground: </strong>
                        @if($entry->Bookings[0])
                            {{ $entry->Bookings[0]->Slot->Ground->name }}
                        @else
                            "N/A"
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Date: </strong>
                        @if($entry->Bookings[0])
                            {{ date('jS F Y', strtotime($entry->Bookings[0]->date)) }}
                        @else
                            "N/A"
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Start Time: </strong>
                        @if($entry->Game)
                            {{ date('H:i', strtotime($entry->Game->start_time)) }}
                        @else
                            @if($entry->Bookings[0])
                                {{ date('h:i A',strtotime($entry->Bookings[0]->start_time)) }}
                            @else
                                N/A
                            @endif
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">End Time: </strong>
                        @if($entry->Game)
                            {{ date('H:i', strtotime($entry->Game->end_time)) }}
                        @else
                            @if($entry->Bookings[0])
                                @php
                                $count = sizeof($entry->Bookings) - 1;
                                @endphp
                                {{ date('h:i A',strtotime($entry->Bookings[$count]->end_time)) }}
                            @else
                                N/A
                            @endif
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Slot Price: </strong>
                        @if($entry->Bookings[0]->Slot)
                            {{ $entry->Bookings[0]->Slot->slot_price.' AED' }}
                        @else
                            N/A
                        @endif
                    </p>
                </div>
            </div>

            <p class="text-right">
                @if($entry->Bookings[0])
                @php
                    $booking = $entry->Bookings()->orderBy('start_time','asc')->first();
                    $date = new \DateTime("now", new \DateTimeZone('Asia/Dubai') );
                    $currenttime =  strtotime($date->format('H:i:s'));
                    $currentdate =  $date->format('Y-m-d');
                    $current_date_time = date('Y-m-d H:i:s', strtotime("$currentdate $currenttime"));
                    $booked_date = date('Y-m-d', strtotime($booking['date']));
                    $booked_time = date('H:i:s', strtotime($booking['start_time']));
                    $booked_date_time = date('Y-m-d H:i:s', strtotime("$booked_date $booked_time"));
                    if (strtotime($booked_date_time)>strtotime($currentdate)){
                       $show  = 1;
                    }else{
                        $show  = 0;
                    }
                    if($booking['status']==0){
                       $canceled  = 1;
                    }else{
                       $canceled  = 0;
                    }
                    if($booking['payment_status']==1){
                       $payment  = 1;
                    }else{
                       $payment  = 0;
                    }
                @endphp

                    @if($show == 1 && $payment == 1 && $canceled == 0)
                        <a href="" class="btn btn-danger" data-toggle="modal" data-target="#cancelBooking">✖ Cancel Booking</a>
                    @elseif($show == 1 && $payment == 0)
                            <a href="javascript:void(0)" class="btn btn-default">Pending Payment</a>
                    @elseif($canceled == 1)
                        <a href="javascript:void(0)" class="btn btn-default">Rejected</a>
                    @elseif($payment == 1)
                        <a href="javascript:void(0)" class="btn btn-success">Completed</a>
                    @endif
                @endif
            </p>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <div  id="cancelBooking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Are you sure you want to cancel this booking</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-inline pull-right">
                                <li><a href="{{ url('admin/bookings/cancel/'.$entry->transaction_identifier) }}" class="btn btn-success">YES</a></li>
                                <li><a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal">NO</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

