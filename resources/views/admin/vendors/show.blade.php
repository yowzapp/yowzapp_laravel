@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::crud.preview') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ trans('backpack::crud.preview') }}
                <span class="text-lowercase">{{ $crud->entity_name }}</span>
            </h3>
        </div>
        <div class="box-body">

            <div class="row">

                <div class="col-md-4">

                    <img  src="{{$entry->profile_pic}}" alt="" style=" width: 150px; height: 150px; border-radius: 50%; object-fit: cover" />

                </div>
                <div class="col-md-4">
                    <p style="display: inline-flex">
                        <strong style="width:100px ;display: inline-block">Name : </strong>
                        {{$entry->name }}
                    </p>
                    <br>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">E-mail : </strong>
                        {{ $entry->email }}
                    </p>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Mobile : </strong>
                        {{ $entry->mobile }}
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Bio: </strong>
                        @if($entry->bio)
                            {{ $entry->bio }}
                        @else
                            N/A
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Location: </strong>
                        @if($entry->location_name)
                            {{ $entry->location_name }}
                        @else
                            N/A
                        @endif
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Mobile Verified: </strong>
                        @if($entry->mobile_verified==1)
                        <i class="fa fa-check green"></i>
                        @else
                            <i class="fa fa-times red"></i>
                        @endif
                    </p>
                </div>
            </div>
            <div style="margin-top: 25px;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="venues"  class="active">
                        <a href="#venues" aria-controls="" role="tab" data-toggle="tab">
                            Venues
                        </a>
                    </li>
                    <li role="accounts"  class="">
                        <a href="#accounts" aria-controls="" role="tab" data-toggle="tab">
                            Bank Accounts
                        </a>
                    </li>
                    <li role="bookings"  class="">
                        <a href="#bookings" aria-controls="" role="tab" data-toggle="tab">
                            Bookings
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="venues">
                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Cover Pic</th>
                                <th>Availability</th>
                                <th>Verification</th>
                                <th>Location</th>
                                <th>Rating</th>
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            @if(count($entry->Venues))
                                @foreach($entry->Venues as $k=>$venue)
                                    <tr>
                                        <td>{{$venue->id}}</td>
                                        <td>{{$venue->name}}</td>
                                        <td><img style="width: 112px; height: 101px;" src="{{$venue->cover_pic}}" alt=""></td>
                                        <td>
                                            @if($venue->is_disabled == 0)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($venue->is_verified == 1)
                                            <i class="fa fa-check green"></i>
                                            @else
                                            <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                        <td>{{$venue->location_name}}</td>
                                        <td>{{$venue->rating}}</td>
                                        <td>
                                            <a href="/admin/venues/{{$venue->id}}" class="btn btn-xs btn-default remove" ><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                    <div role="accounts" class="tab-pane" id="accounts">

                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Bank Name</th>
                                <th>Branch</th>
                                <th>IFSC</th>
                                <th>Account No.</th>
                                <th>Primary</th>
                            </tr>
                            </thead>
                            @php
                                $accountlist = \App\Models\BankAccount::orderBy('updated_at','desc')->where('vendor_id', $entry->id)->get();
                            @endphp
                            @if(count($accountlist))
                                @foreach($accountlist as $k=>$account)
                                    <tr>
                                        <td>{{$account->id}}</td>
                                        <td>{{$account->bank_name}}</td>
                                        <td>{{$account->branch_name}}</td>
                                        <td>{{$account->ifsc_code}}</td>
                                        <td>{{$account->account_number}}</td>
                                        <td>
                                            @if($account->is_primary == 1)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                    <div role="bookings" class="tab-pane" id="bookings">

                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Identifier</th>
                                <th>Amount</th>
                                <th>Payment Status</th>
                                <th>Date</th>
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            @php
                                $bookings =$entry->VendorBookings()->groupBy('transaction_identifier')->get();
                            @endphp

                            @if(count($bookings))
                                @foreach($bookings as $k=>$booked)
                                    <tr>
                                        <td>{{$booked->transaction_identifier}}</td>
                                        <td>
                                            @php
                                               $transaction = $booked->Transaction()->first();
                                            @endphp
                                            {{$transaction['amount'].' AED'}}
                                        </td>
                                        <td>

                                            @if($booked->payment_status ==1 && $booked->details == "Payment mode cash" )
                                            Cash
                                            @elseif($booked->payment_status == 1 )
                                                Successfull
                                            @else
                                                Failed

                                            @endif
                                        </td>
                                        <td>
                                            {{date('jS F Y', strtotime($booked->date))}}
                                        </td>
                                        <td>
                                            @php
                                                $transaction = $booked->Transaction()->first();
                                            @endphp
                                            <a href="/admin/bookings/{{$transaction['id']}}" class="btn btn-xs btn-default remove" ><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                </div>
            </div>




        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection
