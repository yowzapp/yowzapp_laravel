@extends('backpack::layout')

@section('header')
	<section class="content-header">
		<h1>
			{{ trans('backpack::crud.add') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
			<li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
			<li class="active">{{ trans('backpack::crud.add') }}</li>
		</ol>
	</section>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<!-- Default box -->
			@if ($crud->hasAccess('list'))
				<a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
			@endif

			{!! Form::open(array('url' => $crud->route, 'method' => 'post', 'files'=>$crud->hasUploadFields('create'))) !!}
			<div class="box">

				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('backpack::crud.add_a_new') }} {{ $crud->entity_name }}</h3>
				</div>
				<div class="box-body row">
					<!-- load the view from the application if it exists, otherwise load the one in the package -->
					@if(view()->exists('vendor.backpack.crud.form_content'))
						@include('vendor.backpack.crud.form_content', ['fields' => $crud->getFields('create')])
					@else
						@include('crud::form_content', ['fields' => $crud->getFields('create')])
					@endif
					<?php if(isset($crud->columns['nameq'])){ dd('Exists');}; ?>
					<div id="map" class="form-group col-md-12" style="height: 200px;width: 98%"></div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<div class="form-group">
						<span>{{ trans('backpack::crud.after_saving') }}:</span>
						<div class="radio">
							<label>
								<input type="radio" name="redirect_after_save" value="{{ $crud->route }}" checked="">
								{{ trans('backpack::crud.go_to_the_table_view') }}
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="redirect_after_save" value="{{ $crud->route.'/create' }}">
								{{ trans('backpack::crud.let_me_add_another_item') }}
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="redirect_after_save" value="current_item_edit">
								{{ trans('backpack::crud.edit_the_new_item') }}
							</label>
						</div>
					</div>

					<button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> {{ trans('backpack::crud.add') }}</span></button>
					<a href="{{ url($crud->route) }}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label">{{ trans('backpack::crud.cancel') }}</span></a>
				</div><!-- /.box-footer-->

			</div><!-- /.box -->
			{!! Form::close() !!}
		</div>
	</div>

@endsection



<script>
	// This example requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

	function initMap() {

		var pressEnter = document.getElementById('googleMap');
		google.maps.event.addDomListener(pressEnter, 'keydown', function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
			}
		});

		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 12.910984, lng: 77.6432119999999},
			zoom: 19
		});
		var input = /** @type {!HTMLInputElement} */(
				document.getElementById('googleMap'));


		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);

		var infowindow = new google.maps.InfoWindow();
		var marker = new google.maps.Marker({
			position: {lat: 12.910984, lng: 77.6432119999999},
			map: map,
			draggable: true,
			anchorPoint: new google.maps.Point(0, -29)
		});

		autocomplete.addListener('place_changed', function(e) {
			$('button').attr('disabled', true);
			infowindow.close();
			marker.setVisible(false);

			var place = autocomplete.getPlace();
			if (!place.geometry) {
				// User entered the name of a Place that was not suggested and
				// pressed the Enter key, or the Place Details request failed.
				window.alert("No details available for input: '" + place.name + "'");
				return;
			}


			var str = place.adr_address;
			$('body').append('<div class="googleMapLocaiton">'+str+'</div>')


			if($('.googleMapLocaiton span').hasClass('locality')){
				var locality = $('.googleMapLocaiton .locality').html();
				$('#googleMap').val(locality);
			}
			if($('.googleMapLocaiton span').hasClass('region')){
				var region = $('.googleMapLocaiton .region').html();
				$('#state').val(region);
			}

			$('.googleMapLocaiton').detach();



			document.getElementById('lng').setAttribute('value', place.geometry.location.lat());
			document.getElementById('lat').setAttribute('value', place.geometry.location.lng());
			document.getElementById('zoom-level').setAttribute('value', map.getZoom());
			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				map.setCenter(place.geometry.location);
				map.setZoom(17);  // Why 17? Because it looks good.
			}

			marker.setPosition(place.geometry.location);
			marker.setVisible(true);
			$('button').attr('disabled', false);

		});




		google.maps.event.addListener(marker, 'dragend', function(marker){
			var latLng = marker.latLng;
			document.getElementById('lng').setAttribute('value', latLng.lat());
			document.getElementById('lat').setAttribute('value', latLng.lng());
		});

	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw7fmk4fpvRhn8WjSaceIerdwDbmmWJ8c&libraries=places&callback=initMap"
		async defer></script>
