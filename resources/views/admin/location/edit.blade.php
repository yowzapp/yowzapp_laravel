
@extends('backpack::layout')

@section('header')
	<section class="content-header">
		<h1>
			{{ trans('backpack::crud.edit') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
			<li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
			<li class="active">{{ trans('backpack::crud.edit') }}</li>
		</ol>
	</section>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<!-- Default box -->
			@if ($crud->hasAccess('list'))
				<a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
			@endif
			{!! Form::open(array('url' => $crud->route.'/'.$entry->getKey(), 'method' => 'put', 'files'=>$crud->hasUploadFields('update', $entry->getKey()))) !!}
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('backpack::crud.edit') }}</h3>
				</div>
				<div class="box-body row">
					<!-- load the view from the application if it exists, otherwise load the one in the package -->
					@if(view()->exists('vendor.backpack.crud.form_content'))
						@include('vendor.backpack.crud.form_content')
					@else
						@include('crud::form_content', ['fields' => $crud->getFields('update', $entry->getKey())])
					@endif
					@if($entry->lat || $entry->lng)
						<div id="map" class="form-group col-md-12" style="height: 200px;width: 98%"></div>
					@endif
				</div><!-- /.box-body -->
				<div class="box-footer">

					<button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> {{ trans('backpack::crud.save') }}</span></button>
					<a href="{{ url($crud->route) }}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label">{{ trans('backpack::crud.cancel') }}</span></a>
				</div><!-- /.box-footer-->
			</div><!-- /.box -->
			{!! Form::close() !!}
		</div>
	</div>
@endsection

@if($entry->lat || $entry->lng)
	<script>

		function initMap() {
			var pressEnter = document.getElementById('googleMap');
			google.maps.event.addDomListener(pressEnter, 'keydown', function(e) {
				if (e.keyCode == 13) {
					e.preventDefault();
				}
			});
			var latitide = {{$entry->lng}};
			var longitude = {{$entry->lat}};
			var zoomLevel = 17;

			var latlng = {lat: longitude, lng: latitide};
			var map = new google.maps.Map(document.getElementById('map'), {
				center: latlng,
				zoom: zoomLevel
			});
			var input = /** @type {!HTMLInputElement} */(
					document.getElementById('googleMap'));


			var autocomplete = new google.maps.places.Autocomplete(input);
			autocomplete.bindTo('bounds', map);

			var infowindow = new google.maps.InfoWindow();
			var marker = new google.maps.Marker({
				position: latlng,
				map: map,
				draggable: true,
				anchorPoint: new google.maps.Point(0, -29)
			});

			autocomplete.addListener('place_changed', function() {
				$('button').attr('disabled', true);
				infowindow.close();
				marker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) {
					// User entered the name of a Place that was not suggested and
					// pressed the Enter key, or the Place Details request failed.
					window.alert("No details available for input: '" + place.name + "'");
					return;
				}


				if (place.geometry.viewport) {
					map.fitBounds(place.geometry.viewport);
				} else {
					map.setCenter(place.geometry.location);
					map.setZoom(zoomLevel);  // Why 17? Because it looks good.
				}

				marker.setPosition(place.geometry.location);
				marker.setVisible(true);
				var str = place.adr_address;
				$('body').append('<div class="googleMapLocaiton">'+str+'</div>')


				if($('.googleMapLocaiton span').hasClass('locality')){
					var locality = $('.googleMapLocaiton .locality').html();
					$('#googleMap').val(locality);
				}
				if($('.googleMapLocaiton span').hasClass('region')){
					var region = $('.googleMapLocaiton .region').html();
					$('#state').val(region);
				}

				$('.googleMapLocaiton').detach();
				document.getElementById('lng').setAttribute('value', place.geometry.location.lat());
				document.getElementById('lat').setAttribute('value', place.geometry.location.lng());
				document.getElementById('zoom-level').setAttribute('value', map.getZoom());
				$('button').attr('disabled', false);
			});

			google.maps.event.addListener(marker, 'dragend', function(marker){
				var latLng = marker.latLng;
				document.getElementById('lng').setAttribute('value', latLng.lat());
				document.getElementById('lat').setAttribute('value', latLng.lng());
			});


		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw7fmk4fpvRhn8WjSaceIerdwDbmmWJ8c&libraries=places&callback=initMap"
			async defer></script>
@endif
