@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::crud.preview') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ trans('backpack::crud.preview') }}
                <span class="text-lowercase">{{ $crud->entity_name }}</span>
            </h3>
        </div>
        <div class="box-body">

            <div class="row">

                <div class="col-md-4">

                    <img  src="{{$entry->profile_pic}}" alt="" style=" width: 150px; height: 150px; border-radius: 50%; object-fit: cover" />

                </div>
                <div class="col-md-4">
                    <p style="display: inline-flex">
                        <strong style="width:100px ;display: inline-block">Name : </strong>
                        {{$entry->name }}
                    </p>
                    <br>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">E-mail : </strong>
                        {{ $entry->email }}
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Points : </strong>
                        {{ $entry->points }}
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Bio : </strong>
                        @if($entry->bio)
                            {{ $entry->bio }}
                        @else
                            N/A
                        @endif
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Mobile Verified : </strong>
                        @if($entry->mobile_verified==1)
                            <i class="fa fa-check green"></i>
                        @else
                            <i class="fa fa-times red"></i>
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Location : </strong>
                        @if($entry->location_name)
                            {{ $entry->location_name }}
                        @else
                            N/A
                        @endif
                    </p>



                </div>
            </div>
            <div style="margin-top: 25px;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="teams"  class="active">
                        <a href="#teams" aria-controls="" role="tab" data-toggle="tab">
                            Associated Teams
                        </a>
                    </li>
                    <li role="presentation"  class="">
                        <a href="#sports" aria-controls="" role="tab" data-toggle="tab">
                            Favourites Sports
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="teams">
                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Cover Pic</th>
                                <th>Description</th>
                                <th>No. of Members</th>
                                <th>Points</th>
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            @if(count($entry->Teams))
                                @foreach($entry->Teams as $k=>$team)
                                    <tr>
                                        <td>{{$team->name}}</td>
                                        <td><img style="width: 112px; height: 101px;" src="{{$team->cover_pic}}" alt=""></td>
                                        <td>{{$team->description}}</td>
                                        <td>
                                            @php
                                                $membersCount = $team->Players->count()
                                            @endphp
                                            {{$membersCount}}</td>
                                        <td>{{$team->points}}</td>
                                        <td>
                                            <a href="/admin/teams/{{$team->id}}" class="btn btn-xs btn-default remove-speaker" ><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="sports">

                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Image</th>
                            </tr>
                            </thead>
                            @if(count($entry->Interests))
                                @foreach($entry->Interests as $k=>$sport)
                                    <tr>
                                        <td>{{$sport->id}}</td>
                                        <td>{{$sport->name}}</td>
                                        <td><img style="width: 112px; height: 112px;" src="{{$sport->thumbnail}}" alt=""></td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                </div>
            </div>




        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection
