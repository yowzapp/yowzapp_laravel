@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::crud.preview') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{$entry->name}}
            </h3>
        </div>
        <div class="box-body">

            <div class="row">

                <div class="col-md-4">

                    <img  src="{{$entry->cover_pic}}" alt="" style=" max-width: 250px;" />

                </div>
                <div class="col-md-4">
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Name : </strong>
                        {{$entry->name }}
                    </p>
                    <br>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Updated On : </strong>
                        {{ date('jS M, Y', strtotime($entry->updated_at)) }}
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Description: </strong>
                        @if($entry->description)
                            {{ $entry->description }}
                        @else
                            N/A
                        @endif
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Available: </strong>
                        {{--{{$entry['is_disabled']}}--}}
                        @if($entry->is_disabled==0)
                            <i class="fa fa-check green"></i>
                        @else
                            <i class="fa fa-times red"></i>
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Verification: </strong>
{{--                        {{$entry['is_available']}}--}}
                        @if($entry->is_verified==1)
                            <i class="fa fa-check green"></i>
                        @else
                            <i class="fa fa-times red"></i>
                        @endif
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Rating: </strong>
                        @if($entry->rating)
                            {{ $entry->rating }}
                        @else
                            N/A
                        @endif
                    </p>
                </div>
                <div class="col-md-4">
                    <p>

                    </p>

                </div>
            </div>
            <div style="margin-top: 25px;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"

                        @if(isset($_GET['tab']))
                            @if($_GET['tab']=='grounds')
                                class="active"
                            @endif
                        @else
                        class="active"
                        @endif
                    >
                        <a href="#ground" aria-controls="" role="tab" data-toggle="tab">
                            Grounds
                        </a>
                    </li>
                    <li role="presentation"
                        @if(isset($_GET['tab']))
                        @if($_GET['tab']=='gallery')
                        class="active"
                            @endif
                            @endif
                    >
                        <a href="#gallery" aria-controls="" role="tab" data-toggle="tab">
                            Venue Gallery
                        </a>
                    </li>
                    <li role="presentation"
                        @if(isset($_GET['tab']))
                        @if($_GET['tab']=='amenity')
                        class="active"
                            @endif
                            @endif
                    >
                        <a href="#amenity" aria-controls="" role="tab" data-toggle="tab">
                          Venue Amenities
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane
                     @if(isset($_GET['tab']))
                    @if($_GET['tab']=='grounds')
                            active
                    @endif
                    @else active
                    @endif
                    " id="ground">
                        {{--<p style="margin: 20px 0">--}}
                            {{--<a data-toggle="modal" data-target="#addGroundModal" class="btn btn-primary">+ Ground</a>--}}
                        {{--</p>--}}

                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Max Members</th>
                                <th>Available</th>
                                <th>Sport Name</th>
                                {{-- <th>Slot </th> --}}
                                <th class="actions hide">Actions</th>
                            </tr>
                            </thead>
                            {{--{{dd($entry->Grounds)}}--}}
                            @if(count($entry->Grounds))
                                @foreach($entry->Grounds as $k=>$ground)
                                    <tr>
                                        <td>{{$ground->id}}</td>
                                        <td>{{$ground->name}}</td>
                                        <td>{{$ground->max_member}}</td>
                                        <td>
                                            @if($ground->is_disabled==0)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>

                                        {{--<td>{{$ground->is_disabled}}</td>--}}
                                        <td>
                                            @if(isset($ground->Sport->name))
                                                {{$ground->Sport->name}}
                                            @endif

                                        </td>
                                        
                                        <td class="hide">
                                            <a class="btn btn-xs btn-default " data-toggle="modal" data-target="#editGroundModal_{{$ground->id}}"><i class="fa fa-pencil"></i></a>
                                            <a href="javascript: void(0)" data-id="/admin/venueGround/delete/{{$ground->id.'/'.$entry->id}}" class="btn btn-xs btn-default remove-speaker" data-toggle="modal" data-target=".delete-modal"><i class="fa fa-trash"></i></a>

                                            <div class="modal fade" id="editGroundModal_{{$ground->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel" style="display: inline-block">Add Venue Amenity </h4>

                                                        </div>
                                                        <div class="modal-body">
                                                            {!! Form::open(array('url' => 'admin/ground/'.$ground->id, 'method' => 'put' ,'enctype'=>'multipart/form-data','data-parsley-validate'  => '','novalidate')) !!}
                                                            <ul class="list-unstyled">

                                                                <li>
                                                                    <div class="form-group">
                                                                        <label>Name</label><span style="color:red">*</span>
                                                                        <input class="form-control" type="text" name="name" value="{{$ground['name']}}" required >
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group">
                                                                        <label>Max Members</label><span style="color:red">*</span>
                                                                        <input class="form-control" type="text" name="max_member" value="{{$ground['max_member']}}" required >
                                                                    </div>
                                                                </li>

                                                                <li>
                                                                    <div class="form-group">
                                                                        <label>Disabled</label><span style="color:red">*</span>
                                                                        <select class="choose_constant form-control" name="is_disabled" required>
                                                                            <option {{$ground['is_disabled']==0?'selected':''}} value="{{$ground['is_disabled']}}" >Yes</option>
                                                                            <option {{$ground['is_disabled']==1?'selected':''}} value="{{$ground['is_disabled']}}">No</option>
                                                                        </select>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group">
                                                                        <label>Sport Name</label><span style="color:red">*</span>
                                                                        <select class="choose_constant form-control" name="sports_id" required>
                                                                            @foreach($sports as $k=>$v)
                                                                                @if($k==$ground['sports_id'])
                                                                                    <option selected value="{{$k}}">{{$v}}</option>
                                                                                @else
                                                                                    <option value="{{$k}}">{{$v}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </li>

                                                                <li>
                                                                    <div class="form-group">
                                                                        <input type="hidden" name="id" value="{{$ground->id}}" />
                                                                        <input type="hidden" name="venue_id" value="{{$entry->id}}" />
                                                                        <input type="submit" class="btn btn-primary" value="Save" />
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            </form>
                                                            {!! Form::close() !!}

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>

                                    </tr>


                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Grounds Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>

                    <div role="tabpanel" class="tab-pane

                     @if(isset($_GET['tab']))
                    @if($_GET['tab']=='gallery')
                            active
                    @endif
                    @endif

                    " id="gallery">
                        <p style="margin: 20px 0">
                            <a data-toggle="modal" data-target="#addGallery" class="btn btn-primary">+ Gallery</a>
                        </p>

                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Image</th>
                                {{--<th class="actions">Actions</th>--}}
                            </tr>
                            </thead>

                            @if(count($entry->Gallery))
                                @foreach($entry->Gallery as $k=>$gallery)
                                    <tr>
                                        <td><img style="width: 112px; height: 101px;" src="{{$gallery->image}}" alt=""></td>
                                        <td>
                                            <a href="javascript: void(0)" data-id="/admin/venueGallery/delete/{{$gallery->id.'/'.$entry->id}}" class="btn btn-xs btn-default remove-speaker" data-toggle="modal" data-target=".delete-modal"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Galleries Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane
                     @if(isset($_GET['tab']))
                    @if($_GET['tab']=='amenity')
                            active
                    @endif
                    @endif
                    " id="amenity">
                        <p style="margin: 20px 0">
                            <a data-toggle="modal" data-target="#addAmenity" class="btn btn-primary">+ Amenity</a>
                        </p>

                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            @if(count($entry->venueAmenities))
                                @foreach($entry->venueAmenities as $k=>$amenity)

                                    <tr>
                                        <td>{{$amenity->name}}</td>
                                        <td>
                                            <a href="javascript: void(0)" data-id="/admin/venueAmenities/delete/{{$amenity->id.'/'.$entry->id}}" class="btn btn-xs btn-default remove-speaker" data-toggle="modal" data-target=".delete-modal"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>

                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

        <div class="modal fade" id="addGroundModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="display: inline-block">Add Ground </h4>

                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url' => 'admin/ground/', 'method' => 'post' ,'enctype'=>'multipart/form-data','data-parsley-validate'  => '','novalidate')) !!}
                        <ul class="list-unstyled">

                            <li>
                                <div class="form-group">
                                    <label>Name</label><span style="color:red">*</span>
                                    <input class="form-control" type="text" name="name"  required >
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label>Max Members</label><span style="color:red">*</span>
                                    <input class="form-control" type="text" name="max_member"  required >
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <label>Disabled</label><span style="color:red">*</span>
                                    <select class="choose_constant form-control" name="is_disabled" required>
                                        <option  value="1" >Yes</option>
                                        <option  value="0">No</option>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label>Sport Name</label><span style="color:red">*</span>
                                    <select class="choose_constant form-control" name="sports_id" required>
                                        @foreach($sports as $k=>$v)
                                            <option value="{{$k}}">{{$v}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <input type="hidden" name="venue_id" value="{{$entry->id}}" />
                                    <input type="submit" class="btn btn-primary" value="Save" />
                                </div>
                            </li>
                        </ul>
                        </form>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="addGallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="display: inline-block">Add Venue Gallery </h4>

                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url' => 'admin/venueGallery/', 'method' => 'post' ,'enctype'=>'multipart/form-data','data-parsley-validate'  => '','novalidate')) !!}
                        <ul class="list-unstyled">

                            <li>
                                <div class="form-group">
                                    <label>Image:</label><span style="color:red">*</span> <br>
                                    <input class="speaker-profile-pic" type="file" name="image_upload" required />
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <input type="hidden" name="venue_id" value="{{$entry->id}}" />
                                    <input type="submit" class="btn btn-primary" value="Save" />
                                </div>
                            </li>
                        </ul>
                        </form>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addAmenity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="display: inline-block">Add Venue Amenity </h4>

                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url' => 'admin/venueAmenity/add', 'method' => 'post' ,'enctype'=>'multipart/form-data','data-parsley-validate'  => '','novalidate')) !!}
                        <ul class="list-unstyled">

                            <li>
                                <div class="form-group">
                                    <label>Amenity</label><span style="color:red">*</span>
                                    <select class="choose_constant form-control" name="amenity_id" required>
                                        @foreach($amenities as $k=>$v)
                                            <option value="{{$k}}">{{$v}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <input type="hidden" name="venue_id" value="{{$entry->id}}" />
                                    <input type="submit" class="btn btn-primary" value="Save" />
                                </div>
                            </li>
                        </ul>
                        </form>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>



        <div class="modal delete-modal fade" id="product-options " tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Delete</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <h3 style=" margin: 0 0 35px;">Are you sure you want to delete?</h3>
                            <ul class="list-inline">
                                <li>
                                    <a href="/admin/cmeSpeaker/1/delete" id="delete" class="btn btn-primary confirm-delete-detail">Yes</a>
                                </li>
                                <li>
                                    <a class="cancel-gallery btn btn-danger">No</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script>
            $(document).ready(function($) {

                $('.cancel-gallery').click(function(){
                    location.reload();
                });

                $('.remove-speaker').click(function(){
                    var $this = $(this);
                    var id = $this.data('id');
                    $('.confirm-delete-detail').attr('href', id);
                });
            });


        </script>

        <style>
            .parsley-errors-list{
                list-style: none;
                position: absolute;
                padding-left: 0;
            }
            .parsley-errors-list li{
                color:red;
            }
        </style>



@endsection

