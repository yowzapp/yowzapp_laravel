@extends('backpack::layout')
@section('content')
    <h4 style="padding-bottom:15px;">Push Notification</h4>
    <div class="col-md-6">
        {!! Form::open(array('url' => 'admin/pushNotification', 'method' => 'get' ,'enctype'=>'multipart/form-data','data-parsley-validate'  => '','novalidate')) !!}

        <ul class="list-unstyled">
            <li>
                <div class="form-group">
                    <label>Title <span style="color: #ff0000; font-size: 12px">*</span></label>
                    <input class="form-control" name="title" required />
                </div>
            </li>
            <li>
                <div class="form-group">
                    <label>Message <span style="color: #ff0000; font-size: 12px">*</span></label>
                    <textarea class="form-control" rows="3" name="message" required></textarea>
                </div>
            </li>
            <li style="margin-top: 20px;">
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Notify everyone" />
                </div>
            </li>
        </ul>

        {!! Form::close() !!}
    </div>
@endsection
