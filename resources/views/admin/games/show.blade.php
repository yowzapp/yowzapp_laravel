@extends('backpack::layout')

@section('content-header')
    <section class="content-header">
        <h1>
            {{ trans('backpack::crud.preview') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{$entry->name}}

            </h3>
        </div>
        <div class="box-body">

            <div class="row">

                <div class="col-md-4">

                    <img  src="{{$entry->cover_pic}}" alt="" style=" max-width: 250px;" />

                </div>
                <div class="col-md-4">
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Name : </strong>
                        {{$entry->name }}
                    </p>
                    <br>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Game Date : </strong>
                        {{ date('jS M, Y', strtotime($entry->date)) }}
                    </p>
                    <p style="display: inline-flex">
                        <strong style="width:100px;display: inline-block">Start Time : </strong>
                        {{date('H:i A', strtotime($entry->start_time))}}
                    </p>
                    <p style="">
                        <strong style="width:100px;display: inline-block">End Time : </strong>
                        {{ date('H:i A', strtotime($entry->end_time)) }}
                    </p>
                    <p style="">
                        <strong style="width:100px;display: inline-block">Max Members: </strong>
                        {{ $entry->max_members}}
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Description: </strong>
                        @if($entry->description)
                            {{ $entry->description }}
                        @else
                            N/A
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Privacy: </strong>
                        @if($entry->is_private==1)
                            Private
                        @else
                            Public
                        @endif
                    </p>

                </div>
                <div class="col-md-4">

                    <p>
                        <strong style="width:100px;display: inline-block">Status: </strong>
                        @if($entry->is_completed==1):
                            Completed
                        @else
                            Pending
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Sport: </strong>
                            {{ $entry->Sport->name }}
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Ground: </strong>
                        {{ $entry->Ground->name }}
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Venue: </strong>
                        {{ $entry->Ground->Venue->name }}
                    </p>

                    <p>
                        <strong style="width:100px;display: inline-block">Attendance: </strong>
                        @if($entry->attendance_status==1):
                            Marked
                        @else
                            Pending
                        @endif
                    </p>
                    <p>
                        <strong style="width:100px;display: inline-block">Payment status: </strong>
                        @php
                            $bookings =$entry->Bookings()->groupBy('transaction_identifier')->first();
                        @endphp

                        @if($bookings && $bookings->payment_status==1)
                            Paid
                        @else
                            Pending
                        @endif
                        {{ $entry->shout}}
                    </p>

                </div>
            </div>
            <div style="margin-top: 25px;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="tabpanel"  class="active">
                        <a href="#members" aria-controls="" role="tab" data-toggle="tab">
                            Members
                        </a>
                    </li>
                    <li role="tabpanel"  class="">
                        <a href="#teams" aria-controls="" role="tab" data-toggle="tab">
                            Teams
                        </a>
                    </li>
                    <li role="tabpanel"  class="">
                        <a href="#bookings" aria-controls="" role="tab" data-toggle="tab">
                            Booking
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="members">
                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Points</th>
                                <th>Invitation Status</th>
                                <th>Admin</th>
                                <th>Super Admin</th>
                                <th>Invited to Pay</th>
                                <th>Requested to join</th>
                            </tr>
                            </thead>
                            @if(count($entry->Players))
                                @foreach($entry->Players as $k=>$member)
                                    @if($member->pivot->invitation_status == 1)
                                    <tr>
                                        <td>{{$member->name}} <br>
                                            {{$member->mobile}} <br>
                                            {{$member->email}}
                                        </td>
                                        <td><img style="width: 112px; height: 101px;" src="{{$member->profile_pic}}" alt=""></td>
                                        <td>{{$member->pivot->points}}</td>
                                        <td>
                                            @if($member->pivot->invitation_status==1)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($member->pivot->is_admin==1)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($member->pivot->is_super_admin==1)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($member->pivot->invited_to_pay==1)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($member->pivot_is_requested==1)
                                                <i class="fa fa-check green"></i>
                                            @else
                                                <i class="fa fa-times red"></i>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="teams">
                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Cover Pic</th>
                                <th>Description</th>
                                <th>Points</th>
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            @if(count($entry->Teams))
                                @foreach($entry->Teams as $k=>$team)
                                    <tr>
                                        <td>{{$team->name}}</td>
                                        <td><img style="width: 112px; height: 101px;" src="{{$team->cover_pic}}" alt=""></td>
                                        <td>{{$team->description}}</td>
                                        <td>{{$team->points}}</td>
                                        <td>
                                            <a href="/admin/teams/{{$team->id}}" class="btn btn-xs btn-default remove-speaker" ><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="bookings">
                        <table class="table table-striped" id="">
                            <thead>
                            <tr>
                                <th>Identifier</th>
                                <th>Amount</th>
                                <th>Payment Status</th>
                                <th>Date</th>
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            @php
                                $bookings =$entry->Bookings()->groupBy('transaction_identifier')->get();
                            @endphp
                            @if(count($bookings))
                                @foreach($bookings as $k=>$booked)
                                    <tr>
                                        <td>{{$booked->transaction_identifier}}</td>
                                        <td>
                                            @php
                                                $transaction = $booked->Transaction()->first();
                                            @endphp
                                            {{$transaction['amount'].' AED'}}
                                        </td>
                                        <td>

                                            @if($booked->payment_status ==1 && $booked->details == "Payment mode cash" )
                                                Cash
                                            @elseif($booked->payment_status == 1 )
                                                Successfull
                                            @else
                                                Failed

                                            @endif
                                        </td>
                                        <td>
                                            {{date('jS F Y', strtotime($booked->date))}}
                                        </td>
                                        <td>
                                            @php
                                                $transaction = $booked->Transaction()->first();
                                            @endphp
                                            <a href="/admin/bookings/{{$transaction['id']}}" class="btn btn-xs btn-default remove" ><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center"> No Data Found</td>
                                </tr>

                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

@endsection
