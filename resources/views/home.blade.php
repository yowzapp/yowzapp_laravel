<?php header('Access-Control-Allow-Origin: *'); ?>
        <!DOCTYPE html>
<head>
    <title>Groundhog</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IF ITS GOTTA BE, ITS STARTS WITH ME">
    <meta name="keyword" content="project-report">
    <meta name="author" content="designstring sol pvt ltd">
    <link rel="icon" type="image/x-icon" href="img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.6.0/parsley.js"></script>
    <style>
        body{
            text-align: center;
            font-family: 'Raleway', sans-serif;
        }
        form{
            width: 640px;
            display: block;
            margin: 0 auto;
            overflow: hidden;
            position: relative;
            padding-bottom: 30px;
        }
        form input[type=email]{
            border: 1px solid #ecedee;
            border-radius: 70px 0 0 70px;
            display: inline-block;
            height: 42px;
            width:320px;
            outline: none;
            font-family: 'Raleway', sans-serif;
            font-weight: 400;;
        }
        .flash-msg{font-family: 'Raleway', sans-serif;font-weight: 400;}
        form .parsley-errors-list{
            position: absolute;
            left: 25px;
            right: 0;
            top: 29px;
            z-index: 999;
        }
        form .parsley-errors-list li{
            color: #ff0000;
            font-family: 'Raleway', sans-serif;
            font-weight: 400;
        }
        input[type=submit]{
            border: none;
            border-radius: 0 70px 70px 0;
            color: #ffffff;
            display: inline-block;
            font-size: 16px;
            height: 46px;
            left: -6px;
            position: relative;
            text-align: center;
            width:220px;
            cursor: pointer;
            outline: none;
            font-family: 'Raleway', sans-serif;;
            background-image: url('img/Shape.jpg');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }

        .tagline{
            text-align: center;
            padding-top: 18px;
            color:#345560;
            font-size: 36px;
            font-family: 'Raleway', sans-serif;
            font-weight: 500;;
        }


        input[placeholder]{
            color:#345560 ;
            padding-left:22px;
            font-size:16px;
        }
        ul{
            list-style: none;
            padding: 0;
        }
        ul li{
            padding: 0 10px;
            float: left;
            display: inline-block;
        }
        ul li a{
            display: inline-block;
        }
        @media(max-width: 640px){
            body{overflow: auto;}
            .designstring-logo{width: 65%;}
            .tagline{font-size: 28px !important;}
            form{width: 100% !important;


            }
            form input[type=email]{width: 170px;
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
            }
            .parsley-errors-list{
                top: 36px !important;
            }
            form input[type=submit]{width: 100px;}
        }
    </style>
</head>

<body>
<p style="position:fixed; margin:0; top:0;left:0;right:0; z-index:99; display:none;text-align:center; padding:10px 0; background: #dff0d8;color: #3c763d; border: 1px solid #d6e9c6; font-size: 16px;" class="flash-msg">Thanks for registering, will get back to you soon.</p>
<div style="background-image:url(img/background.svg); height: 100vh;display: flex;
    align-items: center;
    justify-content: center;">
    <div class style="
    display: inline-block";
    >
        <img class="designstring-logo" src="img/footer_designstring.png" alt="logo" >
        <p class="tagline">
            IF ITS GOTTA BE, IT STARTS WITH ME
        </p>

        <form data-parsley-validate="" novalidate="">

            <input type="email" name="email" placeholder="Email Address" data-parsley-error-message="Please enter a valid email field" data-parsley-type="email" required>

            <input type="submit" value="Notify Me">

        </form>
        <p style="text-align: center;color:#345560;font-family: 'Raleway', sans-serif;;font-size:14px; clear: both; margin-top: 0">
            We promise not to spam you
        </p>

        <ul style="display: inline-block">
            <li class="social-media">
                <a href="https://www.facebook.com/designstringtech/">
                    <img src="img/fb.svg">
                </a>

            </li>
            <li>
                <a href="https://twitter.com/designstring/">
                    <img src="img/tweet.svg">
                </a>
            </li>
            <li>

                <a href="https://www.instagram.com/designstring/">
                    <img src="img/insta.svg">
                </a>
            </li>
        </ul>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('form').submit(function(e){
            e.preventDefault();
            console.log('script runing');
            var email  = $(this).find('input[type="email"]').val();
            console.log(email);
            $.ajax({
                url: 'https://docs.google.com/a/designstring.com/forms/d/e/1FAIpQLSdIBSpjCfdNWD9qpDnIlx2X5BHyvq9yYvT539OCXe7CCQ0Qcw/formResponse',
                method: 'POST',
                crossDomain: true,
                contentType: 'application/x-www-form-urlencoded',
                data: {'entry.152543842': email},
                dataType: 'xml',
                success: function(){
                    $('.flash-msg').show();
                    setTimeout(function(){
                        $('.flash-msg').hide();
                    },2000);
                    $('form').find('input[type="email"]').val('');
                    console.log('Working Fine');
                }, error: function(){
                    $('.flash-msg').show();
                    setTimeout(function(){
                        $('.flash-msg').hide();
                    },2000);
                    $('form').find('input[type="email"]').val('');
                    console.log('Throwing error');
                }
            })
        });
    });
</script>
</body>
</html>
