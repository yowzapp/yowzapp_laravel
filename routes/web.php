<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::get('/', function () {
    return view('home');
});


//Route::get('logout', 'Auth\LoginController@logout');
//
//
//Route::get('logout', function () {
//    return redirect('admin/login');
//});

//Route::get('/chat', 'socketController@chat');
//Route::post('/chatMessage', 'socketController@chatMessage');

Route::group(['prefix' => 'admin', 'middleware' => ['myadmin'], 'namespace' => 'Admin'], function () {

    CRUD::resource('banner', 'BannerCrudController');
    CRUD::resource('amenities', 'AmentiesCrudController');
    CRUD::resource('locations','LocationsCrudController');
    CRUD::resource('users', 'UsersCrudController');
    CRUD::resource('roles', 'RolesCrudController');

    CRUD::resource('sports', 'SportsCrudController');
    CRUD::resource('venues', 'VenuesCrudController');
    CRUD::resource('games', 'GamesCrudController');
    CRUD::resource('teams', 'TeamsCrudController');
    CRUD::resource('orders', 'OrdersCrudController');
    CRUD::resource('products', 'ProductsCrudController');
    CRUD::resource('points', 'PointsCrudController');
    CRUD::resource('teampoints', 'PointsTeamsCrudController');
    CRUD::resource('bookings', 'BookingsCrudController');
    CRUD::resource('vendors', 'VendorsCrudController');
    CRUD::resource('players', 'PlayersCrudController');

    CRUD::resource('venueGallery', 'VenueGalleryCrudController');
    CRUD::resource('ground', 'GroundsCrudController');
    CRUD::resource('slot', 'SlotCrudController');
    CRUD::resource('bookings', 'BookingsCrudController');
    Route::get('/pushNotification', 'BannerCrudController@pushNotification');
    Route::get('bookings/cancel/{id}', 'BookingsCrudController@cancelBookingAdmin');


});

Route::group(['prefix' => 'admin', 'middleware' => ['myadmin'], 'namespace' => 'Admin'], function () {

    Route::get('/venueGallery/delete/{id}/{vId}', 'VenueGalleryCrudController@delete');
    Route::get('/venueAmenities/delete/{id}/{vId}', 'VenuesCrudController@venueAmenitiesDelete');
    Route::get('/venueGround/delete/{id}/{vId}', 'VenuesCrudController@venueGroundsDelete');
    Route::post('/venueAmenity/add', 'VenuesCrudController@venueAmenity');
    Route::get('/database', function () {
        return redirect('http://54.255.188.142/phpmyadmin/');
    });


});
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/about', function () {
    return view('about-us');
});

Route::get('/privacy', function () {
    return view('privacy-policy');
});




Route::get('/terms', function () {
    return view('terms-conditions');
});


