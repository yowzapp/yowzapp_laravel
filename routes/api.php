<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


//// *** vendor app API's

Route::post('/vendorRegister', 'UsersController@vendorRegister');
Route::get('player/list', 'PlayerController@playerList');
Route::get('updateTable', 'SearchController@updateTable');
Route::get('testNotify', 'PlayerController@sendtestmessage');


////  *** common API's

Route::post('/mobileVerification', 'UsersController@mobileVerification');
Route::post('/otpVerification', 'UsersController@otpVerification');
Route::post('/resendOtp', 'UsersController@resendOtp');
Route::post('/login', 'UsersController@login');
Route::post('/forgotpassword', 'UsersController@forgotpassword');
Route::post('/forgotpasswordverify', 'UsersController@forgotPasswordVerify');
Route::get('/emailExits', 'UsersController@emailExits');
Route::get('/sportsList', 'SportsController@sportsList');
Route::get('/sportsListGround', 'SportsController@sportsListGround');
Route::get('/amenitiesList', 'AmenitiesController@amenitiesList');
Route::get('/banner', 'UsersController@bannerList');


//// *** player app API's
Route::post('/emailRegister', 'PlayerController@emailRegister');
Route::post('/socialAuth', 'PlayerController@socialAuth');
Route::get('/player/venuesList','VenuesController@venuesListPlayer');
Route::get('/player/venuesList/hot','VenuesController@venuesListHot');
Route::get('player/teamList', 'TeamController@teamListing');
Route::get('player/gameList', 'GameController@gameListing');
Route::post('player/gameDetails', 'GameController@gameDetails');
Route::post('player/{type}/detail', 'TeamController@teamDetails');
Route::get('player/details', 'PlayerController@playerDetails');
Route::get('player/badges', 'LeaderBoardController@myBadges');
Route::get('search/following', 'SearchController@searchFollowing');
Route::get('/locationList', 'LocationsController@locationList');

//// *** player search API's

Route::get('/search/all', 'SearchController@searchALL');
Route::get('/search/team', 'SearchController@searchTeam');
Route::get('/search/game', 'SearchController@searchGame');
Route::get('/search/player', 'SearchController@searchPlayer');
Route::get('/search/venue', 'SearchController@searchVenue');




Route::group(['middleware' => 'apiAuth'], function() {
    /////  **** common API's ----

    Route::post('/logout', 'UsersController@logout');
    Route::post('/resetPassword', 'UsersController@resetPassword');
    Route::get('/getUser', 'UsersController@getUser');
    Route::post('/editUser', 'UsersController@editUser');

    /////  **** vendor API's ----

    Route::post('/vendor/addBankAccount', 'BankAccountController@addBankAccount');
    Route::get('/vendor/bankAccountList', 'BankAccountController@bankAccountList');
    Route::post('/vendor/bankAccountDetails', 'BankAccountController@bankAccountDetails');
    Route::post('/vendor/editBankAccount', 'BankAccountController@editBankAccount');
    Route::post('/vendor/deleteBankAccount','BankAccountController@deleteBankAccount');
    Route::post('/vendor/createVenue','VenuesController@createVenue');
    Route::post('/vendor/editVenue','VenuesController@editVenue');
    Route::post('/vendor/addGround','GroundController@addGround');
    Route::post('/vendor/editGround','GroundController@editGround');
    Route::post('/vendor/deleteGround','GroundController@deleteGround');
    Route::post('/vendor/uploadImage','VenuesController@uploadImage');
    Route::get('/vendor/venuesList','VenuesController@venuesList');
    Route::post('/vendor/venueDetails','VenuesController@venueDetails');
    Route::post('/vendor/addVenueImage','VenuesController@addVenueImage');
    Route::post('/vendor/deleteVenueImage','VenuesController@deleteVenueImage');
    Route::post('/vendor/deleteVenue','VenuesController@deleteVenue');
    Route::get('/vendor/newBooking','VenuesController@newBookingDetails');
    Route::post('/vendor/venue/grounds','VenuesController@VenuesGroundList');
    Route::post('/vendor/venue/book','BookingController@vendorBookGround'); 
    Route::get('/vendor/bookings/latest','BookingController@vendorBookingList');
    Route::post('/vendor/slots/all','BookingController@vendorSlotsList');
    Route::post('/vendor/slot/detail','BookingController@slotDetails');
    Route::post('/vendor/slot/cancel','BookingController@cancelBooking');
    Route::get('/vendor/reports','BookingController@getReports');

    Route::get('/vendor/ground/dayRange/{ground_id}','GroundController@dayRange');
    Route::get('/vendor/ground/dayRangeDetail/{ground_id}/{id}','GroundController@dayRangeDetail');
    Route::post('/vendor/ground/dayRangeSave/','GroundController@dayRangeSave');

    /////  **** player API's ----

    Route::post('player/notification/read', 'NotificationController@readNotification');
    Route::get('/insterestList', 'PlayerController@insterestList');
    Route::get('/sendSms', 'NotificationController@sendSms');
    Route::post('player/SportsVenueList', 'VenuesController@GroundListVenue');
    Route::post('player/groundDetails', 'GroundController@groundNewDetails');
    Route::post('player/groundNewDetails', 'GroundController@groundNewDetails');


    Route::post('player/bookGround', 'BookingController@bookGround');
    Route::post('player/bookingAvailable', 'BookingController@isBookingAvailable');
    Route::post('player/transaction/{status}', 'BookingController@transaction');


    Route::post('player/createGame', 'GameController@createGame');
    Route::post('player/editGame', 'GameController@editGame');
    Route::post('player/uploadImage', 'GameController@uploadImage');
    Route::post('player/createTeam', 'TeamController@createTeam');
    Route::post('player/editTeam', 'TeamController@editTeam');
    Route::post('player/deleteTeam', 'TeamController@deleteTeam');
    Route::post('player/inviteToPay', 'BookingController@inviteToPay');
    Route::get('search/player/recent', 'SearchController@recentSearch');
    Route::get('player/activites', 'ActivityController@activitiesList');


    Route::get('player/notificationList', 'NotificationController@notificationList');


    Route::post('player/team/request', 'TeamController@joinTeam');
    Route::post('player/game/request', 'GameController@joinGame');
    Route::post('player/game/rejectPayment', 'BookingController@rejectPayment');
    Route::get('player/myTransactions', 'BookingController@myTransactions');
    Route::get('player/myPoints', 'PlayerController@myPoints');
    Route::post('player/requestGame', 'GameController@requestGame');
    Route::post('player/game/playersList', 'GameController@gamePlayers');
    Route::post('player/game/markAttendance', 'GameController@markAttendance');
    Route::post('player/game/delete', 'GameController@deleteGame');
    Route::post('player/game/shout', 'GameController@shoutForGame');
    Route::post('player/venue/submitRating', 'VenuesController@submitRating');
    Route::get('player/leaderboard', 'LeaderBoardController@leaderBoardListing');
    Route::get('player/chat/list', 'ChatController@chatListing');
    Route::get('player/chat/history', 'ChatController@chatHistory');
    Route::post('player/settings/notification', 'NotificationController@putNotificaitonSetting');
    Route::get('player/settings/notification/list', 'NotificationController@getNotificaitonSetting');

    Route::post('player/{action}/teamImage', 'TeamController@teamImage');
    Route::post('/user/{type}/{action}', 'PlayerController@followUnfollow');
    Route::post('player/activites/{action}', 'ActivityController@likeUnlikeComment');
    Route::post('{type}/notification/{action}', 'NotificationController@action');
    Route::post('/player/{action}', 'PlayerController@acceptReject');
    Route::post('/{type}/player/{action}', 'PlayerController@removeMakeAdmin');


});
